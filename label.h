#ifndef __LABEL_H_LIB
#define __LABEL_H_LIB 
	#include <stdio.h>
  #include <unistd.h>
  #include <string.h>
  #include <ctype.h>
  #include <stdlib.h>
  #include <stdbool.h>
	#include "def.h"

	char * lookupLabel (char* , enum labelMember); 
	int readLabels (void) ;
	bool ref(char* ) ; 
	bool preRef(char* ) ; 

#endif /* end lable.h */
