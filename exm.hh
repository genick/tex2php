/* exm.hh  for example etc Fri Sep  1 11:18:37 CDT 2017 */
# ifndef __EXM__HH
# define __EXM__HH

#	include <iostream>
#	include <fstream>
#	include <sstream>
#	include <string>
#	include <cstring>
#	include "enumerators.h"
#	include "item.hh"
#	include "commSQL.hh"
#	include "utility.hh"
#	include "commDB.hh"
	using namespace std;
	extern commSQL * bt;
	extern FIGURES *ptrF;

	void solutionT(int);
	int exm( string, int, string );
	void TEx(string, int, string);
	void splitTEx(string, int, string);
	void cleaningEx(string, int, string);

#endif   /* End __EXM__HH  */
