#ifndef __SECTION_H_LIB
#define __SECTION_H_LIB
	#include <stdio.h>
  #include <unistd.h>
  #include <string.h>
  #include <ctype.h>
  #include <stdlib.h>
  #include <stdbool.h>
	#include "def.h"

	void cleanSection(char* , char* );
	void cleanSectionS(char* , char* );
	void collectTOC( char*, char*, char*, char*) ;
	char * mkSectionName (char *, char** );
	//extern void mkTOC( char*, char*, char*) ;

#endif /* End __SECTION_H_LIB */
