// figure manipulation.
//
# include "figure.h"
# include "def.h"

void  twImg (char * tokenText){
	tokenText = newLineToSpace (tokenText);
	tokenText = cleanTabNewLine (tokenText ) ;
	//enum dataType FILEnAME = oTxt;
	char *pch , *figLabel , *overText, * figureFileNameB, *capA, *capB, *capT;
	pch = strtok (tokenText,"}");
	char * figureFileNameA = extractFileName (pch) ;	
	int i = 1 ;
	while (pch != NULL) {
		pch = strtok (NULL, "}");
		if ( i == 1 ) { // second file
			figureFileNameB = extractFileName (pch);
			strcpy(figures[figMaxNumber].fileName[0],figureFileNameA);
			strcpy(figures[figMaxNumber].fileName[1],figureFileNameB);
		}
		else if ( i == 2 ) { // image files
		//	overText = cleanTabNewLine(pch ) + 1;
			//figOverText(figureFileNameA, (trim(pch) + 1));
			overText = trim(pch) + 1 ; 
		}
		else if ( i == 4 ) {
			figLabel = lookupLabel((trim(pch) + 1), nu) ; // label
			strcpy (figures[figMaxNumber].captionT[0],figLabel) ;  
		}
		else if ( i == 5 ) { //overtext
			overText = trim(pch) + 1 ;   
			strcpy (figures[figMaxNumber].captionT[0],overText) ;  
		}
		else if ( i == 6 ) { // caption Figure A 
			capA =  trim( pch) +1 ;
			strcpy ( figures[figMaxNumber].caption[1], capA ) ;  
		}
		else if ( i == 7 ) { // caption Figure B 
			capB =  trim( pch) +1 ;
			strcpy ( figures[figMaxNumber].caption[2], capB ) ;  
			// fprintf (stderr, "Title B: %s\n", trim( pch) +1);
		}
		else if ( i == 8 ) { // global caption 
			capT = trim( pch) +1 ;
			strcpy ( figures[figMaxNumber].captionT[0], capT) ;  
			// fprintf (stderr, "Title B: %s\n", (trim( pch) +1));
		}
		else if ( i == 9 ) { // 
			twoFigures(figureFileNameA, capA, figureFileNameB, capB, capT, overText, figLabel);
			printf ("\n<figcaption>Fig. %s %s </figcaption>\n", figLabel, capT );
			finishFigure();
			return ;
		}
		i++;
	}
	figMaxNumber++;
	return ;
}

void cwImg (char * tokenText){
	tokenText = cleanTabNewLine (tokenText);
	char * pch , *figLabel;
	pch = strtok (tokenText,"}");
	char * figureFileName = extractFileName (pch) ;	
	int i = 1 ;
	while (pch != NULL) {
		pch = strtok (NULL, "}");
		if ( i == 1 ) {
			figOverText(figureFileName, (trim(pch) + 1));
			strcpy(figures[figMaxNumber].fileName[0], figureFileName);
		}
		else if ( i == 3 ) {
			figLabel = lookupLabel(trim(pch) + 1, nu) ;
			strcpy(figures[figMaxNumber].label[0], figLabel);
		}
		else if ( i == 4 ) {
			fprintf (stderr, "%s\n", trim( pch) +1);
		}
		else if ( i == 5 ) {
			printf ("\n<figcaption>Fig. %s %s </figcaption>\n", figLabel, (trim(pch) + 1) );
			finishFigure();
			figMaxNumber++;
			return ;
		}
		i++;
	}
}

void twoFigures (char * fileNameA, char * capA, char* fileNameB, char * capB, char * capT,	char *overText, char* figNum ) {
	printf ("\n<div class=\"figContainer\">");
	printf ("\n\t<div class=\"imgContainer\">"); 
	printf ("\n\t\t<img src=\"images/%s.png\" alt=\"%s\" >", fileNameA, overText );
	printf ("\n\t\t<div class=\"caption\"> Fig A. %s</div >", capA);
	printf ("\n\t<div class=\"imgContainer\">"); 
	printf ("\n\t\t<img src=\"images/%s.png\" alt=\"%s\" >", fileNameB, overText );
	printf ("\n\t\t<div class=\"caption\"> Fig B. %s</div >", capB); 
	printf ("\n\t<div class=\"clearBoth, center\">Fig. %s %s</div >", figNum, capT);
	printf ("\n</div >");
}

void figOverText (char * fileName, char *overText) {
	printf ("\n<figure>\n"); 
	printf ("<img src=\"images/%s.png\" alt=\"%s\" >",
		 	fileName, overText );
}

void finishFigure (void){
	printf ("</figure>\n"); 
}

/*
\wImg{scaned/nozzlePressure}
  {Stodola's Nozzle}
  {0.55}
  {intro:fig:nozzlePressure}
  {The measured pressure in a nozzle}
  {The measured pressure in a nozzle taken from Stodola 1927 Steam and Gas Turbines.}
  {16} 
  {-17.0 mm}
  {0.65}
*/
