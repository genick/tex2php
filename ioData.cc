// ioData.cc starting point Fri Mar 24 08:53:59 CDT 2017
# include <iostream>
# include <string>
# include <stdio.h>
# include <cstdlib>
# include <cstring>
//# include <string.h>
# include <fstream>
# include <vector>
# include <sstream>
# include <mysql.h>
# include "commonDef.h"
# include "utili.hh"
# include "extractLabel.hh"
# include "ioData.hh"

using namespace std;



void finishWithError(MYSQL *con) {
	cerr << mysql_error(con);
	mysql_close(con);
	exit(1);
}

enum labelType hashIT(string const& Str) {
	string eFIG="FIG";
	string eSUBFIG="SUBFIG";	
	if (Str == eFIG) {return FIG;}
	if (Str == eSUBFIG) {return SUBFIG;}
	return NOT_GIVEN;
}

void	printData( int figN){
	cout << figures[figMaxNumber].label[figN]		  <<" label" << endl ;
	cout << figures[figMaxNumber].number[figN]    <<" number" << endl ;
	cout << figures[figMaxNumber].caption[figN]   <<" caption" << endl ;
	cout << figures[figMaxNumber].captionO[figN]  <<" captionO" << endl ;
	cout << figures[figMaxNumber].captionT[figN]  <<" captionT" << endl ;
	cout << figures[figMaxNumber].fileName[figN]  <<" fileName" << endl ;
	cout << figures[figMaxNumber].keywords  <<" keywords" << endl ;
	cout << figures[figMaxNumber].page[figN]      <<" page" << endl ;
}

string creatNewTable(MYSQL *con, string LABEL){
	string s1(workingFileName);
	ostringstream convert;
	string sqlQ;
	s1.erase(s1.end()-4, s1.end()-0);
	s1 += LABEL;
	convert << "DROP TABLE IF EXISTS " << s1;
	sqlQ = convert.str();
	convert.clear();
	convert.str(std::string());
	if (mysql_query(con, sqlQ.c_str() )){
		finishWithError(con);
	}
	convert << "CREATE TABLE "  << s1
					<< " (`labelID` int(6) NOT NULL AUTO_INCREMENT, "
					<< "`level` varchar(24) DEFAULT NULL, "
					<< "`label` varchar(56) DEFAULT NULL, "
					<< "`number` varchar(12) DEFAULT NULL, "
					<< "`caption` varchar(256) DEFAULT NULL, "
					<< "`caption1` varchar(256) DEFAULT NULL, "
					<< "`caption2` varchar(256) DEFAULT NULL, "
					<< "`caption3` varchar(256) DEFAULT NULL, "
					<< "`caption4` varchar(256) DEFAULT NULL, "
					<< "`captionT` varchar(256) DEFAULT NULL, "
					<< "`captionT1` varchar(256) DEFAULT NULL, "
					<< "`captionT2` varchar(256) DEFAULT NULL, "
					<< "`captionT3` varchar(256) DEFAULT NULL, "
					<< "`captionT4` varchar(256) DEFAULT NULL, "
					<< "`captionO` varchar(256) DEFAULT NULL, "
					<< "`captionO1` varchar(256) DEFAULT NULL, "
					<< "`captionO2` varchar(256) DEFAULT NULL, "
					<< "`captionO3` varchar(256) DEFAULT NULL, "
					<< "`captionO4` varchar(256) DEFAULT NULL, "
					<< "`fileName` varchar(100) DEFAULT NULL, "
					<< "`fileName1` varchar(100) DEFAULT NULL, "
					<< "`fileName2` varchar(100) DEFAULT NULL, "
					<< "`fileName3` varchar(100) DEFAULT NULL, "
					<< "`fileName4` varchar(100) DEFAULT NULL, "
					<< "`keywords` varchar(256) DEFAULT NULL, "
					<< "`page` INT NOT NULL DEFAULT 0, "
					<< "`nn` INT NOT NULL DEFAULT 0, "
					<< "`dateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP, "
					<< "`` INT NOT NULL DEFAULT 0, "
//					<< "`associatedNumber` INT NOT NULL DEFAULT 0, "
//					<< "`labelType` ENUM('FIG','SUBFIG') NOT NULL DEFAULT 'FIG', "
					<< "PRIMARY KEY (`labelID`) ) ENGINE=MyISAM "
					<< "DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
	sqlQ = convert.str();
	if (mysql_query(con, sqlQ.c_str())){
	  finishWithError(con);
	}
	return s1;
// 	convert << "INSERT INTO " << thisFile 
// 					<< "(level, label, number, caption,"
// 	          << " captionT, captionO, fileName,"
// 	         << " keywords, page, nn, associatedNumber, labelType)"
// 	         << " VALUES('sub','chap:fig', '1.2','this caption',"
// 	         << " 'this caption T', 'caption O',  'thisFile', 'this key word',"
// 	         << i << " , 3, 4, 'SUBFIG' )" ;
// 	  str = convert.str();
// 	  strcpy(qIN, str.c_str());
//   if (mysql_query(con, qIN)){
//     finishWithError(con);
//   }
}

MYSQL * iniSQL(string bookName, struct sqlDetails sqlID){
	MYSQL *con = mysql_init(NULL);
	if (con == NULL) {
		cerr << "mysql_init() failed" << endl;
		exit(1);
	}
	if (!mysql_real_connect(con,
		sqlID.server,
		sqlID.user,
 		sqlID.password,
		sqlID.database, 0, NULL, 0)) {
      cerr << "Conection error :" <<  mysql_error(con) << endl;
      exit(1);
    }
	return con;
}

MYSQL_RES* querySQL(MYSQL *con, string myQuery){
	if (mysql_query(con, myQuery.c_str())){
		cerr << "MySQL query error: " << mysql_error(con) << endl;
		exit(1);
	}
	return mysql_use_result(con);
}

// insert info into the table
void insertSQL (MYSQL *con, string tableName){
	ostringstream ss;
	ss << "INSERT INTO " <<  tableName.c_str()
			<< " (level, label, number, caption, caption1, caption2, caption3, "
			<< "caption4,  captionT, captionT1, captionT2, captionT3, captionT4, "
			<< "captionO, captionO1, captionO2, captionO3, captionO4, fileName, "	
			<< "fileName1, fileName2, fileName3, fileName4, keywords, page, nn, "  
			<< "dateTime) "
			<< "VALUES ('sub','chap:into', '1.2','this caption', 'this caption T', "
			<< "'caption O',  'thisFile', 'this keyword', 2, 4, 4, 'SUBFIG' ) " ;
	
	if (mysql_query(con, ss.str().c_str())){
		cerr << "MySQL query error: " << mysql_error(con) << endl;
		exit(1);
	}
}

extern "C" void readDATA(char* x){
	string cl, rtn;
	ostringstream convert;
# include "pottoSQLid.cc"
	//size_t found;
	//int figType, figNumber;
	//int figNumber;
	//vector <string> lines;
	//vector<string>::iterator lt;
	//char temp[256];
	string bookName ("gasDynamics");
	MYSQL *con;
	MYSQL_RES *res;	
  MYSQL_ROW row;	
	con = iniSQL(bookName, sqlID);
	string ELEMENT("Fig");
	//string currentTableFig = creatNewTable (con, ELEMENT);	
	res = querySQL(con, "show tables");
	while ((row = mysql_fetch_row(res)) !=NULL){
		cout << row[0] << endl;
	}
 
  /* clean up the database result set */
	mysql_free_result(res);
  /* clean up the database link */
	mysql_close(con);

//			ifstream iFile (x);
//			if(iFile.fail()){
//				cerr<< "Error in opening file " << x << endl;
//				return;
//			}
//			else{
//				while (getline(iFile,line)) { 
//					found = line.find('|');
//					while (found!=string::npos) {	
//						memset(temp, '\0', sizeof(char)*sizeof(temp));
//						line.copy(temp,found,0);
//						lines.push_back(trim(temp));
//						line.erase (0,found+1);
//						found = line.find('|');
//					}
//					strcpy(temp, line.c_str());
//				}
//				iFile.close();
//				lt = lines.begin();
//				//labelType type = hashIT((*lt));
//				lt++;
//				figNumber = stoi((*lt));
//				lt++;
//				figures[figMaxNumber].associatedNumber = stoi((*lt));
//				lt++;
//				strcpy(figures[figMaxNumber].label[figNumber],(*lt).c_str());
//				lt++;
//				strcpy(figures[figMaxNumber].number[figNumber],(*lt).c_str());
//				lt++;
//				strcpy(figures[figMaxNumber].caption[figNumber],(*lt).c_str());
//				lt++;
//				strcpy(figures[figMaxNumber].captionO[figNumber],(*lt).c_str());
//				lt++;
//				strcpy(figures[figMaxNumber].captionT[figNumber],(*lt).c_str());
//				lt++;
//				strcpy(figures[figMaxNumber].fileName[figNumber],(*lt).c_str());
//				lt++;
//		//	switch (type){
//		//		case FIG: figType = 0; break;
//		//		case SUBFIG: figType = 1; break;
//		//		default: figType = 0; break;
//		//  }
//				strcpy(figures[figMaxNumber].keywords,(*lt).c_str());
//				lt++;
//				strcpy(figures[figMaxNumber].page,(*lt).c_str());
//				lt++;
//				printData(figNumber);
//				cout << *lt << endl;
//			}
}

