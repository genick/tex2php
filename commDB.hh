// commDB.hh created on Fri Apr 28 12:05:43 CDT 2017
#ifndef __COMMdb_HH
#define __COMMdb_HH
   
	# include <iostream>    
	# include <sstream>    
	# include <fstream>    
	# include <iomanip>    
	# include <string>    
	# include <vector>    
	# include <cstring>    

	#	include <cmath>
	#	include <cstdio>
	// #	include <iostream>
	#	include <fstream>
	# include <sstream>
	#	include <limits>
	#	include <unistd.h>
	#	include <ctype.h>
	#	include <stdlib.h>
	#	include <stdbool.h>
	#	include "item.hh"
	// # include "mangerSQL.hh"
	#	include "enumerators.h"
	# include "commSQL.hh"
	# include "utility.hh"
	# include "io.hh"
	using namespace std;

	extern string peelFirstPart( string &, size_t, char  );
	void updateData(string, string, string, string tn="figure");
	void readLabels(void);
	string lookUpNumber (string, enum itemMember, enum itemMember);
	bool checkInData (string, string, enum itemMember, unsigned int=0);
	void cleanDataBase(string ); // input name of the table 
	void insertDataDB(LABELS*, vector <itemMember>, vector <intMember>,
		 	dataType, string );
	extern ofstream logFile;

#endif /* __COMMdb_HH */
