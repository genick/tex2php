	<?php
		require ("fun.php" );
		loadINI("Compressible Flow", "isontropic flow, shock, oblique",
					"fundamental of Compressible Flow"
		);
		loadJS();
		loadMathJax();
		loadStyleFiles()
	?>
</head>
<body>
	<?php
		// require("header.php");
		require("main.php");
		// require("footer.php");
	?>
</body>
</html>
