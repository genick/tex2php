 <?php
	require ("functions.php" );
	loadINI("Compressible Flow", "isontropic flow, shock, oblique",
	      "fundamental of Compressible Flow", 'no'
	);
	loadCSS();
	loadJS();
	loadMathJax();
	// loadStyleFiles()
 ?>
</head>
<body>

<!-- <div class="container&#45;fluid bg&#45;primary pottoNav"> -->
<!-- <div class="container&#45;fluid fixed&#45;top bg&#45;primary pottoNav"> -->
<!-- <div class="container&#45;fluid bg&#45;primary pottoNav sticky&#45;top bgPotto"> -->
<div class="container-fluid pottoNav sticky-top bgPotto">
	<!-- <div class="container&#45;fluid sticky&#45;top bg&#45;primary"> -->
	<div class="container-fluid sticky-top">
		<?php
			require("nav.php");
		?>
</div>

<div class = "col-sm-12 px-5" > 
<div class = "px-5" > 
<?php
	require("thermoT.php");
?>
</div>

<div class = "px-5" >     
<?php    
  require("footer.php");    
?>    
</div>     


<!-- <div class="jumbotron"> -->
<!-- 	<h1>Bootstrap 4 multi dropdown navbar</h1> -->
<!-- 		<p>This example of bootstrap 4 navigation multi dropdown menu.</p> -->
<!-- </div> <!&#45;&#45; /container &#45;&#45;> -->

	<div class="jumbotron">
		<h1>Bootstrap 4 multi dropdown navbar</h1>
			<p>This example of bootstrap 4 navigation with multi dropdown menu.</p>
	</div> <!-- /container -->

  <script src="js/jquery.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-4-navbar.js"></script>

</body>
</html>
