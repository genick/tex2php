<?php

function mkNavigationLine($name=panel, $href, $alt ,$image ) {
	$contents = "" ;
	#Get image width / height
	//$x = ImageSX($image);
	//$y = ImageSY($image);
	list($w, $h, $type, $attr) = getimagesize($image);
	if ($href!="") {
		$contents .= "<a name='" ; 
		$contents .= $name ; 
		$contents .= "' href='" ; 
		$contents .= $href ; 
		$contents .= "'>" ; 
		$skipPanel=True;
	}
	$contents .= "<img width='";
	$contents .= $w;
	$contents .= "' height='";
	$contents .= $h;
	$contents .= "' align='botton' border='0' alt='";
	$contents .= $alt ;
	$contents .= "' src='";
	$contents .= $image;
	$contents .= "'>";
	if ( $skipPanel == True ) { 
		$contents .= "</a>" ; 
	}
	$contents .= "\n" ; 
	return $contents ;
}

function mkNavigation($next, $nextName, $up, $upName, $previous, $previousName ) {
	$contents = "\n" ;
	//make the next botton
	if (!(empty($next) ) ) { 
		$contents .= mkNavigationLine("topP", $next, $nextName, "figures/next.png");
	}
	else{ 
		$contents .= mkNavigationLine("topP", $next, $nextName, "figures/next_g.png");
	}	
	$contents .= mkNavigationLine("topP", $up, $upName, "figures/up.png");
	if (!(empty($previous) ) ) {
		$contents .= mkNavigationLine("topP", $previous, $previousName, "figures/prev.png");
	}
	else{
		$contents .= mkNavigationLine("topP", $previous, $previousName, "figures/prev_g.png");
	}
	$contents .= mkNavigationLine("topP", "realIndex.php", "Index", "figures/index.png");
	$contents .= mkNavigationLine("topP", "toc.php", "TOC", "figures/contents.png");
	return $contents ;
}


  // <script src="js/jquery.js"></script>
  // <script src="js/popper.min.js"></script>
  // <script src="js/bootstrap.min.js"></script>
  // <script src="js/bootstrap-4-navbar.js"></script>

function loadJS(){
	echo ("<script src=\"js/enquire.min.js\"></script>"); 
	echo ("<script src=\"js/jquery-2.1.4.min.js\"></script>");
	echo ("<script src=\"js/bootstrap.js\"></script>");
	echo ("<script src=\"js/bootstrap-4-navbar.js\"></script>");
	// echo ("<script src=\"js/local.js\"> </script>");
	// echo ("<script src=\"js/navbar.js\"> </script>");
}

function loadCSS(){
	echo ("<link rel=\"stylesheet\" href=\"css/bootstrap.css\">"); 
	echo ("<link href=\"css/bootstrap-4-navbar.css\" rel=\"stylesheet\">");
	echo ("<link rel=\"stylesheet\" href=\"css/b.css\">");
	echo ("<link rel=\"stylesheet\" href=\"css/local.css\">");
}

function loadMathJax(){
	// <script language="php">
		// print <<<EoF 
	echo ("<script type=\"text/x-mathjax-config\">");
	echo ("MathJax.Hub.Register.StartupHook(\"TeX Jax Ready\",function()"); 
	echo ("{ MathJax.Hub.Insert(MathJax.InputJax.TeX.Definitions.macros,");
	echo ("{	cancel: [\"Extension\",\"cancel\"],"); 
	echo ("bcancel: [\"Extension\",\"cancel\"],"); 
	echo ("xcancel: [\"Extension\",\"cancel\"],"); 
	echo ("cancelto: [\"Extension\",\"cancel\"]"); 
	echo ("});");  
	echo ("});");
	echo ("</script>");
	echo ("<script type=\"text/x-mathjax-config\">");
	echo ("MathJax.Hub.Config({ ");
	echo ("tex2jax: {inlineMath: [[\"$\",\"$\"]]} });");
	echo ("</script>");
	
	echo ("<script type=\"text/x-mathjax-config\">");
	echo ("MathJax.Hub.Config({ TeX:");
	echo ("{ equationNumbers: {autoNumber: \"all\"}, ");
	echo ("Macros: {  RRRR: '{\bf R}',  bold:");
	echo ("['\boldsymbol{#1}',1],  bbb: ['\\mathbf{#1}',1] },");
	echo ("extensions: [\"color.js\"]  } });");
	echo ("</script>");
	echo ("<script type=\"text/javascript\" ");
	echo ("src=\"../../MathJax/MathJax.js?config=TeX-AMS_HTML\"> ");
	echo ("</script>");
	// echo ("tex2jax: {inlineMath: [[\"$\",\"$\"],"); //the old stuff for 
	// for the inline brackets
	// echo ("[\"\\(\",\"\\)\"]]} });"); 
	// EoF;
	// </script>
}

function loadStyleFiles () {
	echo ("<link rel=\"stylesheet\"");
	echo ("href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\"");
	echo("integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\"");
	echo ("crossorigin=\"anonymous\">");
  echo ("<link href=\"css/navbar.css\" rel=\"stylesheet\"> ");
	echo ("<link rel=\"stylesheet\" href=\"css/local.css\" >");
}

function loadINI(
	$titleName="Best Fluid Mechanics Textbook", 
	$description="Best Fluid Mechanics by Genick Bar-Meir, Ph.D. Length: ~678 pages",
	$keyWords="Fluid Mechanics, Potto, Gas Dynamics, Speed of Sound, Isentropic Flow"
	){
	echo ("<!DOCTYPE HTML>\n\n");
	echo ("<html>\n");
	echo ("<head>\n") ;
	echo ("<title>". $titleName . "</title>\n") ; 
	echo ("<meta charset='utf-8'>\n") ;
	echo ("<meta name='description' CONTENT='" . $description . "'> \n");
	echo ("<meta name='keywords' CONTENT='" . $keyWords . "'> \n");
	echo ("<meta content= 'Basics of Fluid Mechanics' > \n");
}

function mkHeader($topicName="Best Fluid Mechanics Textbook", 
	$description="Best Fluid Mechanics by Genick Bar-Meir, Ph.D. Length: ~678 pages",
	$keyWords="Fluid Mechanics, Potto, Gas Dynamics, Speed of Sound, Isentropic Flow",
	$styleFile= "potto.css",
	$loadMathJax="yes"
	){
	echo ("<!DOCTYPE HTML>\n\n");
	echo ("<html>\n");
	echo ("<head>\n") ;
	echo ("<title>". $topicName . "</title>\n") ; 
	echo ("<meta charset='utf-8'>\n") ;
	echo ("<meta name='description' CONTENT='" . $description . "'> \n");
	echo ("<meta name='keywords' CONTENT='" . $keyWords . "'> \n");
	echo ("<meta content= 'Basics of Fluid Mechanics' > \n");
	if ($loadMathJax == "yes"){
	echo ("<script type=\"text/x-mathjax-config\"> ");
	echo ("MathJax.Hub.Register.StartupHook(\"TeX Jax Ready\",function () { " ) ;
	echo ("MathJax.Hub.Insert(MathJax.InputJax.TeX.Definitions.macros,{ " ) ;
	echo ("\n");
	echo ("cancel: [\"Extension\",\"cancel\"], ") ;
	echo ("bcancel: [\"Extension\",\"cancel\"], ") ;
	echo ("xcancel: [\"Extension\",\"cancel\"], ") ;
	echo ("cancelto: [\"Extension\",\"cancel\"] ") ;
	echo ("}); ");
	echo (" }); ");
	echo ("</script> ");
	echo ("\n");
	
	echo ("<script type=\"text/x-mathjax-config\">");
	echo ("\n  MathJax.Hub.Config({ ");
	echo ("tex2jax: {inlineMath: [[\"\$\",\"\$\"],");
	echo ("[\"\\\\(\",\"\\\\)\"]]} }); " );
	echo ("</script> ") ;
	echo ("\n");
	echo ("<script type=\"text/x-mathjax-config\">") ;
	echo ("MathJax.Hub.Config({ TeX: { ");
	echo ("equationNumbers: {autoNumber: \"all\"}, ") ;
	echo (" Macros: { ") ;
	echo (" RRRR: '{\\bf R}', ") ;
	echo (" bold: ['\\boldsymbol{#1}',1], ");
	echo (" bbb: ['\\\\mathbf{#1}',1] ");
	echo ("   }, ");
	echo ("extensions: [\"color.js\"]  ");
	echo ("} }); "); 
	echo ("</script>" ) ;
	echo ("\n");
	echo ("<script type=\"text/javascript\" src=\"js/MathJax.js?config=TeX-AMS_HTML\"></script>" ) ;
	echo ("\n");
	echo ("<script type=\"text/javascript\">");
	echo ("function toggle(obj) { ");
	echo ("var obj=document.getElementById(obj);");
	echo ("\tif (obj.style.display == \"block\") obj.style.display = \"none\";") ;
	echo ("\t\telse obj.style.display = \"block\";");
	echo ("}")	;
	echo ("</script>") ;	
	}
	echo ("\t<link rel='stylesheet' type='text/css' href='" . $styleFile . "'>\n")	; 
	//echo ("\t<link rel='stylesheet' type='text/css' href='fluids.css'>\n")	; 
	echo ("</head>\n ");
	echo ("<body >\n");
	echo ("\t<table >\n");
	echo ("\t\t<tr>\n") ;
	echo("\t\t\t<th colspan='3'><img align='middle' src='figures/header.png' alt='Basics of Fluid Mechanics'> </th>\n");
	echo("\t\t</tr>\n");
	echo("\t\t<tr class='hbot' >\n");
	echo("\t\t\t<th class='hbot' ><a href='../index.php'>Potto Home </a></th>\n");
	echo("\t\t\t<th class='hbot' ><a href='../about.php'>About</a></th>\n");
	echo("\t\t\t<th class='hbot' >contact</th>\n");
	echo("\t\t</tr>\n");
}

//  next , nextName, previous, previousName
function mkUnderNavigation($next, $nextName, $previous, $previousName ) {
	$contents = "\n" ;
	echo("<br>\n");
	if (!(empty($next) ) or !(empty($nextName)) ) { 
		echo("<B> Next:</B>\n");
		echo("<a NAME='top' HREF='");
		echo($next);
		echo("' >\n");
		echo($nextName);
		echo("</a> &nbsp;&nbsp;\n");
	}	
	if (!empty($previous) ){ 
   	echo("<B> Previous:</B>\n");
   	echo("<a NAME='top' HREF='");
		echo($previous);
		echo("' >\n");
		echo($previousName);
		echo("</a>\n");
   	echo("<!--End of Navigation Panel--> \n");
	}
}  

function bb_gd_calculator($POSTED=array()) {
   if ($POSTED['latexoutput']!='yes') { $POSTED['latexoutput'] = "no"; }
   if ($POSTED['rangeoutput']!='yes') { $POSTED['rangeoutput'] = "no"; }	
   //Common Coontents
   $contents .= ";This file was created by gdc.php calculator\n";
   $contents .= ";This file can be edited if you know what you doing\n\n\n";      
   $contents .= "[data]\n;The actual data\n";
   $contents .= "k=".$POSTED['cpcvk']."\n";
  //End of Common Contents   
   if ($POSTED['model']=='obliqueFlow') {
	    $obexp = explode("_",$POSTED['ob_input']);
   		$contents .= $obexp[1]."=".$POSTED['obsinput2']."\n";
		$contents .= $obexp[0]."=".$POSTED['obsinput1']."\n";
		$contents .= "\n[integer]\n";
		$contents .= "isTex=".$POSTED['latexoutput']."\n";
		$contents .= "isHTML=yes\n";
		$contents .= "isRange=".$POSTED['rangeoutput']."\n";
		$contents .= "numberNodes=40\n";
		$contents .= "shockLocation=50\n\n";
		
		$contents .= "[InputOutput]\n";
		$contents .= "whatInfo=infoStandard\n";
		if ($POSTED['ob_input']=='Mx_delta') {
			$contents .= "variableName=deltaMV\n";
		}if ($POSTED['ob_input']=='Mx_theta') {
			$contents .= "variableName=thetaMV\n";
		}if ($POSTED['ob_input']=='delta_theta') {
			$contents .= "variableName=deltaThetaV\n";
		}
	   
   } else {
	   if ($POSTED['input']=='M1fld') {
			$contents .= "M1=".$POSTED['M1']."\n";
			$contents .= "fld=".$POSTED['fld']."\n\n";	   
	   } else {
	   		$contents .= $POSTED['input']."=".$POSTED['inputdata']."\n";
	   }
	   if ($POSTED['input']!='M') {   
		   if ($POSTED['sonic']) {
			   if ($POSTED['sonic']=='supersonic') {
				   $contents .= "M=2.0\n";
			   }elseif($POSTED['sonic']=='subsonic') {
				   $contents .= "M=0.5\n";
			   }	   
		   }
	   }
	   $contents .= "\n[integer]\n";
	   $contents .= "isTex=".$POSTED['latexoutput']."\n";
	   $contents .= "isHTML=yes\n";
	   $contents .= "isRange=".$POSTED['rangeoutput']."\n\n";
	   if ($POSTED['input']=='M1fld') {
		   $contents .= "numberNodes=40\n";
		   $contents .= "shockLocation=50\n\n";
	   }
	   $contents .= "[InputOutput]\n";
	   $contents .= "whatInfo=info".$POSTED['whatkindoutput']."\n";
	   if ($POSTED['input']=='M') {
		   $contents .= "variableName=machV\n";
	   } else {
		   $contents .= "variableName=".$POSTED['input']."V"."\n";
	   }
   }
   $contents .= "className=".$POSTED['model']."\n\n\n";
   return $contents;	
}

?>
