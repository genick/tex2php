function loadMathJax(){
	// <script language="php">
		// print <<<EoF 
	echo ("<script type=\"text/x-mathjax-config\">");
	echo ("MathJax.Hub.Register.StartupHook(\"TeX Jax Ready\",function()"); 
	echo ("{ MathJax.Hub.Insert(MathJax.InputJax.TeX.Definitions.macros,");
	echo ("{	cancel: [\"Extension\",\"cancel\"],"); 
	echo ("bcancel: [\"Extension\",\"cancel\"],"); 
	echo ("xcancel: [\"Extension\",\"cancel\"],"); 
	echo ("cancelto: [\"Extension\",\"cancel\"]"); 
	echo ("});");  
	echo ("});");
	echo ("</script>");
	echo ("<script type=\"text/x-mathjax-config\">");
	echo ("MathJax.Hub.Config({ ");
	echo ("tex2jax: {inlineMath: [[\"$\",\"$\"]]} });");
	echo ("</script>");
	
	echo ("<script type=\"text/x-mathjax-config\">");
	echo ("MathJax.Hub.Config({ TeX:");
	echo ("{ equationNumbers: {autoNumber: \"all\"}, ");
	echo ("Macros: {  RRRR: '{\bf R}',  bold:");
	echo ("['\boldsymbol{#1}',1],  bbb: ['\\mathbf{#1}',1] },");
	echo ("extensions: [\"color.js\"]  } });");
	echo ("</script>");
	echo ("<script type=\"text/javascript\" ");
	echo ("src=\"../../MathJax/MathJax.js?config=TeX-AMS_HTML\"> ");
	echo ("</script>");
	// echo ("tex2jax: {inlineMath: [[\"$\",\"$\"],"); //the old stuff for 
	// for the inline brackets
	// echo ("[\"\\(\",\"\\)\"]]} });"); 
	// EoF;
	// </script>
}
