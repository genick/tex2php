<nav class="navbar navbar-expand-md navbar-light pottoNav">
<!-- <nav class="navbar navbar&#45;expand&#45;md navbar&#45;light bg&#45;light"> -->
	<button class="navbar-toggler" type="button"
	 data-toggle="collapse" data-target="#navbarNavDropdown"
	 aria-controls="navbarNavDropdown" aria-expanded="false"
	 aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNavDropdown">
		<ul class="navbar-nav">
			<li class="nav-item active">
				<a class="nav-link" href="#">Potto
					<span class="sr-only">(current)</span>
				</a>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"
				 href="core.php"
				 id="navbarDropdownMenuLink" data-toggle="dropdown"
				 aria-haspopup="true" aria-expanded="false">
					Front Maters
				</a>
				<ul class="dropdown-menu" 
					aria-labelledby="navbarDropdownMenuLink">
					<li><a class="dropdown-item" href="#">Title Page</a></li>
					<li><a class="dropdown-item" href="#">Nomenclature</a></li>
					<li><a class="dropdown-item" href="#">Change Log</a></li>
					<li><a class="dropdown-item" href="#">Contributor List</a></li>
					<li><a class="dropdown-item dropdown-toggle"
						 href="#">About</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">About the Author</a>
							</li>
							<li><a class="dropdown-item" href="#">About the Book</a>
							</li>
							<li><a class="dropdown-item" href="#">How Written</a>
							</li>
						</ul>
					</li>
					<li><a class="dropdown-item dropdown-toggle"
						 href="#">Prologue</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Potto Prologue</a>
							</li>
							<li><a class="dropdown-item" href="#">Book Prologue</a>
							</li>
							<li><a class="dropdown-item" href="#">Preface</a>
							</li>
						</ul>
					</li>
					<li><a class="dropdown-item" href="#">GD Calculator</a></li>
				</ul>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"
				 href="core.php"
				 id="navbarDropdownMenuLink" data-toggle="dropdown"
				 aria-haspopup="true" aria-expanded="false">
					Introduction
				</a>
				<ul class="dropdown-menu" 
					aria-labelledby="navbarDropdownMenuLink">
					<li><a class="dropdown-item"
						 href="thermo.php">Intro to Thermo</a></li>
					<li><a class="dropdown-item dropdown-toggle"
						 href="#">Fluid Mechamics</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Fluid 1</a></li>
							<li><a class="dropdown-item" href="#">Fluid 2</a></li>
							<!-- <li><a class="dropdown&#45;item dropdown&#45;toggle" -->
							<!-- 	 href="#">Subsubmenu</a> -->
							<!-- 	<ul class="dropdown&#45;menu"> -->
							<!-- 		<li><a class="dropdown&#45;item" -->
							<!-- 		 href="#">Subsubmenu action aa</a></li> -->
							<!-- 		<li><a class="dropdown&#45;item" -->
							<!-- 			 href="#">Another subsubmenu action</a></li> -->
							<!-- 	</ul> -->
							<!-- </li> -->
					<!-- 		<li><a class="dropdown&#45;item dropdown&#45;toggle" -->
					<!-- 		 href="#">Second subsubmenu</a> -->
					<!-- 			<ul class="dropdown&#45;menu"> -->
					<!-- 				<li><a class="dropdown&#45;item" -->
					<!-- 					 href="#">Subsubmenu action bb</a></li> -->
					<!-- 				<li><a class="dropdown&#45;item" -->
					<!-- 					 href="#">Another subsubmenu action</a></li> -->
					<!-- 			</ul> -->
					<!-- 		</li> -->
					<!-- 	</ul> -->
					<!-- </li> -->
					<!-- <li><a class="dropdown&#45;item dropdown&#45;toggle" -->
					<!-- 	 href="#">Submenu 2</a> -->
					<!-- 	<ul class="dropdown&#45;menu"> -->
					<!-- 		<li><a class="dropdown&#45;item" href="#">Submenu2.2</a></li> -->
					<!-- 		<li><a class="dropdown&#45;item" href="#">Submenu2.3</a></li> -->
					<!-- 		<li><a class="dropdown&#45;item dropdown&#45;toggle" -->
					<!-- 			 href="#">Subsubmenu3</a> -->
					<!-- 			<ul class="dropdown&#45;menu"> -->
					<!-- 				<li><a class="dropdown&#45;item" -->
					<!-- 					href="#">Subsubmenu 1 3</a></li> -->
					<!-- 				<li><a class="dropdown&#45;item" -->
					<!-- 					href="#">Subsubmenu 2 3</a></li> -->
					<!-- 			</ul> -->
					<!-- 		</li> -->
							<!-- <li><a class="dropdown&#45;item dropdown&#45;toggle" -->
							<!-- 	 href="#">Second subsubmenu 3</a> -->
							<!-- 	<ul class="dropdown&#45;menu"> -->
							<!-- 		<li><a class="dropdown&#45;item" -->
							<!-- 			href="#">Subsubmenu action 3 </a></li> -->
							<!-- 		<li><a class="dropdown&#45;item" -->
							<!-- 			href="#">Another subsubmenu action 3</a></li> -->
							<!-- 	</ul> -->
							<!-- </li> -->
						</ul>
					</li>
				</ul>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"
				 href="core.php"
				 id="navbarDropdownMenuLink" data-toggle="dropdown"
				 aria-haspopup="true" aria-expanded="false">
					Basics Compressible Flow
				</a>
				<ul class="dropdown-menu" 
					aria-labelledby="navbarDropdownMenuLink">
					<li><a class="dropdown-item dropdown-toggle"
						 href="#">Speed of Sound</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Sound 1</a>
							</li>
							<li><a class="dropdown-item" href="#">Sound 2</a>
							</li>
							<li><a class="dropdown-item" href="#">Sound 3</a>
				<a class="nav-link" href="#">Potto Project
					<span class="sr-only">(current)</span>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Front Mater</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Core</a>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"
				 href="https://bootstrapthemes.co"
				 id="navbarDropdownMenuLink" data-toggle="dropdown"
				 aria-haspopup="true" aria-expanded="false">
					Mechanics
				</a>
				<ul class="dropdown-menu" 
					aria-labelledby="navbarDropdownMenuLink">
					<li><a class="dropdown-item" href="#">Thermo</a></li>
					<li><a class="dropdown-item" href="#">Statics</a></li>
					<li><a class="dropdown-item dropdown-toggle"
						 href="#">Submenu</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">11action</a></li>
							<li><a class="dropdown-item" href="#">12 submenu</a></li>
							<li><a class="dropdown-item dropdown-toggle"
								 href="#">Subsubmenu</a>
								<ul class="dropdown-menu">
									<li><a class="dropdown-item"
									 href="#">Subsubmenu action aa</a></li>
									<li><a class="dropdown-item"
										 href="#">Another subsubmenu action</a></li>
								</ul>
							</li>
							<li><a class="dropdown-item dropdown-toggle"
							 href="#">Second subsubmenu</a>
								<ul class="dropdown-menu">
									<li><a class="dropdown-item"
										 href="#">Subsubmenu action bb</a></li>
									<li><a class="dropdown-item"
										 href="#">Another subsubmenu action</a></li>
								</ul>
							</li>
						</ul>
					</li>
					<li><a class="dropdown-item dropdown-toggle"
						 href="#">Isontropic Flow</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Isontropic 1</a>
							</li>
							<li><a class="dropdown-item" href="#">Isontropic 2</a>
							</li>
							<li><a class="dropdown-item" href="#">Isontropic 3</a>
							</li>
						</ul>
					</li>
					<li><a class="dropdown-item dropdown-toggle"
						 href="#">Normal Shock</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Shock 1</a>
							</li>
							<li><a class="dropdown-item" href="#">Shock 2</a>
							</li>
							<li><a class="dropdown-item" href="#">Shock 3</a>
							</li>
						</ul>
					</li>
					<li><a class="dropdown-item dropdown-toggle"
						 href="#">Shock in Variable Areas</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Shock Area 1</a>
							</li>
							<li><a class="dropdown-item" href="#">Shock Area 2</a>
							</li>
							<li><a class="dropdown-item" href="#">Shock Area 3</a>
							</li>
						</ul>
					</li>
					<li><a class="dropdown-item" href="#">External Forces</a></li>
					<li><a class="dropdown-item" href="#">Isothermal Flow </a></li>
					<li><a class="dropdown-item dropdown-toggle"
						 href="#">Fanno Flow</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Fanno 1</a>
							</li>
							<li><a class="dropdown-item" href="#">Fanno 2</a>
							</li>
							<li><a class="dropdown-item" href="#">Fanno 3</a>
							</li>
						</ul>
					</li>
					<li><a class="dropdown-item dropdown-toggle"
						 href="#">Rayleigh Flow</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Rayleigh 1</a>
							</li>
							<li><a class="dropdown-item" href="#">Rayleigh 2</a>
							</li>
							<li><a class="dropdown-item" href="#">Rayleigh 3</a>
							</li>
						</ul>
					</li>
					<li><a class="dropdown-item">Semi Rigid Tank</a> </li>
					<li><a class="dropdown-item">Chambers under External</a> </li>
				</ul>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"
				 href="core.php"
				 id="navbarDropdownMenuLink" data-toggle="dropdown"
				 aria-haspopup="true" aria-expanded="false">
						2-D Flow
				</a>
				<ul class="dropdown-menu" 
					aria-labelledby="navbarDropdownMenuLink">
					<li><a class="dropdown-item dropdown-toggle"
						 href="#">Oblique Shock</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Introduction</a></li>
							<li><a class="dropdown-item" href="#">Basics</a></li>
							<li><a class="dropdown-item" href="#">Basics</a></li>
							<li><a class="dropdown-item" href="#">somethinmk</a></li>
							<li><a class="dropdown-item" href="#">Basics</a></li>
						</ul>
					</li>
					<li><a class="dropdown-item dropdown-toggle"
						 href="#">Prandtl Meyer Function</a>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="#">Introduction</a></li>
							<li><a class="dropdown-item" href="#">Basics</a></li>
							<li><a class="dropdown-item" href="#">Basics</a></li>
							<li><a class="dropdown-item" href="#">somethinmk</a></li>
							<li><a class="dropdown-item" href="#">Basics</a></li>
						</ul>
					</li>
					<li><a class="dropdown-item">Appendix A Program </a> </li>  
					<li><a class="dropdown-item">Appendix B History Oblique Shock</a>
					</li>  
				</ul>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Gas Dynamics Tables</a>
			</li>
		</ul>
	</div>
	<a class="navbar-brand float-right " href="http://www.potto.org">Potto Project NFP</a>
</nav>
