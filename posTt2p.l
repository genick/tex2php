/* pret2p.l */
%{
#	 include <cstdio>
#	 include <iostream>
#	 include <string>
#	 include <sstream>
#  include "preDef.hh"
#  include "lable.hh"
#  include "fig.hh"
#  include "enumerators.h"
#  include "commSQL.hh"
#  include "nomen.hh"
#  include "math.hh"
#  include "inLineEQ.hh"

	/* void exLabel (char *); */
#	 define YY_DECL extern "C" int yylex()
%}

WORD		[:alnum:]+
EQS			\$[^$]+\$
EQ		\$([^$])+?[^\\]\$
ELT		\\end\{longtable\}
BF		\\begin\{figure\}
EF		\\end\{figure\}
sbFIG			\\[[:alpha:]]+(\[[[:alnum:][:space:]-]+\]){2}[[:space:]]+\{(\\[[:alpha:]]+[\[\{][[:alnum:][:punct:]]+\%?([^\}]+)?){2}\}\}
LATEXtoRM  \\[:
%option nounput
%option noinput
%option yylineno
%x LONGTABLE
%x MKVISIBLE
%x SOLUTIONT
%x MKNOMEN
%x SSTEX

%%

{EQS}          inLineEq(yytext);
(^$)+					/*remove two empty lines*/;
%%

int main( int argc, char **argv ) {
	ostringstream ss;
	++argv, --argc;
	yyin = fopen( argv[0], "r" );
	if ( argc > 0 ) {
		if ( yyin == NULL ) {
			cerr << "Unable to open " << argv[1] << ": " << strerror(errno) << endl;  
			exit(EXIT_FAILURE);
		}
		else{
			char *token; /*find the name of the file*/
			string wf;
			strtok_r (argv[0],  ".", &token);
			ss <<  argv[0] << ".php";
			workingFileName = ss.str();
			ss.clear();
			cerr << ss.str() << endl;
			ss.str("");
			ss <<  argv[0] << ".log";
			cerr << ss.str() << endl;
			cerr << "Working on file " << workingFileName << "." << endl;
			openLogFile(ss.str().c_str());
			logFile << "Working on file " << argv[0] << "." << endl;
		}
	}
	else {
		yyin = stdin;
	}
	readLabels();
	yy_flex_debug = 1;
	yylex();
	exit(1);
}

/* \\kSeclog(\{[^\}]*\}\n?){4}		empty(yytext, yylineno, "fig mkSeclog"); */
/* {EQS}					inLineEq(yytext);	 */
