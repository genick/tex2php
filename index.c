/* index.c */
#	 include "def.h"
#	 include "utilities.h"
#	 include "index.h"

bool makeInTextIndex (char* fullIndex, char* pName, int inNu, char* seeAlso, 
			enum dataType x ){
//	fseek(indexAutPHP, 0, SEEK_END);
	char * a = "Index ";
	char bb[2] ;
	if ( x == aut ) {
		bb[0] = 'a';
	}
	else if ( x == sub ) { 
		bb[0] = 's';
	}
	else if ( x == reference ) { 
		bb[0] = 'r';
  }
	else {
		bb[0] = 'a';
	}
	bb[1] = '\0';
	char * b = bb;
	fprintf(stdout,
		 	"<p class=\"hidden\"><a href=\"index.php\" id=\"%s\" title=\"%s to %s\">%d%s</a></p>\n",
		 	trim(pName), a, fullIndex, inNu, b);
	return true ;
}

	/* function to capture second repentances 
	 * find the beginning of the second part 
		 input \index{aut}{index} */
char* getCleanIndexName (char* tokenText) {
	char * secondPart = strstr(tokenText, "}{");
	// peal the brackets 
	char * cleanTxt = removeBrackets( secondPart, '{', '}');
	return cleanTxt;
}

char* getMainIndex (char* cleanTxt) {
  int totalTxt = strlen(cleanTxt); 
  char* cama = strchr(cleanTxt, ',');
	char* space = strchr(cleanTxt, ' ');
  int camaLength;
  if ( cama != NULL ) {
    camaLength = strlen (cama) ;
    cleanTxt=strndup(cleanTxt, totalTxt-camaLength);
  }
	else if (space != NULL ) {
    camaLength = strlen (space) ;
    cleanTxt=strndup(cleanTxt, totalTxt-camaLength);
	}
  else  {
    //cleanIndex = cleanTxt ;
  }
  return cleanTxt;
}
	// free(cleanIndex); 
	// strncpy(cleanIndex, cleanIndex, 18);

bool makeInFileIndex (char * fullIndex, char * thisFile){
	  return true;
}

void mkIndex (char * tokenText, enum dataType x, char* FN){
	int locatedIndexNumber;
	uint *indexMaxPtr;
	struct INDEX *indexPtr ;
	//static unsigned short *indexPtr;
	char potentialFileName[64] ; 
	memset(potentialFileName,'\0',strlen(potentialFileName));
	char *fullIndex = getCleanIndexName (tokenText);
	char *mainIndex = getMainIndex(fullIndex);
	//checking if the index exist 
	if ( x == aut ) {
		indexMaxPtr = &indexAutMaxNumber;
		indexPtr = auts;
	}
	else if ( x == sub ) { 
		indexMaxPtr = &indexSubMaxNumber;
		indexPtr = subs;
	}
	else {
		indexMaxPtr = &indexAutMaxNumber;
		indexPtr = auts;
	}
	foundMainIndex = false;
	for (int i=0; i <= *indexMaxPtr ; i++ ){
		locatedIndexNumber = i ;
		//fprintf (stderr, "%s", indexs[i].indexName );
		sprintf (potentialFileName, "%s#%s%d", workingFileName, mainIndex, locatedIndexNumber); 
		if (strcmp(indexPtr->indexName, mainIndex) == 0) { /* it is old main index */
			foundMainIndex = true;
			break ;
		}
		else { /* check the next index */
		 	continue ;
		}
	}
	if (foundMainIndex == false ){
		strcpy(indexPtr->indexName, mainIndex);
		strcpy(indexPtr->fin, fullIndex);
		strcpy(indexPtr->fileName[0],potentialFileName);
		/* indexs[*indexMaxPtr].numberFiles++; */
		(*indexMaxPtr)++;
	}
	else {  // add to the list the file name
		strcpy(indexPtr->fileName[indexPtr->numberFiles], potentialFileName);
		indexPtr->numberFiles++;
	}
	// index into html text
	makeInTextIndex (fullIndex, potentialFileName,
		 		(locatedIndexNumber+indexPtr->numberFiles), indexPtr->seeAlso, x);
	(*indexMaxPtr)++;
	foundMainIndex = false;
	foundIndexFile = false; 
}

void mkIndexElement (char * tokenText, enum indexStatus stats, enum dataType x, char* FN){
	/* uint matchIndex = 0U ; */
	uint locatedIndexNumber ; 
	char mIndex[64], fIndex [128],  lIndex[128];
	char potentialFileName[64], *fullIndex, *mainIndex, *seeALSO ={"eMpty"} ;
	memset(potentialFileName,'\0',sizeof (char)* sizeof(potentialFileName));
	uint *indexMaxPtr;
	struct INDEX *indexPtr ;
	if ( x == aut ) { 
		indexMaxPtr = &indexAutMaxNumber;
		indexPtr = auts;
	}
	else if ( x == sub ) { 
		indexMaxPtr = &indexSubMaxNumber;
		indexPtr = subs;
	}
	else {
		indexMaxPtr = &indexAutMaxNumber;
	  indexPtr = auts;
	}
	if (stats == regular) {
		fullIndex = getCleanIndexName (tokenText);
		mainIndex = getMainIndex(fullIndex);
	}
	else{
		memset(mIndex,'\0',sizeof (char)* sizeof(mIndex));
		memset(fIndex,'\0',sizeof (char)* sizeof(fIndex));
		memset(lIndex,'\0',sizeof (char)* sizeof(lIndex));
		mkSee(tokenText, mIndex, fIndex, lIndex) ;
		fullIndex = fIndex;
		mainIndex = mIndex;
		seeALSO = lIndex;
	}
	//checking if the index exist 
	foundMainIndex = false;
	//locatedIndexNumber = 0 ;
	if ( *indexMaxPtr > 0){
		while (isalpha(indexPtr->indexName[0]) ){
			if (strcmp(indexPtr->indexName,mainIndex) == 0) { /* it is old main index */
				foundMainIndex = true;
				break ;
			}
			else { /* check the next index */
				indexPtr++;
				locatedIndexNumber++;
				continue ;
			}
		}
	}
	else {
		fprintf(stderr,"no array to check\n");
	}
	sprintf (potentialFileName, "%s#%s%d",
				workingFileName,
				(rmChar(mainIndex, ' ')),
			 	indexPtr->numberFiles); 
	if (foundMainIndex == false ){
		//char insertedFileLink [512];
		//sprintf(insertedFileLink, "%s#%s%d",workingFileName,mainIndex, indexAutMaxNumber);
		strcpy(indexPtr->indexName, rmChar(mainIndex,' '));
		strcpy(indexPtr->fin, fullIndex);
		strcpy(indexPtr->seeAlso, seeALSO);
		strcpy(indexPtr->fileName[0],potentialFileName);
		indexPtr->in = *indexMaxPtr;
		indexPtr->numberFiles = 1;
		/* indexs[indexAutMaxNumber].numberFiles++; */
		//locatedIndexNumber++;
		(*indexMaxPtr)++;
	}
	else {  // add to the list the file name
		strcpy(indexPtr->fileName[indexPtr->numberFiles],
				 potentialFileName);
		indexPtr->numberFiles++;
	}
	// index into html text
	makeInTextIndex(fullIndex, potentialFileName, *indexMaxPtr, seeALSO, x);
	indexPtr++;
	foundMainIndex = false;
	foundIndexFile = false; 
	//printIndex (*indexMaxPtr, x, tokenText );
}

void mkIndexAut (char * tokenText, enum indexStatus stats, enum dataType x, char* FN){
	/* uint matchIndex = 0U ; */
	uint locatedIndexNumber ; 
	//char *cleanIndex, potentialFileName[128];
	char potentialFileName[64] ; //, *extractedFileName = NULL;
	memset(potentialFileName,'\0',sizeof (char)* sizeof(potentialFileName));
	//struct tmpHold tmpHolds;
	//uint iHold=0;
	char *fullIndex = getCleanIndexName (tokenText);
	char *mainIndex = getMainIndex(fullIndex);
	//checking if the index exist 
	foundMainIndex = false;
	for (int i=0; i <= indexAutMaxNumber ; i++ ){
		locatedIndexNumber = i ;
		//fprintf (stderr, "%s", indexs[i].indexName );
		sprintf (potentialFileName, "%s#%s%d",workingFileName, mainIndex, indexAutNumber); 
		if (strcmp(auts[i].indexName,mainIndex) == 0) { /* it is old main index */
			foundMainIndex = true;
			break ;
		}
		else { /* check the next index */
		 	continue ;
		}
	}
	if (foundMainIndex == false ){
		//char insertedFileLink [512];
		//sprintf(insertedFileLink, "%s#%s%d",workingFileName,mainIndex, indexAutMaxNumber);
		strcpy(auts[indexAutMaxNumber].indexName, mainIndex);
		strcpy(auts[indexAutMaxNumber].fin, fullIndex);
		strcpy(auts[indexAutMaxNumber].fileName[0],potentialFileName);
		/* indexs[indexAutMaxNumber].numberFiles++; */
		indexAutMaxNumber++;
	}
	else {  // add to the list the file name
		strcpy(auts[locatedIndexNumber].fileName[auts[locatedIndexNumber].numberFiles],
				 potentialFileName);
		auts[locatedIndexNumber].numberFiles++;
	}
	makeInTextIndex (fullIndex, potentialFileName, locatedIndexNumber, 
			auts[indexAutMaxNumber].seeAlso, x);// index into html text
	foundMainIndex = false;
	foundIndexFile = false; 
}
