// Wed Jul  3 15:40:47 CDT 2019 change from FIGURES to LABELS
//fig.cc starting point Wed Apr 26 22:04:13 CDT 2017
#	include "fig.hh"
vector <string> subFigureLabels;
// extern void extractParameter(FIGURES*, std::string&, int, itemMember, char, char, actionType, char, itemMember);

void outputSubFigData(LABELS* ptrF){
		cout << "\t<li>" << endl << "\t\t\t<a name=\"" 
		<< "#" << ptrF->getMemberValue(LB,figNumber,'\0') 
		<< "\">" << endl ;
	cout << "\t\t\t\t<figure>" << endl;
	cout << "\t\t\t\t\t<img src=\"images/" << 
	ptrF->getMemberValue(FN,0,'\0') << ".png\" alt=\"" 
			<< ptrF->getMemberValue(CAo,0,'\0')
		<< "\">" <<  endl;
	cout << "\t\t\t\t\t<div class\"caption\" >" << endl 
		<<    "\t\t\t\t\t\tFig. "<<ptrF->getMemberValue(NU,0,'\0')
	 	<< " " << ptrF->getMemberValue(CA,0,'\0') << endl 
		<< "\t\t\t\t\t</div>" << endl;
	cout << "\t\t\t\t</figure>" << endl;
	cout << "\t\t\t</a>" << endl
		<< "\t\t</li> <!-- End sub figure. -->" << endl ;
}

void outputFigData(LABELS* ptrF){
	cout << "<div class=\"figContainer\" id =\""<< 
	ptrF->getMemberValue(LB,0,'\0') << "\">" << endl ;
	cout << "\t<div class=\"imgContainer\">" << endl;
	cout << "\t\t<figure>" << endl;
	cout << "\t\t\t<img src=\"images/" 
		<< ptrF->getMemberValue(FN,0,'\0') << ".png\" alt=\"" 
		<< ptrF->getMemberValue(CAo,0,'\0')
		<< "\">" <<  endl ;
	cout << "\t\t\t\t<div class=\"caption\">" << endl
		<< "Fig. "  <<  ptrF->getMemberValue(NU,0,'\0') << " "  
		<< trim(ptrF->getMemberValue(CA,0,'\0')) << endl 
		<< "\t\t\t\t</div >" << endl;
	cout << "\t\t</figure>" << endl;
	cout << "\t</div >" << endl;
	cout << "</div >" << endl;
}

// to move to the utility ?
void insertData (LABELS* ptrF, labelType x ){
	vector <itemMember> data;
	vector <intMember> dataI;
	if (x == FIG){
		for (auto i : {LB, NU, CA, CAt, CAo, FN, PG}){
			data.push_back(i);
		}
		for (auto i : {Pg, Nn}){
			dataI.push_back(i);
		}
	}
	else if (x == SUBFIG){
		for (auto i : {LB, NU, CA, CAt, CAo, FN, PG}){
			data.push_back(i);
		}
		for (auto i : {Pg, Nn}){
			dataI.push_back(i);
		}
	}
	else if (x == RFIG){
		for (auto i : {LB, NU}){
			data.push_back(i);
		}
	}
	else if (x == MAINFIG){
		for (auto i : {LB, NU, CA, CAt, CAo, PG}){
			data.push_back(i);
		}
		int j=0 ;
		for	(auto i : { SN1, SN2, SN3, SN4, SN5}){
			data.push_back(i);
			if ( j >= ptrF->getMemberValueI(Nn)){
				break;
			}
			j++;
		}
		for (auto i : {Pg, Nn}){
			dataI.push_back(i);
		}
	}
	insertDataDB(ptrF, data, dataI, FIGU, "figure");
	return;
}

void wImg (string l, int lineNu, string FROM){
	l = rmTabNewLine(l);
	LABELS fig(1);    
	LABELS *F;    
	F = &fig;    
	// int j=1;
	string page, label;
	logFile << "L: " << lineNu << " Found Caption" << endl;
	logFile << l << endl; 
	extractParameter(F, l, 1, FN, '{', '}', lastPrt);
	extractParameter(F, l, 1, CAt,'{', '}');
	extractParameter(F, l, 2, LB,'{', '}');
	extractParameter(F, l, 1, CAo,'{', '}');
	extractParameter(F, l, 1, CA,'{', '}');
	page = lookUpNumber(F->getMemberValue(LB, 0,' '), LB, PG);
	label = lookUpNumber(F->getMemberValue(LB, 0,' '), LB, NU);
	F->setMemberValue (page, PG);
	F->setMemberValue (label, NU);
	insertData (F, FIG);
	outputFigData(F);
}

void caption (string l, int lineNu, string FROM){
	l = rmTabNewLine(l);
	LABELS fig(1);    
	LABELS *F;    
	F = &fig;    
	string page, label;
	logFile << "L: " << lineNu << " Found Caption" << endl;
	logFile << l << endl; 
	extractParameter(F, l, 1, CAt,'[', ']');
	extractParameter(F, l, 1, CA,'{', '}', adParameter, '/', CAo );
	extractParameter(F, l, 1, LB,'{', '}');
	page = lookUpNumber(F->getMemberValue(LB, 0,' '), LB, PG);
	label = lookUpNumber(F->getMemberValue(LB, 0,' '), LB, NU);
	F->setMemberValue (page, PG);
	F->setMemberValue (label, NU);
	int j=1;
	vector <itemMember> k = {SN1, SN2, SN3, SN4, SN5};
	for (auto i : subFigureLabels){
		F->setMemberValue (i,k.at(j-1));
		//(string key, string newValue, string field, string tn)
		updateData( i, F->getMemberValue(LB, 0,' '), "mainName", "figure");
		j++;
		// cout << i << endl;
	}
	j--;
	F->setMemberValueI (j, Nn);
	F->setMemberValueI (stoi(page), Pg);
	page = lookUpNumber(F->getMemberValue(LB, 0,' '), LB, PG);
	label = lookUpNumber(F->getMemberValue(LB, 0,' '), LB, NU);
	F->setMemberValue(page, PG);
	F->setMemberValue(label, NU);
	insertData (F, MAINFIG);
	cout << "\n\t</ul> <!-- End UL list figures. -->" << endl ; 
	cout << "\t<div class=\"mainCaption\">" << endl;
	cout << "\t\tFig. " << F->getMemberValue(NU, 0, ' ') << " "  
			<< F->getMemberValue(CA, 0, ' ') << endl; 
	cout << "\t</div > <!-- End of the main caption. -->" << endl;
}

void pFigureIni (string l, int lineNu, string FROM){
	logFile << "L: " <<  lineNu << " Starting Big Figure" << endl;
	cout <<  "bIGfIG" << endl; 	
}

void pFigureEnd ( string l, int lineNu, string FROM){
	logFile << lineNu << " Finishing Big Figure" << endl;
	cout <<  "eNDbIGfIG"  << endl; 	
}

void figureIni (string l, int lineNu, string FROM){
	subFigureLabels.clear(); // empty the labels to pass  
	logFile << "L: " <<  lineNu << " Starting Big Figure" << endl;
	cout <<  "bIGfIG" << endl;
}

void figureEnd ( string l, int lineNu, string FROM){
	cout << "</ul>" << endl;
	logFile << lineNu << " Finishing Big Figure" << endl;
	cout <<  "eNDbIGfIG"  << endl;
}

void figIni( string l, int lineNu, string FROM){
	figNumber=0;
	cout << "<div class=\"bigFigure\"> <!-- Start Big Figure. -->" << endl; 
	logFile << lineNu << ": Caught a Figure:" << FROM <<  endl; 
	cout << "\n\t<ul class=\"subFigures\">" << endl ; 
}

void figEnd( string l, int lineNu, string FROM){
	logFile << "last figure is " <<  figNumber << endl
	<<	lineNu << ": Finish the Figure :" << FROM <<  endl; 
	cout << "</div> <!-- End of the big figure. -->" << endl; 
	figNumber = 0;
}

void subFigure ( string l, int lineNu, string FROM){
	l = rmTabNewLine(l);
	string buffer, label;
	toBeChanged ( l, lineNu, FROM);
	LABELS fig(1);
	LABELS *ptrF;
	ptrF = &fig;
	extractParameter(ptrF, l, 1, CAo,'[', ']', adParameter, '/', CAt );
	extractParameter(ptrF, l, 1, CA, '[', ']', doNothing);
	extractParameter(ptrF, l, 2, LB, '{', '}', doLabel);
	subFigureLabels.push_back(ptrF->getMemberValue(LB, 0,' '));//localVariable
	extractParameter(ptrF, l, 1, FN, '{', '}', lastPrt);
	ptrF->setMemberValueI (figNumber, Nn); 
	ptrF->setMemberValue ("sub", LV); 
	outputSubFigData(ptrF);
	insertData (ptrF, SUBFIG );
	figNumber++;
}

void imgPDF(string l){
	l = rmTabNewLine(l);
	// ostringstream ss;
	string buffer, label;
	LABELS fig(1);
	LABELS *ptrF;
	ptrF = &fig;
	extractParameter(ptrF, l, 1, FN, '{', '}', lastPrt);
	extractParameter(ptrF, l, 1, CAo, '{', '}', doNothing);
	extractParameter(ptrF, l, 2, LB, '{', '}', doLabel);
	extractParameter(ptrF, l, 1, CAt, '{', '}', doNothing);
	extractParameter(ptrF, l, 1, CA, '{', '}', doNothing);
	outputFigData(ptrF);
	insertData (ptrF, FIG );
	return;
}

void picTextPDF( string l, int lineNu, string FROM){
	cout << l ; 
}

void setRef ( string l, int lineNu, string FROM){
	cerr << endl;
	l = rmTabNewLine(l);
	string buffer, label;
	peelFirstB(l, 1);
	size_t found = l.find('}');
	if (found!=string::npos) {
		l.erase (found);
	}
	else{
		return;
	}
	buffer = lookUpNumber (l, LB, NU) ;
	cout << itemName.c_str() << " <a href=\"" << l << "\">(" << buffer  << ")</a>";
	// cout << endl;
	LABELS fig(1);
	LABELS *ptrF;
	ptrF = &fig;
	ptrF->setMemberValue (buffer, NU); 
	ptrF->setMemberValue (l, LB);
	insertData (ptrF, RFIG );
	checkInData (l, "figure", CA, 0);
	// ptrF->setMemberValueI(4,CN,2);
}
