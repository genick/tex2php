// mngmnt.cc starting point Thu Apr 27 19:01:56 CDT 2017
# include "mngmnt.hh"

void openLogFile ( string l ) {
	now = time(0);
	logFile.open(l.c_str());
	logFile << ctime(&now) <<  endl << "Opening log File " <<
	 l.c_str() << "!" << 	endl ;
	logFile.flush();
}

void openFootnoteFile ( string l) {
	ostringstream ss;
	ss.str("");
	now = time(0);
	ss.clear();
	ss << trim(l) ;
	ss << ".footnote";
	footnoteFile.open(ss.str());
	footnoteFile << "<!--" <<  ctime(&now) <<  "creating the footnote File \"" 
		<< ss.str() <<  "\"-->" << endl;
	footnoteFile << "<h4> Footnotes for chapter " << toUPPER(mainName) << 
		"</h4>" << endl ;
	footnoteFile.flush();
	errMsg ("opening the footnote file for production") ;
}

void openLogFiles(vector <string> l){
	for (auto i : l){ 
		i.append(".log");
		openLogFile(i);
	}
}

bool openOperations(string l){
	stringstream ss;
	vector <string> ll ; 
	ll.push_back(l);
	openLogFiles(ll);
	openFootnoteFile(l);
	ss << "Initiating files and MYSQL data!"  ;
	errMsg(ss.str());
	readLabels();
	cleanDataBase(l);
	return true;
}

void closeOperations(){
	logFile << ctime(&now) <<  "Closing  operation and MYSQL data!" << endl ;
	logFile.flush();
	logFile.close();
}
