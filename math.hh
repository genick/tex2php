/* mathematics.hh file */
 #ifndef __MATH_H
 #define __MATH_H

# include <iostream>
# include <fstream>
# include <sstream>
# include <string>
# include <cstdio>
# include <cstring>
# include "utility.hh"
# include "io.hh"

using namespace std;

	void empheq (string, int);
	void endEmpheq(string, int);
	void eqLine(string, int, string);
	void calSymbol(string, int);
	void bLongEq( string, int);
	void eLongEq( string, int);
	// void inLineEq(string);
	extern string workingFileName;
	extern string emphEq;

#endif   /* End __MATH_H  */
