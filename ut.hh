/* ut.hh file */
#ifndef UT_H__
#define UT_H__
# include <string>
# include <sstream>
# include <iostream>
# include <cstring>
# include "enumerators.h" 
// # include "preDef.hh" 
using namespace std;

	extern int nmnNumber;
	extern string workingFileName;
	char * trim(char*);
	string trim(string);
	void texStrToPhpStr(string, string);
	void peelFirstB( string & , size_t, char a='{');
	string rmTabNewLine(string );
	string lastPart(string , char );
	// string peelFirstPart(string &, size_t , char);
	string peelFirstPart(string &, int , char);
	void peelFirstB(string &, size_t, char);
	string mkNomeNu (dataType);

#endif  // End UT_H__
