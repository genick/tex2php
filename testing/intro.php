
\htmetainfo{description}{free textbook pdf and HTML open source }
\htmetainfo{keywords}{normal shock, shock tube, moving shocks
shock dynamics, gas dynamics calculator, nozzle efficiency,
compressible flow, Prandtl conditions, manual solution }

<h1>Introduction</h1>

\section{What is Compressible Flow?}

This book deals with an introduction\footnote{This book is
gradually sliding to include more material that isn't so introductory. 
But an attempt is made to present the material in introductory
level.} to the flow of compressible substances (gases).
The main difference between compressible flow and almost
incompressible flow is not the fact that compressibility has to
be considered.
Rather, the difference is in two phenomena that
do not exist in incompressible flow\footnote{It can be argued
that in open channel flow there is a hydraulic jump 
(discontinuity)
and in some ranges no effect of downstream conditions on the flow. 
However, the uniqueness of the phenomena in the gas dynamics provides
spectacular situations of a limited length (see Fanno model) and
thermal choking, etc. Further, there is no equivalent to oblique
shock wave.
Thus, this richness is unique to gas dynamics.}.
\index{sub}{Hydraulic Jump|see{discontinuity}}\index{sub}{discontinuity}
The first phenomenon is the very sharp discontinuity (jump)
in the flow in properties.
The second phenomenon is the choking of the flow.
Choking is when downstream variations don't effect the
flow\footnote{The thermal choking is somewhat different but
a similarity exists.}.
Though choking occurs in certain pipe flows in astronomy, there also
are situations of choking in general
(external) flow\footnote{This book is
intended for engineers and therefore a discussion about astronomical
conditions isn't presented.}.
Choking is referred to as the situation where downstream conditions,
which are beyond a critical value(s), doesn't affect the flow.

The shock wave and choking are not intuitive for most people.
However, one has to realize that \underline{intuition} is really
a condition where one uses his past experiences to predict other
situations.
Here one has to learn to use his intuition as a tool for future use. 
Thus, not only aeronautic engineers, but other engineers, and
even manufacturing engineers will be able use this ``intuition''
in design and even research.

\section{Why Compressible Flow is Important?}

Compressible flow appears in many natural and many technological processes.
Compressible flow deals with more than air, including steam,
natural gas, nitrogen and helium, etc.
For instance, the flow of natural gas in a pipe system, 
a common method of heating in the u.s., should be considered 
a compressible flow. 
These processes include the flow of gas in the exhaust system
of an internal combustion engine, and also gas turbine,
a problem that led to the Fanno flow model. 
The above flows that were mentioned are called internal flows. 
Compressible flow also includes flow around bodies such as the wings
of an airplane, and is considered an external flow.

These processes include situations 
not expected to have a compressible flow, such as manufacturing
process such as the die casting, injection molding. 
The die casting process is a process in which liquid metal,
mostly aluminum, is injected into a mold to obtain a near final shape. 
The air is displaced by the liquid metal in a very rapid manner,
in a matter of milliseconds, therefore the compressibility has
to be taken into account.

Clearly, Aero Engineers are not the only ones  who have to deal with
some aspect of compressible flow. 
For manufacturing engineers there are many situations where
the compressibility or compressible flow understating is essential
for adequate design.  
For instance, the control engineers who are using pneumatic systems
use compressed substances. 
The cooling of some manufacturing systems and design of refrigeration
systems also utilizes compressed air flow knowledge.
Some aspects of these systems require consideration of the unique
phenomena of compressible flow.

Traditionally, most gas dynamics (compressible flow) classes deal
mostly with shock waves and external flow and briefly teach
Fanno flows and Rayleigh flows (two kind of choking flows).
There are very few courses that deal with isothermal flow.
In fact, many books on compressible flow ignore the isothermal 
flow\footnote{Any search on the web on classes of compressible flow
will show this fact and the undersigned can testify that this was true
in his first class as a student of compressible
flow.}
%begin{latexonly}
\index{sub}{Isothermal Flow}.
%end{latexonly}
\begin{htmlonly}
\index{Isothermal Flow}.
\end{htmlonly}

In this book, a greater emphasis is on the internal flow.
This doesn't in any way meant that the important topics
such as shock wave and oblique shock wave should be neglected.
This book contains several chapters which deal with external flow
as well.

\section{Historical Background}

In writing this book it became clear that there is more unknown and
unwritten about the history of compressible fluid than known.
While there are excellent books about the history of fluid mechanics
(hydraulic) see for example book by Rouse\footnote{Hunter Rouse and\index{aut}{Rouse, Hunter}
Simon Inc, History of Hydraulics (Iowa City: Institute of Hydraulic
Research, 1957)}.
There are numerous sources dealing with the
history of flight and airplanes (aeronautic)\footnote{%
Anderson, J. D., Jr. 1997. A History of Aerodynamics: And Its Impact
on Flying Machines, Cambridge University Press, Cambridge, England.}.
Aeronautics is an overlapping
part of compressible flow, however these two fields are different.
For example, the Fanno flow and isothermal flow, which are the
core of gas dynamics, are not part of aerodynamics.
Possible reasons for the lack of written documentation
are one, a large part of this knowledge is relatively new,
and two, for many early contributors this topic was a side issue.  
In fact, only one contributor of the three main models of internal 
compressible flow (Isothermal, Fanno, Rayleigh)
was described by any text book.
This was Lord Rayleigh, for whom the Rayleigh flow was named.
The other two models were, to the undersigned, unknown.
Furthermore, this author did not find any reference to isothermal
flow model
%begin{latexonly}
\index{sub}{Isothermal Flow}
%end{latexonly}
\begin{htmlonly}
\index{Isothermal Flow}
\end{htmlonly}
earlier to Shapiro's book.\index{aut}{Shapiro}
There is no book\footnote{The only remark found about
Fanno flow that it was taken from the Fanno Master thesis by his
adviser.
Here is a challenge: find any book describing the history of the
Fanno model.}
that describes the history of these models.
For instance, the question, who was Fanno, and when did he live,
could not be answered by any of the undersigned's colleagues in
University of Minnesota or elsewhere.

At this stage there are more questions about the history
of compressible flow needing to be answered. 
Sometimes, these questions will appear in a section with a title but
without text or with only a little text. 
Sometimes, they will appear in a footnote like this\footnote{Who
developed the isothermal model? The research so far leads to Shapiro.
Perhaps this flow  should be named after the Shapiro.
Is there any earlier reference to this model?}
%begin{latexonly}
\index{sub}{Shapiro Flow}
%end{latexonly}
\begin{htmlonly}
\index{Shapiro Flow}
\end{htmlonly}
For example, it is obvious that Shapiro published the erroneous
conclusion that all the chocking occurred at 
\latexhtml{$M=1$}{\begin{rawhtml} <i> M=1 </i> \end{rawhtml}} in his article
which contradicts his isothermal model.  
Additional example, who was the first to ``conclude'' the
``all'' the chocking occurs at 
\latexhtml{$M=1$}{\begin{rawhtml} <i> M=1 </i> \end{rawhtml}}? Is it Shapiro?

Originally, there was no idea that there are special effects
and phenomena of compressible flow.
Some researchers even have suggested that compressibility can be
``swallowed'' into the ideal flow (Euler's equation's flow is
sometimes referred to as ideal flow).
Even before Prandtl's idea of boundary layer appeared,
the significant and importance of compressibility emerged.
 
In the first half of nineteen century there was little realization that the compressibility is
important because there were very little applications (if any)
that required the understanding of this phenomenon.
As there were no motivations to investigate the shock wave or choked
flow both were treated as the same, taking compressible flow as if it
were incompressible flow.

It must be noted that researchers were interested in the speed of
sound even long before applications and knowledge could demand any utilization.
The research and interest in the speed of sound was a purely academic interest.
The early application in which compressibility has a major effect was with fire arms.
The technological improvements in fire arms led to a gun capable of
shooting bullets at speeds approaching to the speed of sound.
Thus, researchers were aware that the speed of sound is some kind of limit.

In the second half of the nineteen century, Mach and Fliegner ``stumbled''
%begin{latexonly}
\index{sub}{Mach}
%end{latexonly}
\begin{htmlonly}
\index{Mach}
\end{htmlonly}
%begin{latexonly}
\index{sub}{Fliegner}
%end{latexonly}
\begin{htmlonly}
\index{Fliegner}
\end{htmlonly}
over the shock wave and choking, respectively.
Mach observed shock and Fliegner measured the choking but
theoretical science did not provide explanation for it (or
was award that there is an explanation for it.). 

In the twentieth century the flight industry became the pushing force.
Understandably, aerospace engineering played a significant role in
the development of this knowledge.
Giants like Prandtl
%begin{latexonly}
\index{aut}{Prandtl, Ludwig}
%end{latexonly}
\begin{htmlonly}
\index{Prandtl, Ludwig}
\end{htmlonly}
and his students like
Von Karman\index{aut}{Von Karman}
\begin{htmlonly}
\index{Van Karman}%
\end{htmlonly}
, as well as
others like Shapiro
%begin{latexonly}
\index{aut}{Shapiro}%
%end{latexonly}
\begin{htmlonly}
\index{Shapiro}%
\end{htmlonly}
, dominated the field.
During that time, the modern basic classes became ``solidified.''
Contributions by researchers and educators from other fields were
not as dominant and significant, so almost all text books in this
field are written from an aerodynamic prospective.

%from other fields.
\begin{comment}
{To add history from the work.
 Topics that should be included in this history review but that are not yet added to
this section are as follows: Multi Phase flow, capillary flow and
phase change.}
\end{comment}

\subsection{Early Developments}

The compressible flow is a subset of fluid mechanics/hydraulics and therefore the knowledge
development followed the understanding of incompressible flow.
Early contributors were motivated from a purely intellectual
curiosity, while most later contributions were driven by necessity.
As a result, for a long time the question of the speed of sound was
bounced around.

\subsubsection{Speed of Sound}

The idea that there is a speed of sound
%begin{latexonly}
\index{sub}{speed of sound}
%end{latexonly}
\begin{htmlonly}
\index{speed of sound}
\end{htmlonly}
and that it can be measured is a major achievement.
A possible explanation to this discovery lies in the fact that 
mother nature exhibits in every thunder storm the difference between
the speed of light and the speed of sound.
There is no clear evidence as to who came up with this concept,
but some attribute it to Galileo Galilei: 166x.
%begin{latexonly}
\index{aut}{Galileo Galilei}
%end{latexonly}
\begin{htmlonly}
\index{Galileo Galilei}
\end{htmlonly}
Galileo, an Italian scientist, was one of the earliest contributors
to our understanding of sound.
Dealing with the difference between the two speeds (light, sound)
was a major part of Galileo's work.
However, once there was a realization that sound can be measured,
people found that sound travels in different speeds through
different mediums.
The early approach to the speed of sound was by the measuring
of the speed of sound. 

Other milestones in the speed of sound understanding development were
by Leonardo Da Vinci,
%begin{latexonly}
\index{aut}{Leonardo Da Vinci}
%end{latexonly}
\begin{htmlonly}
\index{Leonardo Da Vinci}
\end{htmlonly}
who discovered that sound travels in waves (1500). 
Marin Mersenne  was the first to measure the speed of sound in air (1640).\index{aut}{Mersenne, Marin}
Robert Boyle discovered that sound waves must travel in a medium (1660) and this lead
to the concept that sound is a pressure change.\index{aut}{Boyle, Robert} 
Newton\index{aut}{Newton} was the first to formulate a relationship
between the speed of sound in gases by relating the density and
compressibility in a medium (by assuming isothermal process).
Newton's equation is missing the heat ratio, $k$ (late 1660's).
Maxwell was the first to derive the speed of sound for gas as $c=\sqrt{k\,R\,T}$ from particles
(statistical)  mechanics.\index{aut}{Maxwell, James Clerk}
Therefore some referred to coefficient $\sqrt{k}$ as Maxwell's coefficient.\index{sub}{Maxwell's coefficient}
%
\subsection{The shock wave puzzle}
Here is where the politics of science was a major obstacle to
achieving an advancement\footnote{Amazingly, science is full of many stories of conflicts and disputes.
Aside from the conflicts of scientists with the Catholic Church and Muslim religion, perhaps the most
famous is that of Newton's netscaping (stealing and embracing) Leibniz['s] invention of 
calculus.\index{aut}{Leibniz}
There are even conflicts from not giving enough credit, like Moody.\index{aut}{Moody}
Even the undersigned encountered individuals who have tried to ride on his work.
The other kind of problem is ``hijacking'' by a sector.
Even on this subject, the Aeronautic sector ``took over'' gas
dynamics as did the emphasis on mathematics like perturbations
methods or asymptotic expansions instead on the physical phenomena. 
Major material like Fanno flow isn't taught in many classes, 
while many of the mathematical techniques are currently practiced.
So, these problems are more common than one might be expected.}.
%begin{latexonly}
\index{sub}{science disputes}
%end{latexonly}
\begin{htmlonly}
\index{science disputes}
\end{htmlonly}
not giving the due credit to Rouse.\index{aut}{Rouse, Hunter}
\begin{htmlonly}
\index{Rouse}
\end{htmlonly}
In the early 18xx, conservation of energy was a concept that
was applied only to mechanical energy.
On the other side, a different group of scientists dealt with calorimetry (internal energy). 
It was easier to publish articles about the second law of 
thermodynamics than to convince anyone of the first law of thermodynamics.
Neither of these groups would agree to ``merge'' or ``relinquish''
control of their ``territory'' to the other.
It took about a century to establish the first law\footnote{%
This recognition of the first law is today the most ``obvious'' for 
engineering students. 
Yet for many it was still debatable up to the middle of the nineteen
century.}.

At first, Poisson
%begin{latexonly}
\index{aut}{Poisson}
%end{latexonly}
\begin{htmlonly}
\index{Poisson}
\end{htmlonly}
found a ``solution'' to the
Euler's equations with certain boundary conditions 
which required discontinuity\footnote{% 
Sim\'{e}on Denis Poisson, French mathematician, 1781-1840 worked in
Paris, France.  "M'emoire sur la th'eorie du son," J.
Ec. Polytech. 14 (1808), 319-392. From Classic Papers in Shock 
Compression Science, 3-65, High-press. Shock Compression Condens.
Matter, Springer, New York, 1998.}
which had obtained an implicit form in 1808.
Poisson showed that solutions could approach a discontinuity 
by using conservation of mass and momentum.
He had then correctly derived the jump conditions that discontinuous
solutions must satisfy.
Later, Challis
%begin{latexonly}
\index{aut}{Challis}
%end{latexonly}
\begin{htmlonly}
\index{Challis}
\end{htmlonly}
had noticed contradictions
concerning some solutions of the equations of compressible gas
dynamics\footnote{%
James Challis, English Astronomer, 1803-1882. worked at Cambridge, England UK.
"On the velocity of sound," Philos. Mag. XXXII (1848), 494-499}.
Again the ``jumping'' conditions were redeveloped by
two different researchers independently:
Stokes and Riemann.
%begin{latexonly}
\index{aut}{Stokes}
%end{latexonly}
\begin{htmlonly}
\index{Stokes}
\end{htmlonly}
%begin{latexonly}
\index{aut}{Riemann}
%end{latexonly}
\begin{htmlonly}
\index{Riemann}
\end{htmlonly}
Riemann, in his 1860 thesis, was not sure whether or not
discontinuity is only a mathematical creature or a real creature.
Stokes in 1848 retreated from his work and wrote an apology on
his ``mistake.''\footnote{Stokes George Gabriel Sir, Mathematical and Physical Papers,
Reprinted from the original journals and transactions, with
additional notes by the author. Cambridge, University Press, 1880-1905.}
Stokes was convinced by Lord Rayleigh
%begin{latexonly}
\index{aut}{Rayleigh}
%end{latexonly}
\begin{htmlonly}
\index{Rayleigh}
\end{htmlonly}
and Lord
Kelvin that he was mistaken on the grounds that energy is conserved
(not realizing the concept of internal
energy).
%begin{latexonly}
\index{sub}{internal energy}
%end{latexonly}
\begin{htmlonly}
\index{internal energy}
\end{htmlonly}

At this stage some experimental evidence was needed.
Ernest Mach studied several fields in physics and also studied philosophy.
He was mostly interested in experimental physics.
The major breakthrough in the understanding of compressible flow came
when Ernest Mach
%begin{latexonly}
\index{aut}{Mach, Ernest}
%end{latexonly}
\begin{htmlonly}
\index{Mach, Ernest}
\end{htmlonly}
``stumbled'' over the discontinuity.
It is widely believed that Mach had done his research as purely intellectual research.
His research centered on optic aspects which lead him to study
acoustic and therefore supersonic flow (high speed, since no Mach
number was known at that time).
However, it is logical to believe that his interest had risen due to
the need to achieve powerful/long--distance shooting rifles/guns.
At that time many inventions dealt with machine guns
which were able to shoot more bullets per minute.
At the time, one anecdotal story suggests a way to make money by
inventing a better killing machine for the Europeans.
While the machine gun turned out to be a good killing machine,
defense techniques started to appear such as sand bags.
A need for bullets that could travel faster to overcome these obstacles was created.
Therefore, Mach's paper from 1876 deals with the flow around bullets.
Nevertheless, no known\footnote{The words
``no known'' refer to the undersigned.
It is possible that some insight was developed but none of
the documents that were reviewed revealed it to the undersigned.}
equations or explanations resulted from these experiments.

Mach used his knowledge in Optics to study the flow around bullets.
What makes Mach's achievement all the more remarkable was
the technique he used to take the historic photograph:
He employed an innovative approach called the shadowgraph.
He was the first to photograph the shock wave.
In his paper discussing "Photographische Fixierung der durch
Projektile in der Luft eingeleiten Vorgange" he showed a picture
of a shock wave (see Figure \ref{intro:fig:bullet}).
He utilized the variations of the air density to clearly show
shock line at the front of the bullet.
Mach had good understanding of the fundamentals of supersonic flow
and the effects on bullet movement (supersonic flow).\index{sub}{Mach's bullet}
Mach's paper from 1876 demonstrated shock wave (discontinuity) and
suggested the importance of the ratio of the velocity to the speed of sound.
He also observed the existence of a conical shock wave
(oblique shock wave).

Mach's contributions can be summarized as providing an
experimental proof to discontinuity.
He further showed that the discontinuity occurs at
\latexhtml{$M=1$}{\begin{rawhtml}<i> M=1 </i> \end{rawhtml}}
and realized that the velocity ratio (Mach number), and not the 
velocity, is the important parameter in the study of the compressible flow.
Thus, he brought confidence to the theoreticians to publish their studies. 
While Mach proved shock wave and oblique shock wave existence,
he was not able to analyze it (neither was he
aware of Poisson's
%begin{latexonly}
\index{aut}{Poisson}
%end{latexonly}
\begin{htmlonly}
\index{Poisson}
\end{htmlonly}
work or the works of others.).

Back to the pencil and paper, the jump conditions were redeveloped and now named after
Rankine\footnote{William John Macquorn Rankine,
 Scottish engineer,
1820-1872. He worked in Glasgow, Scotland UK.
"On the thermodynamic theory of waves of finite longitudinal
disturbance," Philos. Trans. 160 (1870), part II, 277-288.
Classic papers in shock compression science,
133-147, High-press. Shock Compression Condens.
Matter, Springer, New York, 1998}
%begin{latexonly}
\index{aut}{Rankine, John Macquorn}
%end{latexonly}
\begin{htmlonly}
\index{Rankine, John Macquorn}
\end{htmlonly}
and Hugoniot\footnote{%
Pierre Henri Hugoniot, French engineer, 1851-1887.
"Sur la propagation du mouvement dans les corps et sp'ecialement
dans les gaz parfaits, I, II" J.  Ec.  
Polytech. 57 (1887), 3-97, 58 (1889), 1-125. Classic papers in shock
compression science, 161-243, 245-358, High-press.
Shock Compression Condens. Matter, Springer, New York, 1998}.
Rankine and Hugoniot, redeveloped independently the equation
that governs the relationship of the shock wave.
Shock was assumed to be one dimensional and mass, momentum,
and energy equations\footnote{Today it is well established that
shock has three dimensions but small sections can be treated as
one dimensional.}
%begin{latexonly}
\index{aut}{Hugoniot, Pierre Henri}
%end{latexonly}
\begin{htmlonly}
\index{Hugoniot, Pierre Henri}
\end{htmlonly}
lead to a solution which ties the upstream and downstream properties.
What they could not prove or find was that shock occurs
only when upstream is supersonic, i.e., direction of the flow.
Later, others expanded Rankine-Hugoniot's conditions to a more general
form\footnote{%
To add discussion about the general relationships.}.

Here, the second law has been around for over 40 years and yet 
the significance of it was not was well established.
Thus, it took over 50 years for Prandtl to arrive at and to
demonstrate that the shock has only one direction\footnote{
Some view the work of G.~I.~Taylor
from England as the proof (of course utilizing the second law)}.
%begin{latexonly}
\index{aut}{Taylor, G. I.}
%end{latexonly}
\begin{htmlonly}
\index{Taylor, G. I.}
\end{htmlonly}
Today this equation/condition is known as Prandtl's equation or
condition (1908).
In fact Prandtl is the one who introduced the name of
Rankine-Hugoniot's conditions not aware of the earlier
developments of this condition.
Theodor Meyer
%begin{latexonly}
\index{aut}{Meyer, Theodor}
%end{latexonly}
\begin{htmlonly}
\index{Meyer, Theodor}
\end{htmlonly}
 (Prandtl's student)
derived the conditions for oblique shock in
1908\footnote{Theodor Meyer in Mitteil. \"{u}b.
Forsch-Arb.  Berlin, 1908, No. 62, page 62.} as a byproduct
of the expansion work. 

% \begin{htmlonly}
% \begin{figure}
% \htmlimage{scale = 2.8, thumbnail=1.5}
% 	\centerline{\includegraphics{scaned/shock}}
%    \caption{The shock as connection of Fanno and Rayleigh lines.}
% \end{figure}
%       %%%\centerline{\tt The shock as a connection of Fanno and Rayleigh lines}
% \end{htmlonly}


% \wrapImgES{scaned/shock}
\wImg{scaned/shock}
	{Control Volume and System After and  During Motion}
	{0.50}
	{intro:fig:shock}
	{The shock as a connection of Fanno and Rayleigh lines}
	{The shock as a connection of Fanno and Rayleigh lines
   607   after Stodola, Steam and Gas Turbine.}
	{19}%lines
	{-09.0 mm}
	{0.9}

It was probably later that Stodola
\index{aut}{Fanno, Gino Girolamo}%
\index{aut}{Stodola}%
% \begin{htmlonly}
% \index{Fanno, Gino Girolamo}
% \index{Stodola}
% \end{htmlonly}
(Fanno's adviser) realized that the shock
is the intersection of the Fanno line with the Rayleigh line.
%begin{latexonly}
\index{sub}{intersection of Fanno and Rayleigh lines}
%end{latexonly}
% \begin{htmlonly}
% \index{intersection of Fanno and Rayleigh lines}
% \end{htmlonly}
Yet, the supersonic branch is missing from his understanding
(see Figure \eqref{intro:fig:shock}). 
In fact, Stodola suggested the graphical solution utilizing the Fanno line.

The fact that the conditions and direction
were known did not bring the solution to the equations.
The ``last nail'' of understanding
was put by Landau, a Jewish scientist who worked in Moscow University 
in the 1960's during the Communist regimes. 
A solution was found by Landau \& Lifshitz
%begin{latexonly}
\index{aut}{Landau, Lev}
%end{latexonly}
\begin{htmlonly}
\index{Landau, Lev}
\end{htmlonly}
and expanded by Kolosnitsyn \& Stanyukovich (1984).\index{aut}{Kolosnitsyn}\index{aut}{Stanyukovich}
\begin{comment}
{to be add to oblique shock chapter.}
\end{comment}

Since early in the 1950s the analytical relationships between the
oblique shock, deflection angle, shock angle, and Mach number
was described as impossible to obtain.
There were until recently (version 0.3 of this book)
several equations that tied various properties/quantities
for example, the relationship between upstream Mach number and the angles.  
The first full analytical solution connecting the angles
with upstream Mach number was published in this book version 0.3.
The probable reason that analytical solution was not
discovered earlier because the claim in the famous report of NACA 1135
\index{sub}{NACA 1135}%
that explicit analytical solution isn't possible\footnote{%
Since writing this book,  several individuals 
point out that a solution was found in book ``Analytical
Fluid Dynamics'' by Emanuel, George, second edition,
December 2000 (US\$ 124.90).
That solution is based on a transformation of $\sin\theta$ to $\tan\beta$. 
It is interesting that transformation result in one of
root being negative.
While the actual solution all the roots are real and positive
for the attached shock.
The presentation was missing the condition for the detachment
or point where the model collapse.
But more surprisingly, similar analysis was published by Briggs, J.
``Comment on Calculation of Oblique shock waves,''
AIAA Journal Vol 2, No 5 p. 974, 1963. 
Hence, Emanuel's partial solution just redone 36 years work\index{aut}{Emanuel, G.}
(how many times works have to be redone in this field). 
In addition there was additional publishing of similar works 
by Mascitti, V.R. and Wolf, T.\index{aut}{Mascitti V.R.}\index{aut}{Wolf, T.} 
In a way, part of analysis of this book is also redoing old work.
Yet, what is new in this work is completeness of all the three
roots and the analytical condition for detached shock and breaking
of the model.}%
\footnote{See for a longer story in www.potto.org/obliqueArticle.php.}.
\index{sub}{Emanuel's partial solution to oblique shock}%
\index{sub}{Bar-Meir's solution to Oblique shock}%

The question whether the angle of the oblique shock is stable or which of the three roots
is stable was daunting since the early discovery that there are three possible solutions.
\index{sub}{Oblique shock stability}%
It is amazing that early research concluded that only the weak solution
is possible or stable as opposed to the reality.
The first that attempt this question where in 1931 by 
Epstein\footnote{Epstein, P. S., ``On the air resistance of \index{aut}{Epstein, P. S}
Projectiles,'' Proceedings of the National Academy of Science, Vol. 17, 1931, pp. 532-547.}.
His analysis was based on Hamilton's principle when he ignore the boundary condition.
The results of that analysis was that strong shock is unstable.   
The researchers understood that flow after a strong shock
was governed by elliptic equation while the flow after
a weak shock was governed by hyperbolic equations. 
This difference probably results in not recognizing that 
The boundary conditions play an important role in the
stability of the shock\footnote{In study this issue
this author realized only after examining a colleague
experimental Picture \ref{oblique:fig:tflow} that it was
clear that the Normal shock along with strong shock and weak
shock ``live'' together  peacefully and in stable conditions.}.
In fact analysis based on Hamilton's principle isn't suitable
for stability because entropy creation was recognized 1955 by  
Herivel\footnote{Herivel, J. F., ``The Derivation of The\index{aut}{Herivel, J. F.}
Equations of Motion On an Ideal Fluid by Hamilton's Principle,,'' 
Proceedings of the Cambridge philosophical society, Vol. 51, Pt.  2, 1955, pp. 344-349.}.

Carrier\footnote{Carrier, G.F., ``On the Stability of the\index{aut}{Carrier, G.F.}
supersonic Flows Past as a Wedge,'' Quarterly of Applied Mathematics, Vol. 6, 1949, pp. 367--378.}
was first to recognize that strong and weak shocks stable.
In fact, the confusion on this issue is persistent until now.
Even all books that were published recently claimed that
no strong shock was ever observed in flow around cone (Taylor--Maccoll flow).\index{sub}{Taylor--Maccoll flow}
In fact, even this author sinned in this erroneous conclusion. 
The real question isn't if they exist rather under what conditions these shocks exist which was suggested by
Courant and Friedrichs\index{aut}{Courant}\index{aut}{Friedrichs}
in their book ``Supersonic Flow and Shock Waves,'' published
by Interscience Publishers, Inc. New York, 1948, p. 317.   

The effect of real gases was investigated very early since steam was used propel turbines.
In general the mathematical treatment was left to numerical investigation and there is
relatively very little known on the difference between ideal gas model and real gas.
For example, recently,  Henderson and Menikoff\footnote{\index{aut}{Henderson}\index{aut}{Menikoff}
Henderson and Menikoff, ``Triple Shock Entropy Theorem,'' Journal of Fluid Mechanics 366 (1998) pp. 179--210.}  
dealt with only the procedure to find the maximum of oblique shock, but no comparison between real gases
and ideal gas is offered there.

The moving shock
%begin{latexonly}
\index{sub}{moving shock}
%end{latexonly}
\begin{htmlonly}
\index{moving shock}
\end{htmlonly}
and shock tube were study even before World War Two (II).
The realization that in most cases the moving shock can be analyzed
as steady state since it approaches semi steady state can be traced early of 1940's.
Up to this version 0.4.3 of this book (as far it is known, this book is the first to publish this tables),
trial and error method was the only method to solve this problem.
Only after the dimensionless presentation of the problem 
and the construction of the moving shock table the problem became trivial. 
Later, an explicit analytical solution for shock a head of
piston movement (a special case of open valve) was originally 
published in this book for the first time.

\subsection{Choking Flow}

\begin{htmlonly}
\begin{figure}
\htmlimage{scale = 0.8, thumbnail=0.7}
	\centerline{\includegraphics {scaned/de}}
    \caption[The schematic of deLavel's turbine]{} 
	\label{intro:fig:deLavalNozzle}
\end{figure}
	\centerline{\tt The schematic of deLavel's turbine after Stodola,
		Steam and Gas Turbine}
\end{htmlonly}

%begin{latexonly}
\wImg{scaned/shock}
	{The schematic of deLavel's turbine}
	{0.50}
	{intro:fig:shock}
	{The schematic of deLavel's turbine}
	{The schematic of deLavel's turbine 
   after Stodola, Steam and Gas Turbine.}
	{18}%lines
	{-09.0 mm}
	{0.9}

% \begin{wrapfigure}[17]{r}[+4pt]{0.52\textwidth}
% $\;$\par
% \vspace{-3.0 mm}
% 	\centerline{\includegraphics
%         [bb= 25 1 391 348, width=.45\textwidth, clip]
% 		{scaned/de}}
%     \caption[The schematic of deLavel's turbine] 
% 	{The schematic of deLavel's turbine 
%     after Stodola, Steam and Gas Turbine}
% 	\label{intro:fig:deLavalNozzle}
% \end{wrapfigure}
%end{latexonly}
The choking problem is almost unique to gas dynamics and has many different forms.
Choking wasn't clearly to be observed, even when researcher stumbled over it. 
No one was looking for or expecting the choking to occur,
and when it was found the significance of the choking phenomenon was not clear.  
The first experimental choking phenomenon was discovered by Fliegner's experiments\index{aut}{Fliegner
Schweizer Bauztg}
%begin{latexonly}
\index{sub}{Fliegner experiment}
%end{latexonly}
\begin{htmlonly}
\index{Fliegner experiment}
\index{deLavel's nozzle|see{de Laval, Garl Gustaf Patrik}}
\index{de Laval, Carl Gustaf Patrik}
\end{htmlonly}
which were conducted some
time in the middle of 186x\footnote{Fliegner Schweizer Bauztg., Vol 31 1898, p. 68--72.
The theoretical first work on this issue was done by Zeuner,\index{aut}{Zeuner}
``Theorie die Turbinen,'' Leipzig 1899, page 268 f.}
on air flow through a converging nozzle.
As a result deLavel's nozzle
%begin{latexonly}
\index{sub}{deLavel's nozzle|see{de Laval, Garl Gustaf Patrik}}
\index{sub}{de Laval, Carl Gustaf Patrik}
%end{latexonly}
was invented by Carl Gustaf Patrik de Laval\index{aut}{de Laval, Carl Gustaf Patrik}
in 1882 and first successful operation by another inventor\index{aut}{Curtis}
(Curtis) 1896 used in steam turbine.
Yet, there was no realization that the flow is choked just that
the flow moves faster than speed of sound.

The introduction of the steam engine and other thermodynamics
cycles led to the choking problem. 
The problem was introduced because people wanted to increase
the output of the Engine by increasing the flames (larger
heat transfer or larger energy) which failed, leading to the study
and development of Rayleigh flow.\index{sub}{Rayleigh flow}
According the thermodynamics theory (various cycles) the larger 
heat supply for a given temperature difference (larger higher
temperature) the larger the output, but after a certain point
it did matter (because the steam was choked).  
The first to discover (try to explain) the choking phenomenon was
Rayleigh\footnote{Rayleigh was the first to develop the\index{aut}{Rayleigh}
model that bears his name.
It is likely that others had noticed that flow is choked,
but did not produce any model or conduct successful experimental work.}.

After the introduction of the deLavel's converging--diverging nozzle
theoretical work was started by Zeuner\footnote{Zeuner, ``Theorie\index{aut}{Zeuner}
der Turbinen, Leipzig 1899 page 268 f.}.
Later continue by Prandtl's group\footnote{Some of the
publications were not named after Prandtl but rather by his students
like Meyer, Theodor.\index{aut}{Meyer, Theodor}
In the literature appeared reference to article by Lorenz\index{aut}{Lorenz}
in the Physik  Zeitshr., as if in 1904.
Perhaps, there are also other works that this author did
not come a crossed.} starting 1904.
In 1908 Meyer has extend this work to make two dimensional
calculations\footnote{Meyer, Th., \"{U}ber zweidimensionals
Bewegungsvordange eines Gases, Dissertation 1907, erschienen in den 
Mitteilungen \"{u}ber Forsch.-Arb. Ing.-Wes. heft 62, Berlin 1908.}.  
Experimental work by Parenty\footnote{Parenty, Comptes R. Paris,\index{aut}{Comptes R. Paris}
Vol. 113, 116, 119; Ann. Chim. Phys. Vol. 8. 8 1896, Vol 12,
1897.} and others measured the pressure along the converging-diverging nozzle.

%However, as E.R.G. Eckert (private communication) stated
%it might turned out
%that someone one proposed it earlier as many models were
%proposed earlier.

It was commonly believed\footnote{The personal experience of this
undersigned shows that even instructors of Gas Dynamics are not aware
that the chocking occurs at different Mach number and depends on the
model.} that the choking occurs only at 
\latexhtml{$M=1$}{\begin{rawhtml}<i> M=1 </i> \end{rawhtml}}. 
The first one to analyzed that choking occurs at $1/\sqrt{k}$ for
isothermal flow was Shapiro (195x).
It is so strange that a giant like Shapiro did not realize his model
on isothermal contradict his conclusion from his own famous paper.
Later Romer
%begin{latexonly}
\index{sub}{Romer|see{isothermal nozzle}}
%end{latexonly}
\begin{htmlonly}
\index{Romer|see{isothermal nozzle}}
\end{htmlonly}
at el extended it to isothermal variable area flow (1955).
In this book, this author adapts
E.R.G.~Ecert's idea of\index{aut}{Eckert, E.R.G}
dimensionless parameters control which determines where the reality 
lay between the two extremes.
Recently this concept was proposed  (not explicitly) by Dutton and\index{aut}{Dutton}
Converdill \index{aut}{Converdill}(1997)\footnote{These researchers
demonstrate results between two extremes and actually proposed this idea.
However, that the presentation here suggests that topic should be
presented case between two extremes.}.
Namely, in many cases the reality is somewhere between the adiabatic and the isothermal flow.
The actual results will be determined by the modified Eckert number\index{sub}{Eckert number}
to which model they are closer.


\subsubsection{Nozzle Flow}
\parindent 0pt
%\begin{minipage}{\textwidth}
\wImg{scaned/nozzlePressure}
	{Stodotla Nozzle\index{aut}{Stodotla}}
	{0.55}
	{intro:fig:nozzlePressure}
	{The measured pressure in a nozzle}
	{The measured pressure in a nozzle taken from Stodola 1927 Steam and Gas Turbines.}
	{16}
	{-17.0 mm}
	{0.65}
\parindent 25 true  pt 
%\figText{0.6}
The first ``wind tunnel'' was not a tunnel but a rotating arm attached
at the center.
At the end of the arm was the object that was under observation and study. 
The arm's circular motion could reach a velocity above the speed of sound at its end.
Yet, in 1904 the Wright brothers demonstrated that results from the
wind tunnel and spinning arm are different due to the circular motion.
As a result, the spinning arm was no longer used in testing.
Between the turn of the century and 1947-48, when the first supersonic
wind tunnel was built, several models that explained choking at
the throat have been built.

A different reason to study the converging-diverging nozzle was 
the Venturi meter which was used in measuring the flow rate of gases.
Bendemann \footnote{Bendemann Mitteil \"{u}ber Forschungsarbeiten,\index{aut}{Bendemann Mitteil}
Berlin, 1907, No. 37.} carried experiments to study the accuracy
of these flow meters and he measured and refound that the flow
reaches a critical value (pressure ratio of 0.545) that creates the maximum flow rate.   

There are two main models or extremes that describe the flow in the
nozzle: isothermal and adiabatic.



{\parindent 0pt 
\begin{minipage}{\textwidth}
%begin{latexonly}
\begin{wrapfigure}[19]{r}[+4pt]{0.65\textwidth}
$\;$\par
\vspace{-1.0 mm}
	\centerline{\includegraphics
        [bb= 6 3 168 165, width=.5\textwidth, clip]
		{scaned/fanno}}
	\caption[Flow rate as a function of the back pressure] 
	{Flow rate as a function of the back pressure taken
	after Stodola 1927 Steam and Gas Turbines}
	\label{intro:fig:fannoFun}
\end{wrapfigure}
%end{latexonly}
Romer et al\footnotemark{}
analyzed the isothermal flow in a nozzle.
It is remarkable that choking was found as $1 /\sqrt{k} $ as opposed to one (1).
In general when the model is assumed to be isothermal the choking occurs at $1/\sqrt{k}$.  
The concept that the choking point can move from the throat 
introduced by\footnotemark{}
a researcher unknown to this author.
It is very interesting that the isothermal nozzle was proposed by
Romer at el 1955 (who was behind the adviser or the student?).\index{aut}{Romer, Carl Jr}
These researchers were the first ones to realized that choking can occurs at different Mach
number ($1/\sqrt{k}$) other than the isothermal pipe.  
\end{minipage}}
\footnotetext{Romer, I Carl Jr., and Ali Bulent Cambel,\index{aut}{Cambel, Ali Bulent} 
``Analysis of Isothermal Variable Area Flow,'' Aircraft Eng. vol. 27 no 322, p. 398 December 1955.}
\footnotetext{This undersign didn't find the actual
trace to the source  of proposing this effect.
However, some astronomy books showing this effect in a dimensional
form without mentioning the original researcher.
In dimensionless form, this phenomenon produces a dimensionless number
similar to Ozer number and therefor the name Ozer number adapted\index{sub}{Ozer number}
in this book.}

\begin{comment}
{to insert the isothermal nozzle with external forces
like gravity and to show that choking location can move
depending on the direction of the force.}
\end{comment}

\subsubsection{Rayleigh Flow}

%begin{latexonly}
\index{sub}{Rayleigh Flow}
%end{latexonly}
\begin{htmlonly}
\index{Rayleigh Flow}
\end{htmlonly}
Rayleigh was probably\footnote{As most of the history research has
shown, there is also a possibility that someone found it earlier.
For example, Simeon--Denis Poisson was the first one to realize the shock 
wave possibility.}, the first to suggest a model for 
frictionless flow with a constant heat transfer.
Rayleigh's work was during the time when it was debatable as to
whether there are two forms of energies (mechanical, thermal),
even though Watt and others found and proved that they are the same.
Therefore, Rayleigh looked at flow without mechanical energy transfer
(friction) but only thermal energy transfer.
\begin{comment}
{To find where Rayleigh did understand that his model
leads  to $1/\sqrt{k}$ point flow and graphical representation of the flow.
The $1/\sqrt{k}$ question.} 
\end{comment}
In Rayleigh flow, the material reaches choking point due to heat
transfer, hence the term ``thermally choked'' is used;
no additional heat to ``flow'' can occur.

\begin{comment}
{to insert information about the detonation wave and
relationship to Rayleigh line.}
\end{comment}

\subsubsection{Fanno Flow}
%begin{latexonly}
\index{sub}{Fanno flow}
%end{latexonly}
\begin{htmlonly}
\index{Fanno flow}
\end{htmlonly}
The most important model in compressible flow was suggested
by Gino Fanno in his Master's thesis (1904).\index{aut}{Fanno, Gino Girolamo}
The model bears his name.
Yet, according to Dr.~Rudolf Mumenthaler from UTH University (the place where Fanno Studied),
no copy of the thesis can be found in the original University
and perhaps only in the personal
custody of the Fanno family\footnote{This material is very important
and someone should find it and make it available to researchers.}.
Fanno attributes the main pressure reduction to friction.
Thus, flow that is dominantly adiabatic could be simplified and
analyzed.
The friction factor
%begin{latexonly}
\index{sub}{friction factor}
%end{latexonly}
\begin{htmlonly}
\index{friction factor}
\index{Moody diagram}
\end{htmlonly}
is the main component
in the analysis
as Darcy $f$\footnote{Fanning $f$ based radius is only one quarter of\index{aut}{Fanning}
the Darcy $f$ which is based on diameter} had already proposed in 1845.\index{aut}{Darcy} 
The arrival of the Moody diagram,
%begin{latexonly}
\index{sub}{Moody diagram}
%end{latexonly}
which built on Hunter Rouse's (194x) work made Darcy--Weisbach's\index{aut}{Rouse, Hunter}
equation universally useful.
Without the existence of the friction factor data, the Fanno model
wasn't able to produce a prediction useful for the industry.
Additionally an understating of the supersonic branch of the flow 
was unknown (The idea of shock in tube was not raised at that time.).
Shapiro organized all the material in a coherent way
and made this model useful.
%begin{latexonly}
\index{sub}{Shapiro flow}
%end{latexonly}
\begin{htmlonly}
\index{Shapiro flow}
\end{htmlonly}

\begin{htmlonly}
\par \parindent 0pt 
{\Huge Meta}

\par \parindent 25pt 

Did Fanno realize that the flow is choked? 
It appears at least in Stodola's book that choking was\index{aut}{Stodola}
understood in 1927 and even earlier.
The choking was assumed only to be in the subsonic flow.
But because the actual Fanno's thesis is not available,
the question cannot be answered yet.
When was Gas Dynamics (compressible flow) as a separate class started?
Did the explanation for the combination of diverging-converging nuzzle
with tube for Fanno flow first appeared in Shapiro's book? 
\par \parindent 0pt 
{\Huge End Meta}
\par \parindent 25pt 
\end{htmlonly}
%begin{latexonly}
\begin{meta}
Did Fanno realize that the flow is choked? 
It appears at least in Stodola's book that choking was
understood in 1927 and even earlier.
The choking was assumed only to be in the subsonic flow.
But because the actual Fanno's thesis is not available,
the question cannot be answered yet.
When was Gas Dynamics (compressible flow) as a separate class started?
Did the explanation for the combination of diverging-converging nuzzle
with tube for Fanno flow first appeared in Shapiro's book? 
\end{meta}
%end{latexonly}
\parindent 0 true  pt 
 
\begin{comment}
{expanding model by others}
\end{comment}

\subsubsection{Isothermal Flow}
\index{sub}{Isothermal Flow|see{Shapiro flow}}

The earliest reference to isothermal flow was found in Shapiro's Book.
\begin{comment}
{If it turned out that no one had done it before Shapiro, this
flow model should be called Shapiro's flow.
The author invites others to help in this information.}
\end{comment}
The model suggests that the choking occurs at $1/\sqrt{k}$
and it appears that Shapiro was the first one to realize this
difference compared to the other models. 
In reality, the flow is choked somewhere between $1/\sqrt{k}$ to
one for cases that are between Fanno (adiabatic) and isothermal flow.
This fact was evident in industrial applications
where the expectation of the choking is at Mach one, but
can be explained by choking at a lower Mach number. 
No experimental evidence, known by the undersigned, was ever
produced to verify this finding.

\subsection{External flow }
%begin{latexonly}
\index{sub}{External flow}
%end{latexonly}
\begin{htmlonly}
\index{External flow}
\end{htmlonly}

\parindent 25 true  pt 
When the flow over an external body is about .8 Mach or more the flow 
must be considered to be a compressible flow.
However at a Mach number above 0.8 (relative of velocity of the body
to upstream velocity) a local Mach number (local velocity) can reach
\latexhtml{$M=1$}{\begin{rawhtml}<i> M=1 </i> \end{rawhtml}}.  
At that stage, a shock wave occurs which increases the resistance.
The Navier-Stokes equations which describe the flow 
(or even Euler equations) were considered unsolvable during the
mid 18xx because of the high complexity. 
This problem led to two consequences.
Theoreticians tried to simplify the equations and arrive at
approximate solutions representing specific cases.
Examples of such work are Hermann von Helmholtz's concept
of vortex filaments (1858), Lanchester's concept of circulatory
flow (1894),
and the Kutta-Joukowski circulation theory\index{sub}{Kutta-Joukowski circulation theory}
%begin{latexonly}
\index{aut}{Kutta-Joukowski}
%end{latexonly}
\begin{htmlonly}
\index{Kutta-Joukowski}
\index{Wright brothers}
\index{Prandtl, Ludwig}
\end{htmlonly}
of lift  (1906).
Practitioners like the Wright brothers
%begin{latexonly}
\index{aut}{Wright brothers}
%end{latexonly}
relied upon experimentation to figure out what theory could
not yet tell them.

Ludwig Prandtl
%begin{latexonly}
\index{aut}{Prandtl, Ludwig}
%end{latexonly}
in 1904 explained the two most important causes of drag by introducing the boundary layer theory.
Prandtl's boundary layer theory allowed various simplifications of the Navier-Stokes equations.
Prandtl worked on calculating the effect of induced drag on lift.\index{aut}{Prandtl}
He introduced the {\it lifting line theory}, which was published in
1918-1919 and enabled accurate calculations of induced
drag and its effect on lift\footnote{%
The English call this theory the Lanchester--Prandtl theory.\index{sub}{Lanchester--Prandtl theory}
This is because the English Astronomer Frederick\index{aut}{Lanchester, Frederick}
Lanchester published the foundation for Prandtl's theory in
his 1907 book {\it  Aerodynamics}.
However, Prandtl claimed that he was not aware of Lanchester's model when he had begun his work in 1911.
This claim seems reasonable in the light that Prandtl was not 
ware of earlier works when he named erroneously the conditions for the shock wave.
See for the full story in the shock section.}.

During World War I,  Prandtl created his thin--airfoil theory\index{sub}{Thin-airfoil theory}
that enabled the calculation of lift for thin, cambered airfoils.
He later contributed to the Prandtl-Glauert rule for subsonic
airflow that describes the compressibility effects of air at high speeds.
Prandtl's student, Von Karman reduced the equations for supersonic
flow into a single equation.

After the First World War aviation became important and in the 
1920s a push of research focused on what was called the {\em compressibility 
problem}.\index{sub}{Compressibility problem}
Airplanes could not yet fly fast, but the propellers (which are
also airfoils) did exceed the speed of sound, especially at the
propeller tips, thus exhibiting inefficiency.
Frank Caldwell and Elisha Fales demonstrated in 1918 that at a
critical speed (later renamed the {\em critical Mach number})\index{sub}{Critical Mach number}
airfoils suffered dramatic increases in drag and decreases in lift.
Later, Briggs and Dryden showed that the problem was related to\index{aut}{Briggs}
the shock wave.\index{aut}{Dryden}
Meanwhile in Germany, one of Prandtl's assistants, J.  Ackeret,\index{aut}{Ackeret, J.} 
simplified the shock equations so that they became easy to use.
After World War Two, the research had continued and some technical solutions were found.
Some of the solutions lead to tedious calculations which lead to the
creation of Computational Fluid Dynamics (CFD).
Today these methods of perturbations and asymptotic are hardly used
in wing calculations\footnote{This undersigned is aware of only one
case that these methods were really used to calculations of wing.}.
That is the ``dinosaur\footnote{It is like teaching using slide ruler
in today school. By the way, slide rule is sold for about 7.5\$ on the net.
Yet, there is no reason to teach it in a regular school.}'' reason
that even today some instructors are teaching mostly
the perturbations and asymptotic methods in Gas Dynamics classes.

More information on external flow 
can be found in , John D.~Anderson's Book\index{aut}{Anderson, D.}
``History of Aerodynamics and Its Impact on Flying Machines,''
Cambridge University Press, 1997.

\subsection{Filling and Evacuating Gaseous Chambers }

It is remarkable that there were so few contributions made
in the area of a filling or evacuation gaseous chamber.
The earlier work dealing with this issue was by Giffen, 1940, and was
republished by Owczarek, J. A., the model and solution to\index{aut}{Owczarek, J. A.}
the nozzle attached to chamber issue in his book ``Fundamentals
of Gas Dynamics.''\footnote{International Textbook Co., Scranton, Pennsylvania, 1964.}.
He also extended the model to include the unchoked case.
Later several researchers mostly from the University in Illinois
extended this work to isothermal nozzle (choked and unchoked). 

The simplest model of nozzle, is not sufficient in many cases
and a connection by a tube (rather just nozzle or orifice) is more appropriated.
Since World War II considerable works have been carried out
in this area but with very little progress\footnote{In fact, the
emergence of the CFD gave the illusion that there are solutions
at hand, not realizing that garbage in is garbage out, i.e.,
the model has to be based on scientific principles and not detached from reality.
As anecdotal story explaining the lack of progress,
in die casting conference there was a discussion and
presentation on which turbulence model is suitable for a
{\em complete} still liquid.
Other ``strange'' models can be found in the undersigned's book
``Fundamentals of Die Casting Design.}.   
In 1993 the first reasonable models for forced volume were
published by the undersigned.
Later, that model was extended by several research groups,
The analytical solution for forced volume and the ``{\em balloon}''
problem (airbag's problem) model were published first
in this book (version 0.35) in 2005.
The classification of filling or evacuating the chamber as 
external control and internal control (mostly by pressure)
was described in version 0.3 of this book by this author.

\subsection{Biographies of Major Figures}

\wImg{photos/galileo}
	{Galileo Portrait}
	{0.42}
	{intro:fig:galileo}
	{Portrait of Galileo Galilei}
	{Portrait of Galileo Galilei.}
	{14}%lines
	{-10.0 mm}
 	{0.8}

In this section a short summary of major figures that influenced
the field of gas dynamics is present.
There are many figures that should be included and a biased selection
was required.
Much information can be obtained from other resources, such as
the Internet.
In this section there is no originality and none should be expected.  

\subsubsection{Galileo Galilei}
\index{aut}{Galileo Galilei}
\begin{htmlonly}
\begin{figure}
\htmlimage{scale = 0.8, thumbnail=0.4}
	\centerline{\includegraphics {photos/galileo}}
	\caption[Portrait of Galileo Galilei]{}
	\label{intro:fig:galileo}
\end{figure}
	\centerline{\tt Portrait of Galileo Galilei}
\end{htmlonly}
%{\parindent 0pt
%begin{latexonly}
%\begin{minipage}{\textwidth}
%\begin{wrapfigure}[16]{r}[+4pt]{0.5\textwidth}
%$\;$\par
%\vspace{-5 mm}
%	\centerline{\includegraphics
%        [bb= 0 1 587 792, width=.34\textwidth, clip]
%			{photos/galileo}}
%	\caption{Portrait of Galileo Galilei}
%	\label{intro:fig:galileo}
%\end{wrapfigure}
%end{latexonly}
Galileo was born in Pisa, Italy on February 15, 1564 to musician
Vincenzo Galilei and Giulia degli Ammannati.
The oldest of six children, Galileo moved with his family in early 1570 to Florence.
Galileo started his studying at the University of Pisa in 1581.
He then became a professor of mathematics at the University of Padua in 1592.
During the time after his study, he made numerous discoveries such
as that of the pendulum clock, (1602).
Galileo also proved that objects fell with the same velocity
regardless of their size.
%begin{latexonly}
%\end{minipage}
%}
%end{latexonly}

Galileo  had a relationship with Marina Gamba (they never married)\index{Gamba, Marina}
who lived and worked in his house in Padua, where she bore him three children.
However, this relationship did not last and Marina married Giovanni
Bartoluzzi and Galileo's son, Vincenzio, joined him in Florence 
(1613).

Galileo invented many mechanical devices such as the pump and the telescope (1609).  
His telescopes helped him make many astronomic observations which
proved the Copernican system.
Galileo's observations got him into trouble with the Catholic Church,
however, because of his noble ancestry, the church was not harsh with him.
Galileo was convicted after publishing his book {\em Dialogue}, 
and he was put under house arrest for the remainder of his life.
Galileo died in {1642} in his home outside of Florence.

\subsubsection{Ernest Mach (1838-1916)}
\index{aut}{Mach, Ernest}
\wImg{photos/EMach}
	{Mach Photo}
	{0.55}
	{intro:fig:EMach}
	{Photo of Ernest Mach}
	{Photo of Ernest Mach.}
	{11}%lines
 	{-6.0 mm}
	{0.5}
{\parindent 0pt
%begin{latexonly}
% \begin{minipage}{\textwidth}
% \begin{wrapfigure}[15]{r}[+4pt]{0.54\textwidth}
% $\;$\par
% \vspace{-5.0 mm}
% 	\centerline{\includegraphics
%         [bb= 156 273 455 519, width=.45\textwidth, clip]
% 		{photos/EMach}}
% 	\caption{Photo of Ernest Mach }
% 	\label{intro:fig:mach}
% \end{wrapfigure}
%end{latexonly}
Ernest Mach was born in 1838 in Chrlice (now part of Brno), when
Czechia was still a part of the Austro--Hungary empire.
Johann, Mach's father, was a high school teacher who taught Ernest at
home until he was 14, when he studied in Kromeriz Gymnasium, before
he entered the university of Vienna were he studies mathematics,
physics and philosophy.
He graduated from Vienna in 1860.
There Mach wrote his thesis "On Electrical Discharge and Induction."
Mach was interested also in physiology of sensory perception.
% \end{minipage}}

\parindent 25pt

\vspace{1mm}
\begin{htmlonly}
\begin{figure}
\htmlimage{scale = 0.8, thumbnail=0.76}
	\centerline{\includegraphics
		{photos/EMach}}
	\caption[Photo of Ernest Mach ]{}
	\label{intro:fig:mach}
\end{figure}
	\centerline{\tt Photo of Ernest Mach }
\end{htmlonly}
At first he received a professorship position at Graz in mathematics 
(1864) and was then offered a position as a professor of surgery
at the university of Salzburg, but he declined.
He then turned to physics, and in 1867 he received a position in
the Technical University in Prague\footnote{It is 
interesting to point out that Prague provided us two of the top 
influential researchers: E.~Mach and E.R.G.~Eckert.} 
where he taught experimental physics for the next 28 years.

\wImg{photos/bullet}
	{Mach Bullet}
	{0.45}
	{intro:fig:bullet}
	{The bullet photo of in a supersonic flow taken by Mach}
	{The photo of the bullet in a supersonic flow which was taken by Mach.
		Note it was not taken in a wind tunnel}
	{10}%lines
 	{-6.0 mm}
	{0.5}
\index{sub}{Mach's bullet}

Mach was also a great thinker/philosopher and influenced
the theory of relativity dealing with frame of reference.
In 1863, Ernest Mach (1836 - 1916) published Die
Machanik in which he formalized this argument.
Later, Einstein was greatly influenced by it, and in 1918,\index{aut}{Einstein}
he named it {\em Mach's Principle}.
This was one of the primary sources of inspiration for Einstein's
theory of General Relativity.

%\vspace{4mm}
\parindent 25pt 
Mach's revolutionary experiment demonstrated the existence of the
shock wave as shown in Figure \ref{intro:fig:bullet}.
It is amazing that Mach was able to photograph the phenomenon using
the spinning arm technique (no wind tunnel was available at that time
and most definitely nothing that could take a photo at supersonic speeds.
His experiments required exact timing.
He was not able to attach the camera to the arm and utilize the
remote control (not existent at that time).
Mach's shadowgraph technique and a related method called 
{\em Schlieren Photography}  are still used today.

%\vspace{4mm}
Yet, Mach's contributions to supersonic flow were not limited to
experimental methods alone.
Mach understood the basic characteristics of external supersonic flow
where the most important variable affecting the flow
is the ratio of the speed of the flow\footnote{Mach dealt with only air,
but it is reasonable to assume that he understood
that this ratio was applied to other gases.} (U) relative to
the speed of sound (c).
Mach was the first to note the transition that occurs 
when the ratio U/c goes from being less than 1 to greater than 1.
The name Mach Number (M) was coined by J.~Ackeret (Prandtl's
student)  in 1932 in honor of Mach.

\subsubsection{John William Strutt (Lord Rayleigh)}
\index{aut}{Rayleigh}
\wImg{photos/rayleigh}
	{Rayleigh Portrait}
	{0.4}
	{intro:fig:rayleigh}
	{Lord Rayleigh portrait}
	{Lord Rayleigh portrait.}
	{16}%lines
	{-9.0 mm}
	{0.8}
\parindent 25 true  pt 
A researcher with a wide interest, started studies in compressible
flow mostly from a mathematical approach. 
At that time there wasn't  the realization that the flow could
be choked.
It seems that Rayleigh was the first who realized
that flow with chemical reactions (heat transfer) can be choked. 

%\vspace{4mm}
Lord Rayleigh was a British physicist born near Maldon, Essex, on November 12, 1842.
In 1861 he entered Trinity College at Cambridge, where he commenced reading mathematics.
His exceptional abilities soon enabled him to overtake his colleagues.
He graduated in the Mathematical Tripos in 1865 as Senior Wrangler and Smith's Prizeman.
In 1866 he obtained a fellowship at Trinity
which he held until 1871, the year of his marriage.
He served for six years as the president of the government committee
on explosives, and from 1896 to 1919 he acted as Scientific Adviser to Trinity House.
He was Lord Lieutenant of Essex from 1892 to 1901.
%\end{minipage}
%\smallskip
%\vspace{4mm}

Lord Rayleigh's first research was mainly mathematical, concerning
optics and vibrating systems, but his later work ranged over almost
the whole field of physics, covering sound, wave theory,
color vision, electrodynamics, electromagnetism, light scattering,
flow of liquids, hydrodynamics, density of gases, viscosity,
capillarity, elasticity, and photography.
Rayleigh's later work was concentrated on electric and magnetic
problems.
Rayleigh was considered to be an excellent instructor.
His Theory of Sound was published in two volumes during 1877-1878,
and his other extensive studies are reported in his scientific papers,
six volumes issued during 1889-1920.
Rayleigh was also a contributor to the Encyclopedia Britannica.
He published 446 papers which,
reprinted in his collected works, clearly show his capacity
for understanding everything just a little more deeply than anyone else.
He intervened in debates of the House of Lords only on
rare occasions, never allowing politics to interfere with science.
Lord Rayleigh, a Chancellor of Cambridge University, was a Justice
of the Peace and the recipient of honorary science and law degrees.
He was a Fellow of the Royal Society (1873) and served as Secretary
from 1885 to 1896, and as President from 1905 to 1908.
He received the Nobel Prize in 1904.
%Lord Rayleigh died on June 30, 1919, at Witham, Essex.

In 1871 he married Evelyn, sister of the future prime minister,
the Earl of Balfour (of the famous Balfour declaration of the\index{aut}{Balfour}
Jewish state). 
They had three sons, the eldest of whom was to become a
professor of physics at the Imperial College of
Science and Technology, London.

As a successor to James Clerk Maxwell, he was head of the Cavendish\index{aut}{Maxwell, James Clerk}
Laboratory at Cambridge from 1879-1884, and in 1887 became Professor
of Natural Philosophy at the Royal Institute of Great Britain.
Rayleigh died on June 30, 1919 at Witham, Essex.

\subsubsection{William John Macquorn Rankine}
\wImg{photos/rankine}
	{Rankine Photo}
	{0.45}
	{intro:fig:rankine}
	{Portrait of Rankine}
	{Portrait of Rankine.}
	{15}%lines
	{-9.0 mm}
	{0.8}
William John Macquorn Rankine (July 2, 1820 - December 24, 1872) was a
Scottish engineer and physicist.
He was a founding contributor to the science of thermodynamics
(Rankine Cycle).
Rankine developed a theory of the steam engine.
His steam engine manuals were used for many decades.

\parindent 25 true  pt 
Rankine was well rounded interested beside the energy field
he was also interested in civil engineering, strength of materials,
and naval engineering in which he was involved in applying
scientific principles to building ships.

Rankine was born in Edinburgh to British Army lieutenant David
Rankine and Barbara Grahame, Rankine.
As Prandtl due health reasons (only his own) Rankine 
initially had home schooling only later attended public eduction
for a short period of time such as High School of Glasgow (1830).
Later his family to Edinburgh and in 1834 he studied at a Military and Naval Academy. 
Rankine help his father who in the management and engineering 
of the Edinburgh and Dalkeith Railway. 
He never graduated from the University of Edinburgh (1838) and continue
to work for Irish railroad for which he developed a technique,
later known as Rankine's method, for laying out railway curves.
In 1849 he found the relationship between saturated vapor pressure and temperature.
Later he established relationships between the temperature, pressure and density of gases,
and expressions for the latent heat of evaporation of a liquid.

Rankine never married, and his only brother and parents died before him.
The history of Prandtl and Rankine suggest that home school (by a scientist) is much
better than the public school.

\subsubsection{Gino Girolamo Fanno}
\wImg{photos/fanno}
	{Fanno Photo}
	{0.45}
	{intro:fig:fanno}
	{The photo of Gino Fanno approximately in 1950}
	{The photo of Gino Fanno approximately in 1950.}
	{16}%lines
	{-05.0 mm}
	{0.8}
Fanno a Jewish Engineer was born on November 18, 1888.
He studied in a technical institute in Venice and graduated with
very high grades as a mechanical engineer.
Fanno was not as lucky as his brother, who was able to get
into academia. 
Faced with anti--semitism, Fanno left Italy for Zurich, Switzerland
in 1900 to attend graduate school for his master's degree.
In this new place he was able to pose as a Roman Catholic,
even though for short time he went to live in a Jewish home,
Isaak Baruch Weil's family. 
As were many Jews at that time, Fanno was fluent in several languages
including Italian, English, German, and French.
He likely had a good knowledge of Yiddish and possibly some Hebrew.
Consequently, he did not have a problem studying in a different
language.
In July 1904 he received his diploma (master).
When one of Professor Stodola's assistants
attended military service this temporary position was offered to
Fanno.
``Why didn't a talented guy like Fanno keep or
obtain a position in academia after he published his model?''
The answer is tied to the fact that somehow rumors about his roots
began to surface.
Additionally, the fact that his model was not a
``smashing\footnotemark{} success''  did not help.
\footnotetext{Missing data about friction factor}

\parindent 25 true  pt 
Later Fanno had to go back to Italy to find a job in industry. 
Fanno turned out to be a good engineer and he later obtained
a management position.
He married, and like his brother, Marco, was childless.
He obtained a Ph.D. from Regian Istituto Superiore d'Ingegneria di Genova.
However, on February 1939 Fanno was degraded (denounced)
and he lost his Ph.D. (is this the first case in history)
because of his Jewish nationality\footnote{In some places, the ridicules claims that Jews
persecuted only because their religion. Clearly, Fanno was not part of the
Jewish religion (see his picture) only his nationality was Jewish.}.
During the War (WWII), he had to be under house arrest to avoid being
sent to the ``vacation camps.'' 
To further camouflage himself, Fanno converted to Catholicism.
Apparently, Fanno had a cache of old Italian currency 
(which was apparently still highly acceptable) which
helped him and his wife survive the war.
After the war, Fanno was only able to work in agriculture and
agricultural engineering.
Fanno passed way in 1960 without world recognition for his model.
  
Fanno's older brother, mentioned earlier Marco Fanno is a famous
economist who later developed fundamentals of the supply and
demand theory. 

\subsubsection{Ludwig Prandtl}
\index{Sub}{Prandtl, Ludwig}
Perhaps Prandtl's greatest achievement was his ability to produce so
many great scientists. 
It is mind boggling to look at the long list of those who were
his students and colleagues.
There is no one who educated as many great scientists as Prandtl.
Prandtl changed the field of fluid mechanics and is called the modern
father of fluid mechanics because of his introduction of
boundary layer, turbulence mixing theories etc.

\wImg{photos/prandtl}
	{Prandtl Photo}
	{0.5}
	{intro:fig:prandtl}
	{Photo of Prandtl}
	{Photo of Prandtl.}
	{16}%lines
	{-7.0 mm}
	{0.8}
Ludwig Prandtl was born in Freising, Bavaria, in 1874.
His father was a professor of engineering and
his mother suffered from a lengthy illness.
As a result, the young Ludwig spent more time with his father
which made him interested in his father's physics and machinery books.
This upbringing fostered the young Prandtl's interest in science and
experimentation.

Prandtl started his studies at the age of 20 in Munich, Germany and
he graduated at the age of 26 with a Ph.D.
Interestingly, his Ph.D. was focused on solid mechanics.
His interest changed when, in his first job, he was required to design 
factory equipment that involved problems related to the field of fluid
mechanics (a suction device).
Later he sought and found a job as a professor of mechanics at
a technical school in Hannover, Germany (1901).
During this time Prandtl developed his boundary layer theory and studied
supersonic fluid flows through nozzles.
In 1904, he presented the revolutionary paper ``Flussigkeitsbewegung
Bei Sehr Kleiner Reibung'' (Fluid Flow in Very Little Friction),
the paper which describes his boundary layer theory.

His 1904 paper raised Prandtl's prestige.
He became the director of the Institute for Technical Physics at the
University of G\"{o}ttingen.
He developed the Prandtl-Glauert rule for subsonic airflow.
Prandtl, with his student Theodor Meyer, developed the first
theory for calculating the properties of shock and expansion waves in
supersonic flow in 1908 (two chapters in this book).
As a byproduct they produced the theory for {\em oblique shock}.
In 1925 Prandtl became the director of the Kaiser Wilhelm Institute
for Flow Investigation at G\"{o}ttingen.
By the 1930s, he was known worldwide as the leader in the science
of fluid dynamics.
Prandtl also contributed to research in many areas, such as
meteorology and structural mechanics.

Ludwig Prandtl worked at G\"{o}ttingen until his death on August 15, 1953.
His work and achievements in fluid dynamics resulted in equations that
simplified understanding, and many are still used today.
Therefore many referred to him as the father of modern
fluid mechanics.
Ludwig Prandtl died in G\"{o}ttingen, Germany on August 15th 1953.

Prandtl's other contributions include: the introduction of the Prandtl
number in fluid mechanics, airfoils and wing theory (including theories of
aerodynamic interference, wing-fuselage, wing-propeller, biplane, etc);
fundamental studies in the wind tunnel, high speed flow
(correction formula for subsonic compressible flows), theory of
turbulence. His name is linked to the following:

\begin{itemize}
\item
Prandtl number (heat transfer problems)
\item
Prandtl-Glauert compressibility correction
\item
Prandtl's boundary layer equation
\item
Prandtl's lifting line theory
\item
Prandtl's law of friction for smooth pipes
\item
Prandtl-Meyer expansion fans (supersonic flow)
\item
Prandtl's Mixing Length Concept (theory of turbulence) 
\end{itemize}

\subsubsection{Theodor Meyer}
\index{sub}{Meyer, Theodor}
\wImg{photos/meyer}
	{Meyer Photo}
	{0.5}
	{intro:fig:meyer}
	{Thedor Meyer photo} 
	{Thedor Meyer's photo.}
	{18}%lines
	{-9.0 mm}
	{0.80}
Meyer\footnote{This author is grateful to Dr. Settles and colleagues who wrote a very
informative article about Meyer as a 100 years anniversary to his thesis.
The material in this section was taken from 
Settles, G. S.,et al. ``Theodor Meyer--Lost pioneer of gas dynamics''
Prog.~Aero space Sci(2009), doi:10.1016 j. paerosci.2009.06.001.
More information can be found in that article.} 
was Prandtl's student who in one dissertation was able to build large part of
base of the modern compressible flow.
The two chapters in this book, Prandtl--Meyer flow and oblique shock are directly based on
his ideas.
Settles et al in their paper argues that this thesis is the most influential dissertation
in the entire field of fluid mechanics.
No matter if you accept this opinion, it must be the most fundamental thesis or work
in the field of compressible flow (about 20.08\% page wise of this book.).

One of the questions that one can ask, what is
about Meyer's education that brought this success.
In his family, he was described as math genius who astonished his surroundings. 
What is so striking is the list of his instructors who include
Frobenius (Group theory), Helmert (theory of errors), Hettner
(chorology), Knoblauch , Lehmann-Filhes (orbit of double star),
Edmund Landau (number theory), F. Schottkyand (elliptic, abelian, and theta functions and
invented Schottky groups),  mathematicians Caratheodory (calculus of variations,
and measure theory), Herglotz (seismology),
Hilbert, Klein, Lexis, Runge (Runge--Kutta method)
and Zermelo (axiomatic set theory), Abraham (electron), Minkowski (mathematical base
for theory of relativity), Prandtl, and more.
This list demonstrates that Meyer had the best
education one can have at the turn of century.
It also suggest that moving between good universities (3 universities)
is a good way to absorb knowledge and good research skills.
This kind of education provided Meyer with the
tools to tackle the tough job of compressible flow.

\wImg{photos/meyerDiagram}
	{Diagrams  From Meyer Thesis}
	{0.5}
	{intro:fig:meyerDiagram}
	{The diagrams taken from Meyer thesis.}
 	{The diagram is taken from Meyer's dissertation showing the
		schematic of oblique shock and the schematic of Prandtl--Meyer fan.}
	{18}%lines
	{-7.0 mm}
	{0.7}
What is interesting about his work is that Mach number concept was not clear at that stage.
Thus, the calculations (many hand numerical calculations) were complicated by this fact which 
further can magnify his achievement.
Even though the calculations where carried out in a narrow range.
Meyer's thesis is only 46 pages long but it include experimental evidence to prove his theory
in Prandtl--Meyer function and oblique shock.
According to Settles, this work utilized Schlieren images getting quantitative measurements
probably for the first time.
Meyer also was the first one to look at the ratio of the static properties to 
the stagnation proprieties\footnote{This issue is considered still open by this author.
It is not clear who use first and coin the term stagnation properties.}. 

Ackeret attributed the oblique shock theory to Meyer but later this attribution
was dropped and very few books attribute this Meyer ( or even Prandtl).
Among the very few who got this right is this book.
The name Prandtl--Meyer is used because some believe that Prandtl conceived the concept and let 
Meyer to do the actual work.
This contribution is to the mythical Prandtl ability to ``solve'' equations without
doing the math. 
However, it is not clear that Prandtl indeed conceived or dealt with this issues besides 
reviewing Meyer ideas. 
What it is clear that the rigor mathematics is of Meyers and physical intuition
of Prandtl were present.
There is also a question of who came out with the ``method of characteristics,'' Prandtl or Meyer. 

Meyer was the first individual to use the shock polar diagram. 
Due to his diagram, he was able to see the existence of the weak and strong shock.
Only in 1950, Thomson was able to see the third shock.
It is not clear why Meyer missed the third root. 
Perhaps, it was Prandtl influence because he saw only two solutions in the experimental setting
thus suggesting that only two solutions exists.
This suggestion perhaps provides an additional indication that Prandtl was involved heavily 
in Meyer's thesis. 
Meyer also noticed that the deflection angle has a maximum. 

Meyer was born to Theodor Meyer (the same name as his father)
in  July $1^{\underline{st}}$, 1882, and die March $8^{\underline{th}}$, 1972. 
Like Fanno, Meyer never was recognized for his contributions to fluid mechanics.
During the years after Second World War, he was aware that his thesis became a standard
material in every university in world. 
However, he never told his great achievements to
his neighbors or his family or colleagues in the high school where he  was teaching. 
One can only wonder why this field rejects very talented people.
Meyer used the symbol $v$ which is still used to this today for the function. 

\subsubsection{E.R.G.~Eckert}
\wImg{photos/eckertW}
	{Eckert with Bar-Meir Family}
	{0.5}
	{intro:fig:eckert}
	{The photo of Ernst Rudolf George Eckert with Bar-Meir's family}
	{The photo of Ernst Rudolf George Eckert with Bar-Meir's family Aug 1997.}
	{12}%lines
	{-7.0 mm}
	{0.85}
Eckert was born in September 13, 1904 in Prague, where he studied at the German
Institute of Technology.
He received his engineer deploma in 1927, and defend his (engineering sciences) Ph.D.~ in 1931.
He work mostly on radtion for a while in the same pace where he studied.  
He went to work with Schmidt in Danzig known for experimatal experts in exsact messurement.
That was the time that he develop the understading dimensional analysis in the heat transfer in particular and fluid mechanics
in general education must be taught. 
He was critized for using and teaching dimensional analysis.
During World War II, he developed methods for jet engine turbine blade cooling 
so they wouldn't burn up in space.
He emigrated to the United States after the war, and served as a consultant to the U.S. Air Force and
the National Advisory Committee for Aeronautics before coming to Minnesota.

Eckert developed the understanding of heat dissipation
in relation to kinetic energy, especially in compressible flow.
Hence, the dimensionless group has been designated as the Eckert
number, which is associated with the Mach number.
Schlichting suggested this dimensionless group in honor of Eckert. 
In addition to being named to the National Academy of Engineering
in 1970, he authored more than 500 articles and received several medals
for his contributions to science.
His book "Introduction to the Transfer of Heat
and Mass," published in 1937,
is still considered a fundamental text in the field.

Eckert was an excellent mentor to many researchers (including this
author), and he had a reputation for being warm and kindly.
He was also a leading figure in bringing together engineering
in the East and West during the Cold War years. 


\subsubsection{Ascher Shapiro}
\index{sub}{Shapiro, Ascher}

MIT Professor Ascher Shapiro\footnote{Parts taken from Sasha Brown, MIT}, 
the Eckert equivalent for the compressible flow,
was instrumental in using his two volume book
``The Dynamics of Thermodynamics of the Compressible Fluid Flow,'' 
to transform the gas dynamics field to a coherent text material
for engineers.
Furthermore, Shapiro's knowledge of fluid mechanics enabled him to ``sew'' the missing
parts of the Fanno line with Moody's diagram to create the most useful model
in compressible flow.
While Shapiro viewed gas dynamics mostly through aeronautic eyes, 
the undersigned believes that Shapiro was the first one to propose
an isothermal flow model that is not part of the aeronautic field.
Therefore it is being proposed to call this model Shapiro's Flow.
   
In his first 25 years Shapiro focused primarily on power production,
high-speed flight, turbomachinery and propulsion by jet engines and rockets.
Unfortunately for the field of Gas Dynamics, 
Shapiro moved to the field of biomedical engineering  where
he was able to pioneer new work.
Shapiro was instrumental in the
treatment of blood clots, asthma, emphysema and glaucoma.

Shapiro grew up in New York City and received his S.B. in 1938 and
the Sc.D. (It is M.I.T.'s equivalent of a Ph.D. degree) in 1946 in
mechanical engineering from MIT.
He was assistant professor in 1943, three years before receiving his
Sc.D.
%Instructor in 1940[.] 
In 1965 he became the Department of Mechanical Engineering head until 1974.
Shapiro spent most of his active years at MIT.
Ascher Shapiro passed way in November 2004.

\subsubsection{Von Karman, Theodor}
\index{aut}{Von Karman} 

A brilliant scientist who was instrumental in constructing many of the equations and building the 
American aviation and space exploration.  
Von Karman studied fluid mechanics under Prandtl and during that time he saw another graduate student
that was attempting to build ``stable'' experiment what will not have the vortexes.
Von Karman recognized that this situation is inherently unstable and explained the scientific
reasons for this phenomenon.
Now this phenomenon known as Von Karman street. 
Among his achievement that every student study fluid mechanics is the development of
the integral equation of boundary layer. 
\index{sub}{Von Karman integral equation}
Von Karman, a descendant of a famous Rabi Rabbi Judah Loew ben Bezalel (HaMaharl) 
was born raised in Hungary. 
Later he move to Germany to study under Prandtl. 
After his graduation he taught at Gottingen and later as director of the Aeronautical Institute
at RWTH Aachen.
As a Jew realized that he has to leave Germany during the raise of the Nazi.
At 1930 he received offer and accept the directorship of a Laboratory at the California Institute
of Technology.

His achievement in the area of compressible flow area focused around supersonic and rocketry.
For example, he formulate the slender body equations to describe the fluid field around rockets.
Any modern air plane exhibits the swept--back wings
the Von Karman was instrumental in recognizing its importance.
He construct with his student the Von Karman--Tsien compressibility correction.
The Karman--Tsien compressibility correction is a nonlinear approximation for Mach number effects
which works quite well when the velocities are subsonic.
This expression relates the incompressible values to those in compressible flow.
As his adviser, he left many students which have continued his legacy like Tsien who build the
Chinese missile industry.

\subsubsection{Zeldovich, Yakov Borisovich}
\index{Aut}{Zeldovich, Yakov Borisovich}
``Before I meet you here I had thought, that you are a collective of authors, as Burbaki''\\
Stephen W. Hawking.

The statement of Hawking perhaps can illustrate a prolific physicist born in Minsk.
He played an important role in the development of Soviet nuclear and thermonuclear weapons.
His main contribution in the area compressible flow centered around the shock wave and detonation
and material related to cosmotology.
Zeldovich develop several reatlition for the limiting cases still in use today. 
For example he developed the ZND detonation model (where the leter ``Z'' stands for Zeldovich).
