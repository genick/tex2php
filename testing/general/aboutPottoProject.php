	<div class="generalNotice"> 
		<!-- <hr align='center' noshade='noshade' size='4' width='50%'> -->
		<h4>About Potto Project </h4> 
		<p> 
			Potto Project has been created by Dr.  <a href='/genick.php'>Genick Bar-Meir</a>
			and friends to build open source software and open content
			textbooks for college students. Over <strong>1,000,000</strong> (million)
			Potto Project books were downloaded from 175 different countries since 2005.
		</p>
		<p class='indent notice background'>
			Potto Project is under <a href='copyRight.php'> open content</a> 
			licenses, which means that you will always have the freedom
			to use it, make copies of it, and improve it.
			You are encouraged to make use of these freedoms and
			share the textbooks and program with your family and friends!
		</p>
	</div >
