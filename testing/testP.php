<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script>
$(document).ready(function(){
	$("p").click(function(){
		alert("The paragraph was clicked.");
	});
});
$(function() {
	    
	   $("#sortable1 li").not('.emptyMessage').click(function() {
	        alert('Clicked list. ' + this.id);
	   });
});
</script>
</head>
<body>

<p>Click on this paragraph.</p>
<ul id="sortable1" class="connectedSortable ui-sortable">
<li class="ui-state-default" id="1">Name1</li>
<li class="ui-state-default" id="2">Name2</li>
<li class="ui-state-default" id="3">Name3</li>
<li class="ui-state-default" id="4">Name4</li>
<li class="ui-state-default" id="5">Name5</li>
<li style="display: list-item;" class="emptyMessage">No more contacts available</li></ul>

</body>
</html>

