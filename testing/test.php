 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title></title>
 </head>
 <body>
<script >
$(function() {
	$("#sortable1 li").not('.emptyMessage').click(function() {
		alert('Clicked list. ' + this.id);
	});
});
</script>
<script type="text/javascript"
   src="https://example.com/mathjax/MathJax.js?config=TeX-AMS-MML_CHTML">
</script>

<!-- <script type="text/x&#45;mathjax&#45;config"> -->
<!-- MathJax.Hub.Register.StartupHook("TeX Jax Ready",function () { -->
<!--   MathJax.Hub.Insert(MathJax.InputJax.TeX.Definitions.macros,{ -->
<!--     cancel: ["Extension","cancel"], -->
<!--     bcancel: ["Extension","cancel"], -->
<!--     xcancel: ["Extension","cancel"], -->
<!--     cancelto: ["Extension","cancel"] -->
<!--   }); -->
<!-- }); -->
<!-- </script> -->
 	
<ul id="sortable1" class="connectedSortable ui-sortable">
	<li class="ui-state-default" id="1">Name1</li>
	<li class="ui-state-default" id="2">Name2</li>
	<li class="ui-state-default" id="3">Name3</li>
	<li class="ui-state-default" id="4">Name4</li>
	<li class="ui-state-default" id="5">Name5</li>
	<li style="display: list-item;" class="emptyMessage">No more contacts available</li>
</ul>
 </body>
 </html>
