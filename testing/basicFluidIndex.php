<!DOCTYPE html>
<html lang="en">
	<head>
		<?php
			require ("php/functions.php" );
			loadINI("GAS TEST", "isontropic flow, shock, oblique",
				"fundamental of Compressible Flow", 'no'
			);
			loadCSS();
			loadJS();
			loadMathJax();
			// loadStyleFiles()
			?>
	</head>
	<body>
		<header>
			<?php
			// 	// do php stuff
			// 	require('navigation.php');
			?>
		</header>
		<main role="main">
			<div class="row">
				<div class="col-sm-10 offset-md-1">
					<div class="container-fluid">
						<?php
							require('basicFluid.php');
						?>
					</div>
				</div>
			</div>
		</main>
		<footer>
			<?php
				// require('footerContent.php');
			?>
		</footer>
		<?php
			// loadBootstrapJS();
		?>
		<!-- <script type="text/javascript" src="js/jquery.js"> </script> -->
		<!-- <script type="text/javascript" src="js/bootscrip.js"> </script> -->
		<!-- <script type="text/javascript" src="js/local.js"> </script> -->
	</body>
</html>
