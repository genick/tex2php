
<h1 class="chap" id="csec2">Chapter 2 Review of Thermodynamics</h1>


In this chapter, a review of several definitions of 
common thermodynamics terms is presented.
This introduction is provided to bring the student back to 
current place with the material. 

<h2 class="section" id="sec2.1"> &sect; 2.1 Basic Definitions</h2>

The following basic definitions are common to thermodynamics
and will be used in this book. 

<h4 class="issue" id="thermo:issue:iss0">Work</h4>


In mechanics, the work was defined as 
<div class="longEq">
\begin{align}
	\hbox{mechanical work} = \int \mathbf{F} \bullet \mathbf{d\ell}  
	= \int P dV
	\label{thermo:eq:workM}
\end{align}
</div>
<div class="nomen">
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn0">$\ell$</a> :
		</span>
		<span class="depiction">
				Units length.
		</span>
	</div> <!-- nomenclature row -->
</div> <!-- nomenclature end --> 


This definition can be expanded to include two issues.
The first issue that must be addressed, that work done on
the surroundings by the system boundaries similarly is
positive. 
Two, there is a transfer of energy so that its effect 
can cause work.
It must be noted that electrical current is a work while
heat transfer isn't.

<h4 class="issue" id="thermo:issue:iss1">System</h4>

This term will be used in this book and it is defined as a
continuous (at least partially) fixed quantity of matter.
The dimensions of this material can be changed.
In this definition, it is assumed that the system speed is
significantly lower than that of the speed of light.
So, the mass can be assumed constant even though the true
conservation law applied to the combination of mass energy
(see Einstein's law).
In fact for almost all engineering purpose this law is reduced to
two separate laws of mass conservation and energy conservation.

Our system can receive energy, work, etc as long the mass remain
constant the definition is not broken.


<span class="bf">Thermodynamics First Law</span>

This law refers to conservation of energy in a non accelerating system. 
Since all the systems can be calculated in a non accelerating
systems, the conservation is applied to all systems.
The statement describing the law is the following.
\begin{align}
	Q_{12} - W_{12} = E_2 - E_1
	\label{thermo:eq:firstL}
\end{align}
<div class="nomen">
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn1">$Q_{12}$</a> :
		</span>
		<span class="depiction">
				The energy transferred to the system between state 1 and state 2
		</span>
	</div> <!-- nomenclature row -->
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn2">$W_{12}$</a> :
		</span>
		<span class="depiction">
				The work done by the system between state 1 and state 2
		</span>
	</div> <!-- nomenclature row -->
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn3">$E_{i}$</a> :
		</span>
		<span class="depiction">
				System energy at state i
		</span>
	</div> <!-- nomenclature row -->
</div> <!-- nomenclature end --> 

The system energy is a state property.
From the first law it directly implies that for process without
heat transfer (adiabatic process) the following is true
\begin{align}
	 W_{12} = E_1 - E_2
	\label{thermo:eq:adiabatic1}
\end{align} 
Interesting results of equation \eqref{thermo:eq:adiabatic1}
is that the way the work is done and/or intermediate states are 
irrelevant to final results.
There are several definitions/separations of the kind of works
and they include kinetic energy, potential energy (gravity), 
chemical potential, and electrical energy, etc.   
The internal energy is the energy that depends on the other 
is that the way the work is done and/or intermediate states are 
irrelevant to final results.
There are several definitions/separations of the kind of works
and they include kinetic energy, potential energy (gravity), 
chemical potential, and electrical energy, etc.   
The internal energy is the energy that depends on the other 
properties of the system.
For example for pure/homogeneous and simple gases 
it depends on two properties like temperature and pressure. 
The internal energy is denoted in this book as
$E_U$ and it will be treated as a state property.
<div class="nomen">
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn4">$E_U$</a> :
		</span>
		<span class="depiction">
				Internal energy
		</span>
	</div> <!-- nomenclature row -->

</div> <!-- nomenclature end --> 


The potential energy of the system is depended on the body force.
A common body force is the gravity.
For such body force, the potential energy is $m\,g\,z$ 
where $g$ is the gravity force (acceleration), $m$
is the mass and the $z$ is the vertical height from 
a datum.
The kinetic energy is
<div class="longEq">
\begin{align}
	K.E. =  \frac{m U^2}{2}
	\label{thermo:eq:ke}
\end{align}
</div>
<div class="nomen">
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn5">$U$</a> :
		</span>
		<span class="depiction">
				velocity
		</span>
	</div> <!-- nomenclature row -->
</div> <!-- nomenclature end --> 


Thus, the energy equation can be written as 
<div class="empheq">
	<h3 class="eqHead" >System Energy Conservation</h3>
	\begin{align}
		\label{thermo:eq:largeEnergy}
		\frac{m\,{U_1}^2}{2} + m\,g\,z_1 + {E_U}_1 + Q =
		\frac{m\,{U_2}^2}{2} + m\,g\,z_2 + {E_U}_2 + W 
	\end{align}
</div> <!-- end empheq --> 



For the unit mass of the system equation
\eqref{thermo:eq:largeEnergy} is transformed into
<div class="empheq">
	<h3 class="eqHead" >System Energy Conservation per Unit</h3>
	\begin{align}
		\label{thermo:eq:smallEnergy}
		\frac{{U_1}^2}{2} + g\,z_1 + {E_u}_1 + q =
		\frac{{U_2}^2}{2} + g\,z_2 + {E_u}_2 + w 
	\end{align}
</div> <!-- end empheq --> 


<div class="nomen">
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn6">$q$</a> :
		</span>
		<span class="depiction">
				Energy per unit mass
		</span>
	</div> <!-- nomenclature row -->

	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn7">$w$</a> :
		</span>
		<span class="depiction">
				Work per unit mass
		</span>
	</div> <!-- nomenclature row -->

	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn8">$E_u$</a> :
		</span>
		<span class="depiction">
				Internal Energy per unit mass
		</span>
	</div> <!-- nomenclature row -->

</div> <!-- nomenclature end --> 

where $q$ is the energy per unit mass and $w$ is the work per
unit mass.
The &ldquo;new&rdquo; internal energy, $E_u$, is the internal energy per
unit mass.

Since the above equations are true between arbitrary
points, choosing any point in time will make it correct.
Thus  differentiating the  energy equation with respect to
time yields the rate of change energy equation.
The rate of change of the energy transfer is
<div class="longEq">
\begin{align}
	\frac{DQ}{Dt} = \dot{Q}
	\label{thermo:eq:dotQ}
\end{align}
</div>
In the same manner, the work change rate transferred
through the boundaries of the system is 
<div class="longEq">
\begin{align}
	\frac{DW}{Dt} = \dot{W}
	\label{thermo:eq:dotW}
\end{align}
</div>
Since the system is with a fixed mass, the rate energy equation is
<div class="longEq">
\begin{align}
	\dot{Q} - \dot{W} = \frac{D\,E_U} {Dt} +
	m U \frac{DU} {Dt} + m \frac{D\,B_f\,z} {Dt}
	\label{thermo:eq:energyRate}
\end{align}
</div>
<div class="nomen">
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn9">$B_f$</a> :
		</span>
		<span class="depiction">
				Body force
		</span>
	</div> <!-- nomenclature row -->
</div> <!-- nomenclature end --> 

For the case were the body force, $B_f$, is constant with time like in the
case of gravity equation \eqref{thermo:eq:energyRate} reduced to 
<div class="empheq">
	<h3 class="eqHead" >System Energy Conservation per Time</h3>
	\begin{align}
		\label{thermo:eq:energyRateg}
		\dot{Q} - \dot{W} = \frac{D\,E_U}{Dt} +
		m\, U \dfrac{DU} {Dt} + m\,g \dfrac{D\,z} {Dt}
	\end{align}
</div> <!-- end empheq --> 

 
The time derivative operator, $D/Dt$ is used instead of the common
notation because it referred to system property derivative. 

<h4 class="issue" id="thermo:issue:iss2">Thermodynamics Second Law</h4>
 

There are several definitions of the second law.
No matter  which definition is used to describe the second law 
it will end in a mathematical form.
The most common mathematical form is Clausius inequality which 
state that 
<div class="longEq">
\begin{align}
	\oint \frac {\delta Q} { T} \ge  0
	\label{thermo:eq:clausius}
\end{align}
</div>
The integration symbol with the circle represent integral of
cycle (therefor circle) in with system return to the same
condition.
If there is no lost, it is referred as a reversible process
and the inequality change to equality.
<div class="longEq">
\begin{align}
	\oint \frac {\delta Q} { T} =   0
	\label{thermo:eq:clausiusE}
\end{align}
</div>
The last integral can go though several states.
These states are independent of the path the system goes through.
Hence, the integral is independent of the path.
This observation leads to the definition of entropy and designated as
$S$ and the derivative of entropy is 
<div class="longEq">
\begin{align}
	ds \equiv \left( \frac{ \delta Q}{T} \right)_{\hbox{rev}} 
	\label{thermo:eq:engropy}
\end{align}
</div>
<div class="nomen">
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn10">$S$</a> :
		</span>
		<span class="depiction">
				Entropy of the system
		</span>
	</div> <!-- nomenclature row -->

</div> <!-- nomenclature end --> 

Performing integration between two states results in
<div class="longEq">
\begin{align}
	S_2 -S_1 = \int^2_1 \left( \dfrac{ \delta Q}{T}
		\right)_{\hbox{rev}} = 
		\int^2_1 dS
	\label{thremo:eq:deltaS}
\end{align}
</div>

One of the conclusions that can be drawn from this analysis is 
for reversible and adiabatic process $dS=0$.
Thus, the process in which it is reversible and adiabatic, the entropy
remains constant and referred to as 
It can be noted that there is a possibility that a process can be 
irreversible and the right amount of heat transfer to have zero
change entropy change.
Thus, the reverse conclusion that zero change of entropy leads to
reversible process, isn't correct.

For reversible process equation \eqref{thermo:eq:clausiusE}
can be written as 
<div class="longEq">
\begin{align}
	\delta Q = T\, dS
	\label{thermo:eq:dQ}
\end{align}
</div> 
and the work that the system is doing on the surroundings is
<div class="longEq">
\begin{align}
	\delta W = P\,dV
	\label{thermo:eq:dW}
\end{align}
</div>
Substituting equations \eqref{thermo:eq:dQ} \eqref{thermo:eq:dW}
into \eqref{thermo:eq:energyRateg} results in
<div class="longEq">
\begin{align}
	TdS = d\,E_U + P\, dV
	\label{thermo:eq:Tds}
\end{align}
</div>

Even though the derivation of the above equations were done
assuming that there is no change of kinetic or potential energy,
it still remain valid for all situations.
Furthermore, it can be shown that it is valid for reversible and
irreversible processes.

<h4 class="issue" id="thermo:issue:iss3">Enthalpy</h4>


It is a common practice to define a new property, which is the combination
of already defined properties, the enthalpy of the system.
<div class="longEq">
\begin{align}
	H = E_U + P\,V
	\label{thermo:eq:enthalpy}
\end{align}
</div>
<div class="nomen">
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn11">$H$</a> :
		</span>
		<span class="depiction">
				Enthalpy
		</span>
	</div> <!-- nomenclature row -->
</div> <!-- nomenclature end --> 

The specific enthalpy is enthalpy per unit mass and denoted
as, $h$.
<div class="nomen">
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn12">$h$</a> :
		</span>
		<span class="depiction">
				Specific enthalpy
		</span>
	</div> <!-- nomenclature row -->
</div> <!-- nomenclature end --> 


Or in a differential form as
<div class="longEq">
\begin{align}
	dH = dE_U + dP\,V + P\,dV
	\label{thermo:eq:dEnthalpy}
\end{align}
</div>

Combining equations \eqref{thermo:eq:enthalpy} and
\eqref{thermo:eq:Tds}  yields
<div class="empheq">
	<h3 class="eqHead" >Fundamental Entropy</h3>
	\begin{align}
		\label{thermo:eq:TdSH}
		T\,dS = dH -V\,dP  
	\end{align}
</div> <!-- end empheq --> 

For isentropic process, equation \eqref{thermo:eq:Tds} is reduced
to $dH = VdP$.
The equation  \eqref{thermo:eq:Tds} in mass unit is 
<div class="longEq">
\begin{align}
	T\,ds = du + P\,dv = dh - \frac{dP}{\rho}
	\label{thermo:eq:Tdsh}
\end{align}
</div>
when the density enters through the relationship of $\rho = 1/v$.

<h4 class="issue" id="thermo:issue:iss4">Specific Heats</h4>


The change of internal energy and enthalpy requires new definitions.
The first change of the internal energy 
and it is defined as the following
<div class="empheq">
	<h3 class="eqHead" >Volume Specific Heat</h3>
	\begin{align}
		\label{thermo:eq:cv}
		C_v  \equiv \left( \frac {\partial E_u }{\partial T} \right)  
	\end{align}
</div> <!-- end empheq --> 

<div class="nomen">
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn13">$C_v$</a> :
		</span>
		<span class="depiction">
				Specific volume heat
		</span>
	</div> <!-- nomenclature row -->
</div> <!-- nomenclature end --> 

And since the change of the enthalpy involve some
kind of work is defined as 
 
<div class="empheq">
	<h3 class="eqHead" >Pressure Specific Heat</h3>
	\begin{align}
		\label{thermo:eq:cp}
		C_p  \equiv \left( \frac {\partial h }{\partial T} \right)  
	\end{align}
</div> <!-- end empheq --> 

<div class="nomen">
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn14">$C_p$</a> :
		</span>
		<span class="depiction">
				Specific pressure heat
		</span>
	</div> <!-- nomenclature row -->
</div> <!-- nomenclature end --> 


The ratio between the specific pressure heat and the specific 
volume heat is called the ratio of the specific heat and 
it is denoted as, $k$.
<div class="empheq">
	<h3 class="eqHead" >Specific Heat Ratio</h3>
	\begin{align}
		\label{thermo:eq:k}
		k \equiv \dfrac {C_p}{C_v}
	\end{align}
</div> <!-- end empheq --> 

<div class="nomen">
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn15">$k$</a> :
		</span>
		<span class="depiction">
				the ratio of the specific heats
		</span>
	</div> <!-- nomenclature row -->
</div> <!-- nomenclature end --> 


For solid, the ratio of the specific heats is almost 1 and
therefore the difference between them is almost zero. 
Commonly the difference for solid is ignored and both are assumed
to be the same and therefore referred as $C$.
This approximation less strong for liquid but not by that much 
and in most cases it applied to the calculations.
The ratio the specific heat of gases is larger than one.

<h4 class="issue" id="thermo:issue:iss5">Equation of State</h4>


Equation of state is a relation between state variables.
Normally the relationship of temperature, pressure, and specific
volume define the equation of state for gases.
The simplest equation of state referred to as ideal gas
and it is defined as 
<div class="longEq">
\begin{align}
	P = \rho\, R\, T
	\label{thermo:eq:idealGas}
\end{align}
</div>
Application of Avogadro's law, that "all gases at the same pressures 
and temperatures have the same number of molecules per unit of 
volume," allows the calculation of a &ldquo;universal gas constant.&rdquo;
This constant to match the standard units results in
<div class="longEq">
\begin{align}
	\bar{R} = 8.3145 \frac{kj} {kmol\; K }
	\label{thermo:eq:Rbar}
\end{align}
</div>
<div class="nomen">
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn16">$\bar{R}$</a> :
		</span>
		<span class="depiction">
				Universal gas constant
		</span>
	</div> <!-- nomenclature row -->
</div> <!-- nomenclature end --> 


Thus, the specific gas can be calculate as
<div class="longEq">
\begin{align}
	R = \frac{\bar{R}} {M} 
	\label{thermo:eq:R}
\end{align}
</div>
<div class="nomen">
	<div class="NMNrow">
		<span class="symbol"><a name="thermo:nmn:nmn17">$R$</a> :
		</span>
		<span class="depiction">
				Specific gas constant
		</span>
	</div> <!-- nomenclature row -->
</div> <!-- nomenclature end --> 

The specific constants for selected gases at 300K are provided in
Table  <a href="thermo:tab:idealGases">(2.1)</a>.


<div class = "space"> </div>
 
	<!-- <div class="dataTable col&#45;sm&#45;10 offset&#45;md&#45;1"> -->
	<div class="dataTable">
	<table id="thermo:tab:idealGases" class="data">
	<caption>
			Table 2.1: Properties of Various Ideal Gases	
 </caption>
	<tr>
		<th class="head lhead">Gas<br></th>
		<th class="head">Chemical<br>Formula<br></th>
		<th class="head">Molecular<br>Weight<br></th>
		<th class="head">$R$ $\frac{kJ}{kg\, K}$</th>
		<th class="head">$C_v$ $\frac{kJ}{kg\,K}$<br></th>
		<th class="head">$C_p$ $\frac{kJ}{kg\,K}$<br><br></th>
		<th class="head">$k$</th>
	</tr>
	<tr>
		<td class="lhead">Air</td>
		<td class="data">-</td>
		<td class="data">28.970</td>
		<td class="data">0.28700</td>
		<td class="data">1.0035</td>
		<td class="data">0.7165</td>
		<td class="data">1.400</td>
	</tr>
	<tr>
		<td class="lhead">Argon</td>
		<td class="data">Ar</td>
		<td class="data">39.948</td>
		<td class="data">0.20813</td>
		<td class="data">0.5203</td>
		<td class="data">0.3122</td>
		<td class="data">1.667</td>
	</tr>
	<tr>
		<td class="lhead">Butane</td>
		<td class="data">$C_4H_{10}$</td>
		<td class="data">58.124</td>
		<td class="data">0.14304</td>
		<td class="data">1.7164</td>
		<td class="data">1.5734</td>
		<td class="data">1.091</td>
	</tr>
	<tr>
		<td class="lhead">Carbon Dioxide</td>
		<td class="data">$CO_2$</td>
		<td class="data">44.01</td>
		<td class="data">0.18892</td>
		<td class="data">0.8418</td>
		<td class="data">0.6529</td>
		<td class="data">1.289</td>
	</tr>
	<tr>
		<td class="lhead">Carbon Monoxide</td>
		<td class="data">$CO$</td>
		<td class="data">28.01</td>
		<td class="data">0.29683</td>
		<td class="data">1.0413</td>
		<td class="data">0.7445</td>
		<td class="data">1.400</td>
	</tr>
	<tr>
		<td class="lhead">Ethane</td>
		<td class="data">$C_2H_6$</td>
		<td class="data">30.07</td>
		<td class="data">0.27650</td>
		<td class="data">1.7662</td>
		<td class="data">1.4897</td>
		<td class="data">1.186</td>
	</tr>
	<tr>
		<td class="lhead">Ethylene</td>
		<td class="data">$C_2H_4$</td>
		<td class="data">28.054</td>
		<td class="data">0.29637</td>
		<td class="data">1.5482</td>
		<td class="data">1.2518</td>
		<td class="data">1.237</td>
	</tr>
	<tr>
		<td class="lhead">Helium</td>
		<td class="data">$He$</td>
		<td class="data">4.003</td>
		<td class="data">2.07703</td>
		<td class="data">5.1926</td>
		<td class="data">3.1156</td>
		<td class="data">1.667</td>
	</tr>
	<tr>
		<td class="lhead">Hydrogen</td>
		<td class="data">$H_2$</td>
		<td class="data">2.016</td>
		<td class="data">4.12418</td>
		<td class="data">14.2091</td>
		<td class="data">10.0849</td>
		<td class="data">1.409</td>
	</tr>
	<tr>
		<td class="lhead">Methane</td>
		<td class="data">$CH_4$</td>
		<td class="data">16.04</td>
		<td class="data">0.51835</td>
		<td class="data">2.2537</td>
		<td class="data">1.7354</td>
		<td class="data">1.299</td>
	</tr>
	<tr>
		<td class="lhead">Neon</td>
		<td class="data">$Ne$</td>
		<td class="data">20.183</td>
		<td class="data">0.41195</td>
		<td class="data">1.0299</td>
		<td class="data">0.6179</td>
		<td class="data">1.667</td>
	</tr>
	<tr>
		<td class="lhead">Nitrogen</td>
		<td class="data">$N_2$</td>
		<td class="data">28.013</td>
		<td class="data">0.29680</td>
		<td class="data">1.0416</td>
		<td class="data">0.7448</td>
		<td class="data">1.400</td>
	</tr>
	<tr>
		<td class="lhead">Octane</td>
		<td class="data">$C_8H_{18}$</td>
		<td class="data">114.230</td>
		<td class="data">0.07279</td>
		<td class="data">1.7113</td>
		<td class="data">1.6385</td>
		<td class="data">1.044</td>
	</tr>
	<tr>
		<td class="lhead">Oxygen</td>
		<td class="data">$O_2$</td>
		<td class="data">31.999</td>
		<td class="data">0.25983</td>
		<td class="data">0.9216</td>
		<td class="data">0.6618</td>
		<td class="data">1.393</td>
	</tr>
	<tr>
		<td class="lhead">Propane</td>
		<td class="data">$C_3H_8$</td>
		<td class="data">44.097</td>
		<td class="data">0.18855</td>
		<td class="data">1.6794</td>
		<td class="data">1.4909</td>
		<td class="data">1.126</td>
	</tr>
	<tr>
		<td class="lhead">Steam</td>
		<td class="data">$H_2O$</td>
		<td class="data">18.015</td>
		<td class="data">0.46153</td>
		<td class="data">1.8723</td>
		<td class="data">1.4108</td>
		<td class="data">1.327</td>
	</tr>
</table>
</div>


  
From equation of state \eqref{thermo:eq:idealGas} for perfect gas,
it follows 
<div class="longEq">
\begin{align}
	\label{thermo:eq:stateD}
	d(P\,v) = R\,dT
\end{align}
</div>
<div class="longEq">
\begin{align}
	dh = dE_u + d(P\,v)  = dE_u + d(R\,T)  = f(T)\,\hbox{ (only)}
	\label{thermo:eq:dhIdeal}
\end{align}
</div>

From the definition of enthalpy it follows that 
<div class="longEq">
\begin{align}
	d(Pv) = dh - dE_u 
	\label{thermo:eq:defHd}
\end{align}
</div>
Utilizing equation \eqref{thermo:eq:stateD} and subsisting into
equation \eqref{thermo:eq:defHd} and dividing by $dT$ yields
<div class="longEq">
\begin{align}
	C_p - C_v = R
	\label{thermo:eq:CpCvR}
\end{align}
</div>
This relationship is valid only for ideal/perfect gases.

The ratio of the specific heats can be expressed
in several forms as 
<div class="empheq">
	<h3 class="eqHead" >$C_v$ =f(R)</h3>
	\begin{align}
		\label{thermo:eq:Cv}
		C_v = \frac{R}{k-1}
	\end{align}
</div> <!-- end empheq --> 

and
<div class="empheq">
	<h3 class="eqHead" >$C_p$ =f(R)</h3>
	\begin{align}
		\label{thermo:eq:Cp}
		C_p = \frac{k\,R}{k-1}
	\end{align}
</div> <!-- end empheq --> 

The specific heat ratio, $k$ value ranges from unity to about 1.667.
These values depend on the molecular degrees of freedom 
(more explanation can be obtained in Van Wylen &ldquo;F. of Classical
thermodynamics.&rdquo;)
The values of several gases can be approximated as ideal gas
and are provided in Table  <a href="thermo:tab:idealGases">(2.1)</a>.

The entropy for ideal gas can be simplified as the following
<div class="longEq">
\begin{align}
	s_2 - s_1 = \int_1^2 \left(\frac{dh}{T}- \dfrac{dP}{\rho T}\right)   
	\label{thermo:eq:deltaSidealI}
\end{align}
</div>
Using the identities developed so far one can find that 
<div class="longEq">
\begin{align}
	s_2 - s_1 = \int_1^2 C_p \frac{dT}{T} - \int_1^2 \frac{R\,dP}{P}  
	= C_p \, \ln \frac{T_2}{T_1} - R \,\ln \dfrac{P_2}{P_1} 
	\label{thermo:eq:deltaSideal}
\end{align}
</div>
Or using specific heat ratio equation
\eqref{thermo:eq:deltaSideal} transformed into
<div class="longEq">
\begin{align}
	\frac{s_2 - s_1} {R} = 
	\frac{k}{ k -1} \ln \frac{T_2}{T_1} - \ln \frac{P_2}{P_1}
	\label{thermo:eq:deltaSidealK}
\end{align}
</div>
For isentropic process, $\Delta s = 0 $, the following is obtained
<div class="longEq">
\begin{align}
	\ln\, \frac{T_2}{T_1} = 
		\ln\, \left(\dfrac{P_2}{P_1} \right) ^{\tfrac{k -1 }{k}}
	\label{thermo:eq:sZero}
\end{align}
</div>
There are several famous identities that results from equation
\eqref{thermo:eq:sZero} as 
<div class="empheq">
	<h3 class="eqHead" >Isentropic Relationship</h3>
	\begin{align}
		\label{thermo:eq:famousIdeal}
		\begin{array}{rcccl}
		\dfrac {\rho_2}{\rho_1} 
			= &\left( \dfrac{T_2}{T_1} \right)^{\tfrac{1}{k-1}}
			= &\left( \dfrac{P_2}{P_1} \right)^{\tfrac{1}{k}}
			&= &\left(\dfrac{V_1}{V_2} \right) \\
		\dfrac{T_2}{T_1} 
			= &\left(\dfrac{P_2}{P_1} \right)^{\tfrac{k -1 }{k}} 
			= &\left(\dfrac{\rho_2}{\rho_1}\right)^{k -1 } 
			&= &\left(\dfrac{V_1}{V_2} \right)^{(k-1)}	\\
		\dfrac{P_2} {P_1} 
			= &\left (\dfrac{T_2}{T_1}\right)^{\tfrac {k}{k-1}}
			= &\left( \dfrac{\rho_2}{\rho_1} \right)^{k}	
			&= &\left( \dfrac{V_1}{V_2} \right)^{k} \\
		\dfrac {V_2} {V_1}
			=  &\left(\dfrac{T_1}{T_2} \right)^{\tfrac{1}{k-1}}
			=  &\left(\dfrac{\rho_1}{\rho_2} \right)
			&=  &\left(\dfrac{P_1}{P_2} \right )^{\tfrac{1}{k}} \\
		\end{array}
	\end{align}
</div> <!-- end empheq --> 


The ideal gas model is a simplified version of the 
behavior of real gas. In other words the real or the actual
gas has a correction factor
that account for the deviations from the ideal gas model.
This correction factor referred as the compressibility factor
and defined as
<div class="empheq">
	<h3 class="eqHead" >Compressibility Factor</h3>
	\begin{align}
		\label{thermo:eq:Z}
		Z = \dfrac{V_{{\scriptsize actual}}}
				{V_{{\scriptsize ideal gas}}}
			= \dfrac{P\,v}{R\,T}
			= \dfrac{P}{\rho\,R\,T}
	\end{align}
</div> <!-- end empheq --> 

One of the common way to estimate the compressibility factor
(is by using or based on Redlick&mdash;Kwong Equation).
In this method, the equation of state is 
<div class="longEq">
\begin{align}
	\label{thermo:eq:RedlickKwong}
	 P = \dfrac{R\,T}{V_m - B} - \dfrac{A}{\sqrt{T}\, V_m\, (V_m+B)}
\end{align}
</div>
where the  
$V_m$ is the molar volume, 
$A$ is a coefficient accounting for attractive potential of molecules,
and $B$ is a coefficient that accounting for volume correction.

The coefficients are a function of gas.
These coefficients can be estimated using the critical point of the gas
<div class="longEq">
\begin{align}
	\label{thermo:eq:RedlickKwongAB}
	A = \dfrac{0.4275\, R^2 \, {T_c}^{2.5}}{P_c}, \qquad B = \dfrac{0.08664\, R\, T_c}{P_c}
\end{align}
</div>
where:
$T_c$ is the critical temperature, and
$P_c$ is the critical pressure.

Expressing<div class = "px-5" ></div >  <!-- end of "px-5"-- >
<div class="container">
	<button type="button" class="btn btn-primary"
		data-toggle="collapse" data-target="#demo">Footnote #2
	</button>
	<div id="demo" class="collapse"> 
		This idea was suggested by Cultip and Shacham
	</div>
</div>
 the volume as a faction of the other
parameters in equation \eqref{thermo:eq:Z} and then substituting into equation 
\eqref{thermo:eq:RedlickKwong} transfomred it into cubic equation
of $Z$ as
<div class="longEq">
\begin{align}
	\label{thermo:eq:Zeq}
	Z^3 - Z^2 - \eta_1\,Z - \eta_2 = 0
\end{align}
</div>
where
<div class="longEq">
\begin{align}
	\label{thermo:eq:etas}
	\phi_1 = 0.42747 \, \left(\dfrac{P_r}{{T_r}^{\tfrac{5}{2}}} \right) \\
	\phi_2 = 0.08664 \, \left(\dfrac{P_r}{T_r} \right) \\
	\eta_1 = {\phi_2}^2 + \phi_2 - \phi_1  \qquad
	\eta_2 = \phi_1\,\phi_2 
\end{align}
</div>
$Z$ can be solve analytically and will be presented in a Figure.

<h2 class="subsec" id="sec2.1.1"> &sect; 2.1.1 Maximum Work of Expansion Process</h2>

<div class="invisibleAnchor"> <!-- Index hock -->
<div class="invisible"> <!-- Index hock -->
	<a name="thermo:sub:sub0">Pressure Potential!vacuum</a>"
</div>  <!-- End of Index -->



<div class="figContainer" id ="thermo:fig:maxW">
	<div class="imgContainer">
		<figure>
			<img src="images/piston.png" alt="Cylinder and piston configuration of maximum work">
				<div class="caption">
Fig. 2.1 Cylinder and piston configuration of maximum work.
				</div >
		</figure>
	</div >
</div >
In some industries, such as the printing, there is a need to
obtain a high temperature for a short time (to bake the paper)
and later the paper has to be cooled to a room temperature.
On the other hand, when one want to absorb the car energy during the
stopping process 
and reuse it later leads to a theoretical question.
This question what is the minimum work to compress substance or maximum
work or energy that can be obtained from substance undergoes change of
the condition.
The compressibility has a significant effect on the work that can be obtained or required in a piston cylinder
configuration and others configurations.
For example, a shock tube is a device which used to obtain high temperature to study explosions
and potentially has civilian applications.
What is the minimum energy or work which required to compress a piston from state $\pmb{\mathcal A}$ to state $\pmb{\mathcal B}$.
Clearly, the compression has to occur in an isentropic process.
The equation of state has to be specified to have
a specific equation describing the situation.
The simplest equation is the ideal gas model.
The general case has three dimensions and
requires a complex mathematical treatment.
To simplify the discussion, it is assumed that
the process occurs in an one dimensional case.

<h4 class="issue" id="thermo:issue:iss6">Perfect Gas</h4>

The gas confined by a cylinder and piston
undergoes isentropic process during expansion.
First the analysis will be discussed for the case of  perfect gas.
Every stage of the process is denoted by $x$ and 
$L_{\pmb{\scriptsize B}} >x > L_{\pmb{\scriptsize A}}$.
The mass conservation $(m_{\pmb{\scriptsize A}} = m(x))$ and
assuming perfect gas model is
<div class="longEq">
\begin{align}
	\label{thermo:eq:m_conservation}
	\dfrac{P_{\pmb{\mathcal A}} \, V_{\pmb{\mathcal A}} }{T_{\pmb{\mathcal A}} \cancel{R} } =   
	\dfrac{P(x) \, V(x) }{T(x) \cancel{R} }  \Longrightarrow
   P(x) = P_{\pmb{\mathcal A}}\,\dfrac{T(x)\,\overbrace{V_{\pmb{\mathcal A}}}^{\cancel{A}\,L_{\pmb{\mathcal A}}} }
			{T_{\pmb{\mathcal A}} \,\underbrace{V(x)}_{\cancel{A}\,(L_{\pmb{\mathcal A}} +x)} }
 		=P_{\pmb{\mathcal A}} \, \dfrac{T(x)\, L_{\pmb{\mathcal A}} }{T_{\pmb{\mathcal A}} \,\left(L_{\pmb{\mathcal A}} + x \right)}
\end{align}
</div>
Using the isentropic relationship during the process
see equation table \eqref{thermo:eq:famousIdeal} apply,
<div class="longEq">
\begin{align}
	\label{thermo:eq:iso}
	P(x) = P_{\pmb{\mathcal A}}  \left(\dfrac{ V_{\pmb{\mathcal A}}} {V(x)} \right)^{k}
\end{align}
</div> 
It can be noticed that the area can be canceled (see
equation \eqref{thermo:eq:m_conservation} for similar
explanation) and the isentropic relationship should read
\begin{eqnarray}
   \label{isenstripicPA}
   P_(x) = P_{\pmb{\mathcal A}}\,\left(\frac{L_{\pmb{\mathcal A}}} {L_{\pmb{\mathcal A}}+x}\right)^{k}
\end{eqnarray}
Dimensional analysis<div class = "px-5" ></div >  <!-- end of "px-5"-- >
<div class="container">
	<button type="button" class="btn btn-primary"
		data-toggle="collapse" data-target="#demo">Footnote #2
	</button>
	<div id="demo" class="collapse"> 
&ldquo;Basics of Fluid Mechanics&rdquo; by
		Bar&mdash;Meir has extensive discussion about this point.
	</div>
</div>
 can be used
to minimize the labor and introducing a dimensionless variable
\begin{eqnarray}
   \label{xi}
	 \xi = x/L_{\pmb{\mathcal A}}
\end{eqnarray}
By introducing this variable, it is acknowledged that this dimensionless parameter is effecting the solution 
of the problem.
The work done by the gas on the piston is
\begin{eqnarray}
   \label{workIni2}
	 {{W}_{{\pmb{\mathcal A}}\rightarrow {\pmb{\mathcal B}}}} =
		\int_0^{L_{\pmb{\mathcal B}}-L_{\pmb{\mathcal A}}} \overbrace{A\,P(x)}^{F(x)} dx 
\end{eqnarray}
Equation \eqref{workIni2} by area, $A$, which is a constant and substituting equation \eqref{isenstripicPA} 
yields 
\begin{eqnarray}
	\label{workIni1}
	\dfrac{{W}_{{\pmb{\mathcal A}}\rightarrow {\pmb{\mathcal B}}}}{A} 
	= \int_0^{L_{\pmb{\mathcal B}}-L_{\pmb{\mathcal A}}} P_{{\pmb{\mathcal A}}1}\,
			\left(\frac{L_{\pmb{\mathcal A}}} {\left(L_{\pmb{\mathcal A}}+x\right)}\right)^{k} \,
			\dfrac{L_{\pmb{\mathcal A}}\, dx}{L_{\pmb{\mathcal A}}}
\end{eqnarray}
Equation \eqref{workIni1} can be transformed into a
dimensionless form when dividing by Pressure and length.
Also notice the integration limits change as following
\begin{eqnarray}
	\label{workIni}
	\displaystyle \frac{W_{{\pmb{\mathcal A}}\rightarrow {\pmb{\mathcal B}}}}{A\,L_{\pmb{\mathcal A}}\,P_{\pmb{\mathcal A}} } = 
		\int_0^{L_{\pmb{\mathcal B}}-L_{\pmb{\mathcal A}}}
			\left(\dfrac{\cancel{L_{\pmb{\mathcal A}}}\,(1)}
				{\cancel{L_{\pmb{\mathcal A}}}(1 +\xi)}\right)^{k} \,d\xi
		= \int_0^{\tfrac{L_{\pmb{\mathcal B}}}{L_{\pmb{\mathcal A}}} - 1} 
			\left(\dfrac{1 } {1 + \xi }\right)^{k} d\xi
\end{eqnarray}

Carrying the integration of equation \eqref{workIni}) yields
\begin{eqnarray}
	\label{workIniIntegralNonDefinite}
	\dfrac{W_{{\pmb{\mathcal A}}\rightarrow {\pmb{\mathcal B}}}}{A\,L\, P_{{\pmb{\mathcal A}}1}} =
		\left.
			\dfrac{{\left( 1 +\xi\right) }^{1-k}}{1-k} 
			\right|_0^{\tfrac{L_{\pmb{\mathcal B}}}{L_{\pmb{\mathcal A}}} - 1}
\end{eqnarray}
\begin{eqnarray}
	\label{workIniIntegral}
	\dfrac{W_{{\pmb{\mathcal A}}\rightarrow {\pmb{\mathcal B}}}}{A\,L\, P_{{\pmb{\mathcal A}}1}} =
	\dfrac{1}{k -1}
		\left[ 1 - \left( \dfrac{L_{\pmb{\mathcal B}}}{L_{\pmb{\mathcal A}}}\right)^{1-k}\right]
\end{eqnarray}
<div class="figContainer" id ="thermo:fig:workLBLA">
	<div class="imgContainer">
		<figure>
			<img src="images/workLBLA.png" alt="Dimensionless work available in cylinder">
				<div class="caption">
Fig. 2.2 Dimensionless work available in a cylinder piston configuration.
				</div >
		</figure>
	</div >
</div >
<h2 class="section" id="sec2.2"> &sect; 2.2 The Velocity&mdash;Temperature Diagram</h2>

<div class="invisibleAnchor"> <!-- Index hock -->
<div class="invisible"> <!-- Index hock -->
	<a name="thermo:aut:aut0">Spalding</a>"
</div>  <!-- End of Index -->

<div class="invisibleAnchor"> <!-- Index hock -->
<div class="invisible"> <!-- Index hock -->
	<a name="thermo:aut:aut1">Stodola</a>"
</div>  <!-- End of Index -->

<div class="invisibleAnchor"> <!-- Index hock -->
<div class="invisible"> <!-- Index hock -->
	<a name="thermo:sub:sub1">Velocity&mdash;temperature diagram</a>"
</div>  <!-- End of Index -->


The velocity&mdash;temperature (U&mdash;T) diagram was developed
by Stodola (1934) and expended by Spalding (1954).
In the U&mdash;T diagram, the logarithms of temperature is
plotted as a function of the logarithms of velocity.
For simplicity, the diagram here deals with perfect gas only
(constant specific heat)<div class = "px-5" ></div >  <!-- end of "px-5"-- >
<div class="container">
	<button type="button" class="btn btn-primary"
		data-toggle="collapse" data-target="#demo">Footnote #2
	</button>
	<div id="demo" class="collapse"> 
The perfect gas model is used
		because it provides also the trends of more complicated model.
	</div>
</div>
.
The ideal gas equation \eqref{thermo:eq:idealGas} was described before.
This diagram provides a graphical way to analysis the flow and to
study the compressible flow because two properties defines the state.

The enthalpy is a linear function of the temperature due to the
assumptions employed here (the pressure does not affect the enthalpy).
The energy equation \eqref{thermo:eq:enthalpy}
can be written for adiabatic process as
<div class="longEq">
\begin{align}
	h + \frac{U^2}{2} = constant_1
	\label{thermo:eq:enthalpyAdiabatic}
\end{align}
</div>
Taking the logarithms of both sides of equation
\eqref{thermo:eq:enthalpyAdiabatic} results in
<div class="longEq">
\begin{align}
	\log \left( h + \frac{U^2}{2} \right)  =  constant_2 
	\label{thermo:eq:logEnthalpyAdiabatic0}
\end{align}
</div>
or
<div class="longEq">
\begin{align}
	\log \left( T + \frac{U^2}{2\,C_p} \right)  =  constant_3
	\label{thermo:eq:logEnthalpyAdiabatic1}
\end{align}
</div>
<div class="TEx" id="thermo:ex:constants">
<h4>Example 2.1</h4>
Determine the relationship between $constant_3$ in equation
\eqref{thermo:eq:logEnthalpyAdiabatic1} to $constant_1$ in equation
\eqref{thermo:eq:enthalpyAdiabatic}.
</div > <!-- end class ETx-->

<div class="solutionT">
Under construction
</div>

From equation \eqref{thermo:eq:logEnthalpyAdiabatic0},
it can be observed as long as the velocity square is
relatively small compared to multiplication of the specific
heat by the temperature, it remains close to constant.
Around ${U^2} = {2\, C_p \, T}$ the velocity drops rapidly.
These lines are referred to as energy lines because
kinetic energy and thermal remain constant.
These lines are drawn in Figure  <a href="thermo:fig:TU-P">(2.3(b))</a>. 
<div class="bigFigure"> <!-- Start Big Figure. -->

	<ul class="subFigures">

		
			<li>
			<a name="#thermo:fig:TU">
				<figure>
					<img src="images/temperatureU1.png" alt="The pressure lines">
					<div class"caption" >
						Fig. 2.3(a) The pressure lines
					</div>
				</figure>
			</a>
		</li> <!-- End sub figure. -->

	
			<li>
			<a name="#thermo:fig:TU-P">
				<figure>
					<img src="images/TU-P.png" alt="The energy lines">
					<div class"caption" >
						Fig. 2.3(b) Various lines in Velocity--Temperature diagrams
					</div>
				</figure>
			</a>
		</li> <!-- End sub figure. -->

	</ul> <!-- End UL list figures. -->
	<div class="mainCaption">
		Fig. 2.3 Temperature Velocity Pressure
	</div > <!-- End of the main caption. -->

</div> <!-- End of the big figure. -->



The sonic line (the speed of sound will
be discussed in Chapter \ref{chap:sound})
is a line that given by the following equation
<div class="longEq">
\begin{align}
	U = c = \sqrt{k\,R\,T} \rightarrow \ln \, c =
	\frac{1}{2}\log \left( k\,R\,T \right)
	\label{thermo:eq:sonic}
\end{align}
</div>
The reason that logarithms scales are used is so that
the relative speed ($U/c$ also known as Mach number,
will be discussed page \pageref{variableArea:eq:Mach})
for any point on the diagram, can be directly measured.
For example, the Mach number of point
$\mathbf{A}$, shown in Figure  <a href="thermo:fig:TU">(2.3(a))</a>,
is obtained by measuring the distance $\mathbf{A-B}$. 
The distance $\mathbf{A-B}$ represent the ratio of the speed of
sound because
<div class="longEq">
\begin{align}
	\mathbf{A - B} = \log U|_{A} - \log c|_B =  \ \log \frac{U|_{A}}{c}
	\label{thermo:eq:soundR}
\end{align}
</div>
For example, when copying the distance $\mathbf{A-B}$
to the logarithms scale results in Mach number.
For instance, copying the distance to starting point
of 100, the Mach number at point $\mathbf{A}$ will
be the read number from the scale divided by 1000.

Mass conservation reads
<div class="longEq">
\begin{align}
	\frac{\dot{m}}{A} = {U}\,{\rho}
	\label{thermo:eq:pA}
\end{align}
</div>
Subsisting the equation of state \eqref{thermo:eq:idealGas} into equation
\eqref{thermo:eq:pA} results in 
<div class="longEq">
\begin{align}
	\frac{\dot{m}\,R}{A \, P} = \frac{U}{T}
	\label{thermo:eq:PPA}
