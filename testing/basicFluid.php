<h1 class="chap" id="csec3">Chapter 3 Basic of Fluid Mechanics</h1>

<h2 class="section" id="sec3.1"> &sect; 3.1 Introduction</h2>


The reader is expected to be familiar with the fundamentals of
fluid mechanics and this review is provided as refreshment.
These basic principles and concepts are to be use in the book and are
a building blocks of the understanding the material presented later.
Several concepts are reviewed such as control volume.
Several applications of the fluid mechanics will demonstrated. 
First, a discussion about fluid proprieties
(related to compressible flow) is presented.
The integral and differential methods are described.
Later, a discussion about the governing
equations in fluid mechanics is presented.

<h2 class="section" id="sec3.2"> &sect; 3.2 Fluid Properties</h2>


<h3 class="subsec" id="sec3.2.1">3.2.1 Kinds of Fluids</h3>


Some differentiate fluids from solid by the reaction to shear stress.
Generally it is accepted that the fluid continuously and
permanently deformed under shear stress while solid exhibits
a finite deformation which does not change with time.
It is also said that the liquid cannot return
to their original state after the deformation.
This differentiation leads to three groups of materials:
solids and fluids and those between these two limits.
This test creates a new material group that shows
dual behaviors; under certain limits; it behaves
like solid and under others it behaves like fluid.
This book deals with only clear fluid (at    
least, this is the intention at this stage). 
The fluid is mainly divided into two categories: liquids and gases.
The main difference between the liquids and gases state is that
gas will occupy the whole volume while liquids has an almost fix volume.
This difference can be, for most practical purposes considered sharper.

<h4 class="subsubsec" id="sec3.2.1.1">3.2.1.1 Density</h4>

The density is the main property which
causes the field of compressible flow.
The density is a property which requires
that the fluid to be continuous.
<div class="invisible"> <!-- Index hock -->
	<a name="basicFluid:sub:sub0">Density</a>"
</div>  <!-- End of Index -->

The density can be changed and it is a function of
time and space (location) but must be continues.
It doesn't mean that a sharp and abrupt change in fields cannot occur.
The continues requirement is referred to the fact that density is
independent of the sampling size. 
After certain sampling size, the density remains constant.
Thus, the density is defined as 
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\rho = \lim_{\Delta V \longrightarrow \varepsilon} 
	\frac{\Delta m} {\Delta V} 
	\label{basic:eq:rho}
\end{align}
</div> <!-- end long equation -->
<div class="invisible"> <!-- Index hock -->
	<a name="basicFluid:sub:sub1">Density!definition</a>"
</div>  <!-- End of Index -->

It must be noted that $\varepsilon$ is chosen so that
the continuous assumption is not broken, that is, it
did not reach/reduced to the size where the atoms or
molecular statistical calculations are significant.

<h3 class="subsec" id="sec3.2.2">3.2.2 Viscosity</h3>

<div class="invisible"> <!-- Index hock -->
	<a name="basicFluid:sub:sub2">Viscosity</a>"
</div>  <!-- End of Index -->


The shear stress is part of the pressure tensor. 
This book deals with Newtonian fluid and
hence, applying the linear relationship
can be written for the shear stress
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\tau_{xy} = \mu\,\frac{dU}{dy}
	\label{basic:eq:tau_xy}
\end{align}
</div> <!-- end long equation -->
Where $\mu$ is called the absolute viscosity or dynamic viscosity.
Newtonian fluids are fluids which the ratio is constant.
Many fluids fall into this category such as air, water etc.
This approximation is appropriate for many other fluids but
only within some ranges. 

Equation \eqref{basic:eq:tau_xy} can be interpreted as momentum
in the $x$ direction transferred into the $y$ direction.
Thus, the viscosity is the resistance to the flow (flux) or the movement.
The property of viscosity, which is exhibited by all 
fluids, is due to the existence of cohesion and 
interaction between fluid molecules.
These cohesions and interactions hamper the flux
in y&mdash;direction. Some referred to shear stress as
viscous flux of x&mdash;momentum in the y&mdash;direction.
The units of shear stress are the same as flux per time 
as following
<div class="longEq"> 
\begin{align}
	\frac{F}{A} \left[ \frac {kg\, m}{sec^2}\; \frac{1}{m^2} \right]  =
		\frac{\dot{m}\,U} {A} \left[ \frac{kg}{sec}\;
		\frac{m}{sec} \; \frac{1}{m^2}  \right] 
\end{align}
</div>
Thus, the notation of $\tau_{xy}$ is easier to understand and visualize.
In fact, this interpretation is more suitable to
explain the molecular mechanism of the viscosity.
The units of absolute viscosity are [$N\,sec/m^2$].
<div class="invisible"> <!-- Index hock -->
	<a name="basicFluid:sub:sub3">Absolute viscosity</a>"
</div>  <!-- End of Index -->


Viscosity varies widely with temperature.
However, temperature variation has an opposite
effect on the viscosities of liquids and gases.
The difference is due to their fundamentally different
mechanism creating viscosity characteristics.
In gases, molecules are sparse and cohesion is
negligible, while in the liquids, the molecules
are more compact and cohesion is more dominate.
Thus, in gases, the exchange of momentum between layers
brought as a result of molecular movement normal to the
general direction of flow, and it resists the flow.
This molecular activity is known to increase with temperature,
thus, the viscosity of gases will increase with temperature.
This reasoning is a result of the considerations of the kinetic theory.
This theory  indicates that gas viscosities vary directly with
the square root of temperature.
In liquids, the momentum exchange due to molecular movement  
is small compared to the cohesive forces between the molecules.
Thus, the viscosity is primarily dependent on
the magnitude of these cohesive forces.
Since these forces decrease rapidly with increases of temperature,
liquid viscosities decrease as temperature increases.

Well above the critical point (two phase dome),
both phases are only a function of the temperature.
On the liquid side below the critical point,
the pressure has minor effect on the viscosity.
It must be stress that the viscosity in the dome is meaningless.
There is no such a thing of viscosity at 30% liquid.
It simply depends on the structure of the flow, see
for more detail in &ldquo;Basic of Fluid Mechamics,
Bar&mdash;Meir&rdquo; in the chapter on multi phase flow.
Oils have the greatest increase of viscosity with pressure
which is a good thing for many engineering purposes. 

<h3 class="subsec" id="sec3.2.3">3.2.3 Kinematic Viscosity</h3>
 

The kinematic viscosity is another way to look at the viscosity.
The reason for this new definition is that
some experimental data are given in this form.
These results also explained better using the new definition.
The kinematic viscosity embraces both the
viscosity and density properties of a fluid.
The above equation shows that the dimensions
of $\nu$ to be square meter per second, [$m^2/sec]$, which
are acceleration units (a combination of kinematic terms).
This fact explains the name &ldquo;kinematic&rdquo; viscosity. 
The kinematic viscosity is defined  as 
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\nu = \frac{\mu}{\rho}
	\label{intro:eq:nu}
\end{align}
</div> <!-- end long equation --> 

The gas density decreases with the temperature.
However, The increase of the absolute viscosity with the temperature
<div class="invisible"> <!-- Index hock -->
	<a name="basicFluid:sub:sub4">absolute viscosity</a>"
</div>  <!-- End of Index -->

is enough to overcome the increase of density and thus,
the kinematic viscosity also increase with the temperature
for many materials. 

<h3 class="subsec" id="sec3.2.4">3.2.4 Bulk Modulus</h3>


Similar to solids (hook's law), fluids have a property that describes
the volume change as results of pressure change for constant temperature.
It can be noted that this property is not the result of the equation
of state but related to it.
Bulk modulus is usually obtained from experimental or theoretical or
semi theoretical methods.

The bulk modulus is defined as 
<div class="invisible"> <!-- Index hock -->
	<a name="basicFluid:sub:sub5">bulk modulus</a>"
</div>  <!-- End of Index -->

<div class="longEq"> <!-- begin long equation -->
\begin{align}
	B_T = - v \left( \frac{\partial P}{\partial v} \right)_T 
	\label{basic:eq:bulkModulus_v}
\end{align}
</div> <!-- end long equation -->  
Using the identity of $v=1/\rho$ transfers equation 
\eqref{basic:eq:bulkModulus_v} into 
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	B_T = \rho \left( \frac{\partial P}{\partial \rho} \right)_T 
	\label{basic:eq:bulkModulus_r}
\end{align}
</div> <!-- end long equation -->  
The bulk modulus for several selected liquids is presented in 
Table  <a href="basic:tab:bulk">(3.1)</a>.

<div class = "space"> </div>
<div class="dataTable">
	<table id="basic:tab:bulk" class="data">
		<caption class="bottom">
			Bulk modulus for selected materials
		</caption>
		<tr>
			<th class="head lhead">Chemical </th>
			<th class="head">Bulk Modulus</th>
			<th class="head">$T_c$ [$K$]</th>
			<th class="head">$P_c$</th>
		</tr>
 		<tr> 
 			<td class="lhead">
 				Acetic Acid  	
 			</td>
 			<td>
 				2.49 	
 			</td>
 			<td>
 				593K  
 			</td>
 			<td>
 				57.8 [Bar]	 
 			</td>
 		</tr>
 		<tr> 
 			<td class="lhead">
 				Acetone     	
 			</td>
 			<td>
 				0.80 	
 			</td>
 			<td>
 				508 K 		
 			</td>
 			<td>
 				48 [Bar] 
 			</td>
 		</tr>
 		<tr> 
 			<td class="lhead">
 				Benzene			
 			</td>
 			<td>
 				1.10 	
 			</td>
 			<td>
 				562 K 
 			</td>
 			<td>
 				4.74 [MPa]	 
 			</td>
 		</tr>
 		<tr> 
 			<td class="lhead">
 				Carbon Tetrachloride 	
 			</td>
 			<td>
 				1.32 
 			</td>
 			<td>
 				556.4 K 
 			</td>
 			<td>
 				4.49 [MPa] 
 			</td>
 		</tr>
 		<tr> 
 			<td class="lhead">
 				Ethyl Alcohol 	
 			</td>
 			<td>
 				1.06	
 			</td>
 			<td>
 				514 K 
 			</td>
 			<td>
 				6.3 [Mpa] 
 			</td>
 		</tr>
 		<tr> 
 			<td class="lhead">
 				Gasoline 
 			</td>
 			<td>
 				1.3  
 			</td>
 			<td>
 				nf 
 			</td>
 			<td>
 				nf 
 			</td>
 		</tr>
 		<tr> 
 			<td class="lhead">
 				Glycerol 		
 			</td>
 			<td>
 				4.03-4.52	
 			</td>
 			<td>
 				850 K 
 			</td>
 			<td>
 				7.5	[Bar] 
 			</td>
 		</tr>
 		<tr> 
 			<td class="lhead">
 				Mercury	
 			</td>
 			<td>
 				26.2-28.5	
 			</td>
 			<td>
 				1750 K 
 			</td>
 			<td>
 				172.00 [MPa] 
 			</td>
 		</tr>
 		<tr> 
 			<td class="lhead">
 				Methyl Alcohol 
 			</td>
 			<td>
 				0.97	
 			</td>
 			<td>
 				Est 513 	
 			</td>
 			<td>
 				Est 78.5 [Bar] 
 			</td>
 		</tr>
 		<tr> 
 			<td class="lhead">
 				Nitrobenzene   
 			</td>
 			<td>
 				2.20	
 			</td>
 			<td>
 				nf 
 			</td>
 			<td>
 				nf 
 			</td>
 		</tr>
 		<tr>
 			<td class="lhead">
 				Olive Oil 		
 			</td>
 			<td>
 				 1.60	
 			</td>
 			<td>
 				 nf 
 			</td>
 			<td>
 				 nf 
 			</td>
 		</tr>
 		<tr>
 			<td class="lhead">
 				Paraffin Oil	
 			</td>
 			<td>
 				 1.62	
 			</td>
 			<td>
 				 nf 
 			</td>
 			<td>
 				 nf 
 			</td>
 		</tr>
 		<tr>
 			<td class="lhead">
 				SAE 30 Oil 	
 			</td>
 			<td>
 				 	1.5 
 			</td>
 			<td>
 				 na 
 			</td>
 			<td>
 				 na 
 			</td>
 		</tr>
 		<tr>
 			<td class="lhead">
 				Seawater 	
 			</td>
 			<td>
 				 	2.34 
 			</td>
 			<td>
 				 na 
 			</td>
 			<td>
 				 na  
 			</td>
 		</tr>
 		<tr>
 			<td class="lhead">
 				Toluene     	
 			</td>
 			<td>
 				 1.09	
 			</td>
 			<td>
 				 591.79 K 
 			</td>
 			<td>
 				  4.109 [MPa] 
 			</td>
 		</tr>
 		<tr>
 			<td class="lhead">
 				Turpentine  	
 			</td>
 			<td>
 				 1.28 	
 			</td>
 			<td>
 				 na 
 			</td>
 			<td>
 				 na 
 			</td>
 		</tr>
 		<tr>
 			<td class="lhead">
 				Water 	
 			</td>
 			<td>
 				 	2.15-2.174  	
 			</td>
 			<td>
 				 647.096 K		
 			</td>
 			<td>
 				 22.064 [MPa]
 			</td>
 		</tr>
	</table>
	</div>


Additional expansions for similar parameters are defined.
The thermal expansion is defined as 
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\beta_P = \frac{1 }{v} 
		\left( \frac{\partial v}{\partial T} \right) _P
	\qquad
	\beta_v = \frac{1 }{P} 
		\left( \frac{\partial P}{\partial T} \right)_v
	\label{basic:eq:betaP}
\end{align}
</div> <!-- end long equation -->
These parameters are related as 
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\beta_{T}  =  -\frac{\beta_v}{\beta_{P}}
	\label{basic:eq:genealRbeta}
\end{align}
</div> <!-- end long equation -->
The definition of bulk modulus will be used to calculate
the speed of sound in slightly compressed liquid. 

\section[Mass Conservation]
	{The Control Volume and Mass Conservation \label{basic:sec:massCV}}


In this section the conservation of the mass,
momentum, and energy equation are presented.
In simple (solid) system, Newton second law is applied and is   
conserved because the object remains the same (no deformation). 
However, when the fluid system moves relative
location of one particle to another is changing.
Typically, one wants to find or to
predict the velocities in the system.
Thus, using the old approach requires to keep
track of every particle (or small slabs).
This kind of analysis is reasonable and it referred
to in the literature as the Lagrangian Analysis.
This name is in honored J. L. Langrange (1736&mdash;1813) who
formulated the equations of motion for the moving fluid particles.

Even though the Lagrangian system looks reasonable,
this system turned out to be difficult to solve and to
analyze therefore it is used only in very few cases.
The main difficulty lies in the fact that every
particle has to be traced to its original state.
Leonard Euler (1707&mdash;1783) suggested an alternative approach based
on a defined volume. This methods is referred as Eulerian method.
The Eulerian method focuses on a defined area
or location to find the needed information.
The use of the Eulerian methods leads to a set
differentiation equations that is referred to as the
Navier&mdash;Stokes equations which are commonly used.
The Eulerian system leads to integral equations
which will be used in several cases in this book.

<h3 class="subsec" id="sec3.3.1">3.3.1 Control Volume</h3>


The Eulerian method requires to define a
control volume (sometime more than one).
The control volume is a defined volume which
is differentiated into two categories:
non&mdash;deformable and deformable.
Non&mdash;deformable control volume is a
control volume which is fixed in space
relatively to an one coordinate system.
This coordinate system may be in a relative
motion to another (almost absolute) coordinate system.
Deformable control volume is a volume having part or all of its
boundaries in motion during the process at hand.
The control volume is used to build the conservation equations for the
mass, momentum, energy, entropy etc.
The choice of control volume ( deformable or not)
is a function to what bring a simpler solution.

<h3 class="subsec" id="sec3.3.2">3.3.2 Continuity Equation</h3>

\mkSeclog{b}{basic:sec:continuityEq}The mass conservation of a system is 
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\frac{D\,m_{sys}}{Dt} = \frac{D}{Dt} \int_{V_{sys}} \rho dV = 0 	
	\label{basic:eq:Dmdt}
\end{align}
</div> <!-- end long equation -->
The system mass after some time is made of 
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	m_{sys} = m_{c.v.} + m_{out} - m_{in} 
	\label{basic:eq:mSysAfter}
\end{align}
</div> <!-- end long equation -->
Where $m_{out}$ is the mass flow out and $m_{in}$ is the mass flow in.
The change with the time is zero and hence
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	0 = \frac{D\,m_{sys}}{Dt} = \frac{d\,m_{c.v.}}{dt} + \frac{d\, m_{out}}{dt} - \frac{d\,m_{in}}{dt} 
	\label{basic:eq:mSysAfterdt}
\end{align}
</div> <!-- end long equation -->
The first term on the right hand side is converted to integral
and the other two terms on the right hand side are combined and
realizing that the sign can be accounted for flow in or out as
<div class="useBoxEq">
	<h3 class="eqHead" >Continuity</h3>
	\begin{align}
		\label{basic:eq:intS}
		\frac{d}{dt} \int_{c.v.} \rho_s dV =
			-\int_{S_{cv}} \rho\,U_{rn} \, dA
	\end{align}
</div> <!-- end empheq --> 

Equation \eqref{basic:eq:intS} is essentially accounting of
the mass that is the change is result of the in an out flow.
The negative sign in surface integral is because flow out marked
positive which reduces of the mass (negative derivative).

<div class="figContainer" id ="mass:fig:cvPipe">
	<div class="imgContainer">
		<figure>
			<img src="images/cvPipe.png" alt="Pressure Lines Static Fluid">
				<div class="caption">
Fig. ??? Schematics of flow in in pipe with varying density as a function time for example \ref{mass:ex:mercury
				</div >
		</figure>
	</div >
</div >
.} 
The next example is provided to illustrate this concept.
<div class="TEx" id="mass:ex:mercury">
<h4>Example 3.1</h4>

</div > <!-- end class ETx-->
{\rho_0} = \left( 1 - 
			\frac{x}{L} \right)^2 \cos {\frac{t}{t_0}}.
	\end{align}
</div> 
	The conduit shown in Figure  <a href="mass:fig:cvPipe">(???)</a> length
	is $L$ and its area is $A$.
	Express the mass flow in and/or out, and the mass in the
	conduit as a function of time.
	Write the expression for the mass change in the pipe.
}
<div class="solutionT">

	Here it is very convenient to choose a non-deformable control volume
	that is inside the conduit $dV$ is chosen as $\pi\,R^2\,dx$.
	Using equation \eqref{basic:eq:intS}, the flow out (or in) is
	<div class="longEq"> 
\begin{align}
		 \frac{d}{dt}  \int_{c.v.} \rho dV = 
			\frac{d}{dt} \int_{c.v.} 
				\overbrace{\rho_0 \left(1 - \frac{x}{L} \right)^2 
				\cos\left( \frac{t}{t_0} \right)} ^{\rho(t)}
			\overbrace{\pi\,R^2\, dx}^{dV} 
	\end{align}
</div>
	The density is not a function of radius, $r$ and angle,
		$\theta$ and they can be taken out the integral as  
	<div class="longEq"> 
\begin{align}
		 \frac{d}{dt}  \int_{c.v.} \rho dV = 
			\pi\,R^2 \frac{d}{dt} \int_{c.v.} \rho_0 
			\left( 1 - \frac{x}{L} \right)^2 \cos \left(
				\frac{t}{t_0}\right) dx
	\end{align}
</div>
	which results in
	<div class="longEq"> 
\begin{align}
		\mbox{\bf Flow Out} = \overbrace{ \pi\,R^2}^{A}
			\frac{d}{dt} \int_{0}^{L} \rho_0 \left(1-\frac{x}{L}\right)^2 
			\cos {\frac{t}{t_0}} dx =
		- \frac{\pi\,R^2\,L\, \rho_0}{3\,t_0} \, 
				\sin\left( \dfrac{t}{t_0} \right)
	\end{align}
</div>
	The flow out is a function of length, $L$, and time, $t$, and is the
	change of the mass in the control volume.
</div>




When the control volume is fixed with time, the derivative
in equation \eqref{basic:eq:intS} can enter the integral
since the boundaries are fixed in time and hence,
<div class="uselBoxEq">
	<h3 class="eqHead" >Continuity with Fixed b.c.</h3>
	\begin{align}
		\label{basic:eq:intSrho}
		\int_{V_{c.v.}} \frac{d\,\rho}{dt}  dV =
		-\int_{S_{c.v.}} \rho\,U_{rn} \, dA
	\end{align}
</div> <!-- end empheq --> 

Equation \eqref{basic:eq:intSrho} is simpler
than equation \eqref{basic:eq:intS}.

In deformable control volume, the left hand side of
question \eqref{basic:eq:intS} can be examined further to
develop a simpler equation by using the extend Leibniz
integral rule for a constant density and result in
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\frac{d}{dt} \int_{c.v.}\rho \,dV = 
	\overbrace{\int_{c.v.} 
		\overbrace{\frac{d\,\rho}{dt}}^{=0} \,dV }^{{thus, =0}} +
	\rho\,\int_{S_{c.v.}}  \hat{n} \cdot  U_{b}\, dA =
	\rho\,\int_{S_{c.v.}}  U_{bn}\, dA
   \label{basic:eq:dbasict}
\end{align}
</div> <!-- end long equation -->
where $U_b$ is the boundary velocity and $U_{bn}$
is the normal component of the boundary velocity.
<div class="uselBoxEq">
	<h3 class="eqHead" >Steady State Continuity Deformable</h3>
	\begin{align}
		\label{basic:eq:cvmV}
		\int_{S_{c.v.}} U_{bn}\, dA = \int_{S_{c.v.}}   U_{rn}\, dA 
	\end{align}
</div> <!-- end empheq --> 

The meaning of the equation \eqref{basic:eq:cvmV} is the net growth
(or decrease) of the Control volume is by net volume flow into it.
Example \ref{basic:ex:balloon} illustrates this point. 

<div class="TEx" id="basic:ex:balloon">
<h4>Example 3.2</h4>
..
</div > <!-- end class ETx-->

<div class="solutionT">

The applicable equation is 
<div class="longEq"> 
\begin{align}
	 \int_{c.v.} U_{bn} \, dA = 
	\int_{c.v.} U_{rn}\, dA  
\end{align}
</div>
The entrance is fixed, thus the relative velocity, $U_{rn}$ is
<div class="longEq"> 
\begin{align}
	U_{rn} =
		\left\{
			\begin{array}{ccc}
				-U_p &@ \;\mbox{the valve} &\\
				0 & \mbox{every else} &  
			\end{array}
		\right.
\end{align}
</div>
Assume equal distribution of the velocity in balloon surface and that the
center of the balloon is moving, thus the velocity has the following form
<div class="longEq"> 
\begin{align}
	U_b = U_x\,\hat{x} + U_{br}\,\hat{r}
\end{align}
</div>
Where $\hat{x}$ is unit coordinate in $x$ direction and
$U_x$ is the velocity of the center and where $\hat{r}$
is unit coordinate in radius from the center of the
balloon and $U_{br}$ is the velocity in that direction.
The right side of equation \eqref{basic:eq:cvmV}
is the net change due to the boundary is 
<div class="longEq"> 
\begin{align}
	\int_{S_{c.v.}} \left( U_x\,\hat{x} + U_{br}\,\hat{r} \right)
		\cdot \hat{n}\, dA
	= \overbrace{\int_{S_{c.v.}} 
		\left( U_x\,\hat{x} \right) 
		\cdot \hat{n}\, dA}^{\mbox{center movement}} +
	\overbrace{\int_{S_{c.v.}} \left( U_{br}\,\hat{r} \right) 
	\cdot \hat{n}\, dA}^{\mbox{net boundary change}}
\end{align}
</div>
The first integral is zero because it is like
movement of solid body and also yield this value
mathematically (excises for mathematical oriented student).
The second integral  (notice $\hat{n} = \hat{r}$) yields 
<div class="longEq"> 
\begin{align}
	\int_{S_{c.v.}} \left( U_{br}\,\hat{r} \right) \cdot \hat{n}\, dA =
	{4\,\pi\, r^2}\,{U_{br}}
\end{align}
</div>
Substituting into the general equation yields
<div class="longEq"> 
\begin{align}
	\rho\, \overbrace{4\,\pi\, r^2}^{A}{U_{br}} = \rho\,U_p\,A_p = m_i 
\end{align}
</div>
Hence,
<div class="longEq"> 
\begin{align}
	U_{br} = \frac{m_i}{\rho\,4\,\pi\, r^2}
\end{align}
</div>
The center velocity is (also) exactly $U_{br}$.
The total velocity of boundary is 
<div class="longEq"> 
\begin{align}
	U_t = \frac{m_i}{\rho\,4\,\pi\, r^2} \left( \hat{x} + \hat{r}\right)
\end{align}
</div>
It can be noticed that the velocity at the opposite to the
connection to the rigid pipe which is double of the center velocity.
</div>

<h4 class="subsubsec" id="sec3.3.2.1">3.3.2.1 One&mdash;Dimensional Control Volume</h4>


Additional simplification of the continuity
equation is of one dimensional flow.
This simplification provides very useful
description for many fluid flow phenomena.
The main assumption made in this model is that the proprieties  
in the across section are only function of $x$ coordinate.
This assumptions leads 
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:1Dim}
	\int_{A_2} \rho_2\,U_2\, dA - 
	\int_{A_1} \rho_1\,U_1\, dA =  \frac{d}{dt} \int_{V(x)} \rho(x)\,\overbrace{A(x) \, dx}^{dV}
\end{align}
</div> <!-- end long equation -->
When the density can be considered constant
equation \eqref{basic:eq:1Dim} is reduced to
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:1DimMay}
	\int_{A_2} U_2\, dA - 
	\int_{A_1} U_1\, dA =  \frac{d}{dt} \int A(x) dx
\end{align}
</div> <!-- end long equation -->
For steady state but with variations of the velocity and variations
of the density reduces equation \eqref{basic:eq:1Dim} to read
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:1DimSteadyState}
	\int_{A_2} \rho_2\,U_2\,dA = \int_{A_1} \rho_1\,U_1\,dA
\end{align}
</div> <!-- end long equation -->
For steady state and uniform density  and velocity equation \eqref{basic:eq:1DimSteadyState} reduces further to
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:1DconstantRho}
	\rho_1\,A_1\,U_1 = \rho_2\,A_2\,U_2
\end{align}
</div> <!-- end long equation -->
For incompressible flow (constant density),
continuity equation is at its minimum form of
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:1DconstantRhoIncompressible}
	U_1\,A_1 = A_2\,U_2
\end{align}
</div> <!-- end long equation -->

<h3 class="subsec" id="sec3.3.3">3.3.3 Reynolds Transport Theorem</h3>

<div class="invisible"> <!-- Index hock -->
	<a name="basicFluid:sub:sub6">Reynolds Transport Theorem</a>"
</div>  <!-- End of Index -->


It can be noticed that the same derivations carried
for the density can be carried for other intensive
properties such as specific entropy, specific enthalpy.
Suppose that $f$ is intensive property (which can be
a scalar or a vector) undergoes change with time.
The change of accumulative property will be then
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:intensiveProperty}
	\frac{D}{Dt} \int_{sys} f\,\rho dV = 
	\frac{d}{dt} \int_{c.v.} f\,\rho dV +
	\int_{c.v} f\,\rho\,U_{rn} dA
\end{align}
</div> <!-- end long equation -->
<div class="invisible"> <!-- Index hock -->
	<a name="basicFluid:aut:aut0">Reynolds, Osborne</a>"
</div>  <!-- End of Index -->

<div class="invisible"> <!-- Index hock -->
	<a name="basicFluid:aut:aut1">Leibniz</a>"
</div>  <!-- End of Index -->

This theorem named after Reynolds, Osborne, (1842-1912)  which is
actually a three dimensional generalization of
Leibniz integral rule<div class = "px-5" ></div >  <!-- end of "px-5"-- >
<div class="container">
	<button type="button" class="btn btn-primary"
		data-toggle="collapse" data-target="#demo">Footnote #2
	</button>
	<div id="demo" class="collapse"> 
These papers can be read on-line at 
		http://www.archive.org/details/papersonmechanic01reynrich.
	</div>
</div>
. 
<div class="invisible"> <!-- Index hock -->
	<a name="basicFluid:sub:sub7">Leibniz integral rule</a>"
</div>  <!-- End of Index -->
 
To make the previous derivation clearer, the Reynolds
Transport Theorem will be reproofed and discussed.
The ideas are the similar but extended some what.

Leibniz integral rule<div class = "px-5" ></div >  <!-- end of "px-5"-- >
<div class="container">
	<button type="button" class="btn btn-primary"
		data-toggle="collapse" data-target="#demo">Footnote #2
	</button>
	<div id="demo" class="collapse"> 
This material is not necessarily but
is added her for completeness.
This author provides this material just given so no questions will be
		asked.
	</div>
</div>
 is an one dimensional and it is defined as 
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:Leibniz}
	\frac{d}{dy} \,\int_{x_1(y)}^{x_2(y)}f(x,y) \, dx =
		\int_{x_1(y)}^{x_2(y)}\frac {\partial f}{\partial y}\,dx
	+	f(x_2,y)\, \frac{dx_2}{dy} - f(x_1,y)\, \frac{dx_1}{dy}
\end{align}
</div> <!-- end long equation -->
Initially, a proof will be provided and
the physical meaning will be explained.
Assume that there is a function that satisfy the following
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:aF}
	G(x,y)= \int^x f\left ( \alpha,\,y \right) \,d\alpha 
\end{align}
</div> <!-- end long equation -->
Notice that lower boundary of the integral is
missing and is only the upper limit of the
function is present<div class = "px-5" ></div >  <!-- end of "px-5"-- >
<div class="container">
	<button type="button" class="btn btn-primary"
		data-toggle="collapse" data-target="#demo">Footnote #2
	</button>
	<div id="demo" class="collapse"> 
There was a suggestion to insert
arbitrary constant which will be canceled and 
will a provide rigorous proof.
This is engineering book and thus, the exact
mathematical proof is not the concern here.
Nevertheless, if there will be a demand
		for such, it will be provided.
	</div>
</div>
.
For its derivative of equation \eqref{basic:eq:aF} is 
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:daF}
	f(x,y) = \frac{\partial G}{\partial x}
\end{align}
</div> <!-- end long equation -->
differentiating (chain rule $d\,uv = u\,dv+v\,du$)
by part of left hand side of the Leibniz integral
rule (it can be shown which are identical) is
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:daFm}
	\frac{d\, \left[G(x_2,y)-G(x_1,y) \right] }{dy}
	= \overbrace{\frac{\partial G}{\partial x_2} \frac{dx_2}{dy}}^{1}  +
	  \overbrace{\frac{\partial G}{\partial y}(x_2,y)}^{2} -
	  \overbrace{\frac{\partial G}{\partial x_1}\frac{dx_1}{dy}}^{3} -
	  \overbrace{\frac{\partial G}{\partial y}(x_1,y)}^{4}
\end{align}
</div> <!-- end long equation -->
The terms 2 and 4 in equation \eqref{basic:eq:daFm} are
actually (the $x_2$ is treated as a different variable)
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:2and4}
	\frac{\partial G}{\partial y}(x_2,y) -
	\frac{\partial G}{\partial y}(x_1,y) = 
	\int_{x_1(y)}^{x_2(y)}\frac{\partial\,f(x,y) }{\partial y}\,dx
\end{align}
</div> <!-- end long equation -->
The first term (1) in equation \eqref{basic:eq:daFm} is 
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:term1}
	\frac{\partial G}{\partial x_2} \frac{dx_2}{dy} = 
		f(x_2,y)\, \frac{dx_2}{dy} 
\end{align}
</div> <!-- end long equation -->
The same can be said for the third term (3).
Thus this explanation is a proof the Leibniz rule.

The above &ldquo;proof&rdquo; is mathematical in nature
and physical explanation is also provided.
Suppose that a fluid is flowing in a conduit.
The intensive property, $f$ is investigated
or the accumulative property, $F$.
The interesting information that commonly needed is
the change of the accumulative property, $F$, with time.
The change with time is
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:F}
	\frac{DF}{Dt} = \frac{D}{Dt} \int_{sys} \rho\, f\, dV  
\end{align}
</div> <!-- end long equation -->
For one dimensional situation the change with time is 
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:F1D}
	\frac{DF}{Dt} = \frac{D}{Dt} \int_{sys} \rho\, f\, A(x) dx  
\end{align}
</div> <!-- end long equation -->
If two limiting points (for the one dimensional)
are moving with a different coordinate system, the
mass will be different and it will not be a system.
This limiting condition is the control volume
for which some of the mass will leave or enter.
Since the change is very short (differential), the
flow in (or out) will be the velocity of fluid
minus the boundary at $x_1$, $U_{rn} = U_1 - U_b$.
The same can be said for the other side.
The accumulative flow of the property in, $F$, is then 
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:flowOut1D1}
	F_{in} = \overbrace{f_1\,\rho}^{F_1}\,
		\overbrace{ U_{rn}}^{\frac{dx_1}{dt}}   
\end{align}
</div> <!-- end long equation -->
The accumulative flow of the property out, $F$, is then 
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:flowOut1D}
	F_{out} = 
	\overbrace{f_2\,\rho}^{F_2}\,
		\overbrace{ U_{rn}}^{\frac{dx_2}{dt}}   
\end{align}
</div> <!-- end long equation -->
The change with time of the accumulative
property, $F$, between the boundaries is
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:cvD}
	\frac{d}{dt} \int_{c.v.} \rho(x) \,f\,A(x)\,dA 
\end{align}
</div> <!-- end long equation -->
When put together it brings back the Leibniz integral rule.
Since the time variable, $t$, is arbitrary
and it can be replaced by any letter.
The above discussion is one of the
physical meaning of the Leibniz' rule.

Reynolds Transport theorem is a generalization of the
Leibniz rule and thus the same arguments are used.
The only difference is that the velocity has three components and
only the perpendicular component enters into the calculations.

<div class="uselBoxEq">
	<h3 class="eqHead" >Reynolds Transport</h3>
	\begin{align}
		\label{basic:eq:RT}
		\frac{D}{DT} \int _{sys} f\, \rho dV = 
		\frac{d}{dt} \int_{c.v} f\,\rho \, dV  + \int_{S_{c.v.}}
			f\,\rho \, U_{rn}\, dA 
	\end{align}
</div> <!-- end empheq --> 


<div class="figContainer" id ="mass:fig:pLines">
	<div class="imgContainer">
		<figure>
			<img src="images/cvPipe.png" alt="Pressure lines Static Fluid">
				<div class="caption">
Fig. 3.2 Pressure lines a static fluid with a constant density.
				</div >
		</figure>
	</div >
</div >
\mkSeclog{f}{mass:fig:pLines} 	{Pressure lines a static fluid with a constant density} 	{Pressure lines; isopressure}

<div class="TEx" id="mass:ex:cylinderBalloon">
<h4>Example 3.3</h4>
.......
</div > <!-- end class ETx-->


<div class="solutionT">

The applicable equation is
<div class="longEq"> 
\begin{align}
	\overbrace{\int_{V_{c.v}}\frac{d\rho}{dt}\, dV}
		^{\mbox{increase pressure}}
	+ 
	\overbrace{\int_{S_{c.v.}} \rho \, U_{b} dV}
		^{\mbox{boundary velocity}} 
		= \overbrace{\int_{S_{c.v.}} \rho U_{rn}
			\,dA}^{\mbox{in or out flow rate}}  
\end{align}
</div>
Every term in the above equation is analyzed but first the equation
of state and volume to pressure relationship have to be provided.
<div class="longEq"> 
\begin{align}
	\rho = \frac{P}{R\,T}
\end{align}
</div>
and relationship between the volume and pressure is
<div class="longEq"> 
\begin{align}
	P = f \, \pi\, {R_c}^2
\end{align}
</div>
Where $R_c$ is the instantaneous cylinder radius.
Combining the above two equations results in
<div class="longEq"> 
\begin{align}
	\rho = \frac{f \, \pi\, {R_c}^2}{R\,T}
\end{align}
</div>
Where $f$  is a coefficient with the right dimension.
It also can be noticed that boundary velocity
is related to the radius in the following form
<div class="longEq"> 
\begin{align}
	 U_b = \frac{dR_c}{dt}
\end{align}
</div>
The first term requires to find the derivative
of density with respect to time which is
<div class="longEq"> 
\begin{align}
	\frac{d\rho}{dt} = 
	\frac{d}{dt} \left(   \frac{f \, \pi\, {R_c}^2}{R\,T} \right)  
	=  \frac{2\,f\,\pi\,R_c}{R\,T} \overbrace{\frac{dR_c}{dt}}^{U_b}
\end{align}
</div> 
Thus, the first term is 
<div class="longEq"> 
\begin{align}
	\int_{V_{c.v}}\frac{d\rho}{dt}\, \overbrace{dV}^{2\,\pi\,R_c} =
	\int_{V_{c.v}} \frac{2\,f\,\pi\,R_c}{R\,T} U_b 
		\overbrace{dV}^{2\,\pi\,R_c\,dR_c}  =  
	 \frac{4\,f\,\pi^2\,{R_c}^3}{3\,R\,T} U_b
\end{align}
</div>
The integral can be carried when $U_b$ is independent of the 
$R_c$<div class = "px-5" ></div >  <!-- end of "px-5"-- >
<div class="container">
	<button type="button" class="btn btn-primary"
		data-toggle="collapse" data-target="#demo">Footnote #2
	</button>
	<div id="demo" class="collapse"> 
The proof of this idea is based on the chain
differentiation similar to Leibniz rule.
		When the derivative of the second part is $dU_b/dR_c =0$.
	</div>
</div>
 
The second term is 
<div class="longEq"> 
\begin{align}
	\int_{S_{c.v.}} \rho \, U_{b} dA =
		\overbrace{\frac{f \, \pi\, {R_c}^2}{R\,T}}^{\rho}\, U_b \,
	\overbrace{2\,\pi R_c}^{A} 
	= \left(\frac{f \, \pi^3\, {R_c}^2}{R\,T}\right) U_b 
\end{align}
</div>
substituting in the governing equation obtained the form of
<div class="longEq"> 
\begin{align}
	\frac{f\,\pi^2\,{R_c}^3}{R\,T} U_b + \frac{4\,f \, \pi^2\, {R_c}^3}{3\,R\,T}\, U_b = m_i 
\end{align}
</div>
The boundary velocity is then
<div class="longEq"> 
\begin{align}
	U_b = \dfrac{m_i}{\dfrac{7\,f \, \pi^2\, {R_c}^3}{3\,R\,T} }
	G = \dfrac{3\,m_i\,R\,T} {7\,f \, \pi^2\, {R_c}^3} 
\end{align}
</div>
</div>
<div class="TEx" id="mass:ex:balloonRho">
<h4>Example 3.4</h4>
.....
</div > <!-- end class ETx-->

<div class="solutionT">

The question is more complicated than Example \ref{mass:ex:balloonRho}.
% and additional equation is needed. 
The ideal gas law is
<div class="longEq"> 
\begin{align}
	\rho = \frac{P}{R\,T}
\end{align}
</div>
The relationship between the pressure and volume is 
<div class="longEq"> 
\begin{align}
	P = f_v \, V = \frac{4 \, f_v \,\pi\, {R_b}^3 }{3}
\end{align}
</div>
The combining of the ideal gas law with the relationship between the
pressure and volume results
<div class="longEq"> 
\begin{align}
	\rho =  \dfrac{4 \, f_v \,\pi\, {R_b}^3 }{3\,R\,T}
\end{align}
</div>
The applicable equation is
<div class="longEq"> 
\begin{align}
	\int_{V_{c.v}}\frac{d\rho}{dt}\, dV + \int_{S_{c.v.}} \rho \, \left(U_c\,\hat{x} + U_{b}\hat{r} \right) dA
		= \int_{S_{c.v.}} \rho U_{rn} \,dA
\end{align}
</div>
The right hand side of the above equation is 
<div class="longEq"> 
\begin{align}
	 \int_{S_{c.v.}} \rho U_{rn} \,dA = m_i
\end{align}
</div>
The density change is 
<div class="longEq"> 
\begin{align}
	\frac{d\rho} {dt} = \frac{12\,f_v\,\pi\,{R_b}^2}{R\,T} 
		\overbrace{\frac{dR_b}{dt}}^{U_b}
\end{align}
</div>
The first term is 
<div class="longEq"> 
\begin{align}
	\int_{0}^{R_b} \overbrace{\frac{12\,f_v\,\pi\,{R_b}^2}{R\,T}\, {U_b}}
		^{\neq f(r)} \overbrace{4\,\pi\,r^2\,dr}^{dV} =
	\frac{16\,f_v\,\pi^2\,{R_b}^5}{3\,R\,T}\, {U_b}
\end{align}
</div>
The second term is 
<div class="longEq"> 
\begin{align}
	\int_{A} \dfrac{4 \, f_v \,\pi\, {R_b}^3 }{3\,R\,T} \,U_b\, dA =
	\dfrac{4 \, f_v \,\pi\, {R_b}^3 }{3\,R\,T}
		\,U_b\, \overbrace{4\,\pi\,{R_b}^2}^{A}
	= \dfrac{8 \, f_v \,\pi^2\, {R_b}^5 }{3\,R\,T}  \,U_b
\end{align}
</div>
Subsisting the two equations of the applicable equation results  
<div class="longEq"> 
\begin{align}
	U_b =  \frac{1}{8} \dfrac{m_i\,R\,T}{  f_v \,\pi^2\, {R_b}^5} 
\end{align}
</div>
Notice that first term is used to increase the
pressure and second the change of the boundary.
</div>

\section[Momentum Conservation]{Momentum Conservation 
		\label{basic:sec:momCons}}
<div class="invisible"> <!-- Index hock -->
	<a name="basicFluid:sub:sub8">Momentum Conservation</a>"
</div>  <!-- End of Index -->


In the previous section, the Reynolds Transport
Theorem (RTT) was applied to mass conservation.
Mass is a scalar (quantity without magnitude).
This section deals with momentum conservation which is a vector.
The Reynolds Transport Theorem (RTT) can be applicable
to any quantity and hence can be apply to vectors.
Newton's second law for a single body can apply to multiply body
system which further extended to continuous infinitesimal elements.
In analysis the Newton's law, it is common to differentiate
the external forces into body forces, surface forces.
In many problems, the main body force is the
gravity which acts on all the system elements.

The surface forces are divided into two categories:
one perpendicular to the surface and one in the surface plane.
Thus, it can be written as
<div class="longEq"> <!-- begin long equation -->
\begin{align}
	\label{basic:eq:cFs}
	\sum \pmb{F}_s =
	\int _{c.v.} \pmb{S_n}\,dA +  \int _{c.v.} \boldsymbol{\tau} \,dA 
\end{align}
</div> <!-- end long equation -->
<div class="nomen">
	<div class="NMNrow">
		<span class="symbol"><a name="basicFluid:nmn:nmn0">$\boldsymbol{\tau}$</a> :
		</span>
		<span class="depiction">
				The shear stress Tenser
		</span>
	</div> <!-- nomenclature row -->























































































































	<div class="NMNrow">
		<span class="symbol"><a name="basicFluid:nmn:nmn1">$\mathfrak{M}$</a> :
		</span>
		<span class="depiction">
				Angular Momentum
		</span>
	</div> <!-- nomenclature row -->





































































































































	<div class="NMNrow">
		<span class="symbol"><a name="basicFluid:nmn:nmn2">$k_T$</a> :
		</span>
		<span class="depiction">
				Fluid thermal conductivity
		</span>
	</div> <!-- nomenclature row -->




































































































































































































































































































































































































































































































































































































































































































































