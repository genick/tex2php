<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Free Gas Dynamics Textbook </title>
	<link rel="stylesheet" href="css/test.css">
	<script src="js/enquire.min.js"></script> 
	<script src="js/jquery-2.1.4.min.js"></script>
<script type="text/x-mathjax-config"> MathJax.Hub.Register.StartupHook("TeX Jax Ready",function () { MathJax.Hub.Insert(MathJax.InputJax.TeX.Definitions.macros,{
cancel: ["Extension","cancel"], bcancel: ["Extension","cancel"], xcancel: ["Extension","cancel"], cancelto: ["Extension","cancel"] });  }); </script>
<script type="text/x-mathjax-config"> MathJax.Hub.Config({ tex2jax: {inlineMath: [["$","$"],["\\(","\\)"]]} }); </script>
<script type="text/x-mathjax-config">MathJax.Hub.Config({ TeX: { equationNumbers: {autoNumber: "all"},  Macros: {  RRRR: '{\bf R}',  bold: ['\boldsymbol{#1}',1],  bbb: ['\\mathbf{#1}',1]    }, extensions: ["color.js"]  } }); </script>
<script type="text/javascript" src="../MathJax/MathJax.js?config=TeX-AMS_HTML"></script>
<script type="text/javascript">function toggle(obj) { var obj=document.getElementById(obj);	if (obj.style.display == "block") obj.style.display = "none";		else obj.style.display = "block";}</script>
<!-- <script type="text/javascript" async -->
<!--   src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX&#45;MML&#45;AM_CHTML"> -->
<!-- </script> -->
<!-- <script type="text/x&#45;mathjax&#45;config">                               -->
<!-- 	MathJax.Hub.Register.StartupHook("TeX Jax Ready",function () { " -->
<!-- 		MathJax.Hub.Insert(MathJax.InputJax.TeX.Definitions.macros,{         -->
<!-- 			cancel:	  ["Extension","cancel"],                                  -->
<!-- 			bcancel:  ["Extension","cancel"],                                 -->
<!-- 			xcancel:  ["Extension","cancel"],                                -->
<!-- 			cancelto: ["Extension","cancel"]                                 -->
<!-- 		});                                                                   -->
<!-- 	};                        -->
<!-- <script type="text/javascript" async -->
<!--   src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX&#45;AMS&#45;MML_CHTML"> -->
<!-- </script> -->

	<!-- <link rel="stylesheet" href="css/jquery&#45;ui.css"> -->
	<!-- <script src="js/jquery&#45;ui.js"></script> -->
</head>
	<header>
		<?php
		// 	// do php stuff
		// 	require('navigation.php');
		?>
	</header>
<body>
	<main>
	<?php
		require('thermo.php');
	?>
	</main>
	<footer>
		<?php
			// require('footerContent.php');
		?>
	</footer>
	<script type="text/javascript" src="js/local.js"> </script>
</body>
</html>
