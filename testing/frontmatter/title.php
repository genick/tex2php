<!--  to insert the following message into the postcript file and pdffiles. -->
						 
<div id="frontArea">
	<div class="mainFixedLength">
		<div class="mainContainer ">
			<div class="centered noticed ">
				<h1>
					Fundamentals of Compressible Fluid Mechanics
				</h1>
				<h2> 
					Genick Bar--Meir, Ph.D. 
				</h2>
				<div> Version 0.4.8.5  </div>
				<div>7449 North Washtenaw Ave </div> 
				<div>Chicago, IL 60645 </div>
				<div> email: barmeir at gmail dot com </div>
				Copyright &copy; 2016, 2013, 2012, 2009, 2008,
				2007, 2006, 2005, and 2004 by Genick Bar-Meir 
				See the file copying.fdl or copyright.tex for copying conditions.
			</div>
			<?php
				require("frontmatter/moto.php"); 
			?>	
		</div>
	</div>
</div>
<?php
	// require('frontmatter/exampleList.php');
?>
