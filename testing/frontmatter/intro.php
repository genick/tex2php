<p>
<p>
<p>
<p>
<p>

<h2 class='Chapter'><a name="#s 1"></a> Chapter  1 Introduction</h2> 
<p>
<p>
<p>

<h2 class='sub'><a name="#s 1.1"></a> Section  1.1 What is Compressible Flow?</h2> 
<p>
		{intro:sec:whatIsGD}
<p>
This book deals with an introduction<sup><a href="#fn1" id="ref1">1</a></sup> to the flow of compressible substances (gases).
The main difference between compressible flow and almost
incompressible flow is not the fact that compressibility has to
be considered.
Rather, the difference is in two phenomena that
do not exist in incompressible flow<sup><a href="#fn2" id="ref2">2</a></sup>.
<p class="hidden"> <a href="index.php" id="intro.php#Hydraulic0" title="Index to to Hydraulic Jump">1s</a></p>
<p class="hidden"> <a href="index.php" id="intro.php#Discontinuity0" title="Index to to Discontinuity">2s</a></p>

The first phenomenon is the very sharp discontinuity (jump)
in the flow in properties.
The second phenomenon is the choking of the flow.
Choking is when downstream variations don't effect the
flow<sup><a href="#fn3" id="ref3">3</a></sup>.
Though choking occurs in certain pipe flows in astronomy, there also
are situations of choking in general
(external) flow<sup><a href="#fn4" id="ref4">4</a></sup>.
Choking is referred to as the situation where downstream conditions,
which are beyond a critical value(s), doesn't affect the flow.
<p>
The shock wave and choking are not intuitive for most people.
However, one has to realize that <span class='underline'>intuition</span> '>intuition</span>on where one uses his past experiences to predict other
situations.
Here one has to learn to use his intuition as a tool for future use. 
Thus, not only aeronautic engineers, but other engineers, and
even manufacturing engineers will be able use this &ldquo;intuition&rdquo;
in design and even research.
<p>

<h2 class='sub'><a name="#s 1.2"></a> Section  1.2 Why Compressible Flow is Important?</h2> 
<p>
<p>
Compressible flow appears in many natural and many technological processes.
Compressible flow deals with more than air, including steam,
natural gas, nitrogen and helium, etc.
For instance, the flow of natural gas in a pipe system, 
a common method of heating in the U.S., should be considered 
a compressible flow. 
These processes include the flow of gas in the exhaust system
of an internal combustion engine, and also gas turbine,
a problem that led to the Fanno flow model. 
The above flows that were mentioned are called internal flows. 
Compressible flow also includes flow around bodies such as the wings
of an airplane, and is considered an external flow.
<p>
These processes include situations 
not expected to have a compressible flow, such as manufacturing
process such as the die casting, injection molding. 
The die casting process is a process in which liquid metal,
mostly aluminum, is injected into a mold to obtain a near final shape. 
The air is displaced by the liquid metal in a very rapid manner,
in a matter of milliseconds, therefore the compressibility has
to be taken into account.
<p>
Clearly, Aero Engineers are not the only ones  who have to deal with
some aspect of compressible flow. 
For manufacturing engineers there are many situations where
the compressibility or compressible flow understating is essential
for adequate design.  
For instance, the control engineers who are using pneumatic systems
use compressed substances. 
The cooling of some manufacturing systems and design of refrigeration
systems also utilizes compressed air flow knowledge.
Some aspects of these systems require consideration of the unique
phenomena of compressible flow.
<p>
Traditionally, most gas dynamics (compressible flow) classes deal
mostly with shock waves and external flow and briefly teach
Fanno flows and Rayleigh flows (two kind of choking flows).
There are very few courses that deal with isothermal flow.
In fact, many books on compressible flow ignore the isothermal 
flow<sup><a href="#fn5" id="ref5">5</a></sup>.
<p class="hidden"> <a href="index.php" id="intro.php#Isothermal0" title="Index to to Isothermal Flow">3s</a></p>

<p>
In this book, a greater emphasis is on the internal flow.
This doesn't in any way meant that the important topics
such as shock wave and oblique shock wave should be neglected.
This book contains several chapters which deal with external flow
as well.
<p>

<h2 class='sub'><a name="#s 1.3"></a> Section  1.3 Historical Background</h2> 
<p>
<p>
<p class="hidden"> <a href="index.php" id="intro.php#Rouse0" title="Index to to Rouse, Hunter">1a</a></p>

In writing this book it became clear that there is more unknown and
unwritten about the history of compressible fluid than known.
While there are excellent books about the history of fluid mechanics
(hydraulic) see for example book by Rouse<sup><a href="#fn6" id="ref6">6</a></sup>.
There are numerous sources dealing with the
history of flight and airplanes (aeronautic)<sup><a href="#fn7" id="ref7">7</a></sup>.
Aeronautics is an overlapping
part of compressible flow, however these two fields are different.
For example, the Fanno flow and isothermal flow, which are the
core of gas dynamics, are not part of aerodynamics.
Possible reasons for the lack of written documentation
are one, a large part of this knowledge is relatively new,
and two, for many early contributors this topic was a side issue.  
In fact, only one contributor of the three main models of internal 
compressible flow (Isothermal, Fanno, Rayleigh)
was described by any text book.
This was Lord Rayleigh, for whom the Rayleigh flow was named.
The other two models were, to the undersigned, unknown.
Furthermore, this author did not find any reference to isothermal
flow model
<p class="hidden"> <a href="index.php" id="intro.php#Isothermal1" title="Index to to Isothermal Flow">3s</a></p>

earlier to Shapiro's book.<p class="hidden"> <a href="index.php" id="intro.php#Shapiro0" title="Index to to Shapiro, Ascher">2a</a></p>

There is no book<sup><a href="#fn8" id="ref8">8</a></sup>
that describes the history of these models.
For instance, the question, who was Fanno, and when did he live,
could not be answered by any of the undersigned's colleagues in
University of Minnesota or elsewhere.
<p>
At this stage there are more questions about the history
of compressible flow needing to be answered. 
Sometimes, these questions will appear in a section with a title but
without text or with only a little text. 
Sometimes, they will appear in a footnote like this<sup><a href="#fn9" id="ref9">9</a></sup>.
<p class="hidden"> <a href="index.php" id="intro.php#Shapiro0" title="Index to to Shapiro Flow">4s</a></p>

For example, it is obvious that Shapiro published the erroneous
conclusion that all the chocking occurred at in his article
which contradicts his isothermal model.  
Additional example, who was the first to &ldquo;conclude&rdquo; the
&ldquo;all&rdquo; the chocking occurs at? Is it Shapiro?
<p>
Originally, there was no idea that there are special effects
and phenomena of compressible flow.
Some researchers even have suggested that compressibility can be
&ldquo;swallowed&rdquo; into the ideal flow (Euler's equation's flow is
sometimes referred to as ideal flow).
Even before Prandtl's idea of boundary layer appeared,
the significant and importance of compressibility emerged.
 
In the first half of nineteen century there was little realization that the
compressibility is important because there were very little applications (if
any) that required the understanding of this phenomenon. As there were no
motivations to investigate the shock wave or choked flow both were treated
as the same, taking compressible flow as if it were incompressible flow.
<p>
It must be noted that researchers were interested in the speed of
sound even long before applications and knowledge could demand any utilization.
The research and interest in the speed of sound was a purely academic interest.
The early application in which compressibility has a major effect was with fire arms.
The technological improvements in fire arms led to a gun capable of
shooting bullets at speeds approaching to the speed of sound.
Thus, researchers were aware that the speed of sound is some kind of limit.
<p>
<p class="hidden"> <a href="index.php" id="intro.php#Mach0" title="Index to to Mach, Ernest">3a</a></p>

<p class="hidden"> <a href="index.php" id="intro.php#Fliegner0" title="Index to to Fliegner, Schweizer Bauztg">4a</a></p>

In the second half of the nineteen century, Mach and Fliegner &ldquo;stumbled&rdquo;
over the shock wave and choking, respectively.
Mach observed shock and Fliegner measured the choking but
theoretical science did not provide explanation for it (or
was award that there is an explanation for it.). 
<p>
In the twentieth century the flight industry became the pushing force.
Understandably, aerospace engineering played a significant role in
the development of this knowledge.
Giants like Prandtl <p class="hidden"> <a href="index.php" id="intro.php#Prandtl0" title="Index to to Prandtl, Ludwig">5a</a></p>

and his students like Von Karman<p class="hidden"> <a href="index.php" id="intro.php#VonKarman0" title="Index to to Von Karman, Theodore">6a</a></p>
, as well as
others like Shapiro <p class="hidden"> <a href="index.php" id="intro.php#Shapiro1" title="Index to to Shapiro, Ascher">6a</a></p>
, dominated the field.
During that time, the modern basic classes became &ldquo;solidified.&rdquo;
Contributions by researchers and educators from other fields were
not as dominant and significant, so almost all text books in this
field are written from an aerodynamic prospective.
<p>
<p>
<p>

<h2 class='subsub'><a name="#s 1.3.1"></a> Section  1.3.1 Early Developments</h2> 
 
<p>
The compressible flow is a subset of fluid mechanics/hydraulics and therefore the knowledge
development followed the understanding of incompressible flow.
Early contributors were motivated from a purely intellectual
curiosity, while most later contributions were driven by necessity.
As a result, for a long time the question of the speed of sound was
bounced around.
<p>

<h2 class='subsubsub'><a name="#s*"></a> Section * Speed of Sound</h2> 
<p>
<p>
The idea that there is a speed of sound <p class="hidden"> <a href="index.php" id="intro.php#Speed0" title="Index to to Speed of sound">5s</a></p>

and that it can be measured is a major achievement.
A possible explanation to this discovery lies in the fact that 
mother nature exhibits in every thunder storm the difference between
the speed of light and the speed of sound.
There is no clear evidence as to who came up with this concept,
but some attribute it to Galileo Galilei: 166x.
<p class="hidden"> <a href="index.php" id="intro.php#Galilei0" title="Index to to Galilei, Galileo ">7a</a></p>

Galileo, an Italian scientist, was one of the earliest contributors
to our understanding of sound.
Dealing with the difference between the two speeds (light, sound)
was a major part of Galileo's work.
However, once there was a realization that sound can be measured,
people found that sound travels in different speeds through
different mediums.
The early approach to the speed of sound was by the measuring
of the speed of sound. 
<p>
Other milestones in the speed of sound understanding development were
by Leonardo Da Vinci,
<p class="hidden"> <a href="index.php" id="intro.php#DaVinci0" title="Index to to Da Vinci, Leonardo">8a</a></p>

who discovered that sound travels in waves (1500). 
Marin Mersenne  was the first to measure the speed of sound in air (1640).
<p class="hidden"> <a href="index.php" id="intro.php#Mersenne0" title="Index to to Mersenne, Marin">9a</a></p>

Robert Boyle discovered that sound waves must travel in a medium (1660) and this lead
to the concept that sound is a pressure change.<p class="hidden"> <a href="index.php" id="intro.php#Boyle0" title="Index to to Boyle, Robert">10a</a></p>
 
Newton<p class="hidden"> <a href="index.php" id="intro.php#Newton0" title="Index to to Newton, Isaac">11a</a></p>
 was the first to formulate a relationship
between the speed of sound in gases by relating the density and
compressibility in a medium (by assuming isothermal process).
Newton's equation is missing the heat ratio, (late 1660's).
Maxwell was the first to derive the speed of sound for gas as
from particles (statistical) mechanics.<p class="hidden"> <a href="index.php" id="intro.php#Maxwell0" title="Index to to Maxwell, James Clerk">12a</a></p>

Therefore some referred to coefficient as Maxwell's coefficient.
<p class="hidden"> <a href="index.php" id="intro.php#Maxwell's0" title="Index to to Maxwell's coefficient">6s</a></p>

<p>

<h2 class='subsub'><a name="#s 1.3.2"></a> Section  1.3.2 The shock wave puzzle</h2> 
<p>
<p class="hidden"> <a href="index.php" id="intro.php#Leibniz0" title="Index to to Leibniz, Gottfried Wilhelm">13a</a></p>
<p class="hidden"> <a href="index.php" id="intro.php#Moody0" title="Index to to Moody, Lewis Ferry">14a</a></p>
<p class="hidden"> <a href="index.php" id="intro.php#Science0" title="Index to to Science disputes">7s</a></p>

Here is where the politics of science was a major obstacle to
achieving an advancement<sup><a href="#fn10" id="ref10">10</a></sup>.
not giving the due credit to Rouse.<p class="hidden"> <a href="index.php" id="intro.php#Rouse1" title="Index to to Rouse, Hunter">14a</a></p>

In the early 18xx, conservation of energy was a concept that
was applied only to mechanical energy.
On the other side, a different group of scientists dealt with calorimetry (Internal energy). 
It was easier to publish articles about the second law of 
thermodynamics than to convince anyone of the first law of thermodynamics.
Neither of these groups would agree to &ldquo;merge&rdquo; or &ldquo;relinquish&rdquo;
control of their &ldquo;territory&rdquo; to the other.
It took about a century to establish the first law<sup><a href="#fn11" id="ref11">11</a></sup>.
<p>
<p class="hidden"> <a href="index.php" id="intro.php#Poisson0" title="Index to to Poisson, Sim&egrave;on Denis ">15a</a></p>

At first, Poisson found a &ldquo;solution&rdquo; to the
Euler's equations with certain boundary conditions 
which required discontinuity<sup><a href="#fn12" id="ref12">12</a></sup>
which had obtained an implicit form in 1808.
Poisson showed that solutions could approach a discontinuity 
by using conservation of mass and momentum.
He had then correctly derived the jump conditions that discontinuous
solutions must satisfy.
Later, Challis <p class="hidden"> <a href="index.php" id="intro.php#Challis0" title="Index to to Challis, James">16a</a></p>
 had noticed contradictions
concerning some solutions of the equations of compressible gas
dynamics<sup><a href="#fn13" id="ref13">13</a></sup>.
Again the &ldquo;jumping&rdquo; conditions were redeveloped by
two different researchers independently:
Stokes and Riemann.
<p class="hidden"> <a href="index.php" id="intro.php#Stokes0" title="Index to to Stokes, George">17a</a></p>

<p class="hidden"> <a href="index.php" id="intro.php#Riemann0" title="Index to to Riemann, Bernhard">18a</a></p>

Riemann, in his 1860 thesis, was not sure whether or not
discontinuity is only a mathematical creature or a real creature.
Stokes in 1848 retreated from his work and wrote an apology on
his &ldquo;mistake.&rdquo;<sup><a href="#fn14" id="ref14">14</a></sup>
Stokes was convinced by Lord Rayleigh
<p class="hidden"> <a href="index.php" id="intro.php#Rayleigh0" title="Index to to Rayleigh, John William Strutt">19a</a></p>
 
<p class="hidden"> <a href="index.php" id="intro.php#Kelvin0" title="Index to to Kelvin, William Thomson">20a</a></p>
 
and Lord Kelvin that he was mistaken on the grounds that energy is conserved
(not realizing the concept of internal energy).
<p class="hidden"> <a href="index.php" id="intro.php#Internal0" title="Index to to Internal energy">8s</a></p>

<p>
At this stage some experimental evidence was needed.
Ernest Mach studied several fields in physics and also studied philosophy.
He was mostly interested in experimental physics.
The major breakthrough in the understanding of compressible flow came
when Ernest Mach
<p class="hidden"> <a href="index.php" id="intro.php#Mach1" title="Index to to Mach, Ernest">20a</a></p>

&ldquo;stumbled&rdquo; over the discontinuity.
It is widely believed that Mach had done his research as purely intellectual research.
His research centered on optic aspects which lead him to study
acoustic and therefore supersonic flow (high speed, since no Mach
number was known at that time).
However, it is logical to believe that his interest had risen due to
the need to achieve powerful/long&mdashdistance shooting rifles/guns.
At that time many inventions dealt with machine guns
which were able to shoot more bullets per minute.
At the time, one anecdotal story suggests a way to make money by
inventing a better killing machine for the Europeans.
While the machine gun turned out to be a good killing machine,
defense techniques started to appear such as sand bags.
A need for bullets that could travel faster to overcome these obstacles was created.
Therefore, Mach's paper from 1876 deals with the flow around bullets.
Nevertheless, no known<sup><a href="#fn15" id="ref15">15</a></sup>
equations or explanations resulted from these experiments.
<p>
Mach used his knowledge in Optics to study the flow around bullets.
What makes Mach's achievement all the more remarkable was
the technique he used to take the historic photograph:
He employed an innovative approach called the shadowgraph.
He was the first to photograph the shock wave.
In his paper discussing &ldquo;Photographische Fixierung der durch
Projektile in der Luft eingeleiten Vorgange&rdquo; he showed a picture
of a shock wave (see Figure  1.7).
He utilized the variations of the air density to clearly show
shock line at the front of the bullet.
Mach had good understanding of the fundamentals of supersonic flow
and the effects on bullet movement (supersonic flow).<p class="hidden"> <a href="index.php" id="intro.php#Mach's0" title="Index to to Mach's bullet">9s</a></p>

Mach's paper from 1876 demonstrated shock wave (discontinuity) and
suggested the importance of the ratio of the velocity to the speed of sound.
He also observed the existence of a conical shock wave
(oblique shock wave).
<p>
Mach's contributions can be summarized as providing an
experimental proof to discontinuity.
He further showed that the discontinuity occurs at
and realized that the velocity ratio (Mach number), and not the 
velocity, is the important parameter in the study of the compressible flow.
Thus, he brought confidence to the theoreticians to publish their studies. 
While Mach proved shock wave and oblique shock wave existence,
he was not able to analyze it (neither was he
aware of Poisson's
<p class="hidden"> <a href="index.php" id="intro.php#Poisson1" title="Index to to Poisson">20a</a></p>

work or the works of others.).
<p>
Back to the pencil and paper, the jump conditions were redeveloped and now named after
Rankine<sup><a href="#fn16" id="ref16">16</a></sup>
<p class="hidden"> <a href="index.php" id="intro.php#Rankine0" title="Index to to Rankine, John Macquorn">21a</a></p>

and Hugoniot<sup><a href="#fn17" id="ref17">17</a></sup>.
Rankine and Hugoniot, redeveloped independently the equation
that governs the relationship of the shock wave.
Shock was assumed to be one dimensional and mass, momentum,
and energy equations<sup><a href="#fn18" id="ref18">18</a></sup>
<p class="hidden"> <a href="index.php" id="intro.php#Hugoniot0" title="Index to to Hugoniot, Pierre Henri">22a</a></p>

lead to a solution which ties the upstream and downstream properties.
What they could not prove or find was that shock occurs
only when upstream is supersonic, i.e., direction of the flow.
Later, others expanded Rankine-Hugoniot's conditions to a more general
form<sup><a href="#fn19" id="ref19">19</a></sup>.
<p>
Here, the second law has been around for over 40 years and yet 
the significance of it was not was well established.
Thus, it took over 50 years for Prandtl to arrive at and to
demonstrate that the shock has only one direction<sup><a href="#fn20" id="ref20">20</a></sup>.
<p class="hidden"> <a href="index.php" id="intro.php#Taylor0" title="Index to to Taylor, G. I.">23a</a></p>

Today this equation/condition is known as Prandtl's equation or
condition (1908).
In fact Prandtl is the one who introduced the name of
Rankine-Hugoniot's conditions not aware of the earlier
developments of this condition.
Theodor Meyer
<p class="hidden"> <a href="index.php" id="intro.php#Meyer0" title="Index to to Meyer, Theodor">24a</a></p>

(Prandtl's student)
derived the conditions for oblique shock in
1908<sup><a href="#fn21" id="ref21">21</a></sup> as a byproduct
of the expansion work. 
<p>
<p>

<figure>
<img src="images/shock.png" alt="Control Volume and System After and  During Motion" >
<figcaption>Fig.  1.1 The shock as a connection of Fanno and Rayleigh lines     after Stodola, Steam and Gas Turbine. </figcaption>
</figure>
Stodola <p class="hidden"> <a href="index.php" id="intro.php#Fanno0" title="Index to to Fanno, Gino Girolamo">25a</a></p>
 <p class="hidden"> <a href="index.php" id="intro.php#Stodola0" title="Index to to Stodola, Aurel">26a</a></p>

(Fanno's adviser) realized that the shock
is the intersection of the Fanno line with the Rayleigh line.
<p class="hidden"> <a href="index.php" id="intro.php#Intersection0" title="Index to to Intersection of Fanno and Rayleigh lines">10s</a></p>

Yet, the supersonic branch is missing from his understanding
(see Figure  1.1). 
In fact, Stodola suggested the graphical solution utilizing the Fanno line.
<p>
The fact that the conditions and direction
were known did not bring the solution to the equations.
The &ldquo;last nail&rdquo; of understanding
was put by Landau, a Jewish scientist who worked in Moscow University 
in the 1960's during the Communist regimes. 
A solution was found by Landau \& Lifshitz
and expanded by Kolosnitsyn \& Stanyukovich (1984).<p class="hidden"> <a href="index.php" id="intro.php#Kolosnitsyn0" title="Index to to Kolosnitsyn, N. I.">27a</a></p>
<p class="hidden"> <a href="index.php" id="intro.php#Stanyukovich0" title="Index to to Stanyukovich, K. P.">28a</a></p>
<p class="hidden"> <a href="index.php" id="intro.php#Landau0" title="Index to to Landau, Lev">29a</a></p>

The model suggests that the choking occurs at
and it appears that Shapiro was the first one to realize this
difference compared to the other models. 
In reality, the flow is choked somewhere between to
one for cases that are between Fanno (adiabatic) and isothermal flow.
This fact was evident in industrial applications
where the expectation of the choking is at Mach one, but
can be explained by choking at a lower Mach number. 
No experimental evidence, known by the undersigned, was ever
produced to verify this finding.
<p>

<h2 class='subsub'><a name="#s 1.3.4"></a> Section  1.3.4 External flow</h2> 
<p>
<p class="hidden"> <a href="index.php" id="intro.php#External0" title="Index to to External flow">11s</a></p>

<p>
 of circulatory
flow (1894), and the Kutta-Joukowski circulation theory
<p class="hidden"> <a href="index.php" id="intro.php#Kutta-Joukowski0" title="Index to to Kutta-Joukowski circulation theory">12s</a></p>

<p class="hidden"> <a href="index.php" id="intro.php#Kutta0" title="Index to to Kutta, Martin Wilhelm">30a</a></p>
\index{Joukowski, Nikolai}
of lift  (1906).
Practitioners like the Wright brothers
<p class="hidden"> <a href="index.php" id="intro.php#Wright0" title="Index to to Wright, brothers">31a</a></p>

relied upon experimentation to figure out what theory could
not yet tell them.
<p>
Ludwig Prandtl <p class="hidden"> <a href="index.php" id="intro.php#Prandtl1" title="Index to to Prandtl, Ludwig">31a</a></p>
 in 1904 explained the two
most important causes of drag by introducing the boundary layer theory.
Prandtl's boundary layer theory allowed various simplifications of the
Navier-Stokes equations.
Prandtl worked on calculating the effect of induced drag on lift.
He introduced the <span class='it'>lifting line theory</span>, theory</span>ished in
1918-1919 and enabled accurate calculations of induced
drag and its effect on lift<sup><a href="#fn22" id="ref22">22</a></sup>.
<p class="hidden"> <a href="index.php" id="intro.php#Lanchester0" title="Index to to Lanchester, Frederick">32a</a></p>

<p class="hidden"> <a href="index.php" id="intro.php#Lanchester&mdashPrandtl0" title="Index to to Lanchester&mdashPrandtl theory">13s</a></p>

<p>
During World War I,  Prandtl created his thin&mdashairfoil theory
that enabled the calculation of lift for thin, cambered airfoils.
<p class="hidden"> <a href="index.php" id="intro.php#Thin-airfoil0" title="Index to to Thin-airfoil theory">14s</a></p>

He later contributed to the Prandtl&mdashGlauert rule for subsonic
airflow that describes the compressibility effects of air at high speeds.
Prandtl's student, Von Karman reduced the equations for supersonic
flow into a single equation.
<p>
After the First World War aviation became important and in the 
1920s a push of research focused on what was called the <span class='emph'>compressibility 
problem</span>.ty 
problem</span>ssibility problem}
Airplanes could not yet fly fast, but the propellers (which are
also airfoils) did exceed the speed of sound, especially at the
propeller tips, thus exhibiting inefficiency.
Frank Caldwell and Elisha Fales demonstrated in 1918 that at a
critical speed (later renamed the <span class='emph'>critical Mach number</span>)Mach number</span>al Mach number}
airfoils suffered dramatic increases in drag and decreases in lift.
Later, Briggs and Dryden showed that the problem was
related to<p class="hidden"> <a href="index.php" id="intro.php#Briggs0" title="Index to to Briggs, Lyman James">33a</a></p>

the shock wave.<p class="hidden"> <a href="index.php" id="intro.php#Dryden0" title="Index to to Dryden, Hugh Latimer ">34a</a></p>

Meanwhile in Germany, one of Prandtl's assistants, J.  
Ackeret,<p class="hidden"> <a href="index.php" id="intro.php#Ackeret0" title="Index to to Ackeret, Jakob">35a</a></p>
 
simplified the shock equations so that they became easy to use.
After World War Two, the research had continued and some technical
solutions were found.
Some of the solutions lead to tedious calculations which lead to the
creation of Computational Fluid Dynamics (CFD).
Today these methods of perturbations and asymptotic are hardly used
in wing calculations<sup><a href="#fn23" id="ref23">23</a></sup>.
That is the &ldquo;dinosaur<sup><a href="#fn24" id="ref24">24</a></sup>&rdquo; reason
that even today some instructors are teaching mostly
the perturbations and asymptotic methods in Gas Dynamics classes.
<p>
More information on external flow 
can be found in , John D.~Anderson's Book<p class="hidden"> <a href="index.php" id="intro.php#Anderson0" title="Index to to Anderson, John">36a</a></p>

&ldquo;History of Aerodynamics and Its Impact on Flying Machines,&rdquo;
Cambridge University Press, 1997.
<p>

<h2 class='subsub'><a name="#s 1.3.5"></a> Section  1.3.5 Filling and Evacuating Gaseous Chambers</h2> 
<p>
<p>
It is remarkable that there were so few contributions made
in the area of a filling or evacuation gaseous chamber.
The earlier work dealing with this issue was by Giffen, 1940, and was
republished by Owczarek, J. A., the model and solution to<p class="hidden"> <a href="index.php" id="intro.php#Owczarek0" title="Index to to Owczarek, J. A.">37a</a></p>

the nozzle attached to chamber issue in his book &ldquo;Fundamentals
of Gas Dynamics.&rdquo;<sup><a href="#fn25" id="ref25">25</a></sup>.
He also extended the model to include the unchoked case.
Later several researchers mostly from the University in Illinois
extended this work to isothermal nozzle (choked and unchoked). 
<p>
The simplest model of nozzle, is not sufficient in many cases
and a connection by a tube (rather just nozzle or orifice) is more appropriated.
Since World War II considerable works have been carried out
in this area but with very little progress<sup><a href="#fn26" id="ref26">26</a></sup>.   
In 1993 the first reasonable models for forced volume were
published by the undersigned.
Later, that model was extended by several research groups,
The analytical solution for forced volume and the &ldquo;<span class='emph'>balloon</span>'ph'>balloon</span> problem) model were published first
in this book (version 0.35) in 2005.
The classification of filling or evacuating the chamber as 
external control and internal control (mostly by pressure)
was described in version 0.3 of this book by this author.
<p>

<h2 class='subsub'><a name="#s 1.3.6"></a> Section  1.3.6 Biographies of Major Figures</h2> 
<p>
<p>

<figure>
<img src="images/galileo.png" alt="Galileo Portrait" >
<figcaption>Fig.  1.5 Portrait of Galileo Galilei. </figcaption>
</figure>

<h2 class='subsubsub'><a name="#s*"></a> Section * Galileo Galilei</h2> 
<p>
<p class="hidden"> <a href="index.php" id="intro.php#Galileo0" title="Index to to Galileo, Galilei">38a</a></p>

Galileo was born in Pisa, Italy on February 15, 1564 to musician
Vincenzo Galilei and Giulia degli Ammannati.
The oldest of six children, Galileo moved with his family in early 1570 to Florence.
Galileo started his studying at the University of Pisa in 1581.
He then became a professor of mathematics at the University of Padua in 1592.
During the time after his study, he made numerous discoveries such
as that of the pendulum clock, (1602).
Galileo also proved that objects fell with the same velocity
regardless of their size.
<p>
Galileo  had a relationship with Marina Gamba (they never married)<p class="hidden"> <a href="index.php" id="intro.php#Gamba0" title="Index to to Gamba, Marina">39a</a></p>

who lived and worked in his house in Padua, where she bore him three children.
However, this relationship did not last and Marina married Giovanni
Bartoluzzi and Galileo's son, Vincenzio, joined him in Florence 
(1613).
<p>
Galileo invented many mechanical devices such as the pump and the telescope (1609).  
His telescopes helped him make many astronomic observations which
proved the Copernican system.
Galileo's observations got him into trouble with the Catholic Church,
however, because of his noble ancestry, the church was not harsh with him.
Galileo was convicted after publishing his book <span class='emph'>Dialogue</span>,h'>Dialogue</span>er house arrest for the remainder of his life.
Galileo died in {1642} in his home outside of Florence.
<p>

<h2 class='subsubsub'><a name="#s*"></a> Section * Ernest Mach (1838-1916)</h2> 
<p>
<p class="hidden"> <a href="index.php" id="intro.php#Mach2" title="Index to to Mach, Ernest">39a</a></p>


<figure>
<img src="images/EMach.png" alt="Mach Photo" >
<figcaption>Fig.  1.6 Photo of Ernest Mach. </figcaption>
</figure>
{
Ernest Mach was born in 1838 in Chrlice (now part of Brno), when
Czechia was still a part of the Austro&mdashHungary empire.
Johann, Mach's father, was a high school teacher who taught Ernest at
home until he was 14, when he studied in Kromeriz Gymnasium, before
he entered the university of Vienna were he studies mathematics,
physics and philosophy.
He graduated from Vienna in 1860.
There Mach wrote his thesis &ldquo;On Electrical Discharge and Induction.&rdquo;
Mach was interested also in physiology of sensory perception.
<p>
<p>
<p>

At first he received a professorship position at Graz in mathematics 
(1864) and was then offered a position as a professor of surgery
at the university of Salzburg, but he declined.
He then turned to physics, and in 1867 he received a position in
the Technical University in Prague<sup><a href="#fn27" id="ref27">27</a></sup> 
where he taught experimental physics for the next 28 years.
<p>

<figure>
<img src="images/bullet.png" alt="Mach Bullet" >
<figcaption>Fig.  1.7 The photo of a bullet in a supersonic flow which was taken by Mach.Note it was not taken in a wind tunnel </figcaption>
</figure>
<p class="hidden"> <a href="index.php" id="intro.php#Mach's1" title="Index to to Mach's bullet">14s</a></p>

<p>
Mach was also a great thinker/philosopher and influenced
the theory of relativity dealing with frame of reference.
In 1863, Ernest Mach (1836 - 1916) published Die
Machanik in which he formalized this argument.
Later, Einstein was greatly influenced by it, and in 1918,<p class="hidden"> <a href="index.php" id="intro.php#Einstein0" title="Index to to Einstein, Albert">40a</a></p>

he named it <span class='emph'> Mach's Principle</span>.s Principle</span>e of the primary sources of inspiration for Einstein's
theory of General Relativity.
<p>
 
Mach's revolutionary experiment demonstrated the existence of the
shock wave as shown in Figure  1.7.
It is amazing that Mach was able to photograph the phenomenon using
the spinning arm technique (no wind tunnel was available at that time
and most definitely nothing that could take a photo at supersonic speeds.
His experiments required exact timing.
He was not able to attach the camera to the arm and utilize the
remote control (not existent at that time).
Mach's shadowgraph technique and a related method called 
<span class='emph'>Schlieren Photography</span> Photography</span>ay.
<p>
Yet, Mach's contributions to supersonic flow were not limited to
experimental methods alone.
Mach understood the basic characteristics of external supersonic flow
where the most important variable affecting the flow
is the ratio of the speed of the flow<sup><a href="#fn28" id="ref28">28</a></sup> (U) relative to
the speed of sound (c).
Mach was the first to note the transition that occurs 
when the ratio U/c goes from being less than 1 to greater than 1.
The name Mach Number (M) was coined by J.~Ackeret (Prandtl's
student)  in 1932 in honor of Mach.
<p>

<h2 class='subsubsub'><a name="#s*"></a> Section * John William Strutt (Lord Rayleigh)</h2> 
<p>
<p class="hidden"> <a href="index.php" id="intro.php#Rayleigh1" title="Index to to Rayleigh, John William Strutt">40a</a></p>


<figure>
<img src="images/rayleigh.png" alt="Rayleigh Portrait" >
<figcaption>Fig.  1.8 Lord Rayleigh portrait. </figcaption>
</figure>
the Earl of Balfour (of the famous Balfour declaration of the<p class="hidden"> <a href="index.php" id="intro.php#Balfour0" title="Index to to Balfour, Earl">41a</a></p>

Jewish state). 
They had three sons, the eldest of whom was to become a
professor of physics at the Imperial College of
Science and Technology, London.
<p>
As a successor to James Clerk Maxwell, he was head of the Cavendish<p class="hidden"> <a href="index.php" id="intro.php#Maxwell1" title="Index to to Maxwell, James Clerk">41a</a></p>

Laboratory at Cambridge from 1879-1884, and in 1887 became Professor
of Natural Philosophy at the Royal Institute of Great Britain.
Rayleigh died on June 30, 1919 at Witham, Essex.
<p>

<h2 class='subsubsub'><a name="#s*"></a> Section * William John Macquorn Rankine</h2> 
<p>

<figure>
<img src="images/rankine.png" alt="Rankine Photo" >
<figcaption>Fig.  1.9 Portrait of Rankine. </figcaption>
</figure>

<figure>
<img src="images/fanno.png" alt="Fanno Photo" >
<figcaption>Fig.  1.4 The photo of Gino Fanno approximately in 1950. </figcaption>
</figure>

<h2 class='subsubsub'><a name="#s*"></a> Section * Gino Girolamo Fanno</h2> 
<p>
Fanno a Jewish Engineer was born on November 18, 1888.
He studied in a technical institute in Venice and graduated with
very high grades as a mechanical engineer.
Fanno was not as lucky as his brother, who was able to get into academia. 
Faced with anti&mdashsemitism, Fanno left Italy for Zurich, Switzerland
in 1900 to attend graduate school for his master's degree.
In this new place he was able to pose as a Roman Catholic,
even though for short time he went to live in a Jewish home,
Isaak Baruch Weil's family. 
As were many Jews at that time, Fanno was fluent in several languages
including Italian, English, German, and French.
He likely had a good knowledge of Yiddish and possibly some Hebrew.
Consequently, he did not have a problem studying in a different
language.
In July 1904 he received his diploma (master).
When one of Professor Stodola's assistants
attended military service this temporary position was offered to Fanno.
&ldquo;Why didn't a talented guy like Fanno keep or
obtain a position in academia after he published his model?&rdquo;
The answer is tied to the fact that somehow rumors about his roots
began to surface.
Additionally, the fact that his model was not a
&ldquo;smashing<sup><a href="#fn29" id="ref29">29</a></sup>
success&rdquo;  did not help.
<p>
 
Later Fanno had to go back to Italy to find a job in industry. 
Fanno turned out to be a good engineer and he later obtained
a management position.
He married, and like his brother, Marco, was childless.
He obtained a Ph.D. from Regian Istituto Superiore d'Ingegneria di Genova.
However, on February 1939 Fanno was degraded (denounced)
and he lost his Ph.D. (is this the first case in history)
because of his Jewish nationality<sup><a href="#fn30" id="ref30">30</a></sup>.
During the War (WWII), he had to be under house arrest to avoid being
sent to the &ldquo;vacation camps.&rdquo; 
To further camouflage himself, Fanno converted to Catholicism.
Apparently, Fanno had a cache of old Italian currency 
(which was apparently still highly acceptable) which
helped him and his wife survive the war.
After the war, Fanno was only able to work in agriculture and
agricultural engineering.
Fanno passed way in 1960 without world recognition for his model.
  
Fanno's older brother, mentioned earlier Marco Fanno is a famous economist
who later developed fundamentals of the supply and demand theory. 
<p>

<h2 class='subsubsub'><a name="#sNO VALUE TO REPLACE"></a> Section NO VALUE TO REPLACE Ludwig Prandtl</h2> 
<p class="hidden"> <a href="index.php" id="intro.php#Prandtl2" title="Index to to Prandtl, Ludwig">41a</a></p>

Perhaps Prandtl's greatest achievement was his ability to produce so
many great scientists. 
It is mind boggling to look at the long list of those who were
his students and colleagues.
There is no one who educated as many great scientists as Prandtl.
Prandtl changed the field of fluid mechanics and is called the modern
father of fluid mechanics because of his introduction of
boundary layer, turbulence mixing theories etc.
<p>

<figure>
<img src="images/prandtl.png" alt="Prandtl Photo" >
<figcaption>Fig.  1.11 Photo of Prandtl. </figcaption>
</figure>
<ul>
<li>
Prandtl number (heat transfer problems)
</li>
<li>
Prandtl-Glauert compressibility correction
</li>
<li>
Prandtl's boundary layer equation
</li>
<li>
Prandtl's lifting line theory
</li>
<li>
Prandtl's law of friction for smooth pipes
</li>
<li>
Prandtl-Meyer expansion fans (supersonic flow)
</li>
<li>
Prandtl's Mixing Length Concept (theory of turbulence) 
<\li>
<\ul>
<p>

<h2 class='subsubsub'><a name="#s*"></a> Section * Theodor Meyer</h2> 
<p>
<p class="hidden"> <a href="index.php" id="intro.php#Meyer1" title="Index to to Meyer, Theodor">41a</a></p>


<div class="figContainer">
	<div class="imgContainer">
		<img src="images/meyer.png" alt="Theodor Meyer's photo" >
		<div class="caption"> Fig A. The diagrams taken from Meyer's thesis.</div >
	<div class="imgContainer">
		<img src="images/meyerDiagram.png" alt="Theodor Meyer's photo" >
		<div class="caption"> Fig B. The photo and famous diagram of Theodor Meyer</div >
	<div class="clearBoth, center">Fig.  1.12 The diagram is taken from Meyer's dissertation showing the schematic of oblique shock and the schematic of Prandtl&mdashMeyer fan.</div >
</div >
<figcaption>Fig.  1.12 The diagram is taken from Meyer's dissertation showing the schematic of oblique shock and the schematic of Prandtl&mdashMeyer fan. </figcaption>
</figure>
Meyer<sup><a href="#fn31" id="ref31">31</a></sup> 
was Prandtl's student who in one dissertation was able to build large part of
base of the modern compressible flow.
The two chapters in this book, Prandtl&mdashMeyer flow and oblique shock are directly
based on his ideas.
Settles et al in their paper argues that this thesis is the most influential
dissertation in the entire field of fluid mechanics.
No matter if you accept this opinion, it must be the most fundamental thesis or work
in the field of compressible flow (about 20.08<p>
One of the questions that one can ask, what is
about Meyer's education that brought this success.
In his family, he was described as math genius who astonished his surroundings. 
What is so striking is the list of his instructors who include
Frobenius (Group theory), Helmert (theory of errors), Hettner
(chorology), Knoblauch , Lehmann-Filhes (orbit of double star),
Edmund Landau (number theory), F. Schottkyand (elliptic, abelian, and theta functions
and invented Schottky groups),  mathematicians Caratheodory (calculus of variations,
and measure theory), Herglotz (seismology),
Hilbert, Klein, Lexis, Runge (Runge&mdashKutta method)
and Zermelo (axiomatic set theory), Abraham (electron), Minkowski (mathematical base
for theory of relativity), Prandtl, and more.
This list demonstrates that Meyer had the best
education one can have at the turn of century.
It also suggest that moving between good universities (3 universities)
is a good way to absorb knowledge and good research skills.
This kind of education provided Meyer with the
tools to tackle the tough job of compressible flow.
<p>
What is interesting about his work is that Mach
number concept was not clear at that stage.
Thus, the calculations (many hand numerical calculations) were
complicated by this fact which further can magnify his achievement.
Even though the calculations where carried out in a narrow range.
Meyer's thesis is only 46 pages long but it include experimental evidence to prove
his theory in Prandtl&mdashMeyer function and oblique shock.
According to Settles, this work utilized Schlieren images getting quantitative
measurements probably for the first time.
Meyer also was the first one to look at the ratio of the static properties to 
the stagnation proprieties<sup><a href="#fn32" id="ref32">32</a></sup>. 
<p>
Ackeret attributed the oblique shock theory to Meyer but later this attribution
was dropped and very few books attribute this Meyer ( or even Prandtl).
Among the very few who got this right is this book.
The name Prandtl&mdashMeyer is used because some believe that Prandtl conceived
the concept and let Meyer to do the actual work.
This contribution is to the mythical Prandtl ability to &ldquo;solve&rdquo; equations without
doing the math. 
However, it is not clear that Prandtl indeed conceived or dealt with this issues besides 
reviewing Meyer ideas. 
What it is clear that the rigor mathematics is of Meyers and physical intuition
of Prandtl were present.
There is also a question of who came out with the &ldquo;method of characteristics,&rdquo;
Prandtl or Meyer. 
<p>
Meyer was the first individual to use the shock polar diagram. 
Due to his diagram, he was able to see the existence of the weak and strong shock.
Only in 1950, Thomson was able to see the third shock.
It is not clear why Meyer missed the third root. 
Perhaps, it was Prandtl influence because he saw only two solutions in the
experimental setting thus suggesting that only two solutions exists.
This suggestion perhaps provides an additional indication that Prandtl was involved heavily 
in Meyer's thesis. 
Meyer also noticed that the deflection angle has a maximum. 
<p>
Meyer was born to Theodor Meyer (the same name as his father)
in  July, 1882, and die March, 1972. 
Like Fanno, Meyer never was recognized for his contributions to fluid mechanics.
During the years after Second World War, he was aware that his thesis became a standard
material in every university in world. 
However, he never told his great achievements to
his neighbors or his family or colleagues in the high school where he  was teaching. 
One can only wonder why this field rejects very talented people.
Meyer used the symbol which is still used to this today for the function. 
<p>

<h2 class='subsubsub'><a name="#sNO VALUE TO REPLACE"></a> Section NO VALUE TO REPLACE E.R.G.~Eckert</h2> 

<figure>
<img src="images/eckertW.png" alt="Eckert with Bar-Meir Family" >
<figcaption>Fig. NO VALUE TO REPLACE The photo of Ernst Rudolf George Eckert with Bar-Meir's family Aug 1997. </figcaption>
</figure>

<h2 class='subsubsub'><a name="#sNO VALUE TO REPLACE"></a> Section NO VALUE TO REPLACE Ascher Shapiro</h2> 
<p class="hidden"> <a href="index.php" id="intro.php#Shapiro1" title="Index to to Shapiro, Ascher">14s</a></p>

<p>
MIT Professor Ascher Shapiro<sup><a href="#fn33" id="ref33">33</a></sup>, 
the Eckert equivalent for the compressible flow,
was instrumental in using his two volume book
&ldquo;The Dynamics of Thermodynamics of the Compressible Fluid Flow,&rdquo; 
to transform the gas dynamics field to a coherent text material
for engineers.
Furthermore, Shapiro's knowledge of fluid mechanics enabled him to &ldquo;sew&rdquo; the missing
parts of the Fanno line with Moody's diagram to create the most useful model
in compressible flow.
While Shapiro viewed gas dynamics mostly through aeronautic eyes, 
the undersigned believes that Shapiro was the first one to propose
an isothermal flow model that is not part of the aeronautic field.
Therefore it is being proposed to call this model Shapiro's Flow.
   
In his first 25 years Shapiro focused primarily on power production,
high-speed flight, turbomachinery and propulsion by jet engines and rockets.
Unfortunately for the field of Gas Dynamics, 
Shapiro moved to the field of biomedical engineering  where
he was able to pioneer new work.
Shapiro was instrumental in the
treatment of blood clots, asthma, emphysema and glaucoma.
<p>
Shapiro grew up in New York City and received his S.B. in 1938 and
the Sc.D. (It is M.I.T.'s equivalent of a Ph.D. degree) in 1946 in
mechanical engineering from MIT.
He was assistant professor in 1943, three years before receiving his
Sc.D.
In 1965 he became the Department of Mechanical Engineering head until 1974.
Shapiro spent most of his active years at MIT.
Ascher Shapiro passed way in November 2004.
<p>

<h2 class='subsubsub'><a name="#sNO VALUE TO REPLACE"></a> Section NO VALUE TO REPLACE Theodor Von Karman</h2> 
<p class="hidden"> <a href="index.php" id="intro.php#VonKarman0" title="Index to to Von Karman, Theodore">42a</a></p>
 
<p>
A brilliant scientist who was instrumental in constructing many of the equations
and building the American aviation and space exploration.  
Von Karman studied fluid mechanics under Prandtl and during that time he
saw another graduate student
that was attempting to build &ldquo;stable&rdquo; experiment what will not have the vortexes.
Von Karman recognized that this situation is inherently unstable and explained the
scientific reasons for this phenomenon.
Now this phenomenon known as Von Karman street. 
Among his achievement that every student study fluid mechanics is the development of
the integral equation of boundary layer. 
<p class="hidden"> <a href="index.php" id="intro.php#Von0" title="Index to to Von Karman integral equation">15s</a></p>

Von Karman, a descendant of a famous Rabi Rabbi Judah Loew ben Bezalel (HaMaharl) 
was born raised in Hungary. 
Later he move to Germany to study under Prandtl. 
After his graduation he taught at Gottingen and later as director of the Aeronautical
Institute at RWTH Aachen.
As a Jew realized that he has to leave Germany during the raise of the Nazi.
At 1930 he received offer and accept the directorship of a Laboratory at the
California Institute of Technology.
<p>
His achievement in the area of compressible flow area focused around supersonic
and rocketry. For example, he formulate the slender body equations to
describe the fluid field around rockets. Any modern air plane exhibits
the swept&mdashback wings the Von Karman was instrumental in recognizing its
importance. He construct with his student the Von Karman&mdashTsien compressibility
correction. The Karman&mdashTsien compressibility correction is a nonlinear
approximation for Mach number effects which works quite well when the
velocities are subsonic. This expression relates the incompressible values
to those in compressible flow. As his adviser, he left many students which
have continued his legacy like Tsien who build the Chinese missile industry.
<p class="hidden"> <a href="index.php" id="intro.php#Tsien0" title="Index to to Tsien, Hsue-Shen">43a</a></p>

<p>

<h2 class='subsubsub'><a name="#sNO VALUE TO REPLACE"></a> Section NO VALUE TO REPLACE Zeldovich, Yakov Borisovich</h2> 
<p class="hidden"> <a href="index.php" id="intro.php#Zeldovich0" title="Index to to Zeldovich, Yakov Borisovich">44a</a></p>

&ldquo;Before I meet you here I had thought, that you are a collective of authors, as
Burbaki&rdquo;
Stephen W. Hawking.
<p>
The statement of Hawking perhaps can illustrate a prolific physicist born in Minsk.
He played an important role in the development of Soviet nuclear and thermonuclear weapons.
His main contribution in the area compressible flow centered around the shock wave
and detonation and material related to cosmotology.
Zeldovich develop several reatlition for the limiting cases still in use today. 
For example he developed the ZND detonation model (where the leter &ldquo;Z&rdquo; stands
for Zeldovich).

Footnote:
<hr></hr>
<p> 1. This book is
gradually sliding to include more material that isn't so introductory. 
But an attempt is made to present the material in introductory
level.<a id="fn1" href="ref1" title= "Jump to footnote 1 in the text.">1</a></p>
<p> 2. It can be argued
that in open channel flow there is a hydraulic jump 
(discontinuity)
and in some ranges no effect of downstream conditions on the flow. 
However, the uniqueness of the phenomena in the gas dynamics provides
spectacular situations of a limited length (see Fanno model) and
thermal choking, etc. Further, there is no equivalent to oblique
shock wave.
Thus, this richness is unique to gas dynamics.<a id="fn2" href="ref2" title= "Jump to footnote 2 in the text.">2</a></p>
<p> 3. The thermal choking is somewhat different but
a similarity exists.<a id="fn3" href="ref3" title= "Jump to footnote 3 in the text.">3</a></p>
<p> 4. This book is
intended for engineers and therefore a discussion about astronomical
conditions isn't presented.<a id="fn4" href="ref4" title= "Jump to footnote 4 in the text.">4</a></p>
<p> 5. Any search on the web on classes of compressible flow
will show this fact and the undersigned can testify that this was true
in his first class as a student of compressible
flow.<a id="fn5" href="ref5" title= "Jump to footnote 5 in the text.">5</a></p>
<p> 6. Hunter Rouse and
Simon Inc, History of Hydraulics (Iowa City: Institute of Hydraulic
Research, 1957)<a id="fn6" href="ref6" title= "Jump to footnote 6 in the text.">6</a></p>
<p> 7. Anderson, J. D., Jr. 1997. A History of Aerodynamics: And Its Impact
on Flying Machines, Cambridge University Press, Cambridge, England.<a id="fn7" href="ref7" title= "Jump to footnote 7 in the text.">7</a></p>
<p> 8. The only remark found about
Fanno flow that it was taken from the Fanno Master thesis by his
adviser.
Here is a challenge: find any book describing the history of the
Fanno model.<a id="fn8" href="ref8" title= "Jump to footnote 8 in the text.">8</a></p>
<p> 9. Who
developed the isothermal model? The research so far leads to Shapiro.
Perhaps this flow  should be named after the Shapiro.
Is there any earlier reference to this model?<a id="fn9" href="ref9" title= "Jump to footnote 9 in the text.">9</a></p>
<p> 10. Amazingly, science is full of many stories
of conflicts and disputes.
Aside from the conflicts of scientists with the Catholic Church and Muslim religion,
perhaps the most famous is that of Newton's netscaping (stealing and
embracing) Leibniz['s] invention of calculus.
There are even conflicts from not giving enough credit, like Moody.
Even the undersigned encountered individuals who have tried to ride on his work.
The other kind of problem is &ldquo;hijacking&rdquo; by a sector.
Even on this subject, the Aeronautic sector &ldquo;took over&rdquo; gas
dynamics as did the emphasis on mathematics like perturbations
methods or asymptotic expansions instead on the physical phenomena. 
Major material like Fanno flow isn't taught in many classes, 
while many of the mathematical techniques are currently practiced.
So, these problems are more common than one might be expected.<a id="fn10" href="ref10" title= "Jump to footnote 10 in the text.">10</a></p>
<p> 11. This
recognition of the first law is today the most &ldquo;obvious&rdquo; for 
engineering students. 
Yet for many it was still debatable up to the middle of the nineteen century.<a id="fn11" href="ref11" title= "Jump to footnote 11 in the text.">11</a></p>
<p> 12. Sim&egrave;on Denis Poisson,
French mathematician, 1781-1840 worked in
Paris, France.  "M'emoire sur la th'eorie du son," J.
Ec. Polytech. 14 (1808), 319-392. From Classic Papers in Shock 
Compression Science, 3-65, High-press. Shock Compression Condens.
Matter, Springer, New York, 1998.<a id="fn12" href="ref12" title= "Jump to footnote 12 in the text.">12</a></p>
<p> 13. James Challis, English Astronomer,
1803-1882. worked at Cambridge, England UK.
&ldquo;On the velocity of sound,&rdquo; Philos. Mag. XXXII (1848), 494-499<a id="fn13" href="ref13" title= "Jump to footnote 13 in the text.">13</a></p>
<p> 14. Stokes George Gabriel Sir, Mathematical and Physical Papers,
Reprinted from the original journals and transactions, with
additional notes by the author. Cambridge, University Press, 1880-1905.<a id="fn14" href="ref14" title= "Jump to footnote 14 in the text.">14</a></p>
<p> 15. The words
&ldquo;no known&rdquo; refer to the undersigned.
It is possible that some insight was developed but none of
the documents that were reviewed revealed it to the undersigned.<a id="fn15" href="ref15" title= "Jump to footnote 15 in the text.">15</a></p>
<p> 16. William John Macquorn Rankine,
 Scottish engineer,
1820-1872. He worked in Glasgow, Scotland UK.
&ldquo;On the thermodynamic theory of waves of finite longitudinal
disturbance,&rdquo; Philos. Trans. 160 (1870), part II, 277-288.
Classic papers in shock compression science,
133-147, High-press. Shock Compression Condens.
Matter, Springer, New York, 1998<a id="fn16" href="ref16" title= "Jump to footnote 16 in the text.">16</a></p>
<p> 17. Pierre Henri Hugoniot, French engineer, 1851-1887.
&ldquo;Sur la propagation du mouvement dans les corps et sp'ecialement
dans les gaz parfaits, I, II&rdquo; J.  Ec.  
Polytech. 57 (1887), 3-97, 58 (1889), 1-125. Classic papers in shock
compression science, 161-243, 245-358, High-press.
Shock Compression Condens. Matter, Springer, New York, 1998<a id="fn17" href="ref17" title= "Jump to footnote 17 in the text.">17</a></p>
<p> 18. Today it is well established that
shock has three dimensions but small sections can be treated as
one dimensional.<a id="fn18" href="ref18" title= "Jump to footnote 18 in the text.">18</a></p>
<p> 19. To add discussion about the general relationships.<a id="fn19" href="ref19" title= "Jump to footnote 19 in the text.">19</a></p>
<p> 20. 
Some view the work of G.~I.~Taylor
from England as the proof (of course utilizing the second law)<a id="fn20" href="ref20" title= "Jump to footnote 20 in the text.">20</a></p>
<p> 21. Theodor Meyer in Mitteil. &uuml;b.
Forsch-Arb.  Berlin, 1908, No. 62, page 62.<a id="fn21" href="ref21" title= "Jump to footnote 21 in the text.">21</a></p>
<p> 22. 
The English call this theory the Lanchester&mdashPrandtl theory.
This is because the English Astronomer Frederick
Lanchester published the foundation for Prandtl's theory in
his 1907 book <span class='it'>Aerodynamics</span>.ynamics</span>tl claimed that he was not aware of Lanchester's model when he had
begun his work in 1911.
This claim seems reasonable in the light that Prandtl was not aware of earlier
works when he named erroneously the conditions for the shock wave.
See for the full story in the shock section.<a id="fn22" href="ref22" title= "Jump to footnote 22 in the text.">22</a></p>
<p> 23. This undersigned is aware of only one
case that these methods were really used to calculations of wing.<a id="fn23" href="ref23" title= "Jump to footnote 23 in the text.">23</a></p>
<p> 24. It is like teaching using slide ruler
in today school. By the way, slide rule is sold for about 7.5\$ on the net.
Yet, there is no reason to teach it in a regular school.<a id="fn24" href="ref24" title= "Jump to footnote 24 in the text.">24</a></p>
<p> 25. International Textbook Co., Scranton, Pennsylvania, 1964.<a id="fn25" href="ref25" title= "Jump to footnote 25 in the text.">25</a></p>
<p> 26. In fact, the
emergence of the CFD gave the illusion that there are solutions
at hand, not realizing that garbage in is garbage out, i.e.,
the model has to be based on scientific principles and not detached from reality.
As anecdotal story explaining the lack of progress,
in die casting conference there was a discussion and
presentation on which turbulence model is suitable for a
<span class='emph'>complete</span> h'>complete</span> &ldquo;strange&rdquo; models can be found in the undersigned's book
&ldquo;Fundamentals of Die Casting Design.<a id="fn26" href="ref26" title= "Jump to footnote 26 in the text.">26</a></p>
<p> 27. It is 
interesting to point out that Prague provided us two of the top 
influential researchers: E.~Mach and E.R.G.~Eckert.<a id="fn27" href="ref27" title= "Jump to footnote 27 in the text.">27</a></p>
<p> 28. Mach dealt with only air,
but it is reasonable to assume that he understood
that this ratio was applied to other gases.<a id="fn28" href="ref28" title= "Jump to footnote 28 in the text.">28</a></p>
<p> 29. Missing data about friction factor was major hindrance.<a id="fn29" href="ref29" title= "Jump to footnote 29 in the text.">29</a></p>
<p> 30. In some places, the ridicules claims that Jews
persecuted only because their religion. Clearly, Fanno was not part of the
Jewish religion (see his picture) only his nationality was Jewish.<a id="fn30" href="ref30" title= "Jump to footnote 30 in the text.">30</a></p>
<p> 31. This author is grateful to Dr. Settles and colleagues who wrote a very
informative article about Meyer as a 100 years anniversary to his thesis.
The material in this section was taken from 
Settles, G. S.,et al. &ldquo;Theodor Meyer&mdashLost pioneer of gas dynamics&rdquo;
Prog.~Aero space Sci(2009), doi:10.1016 j. paerosci.2009.06.001.
More information can be found in that article.<a id="fn31" href="ref31" title= "Jump to footnote 31 in the text.">31</a></p>
<p> 32. This issue is considered still open by this author.
It is not clear who use first and coin the term stagnation properties.<a id="fn32" href="ref32" title= "Jump to footnote 32 in the text.">32</a></p>
<p> 33. Parts taken from Sasha Brown, MIT<a id="fn33" href="ref33" title= "Jump to footnote 33 in the text.">33</a></p>
