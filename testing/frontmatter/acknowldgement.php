
<h1 class='chapter'> Chapter III CONTRIBUTOR LIST  </h1>

<h2 class='section'> Section III.1  How to contribute to this book  </h2>
<p class='justify'>
 
As a copylefted work, this book is open to revision and expansion by
any interested parties.
The only "catch" is that credit must be given where credit is due.
 This \textit{is} a copyrighted work:
it is \textit{not} in the public domain!

If you wish to cite portions of this book in a work of your own,
you must follow the same guidelines as for any other GDL
copyrighted work.

<h2 class='section'> Section III.2  Credits  </h2>
<p class='justify'>
 

All entries arranged in alphabetical order of surname.
Major contributions are listed by individual name with some detail
on the nature of the contribution(s), date, contact info, etc.
Minor contributions (typo corrections, etc.) are listed
by name only for reasons of brevity.
Please understand that when I classify a contribution as "minor,"
it is in no way inferior to the effort or value of a "major"
contribution, just smaller in the sense of less text changed.
Any and all contributions are gratefully accepted.
I am indebted to all those who have given freely of their own
knowledge, time, and resources to make this a better book!

<div class='list' >
<p class='justify'>
<ul type='1|a|A|i|I'>
<li>
	 <span class='bf'>Date(s) of contribution(s):</span>   2004 to present 
</li>
<li>
	 <span class='bf'>Nature of contribution:</span>   Original author. 
</li>
<li>
	 <span class='bf'>Contact at:</span>    <span class='tt'>barmeir at gmail.com</span>    

</li>
</ol>
</div>


<h3 class='subsection'>III.3 John Martones  </h3>
<p class='justify'>
<div class='list' >
<p class='justify'>
<ul type='1|a|A|i|I'>
<li>
	 <span class='bf'>Date(s) of contribution(s):</span>   June 2005 
</li>
<li>
	 <span class='bf'>Nature of contribution:</span>   HTML formatting,  
some error corrections.

</li>
</ol>
</div>

<h3 class='subsection'>III.4 Grigory Toker  </h3>
<p class='justify'>
<div class='list' >
<p class='justify'>
<ul type='1|a|A|i|I'>
<li>
	 <span class='bf'>Date(s) of contribution(s):</span>   August 2005 
</li>
<li>
	 <span class='bf'>Nature of contribution:</span>   Provided pictures 
	of the oblique shock for oblique shock chapter.

</li>
</ol>
</div>

<h3 class='subsection'>III.5 Ralph Menikoff  </h3>
<p class='justify'>
<div class='list' >
<p class='justify'>
<ul type='1|a|A|i|I'>
<li>
	 <span class='bf'>Date(s) of contribution(s):</span>   July 2005 
</li>
<li>
	 <span class='bf'>Nature of contribution:</span>   Some discussions about 
	the solution to oblique shock and about the Maximum Deflection 
	of the oblique shock. 

</li>
</ol>
</div>

<h3 class='subsection'>III.6 Domitien Rataaforret  </h3>
<p class='justify'>
<div class='list' >
<p class='justify'>
<ul type='1|a|A|i|I'>
<li>
	 <span class='bf'>Date(s) of contribution(s):</span>   Oct 2006 
</li>
<li>
	 <span class='bf'>Nature of contribution:</span>   Some discussions about 
	the French problem and help with the new wrapImg command. 

</li>
</ol>
</div>

<h3 class='subsection'>III.7 Gary Settles  </h3>
<p class='justify'>
<div class='list' >
<p class='justify'>
<ul type='1|a|A|i|I'>
<li>
	 <span class='bf'>Date(s) of contribution(s):</span>   Dec 2008, July 2009 
</li>
<li>
	 <span class='bf'>Nature of contribution:</span>   Four images for oblique shock two 
	dimensional, and cone flow. 
</li>
<li>
	 <span class='bf'>Nature of contribution:</span>   Information about T. Meyer --2009.  

</li>
</ol>
</div>


<h3 class='subsection'> Your name here </h3>
<p class='justify'>
<div class='list' >
<p class='justify'>
<ul type='1|a|A|i|I'>
<li>
	 <span class='bf'>Date(s) of contribution(s):</span>   Month and year of contribution 
</li>
<li>
	 <span class='bf'>Nature of contribution:</span>   Insert text here, describing how you 
	contributed to the book.
</li>
<li>
	 <span class='bf'>Contact at:</span>    <span class='tt'>my-email@provider.net</span>    

</li>
</ol>
</div>


<h3 class='subsection'>III.8 Typo corrections and "minor" contributions  </h3>
<p class='justify'>
<div class='list' >
<p class='justify'>
<ul type='1|a|A|i|I'>
<li>
	 <span class='bf'>H. Gohrah, Ph. D.</span>  ,   September 2005, some LaTeX issues.  
</li>
<li>
	 <span class='bf'>Roy Tate</span>   November 2006, Suggestions on improving English and grammar. 
</li>
<li>
	 <span class='bf'>Nancy Cohen</span>    <span class='bf'>Nancy Cohen</span>   2006, Suggestions on improving  
English and style for various issues.
</li>
<li>
	 <span class='bf'>Irene Tan</span>   2006, proof reading many chapters  
and for various other issues.
</li>
<li>
	 <span class='bf'>Michael Madden</span>   2009, gas properties table corrections  
</li>
<li>
	 <span class='bf'>Heru Reksoprodjo</span>   2009, point to affecting  dimensional parameter in multi layer sound travel, and also 
point to the mistake in the gas properties.
</li>
<li>
	 <span class='bf'>Raghvendra Gupta, Ph. D.</span>  ,   Nov 2013, Correction of  
	example <span class='tt'><a href='varibleArea.php#varibleArea:ex:airRserviar'>5.1</a></span>. 
</li>
<li>
	 <span class='bf'>David Heinze</span>  , Feb 2014, Corrections above equation 14.12  
	and equation <span class='tt'><a href='oblique.php#oblique:eq:emanuel'></a></span>. 

</li>
</ol>
</div>
