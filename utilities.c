#include "utilities.h"
#include "def.h"
#define new_max(x,y) ((x) >= (y)) ? (x) : (y)
#define new_min(x,y) ((x) <= (y)) ? (x) : (y)

void removeSpaces (char* restrict trimmed, const char* restrict untrimmed) {
	while (*untrimmed != '\0'){
		if(!isspace(*untrimmed)){
	  	*trimmed = *untrimmed;
	  	trimmed++;
		}
		untrimmed++;
	}
	*trimmed = '\0';
}

// 	int compareTOC(const void *a, const void *b) {
// 		struct INDEX *ia = (struct INDEX *)a;
// 		struct INDEX *ib = (struct INDEX *)b;
// 		int s=strcmp(ia->indexName, ib->indexName);
// 		return s;
// 	}

	//const char *aa, *bb; 
	//aa = ia->indexName ;
	//bb = ib->indexName ;
	//int s=strcmp(aa, bb);
int compareLos(const void *a, const void *b) {
	struct LOS *ia = (struct LOS *)a;
	struct LOS *ib = (struct LOS *)b;
	int s, len; // ia->title ib->title ia->number ib->number 
	if ( s = (ia->n[0] - ib->n[0]) != 0){
		return s;
	}
	s=(ia->nN - ib->nN); 
	len = (ia->nN <= ib->nN) ? ia->nN : ib->nN ;
	if(s == 0 ){
		for (int k=1; k -1 < len; k++) {
			s = ia->n[k] - ib->n[k];
			if (s != 0) 
				return s;
		}
	}
	else if (s < 0) {
		for (int j=1; j<=len ; j++){
			if (ia->n[j] < 0 ) {
				return ia->n[j];
			}
			else {
				s=(ia->n[j] - ib->n[j]);
				if (s != 0 ){ 
					return s;
				}
			}
		}
		return s;
	}
	else {
		for (int j=1; j<=len ; j++){
			if (ib->n[j] < 0 ) {
				return ia->n[j];
			}
			else {
				s=(ia->n[j] - ib->n[j]);
				if (s != 0 ){ 
					return s;
				}
			}
		}
		return s;
	}
	return s;
}

int compareIndexs(const void *a, const void *b) {
	struct INDEX *ia = (struct INDEX *)a;
	struct INDEX *ib = (struct INDEX *)b;
	int s=strcmp(ia->indexName, ib->indexName);
	return s;
}

void printLos(size_t len,  char * lastRecord) { 
	for (int i =0 ; i < len; i++){
		fprintf (stderr," %s %s",tocs[i].number, tocs[i].title);
		int j=0;
		for (j=0 ; j < tocs[i].nN ; j++ ){
			fprintf (stderr," Number %d = %d", (j+1), tocs[i].n[j] );
		}
		fprintf (stderr," |||| and the total numbers is %d \n ", tocs[i].nN);
	}
}

void printIndex(size_t len, enum dataType x, char * lastRecord) { 
	if (len < 1) {
		fprintf(stderr, "no ARRAY to print\n");
		return;
	}
	size_t i, limit;
	struct INDEX *indexPtr ;
	uint *indexMaxPtr;
	if ( x == aut ) { 
		fprintf (stderr,"----Authors Index --------" ); 
		indexPtr = auts ;
		indexMaxPtr = &indexAutMaxNumber ;
	}
	else if ( x == sub ) { 
		fprintf (stderr,"----Subject Index --------" ); 
		indexPtr = subs ;
		indexMaxPtr = &indexSubMaxNumber ;
	}   
	else if ( x == fig ) { 
		fprintf (stderr,"----Figure Index --------" ); 
		//indexPtr = figures;
		indexMaxPtr = &figMaxNumber ;
	}
	else {
		fprintf (stderr,"----Authors Index --------" ); 
		indexPtr = auts ;
		indexMaxPtr = &indexAutMaxNumber ;
	}
	limit = (size_t) (*indexMaxPtr);
	len = (len > limit) ? len : limit;
	fprintf (stderr,"+++++%c records %lu: \n", '#', len  ); 
	int largestRecord=0;
 	float averageRecords=0.;
	fprintf (stderr,"%-10s|%-20s|%3s|%-15s|%-5s|\n",
			"INDEX",
		 	"FULL INDEX",
		 	"#", 
			"seeAlso",
		 	"# File"); 
  for(i=0; i<len; i++) { 
		// char * inPtr = (char*) malloc(12 * sizeof(char));
		char *cAlso, tin[12], tfin[22], *htin, * htfin;
		memset(tin, '\0', sizeof(char)*sizeof(tin));
		memset(tfin, '\0', sizeof(char)*sizeof(tfin));
		strncpy (tin, indexPtr->indexName,10);
		strncpy (tfin, indexPtr->fin,20);
		shortnStg(indexPtr->indexName, &htin, 10) ;
		shortnStg(indexPtr->fin, &htfin, 20) ;
		shortnStg(indexPtr->seeAlso, &cAlso, 15) ;
		fprintf(stderr,"%-10s|%-20s|%3d|%-15s|%5d",
			htin,
			htfin,
		 	indexPtr->numberFiles,
			cAlso,
		 	indexPtr->in);
		if (htin) 
			free(htin);
		if (htfin) 
			free(htfin);
		if (cAlso) 
			free(cAlso);

		largestRecord =(largestRecord>indexPtr->numberFiles)?largestRecord:indexPtr->numberFiles;
		averageRecords += indexPtr->numberFiles;
 		for (int j=0; j< indexPtr->numberFiles; j++){
			fprintf (stderr," %-15s", indexPtr->fileName[j]) ;
		}
		fprintf(stderr,"\n");
		indexPtr++;
	}
	averageRecords = averageRecords/len;
	fprintf(stderr,"-----------------------Summery-------------------------\n");
	fprintf(stderr,"Total number of Records: %lu\n", len) ;
	//fprintf(stderr,"Larger record of Records: %u\n", *indexMaxPtr) ;
	fprintf(stderr,"Average Records: %2.3f\n", averageRecords) ;
	fprintf(stderr,"Largest Records: %3d\n", largestRecord) ;
	fprintf(stderr,"-------------------------------------------------------\n");
	//puts("-------------------------------------------------------\n");
}


/* sorting the struct*/
// size_t structs_len = sizeof(structs) / sizeof(struct st_ex);
void sortIndexData (enum dataType x){
	struct INDEX *indexPtr ;
	struct LOS *losPtr ;
	uint *indexMaxPtr;
	size_t limit, aLength;
	if ( x == aut ) { 
		indexPtr = auts ;
		indexMaxPtr = &indexAutMaxNumber ;
 		aLength = sizeof(auts) / sizeof (struct INDEX ) ; 
	}
	else if ( x == sub) { 
		indexPtr = subs;
		indexMaxPtr = &indexSubMaxNumber ;
 		aLength = sizeof(subs) / sizeof (struct INDEX ) ; 
	}
	else if ( x == toc ) { 
		losPtr = tocs;
		indexMaxPtr = &tocMaxNumber ;
 		aLength = sizeof(tocs) / sizeof (struct LOS ) ; 
		goto toi;
	}
	else{
		return;
	}
	limit = (size_t) *indexMaxPtr;
	aLength = (aLength > limit) ? limit : aLength;
	qsort(indexPtr, aLength, sizeof(struct INDEX), compareIndexs);
	return;
	toi:
	limit = (size_t) *indexMaxPtr;
	aLength = (aLength > limit) ? limit : aLength;
	qsort(losPtr, aLength, sizeof(struct LOS), compareLos);
	//printIndex(aLength, x, "in qsort");
}

void checkData (enum dataType x) {
	size_t limit, aLength = sizeof(auts) / sizeof (struct INDEX ) ; 
	limit = (size_t) indexAutMaxNumber;
	aLength = (aLength > limit) ? limit : aLength;
	printIndex(aLength, aut, "in checkData");
}

/* a fun to find a substring in a string */
int isSubstring(char * haystack, char * needle) {
  /* printf ("this is a haystack: %s\n this is a needle: %s\n", haystack, needle);  */
  int i = 0;
  int d = 0;
  if (strlen(haystack) >= strlen(needle)) {
    for (i = strlen(haystack) - strlen(needle); i >= 0; i--) {
      int found = 1; //assume we found (wanted to use boolean)
      for (d = 0; d < strlen(needle); d++) {
          if (haystack[i + d] != needle[d]) {
            found = 0;
            break;
          }
      }
      if (found == 1) {
        return i;
      }
    }
    return -1;
  }
  else {
    return -2; //big needle
      //fprintf(stdout, "haystack smaller\n"); 
  }
}

char *replaceChar (char *str, char find, char *replace) {
	char *ret=str;
	char *wk, *s;
	wk = s = strdup(str);
	while (*s != 0) {
		if (*s == find){
			while(*replace)
				*str++ = *replace++;
			++s;
		} 
		else
		    *str++ = *s++;
	}
	*str = '\0';
	free(wk);
	return ret;
}

char * extractFileName (char * tokenText ) {
	char * pch, *tmpStr;
	tmpStr = strdup (tokenText);
	int slashPosition ;
	do {
		slashPosition = isSubstring (tmpStr, "/");
		pch =  tmpStr ; 
		if ( slashPosition > 0 ){
			tmpStr = tmpStr + slashPosition + 1 ; 
		} 
	}  while ( slashPosition > 0);	
	return pch;
}

char* removeBrackets(char* txt, char oBracket , char cBracket ) {
  char* opening = strchr(txt, oBracket);
  opening++; // move next character past the opening 
  char* closing = strchr(opening, cBracket );
  int totalSLength = strlen(opening);
  int closingLength = strlen(closing);
  // char* cleanTXT = (char *) malloc ((int) (totalSLength) * sizeof (char));
  //cleanTXT=strndup(opening, totalSLength-closingLength); 
  txt=strndup(opening, totalSLength-closingLength); 
  return txt ;
}

char *trim(char *s) {
	size_t size;
	char *end;
	size = strlen(s);
	if (!size)
		return s;
	
	end = s + size - 1;
	while (end >= s && isspace(*end))
		end--;
	*(end + 1) = '\0';
	
	while (*s && isspace(*s))
		s++;
	
	return s;
}

void cleanNumeric( char * token, char ** nf){
	int c=0, d =0,  length = strlen(token); 
	*nf = (char *) malloc ( 10*length * (sizeof(char)) + 1 );
	memset(*nf, '\0', length);
	while (*(token+c) != '\0') { 
		if (isalpha(token[c]) ){
			(*nf)[d] = token[c] ; 
			c++; d++;
		}
		else {
			c++; 
		}
	}	
	(*nf)[++d] = '\0';
	return ;
}

char * extractSecondPart ( char* token, char symble){
	char * pch, tokenText[64] , tmpToken[64] ;
	uint i =0, length, symbleLocation;
	strcpy(tokenText, trim(token));
	length = strlen ( tokenText);
	if ( length == 0 ){
		return "FALSE";
	}
	memset(tmpToken, '\0', sizeof(tokenText));
	while ((tokenText[i] != symble) && (i < length)) {
		//tmpToken[i] = tokenText[i];
		symbleLocation = ++i;
	} 
	pch = tokenText + symbleLocation +1 ; 
	if (pch != NULL ){
		length = strlen (pch);
		if (length < 2 ){
			return "FALSE" ;
		}
		else {
			return pch ;
		}
	}
	else 
		return "FALSE" ;
}

struct RTN extractFileParts ( char* token, char symble){
	struct RTN items = {.fP ="", .sP=""} ;
	char tokenText[64]; 
	uint i =0, length;
	length = strlen (token);
	strcpy(tokenText, trim(token)) ;
	if ( length == 0 ){
		strcpy(items.fP,"Zero Length");
		strcpy(items.sP,"Zero Length");
		return items;
	}
	if (strchr(token, symble)==NULL ){
		strcpy(items.fP,"No Symble");
		strcpy(items.sP,"No Symble");
		return items;
	}
	while ((tokenText[i] != symble) && (i < length)) {
		items.fP[i] = tokenText[i];
		i++;
	}
	//tmpToken[i] = '\0';
	i++;
	int j=0;
	while (tokenText[i] != '\0') {
		items.sP[j] = tokenText[i];
		i++;
		j++;
	}
	if ((strlen(items.sP) >= length-1) || (strlen(items.fP) >= length-1)){ // too long
		strcpy(items.fP,"Zero Length results");
		strcpy(items.sP,"Zero Length results");
	}
	return items;
}

char * extractFirstPart (char* token, char symble){
	char * pch, tokenText[64] , tmpToken[64] ;
	uint i =0, length;
	strcpy(tokenText, trim(token)) ;
	length = strlen ( tokenText);
	if ( length == 0 ){
		return "FALSE";
	}
	memset(tmpToken, '\0', sizeof(tokenText));
	while ((tokenText[i] != symble) && (i < length)) {
		tmpToken[i] = tokenText[i];
		i++;
	}
	tmpToken[i] = '\0';
	pch = tmpToken;
	if (i >= length-1)  { // too long
		return "FALSE" ;
	}
	else {
	 	return pch ;
	}
}

char * cleanText(char* token, char* cssClass ){
	char * pch;
	pch=strchr(token,'}');
	if (pch==NULL) {
		return "FALSE" ;
	}
	int closingLocation = pch-token + 1;
	pch=strchr(token,'{');
	if (pch==NULL) {
		return "FALSE" ;
	}
	int openingLocation = pch-token + 1;
  char* to;
  to=strndup(pch+1 , closingLocation- openingLocation- 1 );
  sprintf ( token, "<span class='%s'>%s</span>", cssClass, to);
  return token;
}

char * cleanTabNewLine (char* token ){
	token = replaceChar(token, '\n', " \0" );
	token = replaceChar(token, '\t', "");
	return  token ;
}

char * rmChar(char * token, char toBeRemove){
	int length, c, d;
	char *start;
	c = d = 0;
	length = strlen(token);
	if (length < 1 ){
		return NULL ; 
	}
	start = (char*) malloc(length+1);
	if (start == NULL) {
		return NULL ; 
	}
	while (*(token+c) != '\0') {
		if (*(token+c) != toBeRemove ) {
			*(start+d) = *(token+c);
			c++ ; d++;
		}
		else {
			c++;
		}
	}
	*(start+d)= '\0';
	strcpy (token , start );
	free (start);
	return token;
}

char * newLineToSpace (char* token ){
	int length, c, d;
	char *start;
	c = d = 0;
	length = strlen(token);
	start = (char*)malloc(length+1);
	if (start == NULL) {
	   //exit(EXIT_FAILURE);
		return NULL ; 
	}
	while (*(token+c) != '\0') {
		if (*(token+c) == '\n') {
			*(start+d) = ' ';
		}
		else {
			if (*(token+c) == ' ') {
				int temp = c + 1;
				if (*(token+temp) != '\0') {
					while (*(token+temp) == ' ' && *(token+temp) != '\0') {
						if (*(token+temp) == ' ') {
							c++;
						}  
						temp++;
					}
				}
			}
			*(start+d) = *(token+c);
		}
		c++;
		d++;
	}
	*(start+d)= '\0';
	strcpy (token , start );
	if (start)
		free (start);
	return token;
}

void comment (char * token) {
	return ;
}

void emptyLine ( char * token) {
	printf ("<p>\n");
	return;
}	

void emptySpace( char * token, char* name){
	return;
}

void realPercent ( char * token) {
	printf("%%");
}

void mkSee (char * textToken, char* m,  char * f, char * l) {
	int seperation=0 , start=0, tmpStart=0, end=0, space=0; 
	for (int i = 0; textToken[i] != '}' ; i++ ) {
		tmpStart = i ; 
		continue ;
	}
	for (int i = tmpStart+1 ; textToken[i] != '{' ; i++ ) {
		start = i+2 ; 
	}
	for (int i = start; textToken[i] != '|' ; i++ ) {
		seperation = i+1 ; 
		continue ;
	}
	for (int i = start ; (textToken[i] != ' ') && (i <= seperation) ; i++ ) {
			space = i+1 ;
	}
	for (int i = seperation+1; textToken[i] != '}' ; i++ ) {
		end = i+1 ;
		continue ;
	}
	for (int i=start; i < seperation; i++) {
		f[i-start] = textToken[i] ;
	}
	if ( seperation - space > 2){
		for (int i=start; i < space; i++) {
			m[i-start] = textToken[i] ;
		}
	} 
	else{
		strcpy(m,f);
	}
	for (int i=seperation+5; i < end; i++) {
		l[i-seperation-5] = textToken[i] ;
	}
	return;
}

void shortnStg(char * token, char **input, int s) {
	char t[s]; 
	*input = (char *) malloc ( s * (sizeof(char)) );
	strcpy(t, token);
	int l = strlen (token);
	if (l < s){
		for (int i=0; i<l ; i++){
			(*input)[i] = t[i];
		}
		(*input)[l]= '\0';
	}
	else {
		for (int i=0; i< (s-3) ; i++){
			(*input)[i] = t[i];
		}
		for (int i=s-3; i< s ; i++){
			(*input)[i] = '.';
		}
		(*input)[s]= '\0';
	}
}

void tocToNumber(char * token){
	int orderN=0, j=0 ;
	char nStr [5];
	memset(nStr, '\0', sizeof(char)*sizeof(nStr));
 	for (int i=0 ; i <= strlen(token) ; i++){
		if (isdigit(token[i]) ){
			nStr[j]= token[i];
			j++;
			continue;
		}
		else if (token[i] == '.' ) { 
			tocs[tocMaxNumber].n[orderN]	= atoi (nStr) ;
			memset(nStr, '\0', sizeof(char)*sizeof(nStr));
			j=0;
			orderN++;
			continue;
		}	
		else if (isalpha(token[i]) ){
			tocs[tocMaxNumber].apdxN =  token[i];
		}
		else if (token[i] == '\0' ){
			tocs[tocMaxNumber].n[orderN] = atoi (nStr);
			orderN++;
			tocs[tocMaxNumber].nN=orderN;
			break;
		}
	}
	for (int j=orderN; j< 7 ; j++ ){
		tocs[tocMaxNumber].n[j] = -1;
	}
	//printLos (tocMaxNumber, "tocToNumber"); 
	return;
}
const char* getEnumName(enum dataType x) {
	switch (x) {
		case sub: return "Subject Index";
		case aut: return "Authors Index";
		case toc: return "Table of Content";
		case sec: return "sec";
		case exm: return "Examples";
		case nmn: return "Nomenclature";
		case fig: return "Figure";
		case table: return "Table";
		case reference: return "Reference";
	}
	return "no name";
}

// char * shortnStg (char * token, char * holder, int maxLength){
// 	if (strlen(token) < maxLength){
// 		return token;
// 	}
// 	else {
// 		//memset(holder, '\0', sizeof(holder));
// 		strncpy(holder, token, (maxLength-3)) ;
// 		for (int i=0; i<3; i++){
// 			*(holder+(maxLength-3)+i) = '.';
// 		}
// 		*(holder + maxLength) ='\0';
// 		//strcpy(token,strPtr);
// 		// strPtr = trimStr;
// 		//if (strPtr)
// 		//	free(strPtr);
// 		return holder;
// 	}
// }	
