#ifndef __LIST_HH_LIB
#define __LIST_HH_LIB
	#include <stdio.h>
  #include <unistd.h>
  #include <string.h>
  #include <ctype.h>
  #include <stdlib.h>
  #include <stdbool.h>
	#include "def.hh"

	void makeList (char *, char * );
	void closeList (char *, char * );
	void makeListItem (char *, char * );
	void setListSwitch (char* );

	//static uint listSwitchStatus;  /* 0 close, 1 openIni, 2 regOpen,    */

#endif /* End __LIST_HH_LIB  list.h */

