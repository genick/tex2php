#ifndef __LATEX_H
#define __LATEX_H
# include <stddef.h>
# include "commonDef.hh" 

#define strlens(s) (s==NULL?0:strlen(s))
#define BUFFER_MAX_LENGTH 1024

// 	enum listStatus {unOpenList=1, firstOpenList, middleFirstList, finishedFirstList,
// 			secondOpenList, middleSecondList, finishedSecondList};
// 	typedef enum { regular=100, see=101 } indexStatus;
// 	enum dataType { reference = 314, table, figure };
// 	enum FILEnAME { label=0,  overTxt  };
	// typedef enum { reference=0, table=1,  figure=2 } dataType;
	/* set the default */
	extern enum listStatus lS ;
	int numberOfLabels;
	int  isSubstring(char *, char * ) ;
	// void openFiles(void);
	int closeTheFile (void);
	char* trim(char* ) ; 
	//void mkIndexAut(char*, indexStatus, char* ) ;
	void mkIS (char * , enum indexStatus , char* );
	void mkIP ( char* );
	void section (char*, char*);
	void mkIA (char * , enum indexStatus, char* );
	char* removeBrackets(char*, char, char ) ; 
	void wImgTMP (char * ) ;

#endif /* __LATEX_H */
