<h2 class="chapter"> Authors Index</h2>
<div class="indexWrap">
	<div class="accordion">
		<ul class="letter">
			<li>
				<a class= "toggle" href="javascript:void(0);" >H</a>
				<ul class="inner">
					<li><a href="intro.php#Hydraulic0"
					 title="Hydraulic in file intro.php">Hydraulic Jump</a></li>
				</ul>
			</li>
			<li>
				<a class= "toggle" href="javascript:void(0);" >D</a>
				<ul class="inner">
					<li><a href="intro.php#Discontinuity0"
					 title="Discontinuity in file intro.php">Discontinuity</a></li>
				</ul>
			</li>
			<li>
				<a class= "toggle" href="javascript:void(0);" >I</a>
				<ul class="inner">
					<li><a href="intro.php#Isothermal0"
					 title="Isothermal in file intro.php">Isothermal Flow</a></li>
					<li><a href="intro.php#Isothermal1"
					 title="Isothermal in file intro.php">Isothermal Flow</a></li>
				</ul>
			</li>
			<li>
				<a class= "toggle" href="javascript:void(0);" >S</a>
				<ul class="inner">
					<li><a href="intro.php#Shapiro0"
					 title="Shapiro in file intro.php">Shapiro Flow</a></li>
					<li><a href="intro.php#Shapiro1"
					 title="Shapiro in file intro.php">Shapiro Flow</a></li>
					<li><a href="intro.php#Speed0"
					 title="Speed in file intro.php">Speed of sound</a></li>
				</ul>
			</li>
			<li>
				<a class= "toggle" href="javascript:void(0);" >M</a>
				<ul class="inner">
					<li><a href="intro.php#Maxwell's0"
					 title="Maxwells in file intro.php">Maxwell's coefficient</a></li>
				</ul>
			</li>
			<li>
				<a class= "toggle" href="javascript:void(0);" >S</a>
				<ul class="inner">
					<li><a href="intro.php#Science0"
					 title="Science in file intro.php">Science disputes</a></li>
				</ul>
			</li>
			<li>
				<a class= "toggle" href="javascript:void(0);" >I</a>
				<ul class="inner">
					<li><a href="intro.php#Internal0"
					 title="Internal in file intro.php">Internal energy</a></li>
				</ul>
			</li>
			<li>
				<a class= "toggle" href="javascript:void(0);" >M</a>
				<ul class="inner">
					<li><a href="intro.php#Mach's0"
					 title="Machs in file intro.php">Mach's bullet</a></li>
					<li><a href="intro.php#Mach's1"
					 title="Machs in file intro.php">Mach's bullet</a></li>
				</ul>
			</li>
			<li>
				<a class= "toggle" href="javascript:void(0);" >I</a>
				<ul class="inner">
					<li><a href="intro.php#Intersection0"
					 title="Intersection in file intro.php">Intersection of Fanno and Rayleigh lines</a></li>
				</ul>
			</li>
			<li>
				<a class= "toggle" href="javascript:void(0);" >E</a>
				<ul class="inner">
					<li><a href="intro.php#External0"
					 title="External in file intro.php">External flow</a></li>
				</ul>
			</li>
			<li>
				<a class= "toggle" href="javascript:void(0);" >K</a>
				<ul class="inner">
					<li><a href="intro.php#Kutta-Joukowski0"
					 title="KuttaJoukowski in file intro.php">Kutta-Joukowski circulation theory</a></li>
				</ul>
			</li>
			<li>
				<a class= "toggle" href="javascript:void(0);" >L</a>
				<ul class="inner">
					<li><a href="intro.php#Lanchester&mdashPrandtl0"
					 title="LanchestermdashPrandtl in file intro.php">Lanchester&mdashPrandtl theory</a></li>
				</ul>
			</li>
			<li>
				<a class= "toggle" href="javascript:void(0);" >T</a>
				<ul class="inner">
					<li><a href="intro.php#Thin-airfoil0"
					 title="Thinairfoil in file intro.php">Thin-airfoil theory</a></li>
				</ul>
			</li>
			<li>
				<a class= "toggle" href="javascript:void(0);" >V</a>
				<ul class="inner">
					<li><a href="intro.php#Von0"
					 title="Von in file intro.php">Von Karman integral equation</a></li>
				</ul>
			</li>
		</ul>
	</div>
</div>
<footer> Constructed: Authors Index  Thu Apr 13 20:31:43 2017</footer>
