/* index.hh Sun Oct  8 09:30:54 CDT 2017 */
# ifndef __INDEX_HH
# define __INDEX_HH

#	include <iostream>
#	include <fstream>
#	include <sstream>
#	include <string>
#	include <cstring>
#	include "enumerators.h"
#	include "item.hh"
#	include "commSQL.hh"
#	include "utility.hh"
#	include "commDB.hh"
//#	include "mangerSQL.hh"
//# include "t2p.l"

	using namespace std;
	void index (string, int, dataType );

#endif   /* End __INDEX_HH  */
