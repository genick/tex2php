#ifndef __IOFILES_H
#define __IOFILES_H
	#include "includeFiles.h"
	#include "def.h"
	#include "latex.h"
	#include "mkHtml.h"

#define strlens(s) (s==NULL?0:strlen(s))
#define BUFFER_MAX_LENGTH 1024
  extern struct INDEX indexs[900];
  extern void mkIndexHTML(void);
  extern void mkTOC();
	extern bool openOperations(void);
	extern int closeOperations(void);

	bool openFile(FILE *, char*,char) ;
	int saveIndexFiles(void) ;
	//void readDataFiles( enum dataType) ;
	void readDataFiles( FILE *, enum dataType );
	void saveDataFiles( FILE *, enum dataType );
	int readIndexs(void);
	void saveIndex(uint, FILE *, struct INDEX *);
	void saveTOI(uint, FILE *, struct LOS *);
	void saveNMN(uint, FILE *, struct NOMENCLATURE *);
	//void saveEXM(uint, FILE *, struct element *);
	void readIData (FILE *, enum dataType);
	void readData (FILE *, enum dataType );
	void readFigure ( FILE *, enum dataType );
	void readTOC ( FILE *, enum dataType ) ;
	void closeFile( FILE *) ;
	int saveIData ( FILE *, enum dataType x); 
	FILE * openDataFile(char *,  enum oMode );
	void readTOI ( FILE * , enum dataType );
	void readNMN ( FILE * , enum dataType );
	void readEXM ( FILE * , enum dataType );

#endif
