// sec.cc starting point Wed Mar  1 16:50:22 CST 2017
# include "sec.hh"
using namespace std;

void issue (string  x, string key, int lineNU, string From ) {
	logFile << "working issue" << endl << x << endl;
	ostringstream ss;
	LABELS fig(1);
	LABELS *ptrF;
	ptrF = &fig;
	string l;
	// srand(time(0));
	peelFirstB(x, 1);
	string buffer; 
	size_t found = x.find("}");
	if (found!=string::npos) {
		l = trim(x.substr(0, found));
		ptrF->setMemberValue (l,CA);
		ptrF->setMemberValue ("issue",LV);
		// set the id number 
		buffer = mkRefNu(ISSUE);
		// ss << mainName << ":iss:" <<
		// 	"iss" << issueNumber ;
		// cerr << ss.str() << endl ;
		// issueNumber++;
		ptrF->setMemberValue (buffer,RF);
		ptrF->setMemberValue (workingFileName,LB);
		cout << "<h4 class=\"" << key<< "\" " << "id=\"" << buffer 
			<< "\">" << ptrF->getMemberValue(CA,0,' ') << "</h4>" << endl;
		// cout << "\">" << ptrF->getMemberValue(CA,0,' ') << "<h4>" << endl;
		vector <itemMember> data = {CA, LV, LB, RF};
		vector <intMember> dataI = {};
		insertDataDB(ptrF, data, dataI, ISSUE, "issue");
	}
	return;
}

void section (string  x, string key, int lineNU, string From){
	logFile << "section: line " << lineNU << " " << x << endl;
	ostringstream ss;
	LABELS fig(1);
	LABELS *ptrF;
	ptrF = &fig;
	string l;
	peelFirstB(x, 1);
	size_t found = x.find("\\label");
	if (found!=string::npos) {
		l = trim(x.substr(0, found-1));
		ptrF->setMemberValue (l,CA, false);
		x = x.erase (0,found);
		peelFirstB(x, 1);
		found = x.find('}');
		if (found!=string::npos) {
			x = x.substr(0,found);
		}
		ptrF->setMemberValue (x,LB);
		l = lookUpNumber(x, LB, PG);
		ptrF->setMemberValue (l,PG);
		ptrF->setMemberValue (key,LV);
		l = lookUpNumber(x, LB, NU);
		ptrF->setMemberValue (l,NU);
	}
	ss << "sec" << l;
	ptrF->setMemberValue (ss.str(),RF);
	string yy("chap");
	if ( key == yy){
		cout << "<h1 class=\"" << key<< "\" " << "id=\"c" << ss.str() 
			<< "\">Chapter " << l << " " << 
		 	ptrF->getMemberValue(CA,0,' ') << "</h1>" << endl;
	}
	else if (key == "section"){
		cout << "<h2 class=\"" << key<< "\" " << "id=\"" << ss.str() 
			<< "\"> &sect; " << l << " " <<  
			ptrF->getMemberValue(CA,0,' ') << "</h2>" << endl;
	}
	else if (key == "subsec"){
		cout << "<h3 class=\"" << key<< "\" " << "id=\"" << ss.str() 
			<< "\">" << l << " " <<  
			ptrF->getMemberValue(CA,0,' ') << "</h3>" << endl;
	}
	else if (key == "subsubsec"){
		cout << "<h4 class=\"" << key<< "\" " << "id=\"" << ss.str() 
			<< "\">" << l << " " <<  
			ptrF->getMemberValue(CA,0,' ') << "</h4>" << endl;
	}
	else{ //issue
	}
	vector <itemMember> data = {LB, NU, CA, LV, RF, PG};
	vector <intMember> dataI = {Pg, Nn, Lb};
	insertDataDB(ptrF, data, dataI, SEC, key);
	//secMaxNumber++;
}
