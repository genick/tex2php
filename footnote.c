/* footnote files */

	#include "footnote.h"

void doFootNoteMark(void){
	footNoteNumber++;
		printf ("<sup><a href=\"#fn%d\" id=\"ref%d\">%d</a></sup>\n",
			footNoteNumber, footNoteNumber, footNoteNumber);
}


void doFootNoteText (char* tokenText){
	/* printf ("printing doFootNoteText: %s", tokenText); */
	char * cleanTxt = removeBrackets( tokenText,'{', '}');
	if (footNoteNumber == 1){
			/* remove(footNoteFile); */
		lS = firstOpenList ;
		unlink ("footnote.php");
		footNoteFile = fopen("footnote.php", "w");
	}
	else {
		footNoteFile = fopen("footnote.php", "a");
	}
  if (footNoteFile == NULL){
    fprintf(footNoteFile,"CANNOT OPEN FOOTNOTE FILE!\n");
    return ;
  }
	else {
    fprintf(footNoteFile, "<p> %d. %s<a id=\"fn%d\" href=\"ref%d\" title= \"Jump back to footnote %d in the text.\">%d</a></p>\n",
		 footNoteNumber, cleanTxt, footNoteNumber, footNoteNumber, footNoteNumber, footNoteNumber);
	}
	fclose(footNoteFile);
}

void doFootNote(char *tokenText){
	footNoteNumber++;
	char opening ='{';
	char closing ='}';
	char * cleanTxt = removeBrackets( tokenText, opening, closing);
	/* printf ("CLEAN TEXT %s", cleanTxt); */
	int totalSLength = strlen (cleanTxt) ;
	char* toPHP = (char *) malloc ((int) (totalSLength+40) * sizeof (char));
  sprintf (toPHP,"<sup><a href=\"#fn%d\" id=\"ref%d\">%d</a></sup>", footNoteNumber,
		footNoteNumber, footNoteNumber);
	printf ("%s",toPHP);
		/* system("/usr/bin/rm -f footnote.php"); */
	if (footNoteNumber == 1){
			/* remove(footNoteFile); */
		lS = firstOpenList ;
		unlink ("footnote.php");
		footNoteFile = fopen("footnote.php", "w");
	}
	else {
		footNoteFile = fopen("footnote.php", "a");
	}
  if (footNoteFile == NULL){
    fprintf(footNoteFile,"CANNOT OPEN FOOTNOTE FILE!\n");
		free (toPHP);
    return ;
  }
	else {
    fprintf(footNoteFile, "<p> %d. %s<a id=\"fn%d\" href=\"ref%d\" title= \"Jump to footnote %d in the text.\">%d</a></p>\n",
		 footNoteNumber, cleanTxt, footNoteNumber, footNoteNumber, footNoteNumber, footNoteNumber);
	}
	free (toPHP);
	fclose(footNoteFile);
}

int closeTheFile(void){
	if (footNoteNumber == 0) {
		return 0;
	}
	printf("\n<h3 class=\"footnote\" >Footnote:</h3>\n<hr></hr>\n");
	int c;
	FILE *file;
	file = fopen("footnote.php", "r");
	if (file) {
		while ((c = getc(file)) != EOF)
			putchar(c);
		fclose(file);
	}
	/* system("/usr/bin/cat footnote.php"); */
	return 0;
}

void tColorBox ( char* tokenText){
	printf ("TOKEN:    %s\n", tokenText);	
	if (strstr(tokenText, "footnotemark") ){  
		doFootNoteMark();
		/* printf ("found footnotemark\n\n"); */
	}
}
