// utility.hh Mon Aug 28 14:34:30 CDT 2017
#ifndef UTILITY_HH__
#define UTILITY_HH__
# include <string>
# include <fstream>
# include <sstream>
# include <iostream>
# include <cstring>
# include "enumerators.h"
# include "item.hh"
# include "io.hh"
	using namespace std;
	class FIGURES;
	class LABELS;
                                                                
	extern string lookUpNumber (string, enum itemMember, enum itemMember);
	extern string workingFileName, emphEq;
	extern int nmnNumber, secNumber, issueNumber, figNumber,
			subNumber, autNumber;
	extern ofstream logFile;
	extern time_t now;

	char * trim(char *);
	string trim(string );
	string lastPart(string, char);
	string firstPart(string, char);
	string peelFirstPart( string &, size_t, char );
	string peelLastPart( string &, size_t, char );
	void peelFirstB( string &, size_t, char='{' );
	string rmTabNewLine(string);
	size_t strpos(string, char *, int );//find the needle nth location
	void texStrToPhpStr(string , string );
	// string mkNomeNu (dataType );
	string mkRefNu (dataType );
	void empty(string, int, string="percent");
	void realPercent(string, int, string="realPercent");
	void unToutched (string, int, string );
	void toBeChanged (string, int, string );
	void extractParameter(LABELS*, string& , int=1, enum itemMember=LB,
		char= '{', char='}', actionType=doNothing, char= '/',
		enum itemMember= CAt );
	string cleanFileName (string );

	string toLOWER(string);
	string toUPPER(string);
	string toUpper(string, int=0);
	string toLower(string, int=0);

#endif  // End UTILITY_H__
