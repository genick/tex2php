// lable.cc Thu Apr 27 21:39:01 CDT 2017
#	include <iostream>
#	include <sstream>
#	include <fstream>
#	include <iomanip>
#	include <string>
#	include <cstring>
#	include <cstdlib>
#	include <mysql.h>
#	include "lable.hh"
#	include "item.hh"
#	include "mangerSQL.hh"
// #	include "commSQL.hh"
// #	include "mangerSQL.hh"
// #	include "ut.hh"

using namespace std;

//	string peelFirstPart( string &l, size_t times, char a ){
//		string s;
//		size_t found = l.find(a);
//		for (size_t i = 0 ; i < times ; i++){
//			if (found!=string::npos) {
//				s = l.substr(0, found);
//				l.erase(0,found+1);
//			}   
//			else {
//				return "NULL";
//			}   
//			found = l.find(a);
//		}
//		return s;
//	}

string lookUpNumber (string l, enum itemMember x, enum itemMember y){
	l = bt->getValue(l, x, y);
	return l;
}

extern string peelFirstPart( string &, size_t, char  ); 

void readLabels(void) {
	LABELS leb(0);
	FIGURES *ptrF; 
	ptrF = &leb;
	string tn ("rawLabel");
	bt->setTableName(tn);
	if (bt->isTableExists(tn) ==1 ){
		bt->removeTableData(tn);
		cerr <<" remving data from table " << tn << "!" <<  endl;
	}
	else {
		cerr <<" trying to crate table: " << tn << " !" <<  endl;
		bt->createTable(LABEL, "rawLabel");
	}
	for(string line; getline( input, line ); ){
		//cout << line << endl;
		leb.setMemberValue (peelFirstPart(line, 1, ','),NU,true);
		leb.setMemberValue (peelFirstPart(line, 1, ','),TY);
		leb.setMemberValue (peelFirstPart(line, 1, ','),LB);
		leb.setMemberValue (peelFirstPart(line, 1, ','),CA);
		leb.setMemberValue (peelFirstPart(line, 1, ','),PG);
		leb.setMemberValue (peelFirstPart(line, 1, ','),KW);
		bt->insertRow (ptrF, LABEL);
	}
	input.close();
	cerr << "Label data was inserted to Table " << tn <<  " successfully." << endl;
	return;
}

void exLabel(string l){ 
	// string buffer;
	// size_t found = l.find('}');
	// if (found!=string::npos) {
	// 	buffer = l.substr(found, 0);
	// }
		// cout << "<h5> Example " <<
		//  	lookUpNumber("thermo:sec:definition", LB, NU) << "</h5>" << endl; 
// 		cout << "<p>" ;
}
