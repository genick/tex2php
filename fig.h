/* fig.h file */
 #ifndef __FIG_H
 #define __FIG_H

//extern "C" {
// #include "utilities.h"
//}
//# include <iostream>

	void imgPDF( char* );
	void subfigure( char* );
	void caption( char* );

#endif   /* End __FIG_H  */
