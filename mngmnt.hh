/*  mngmnt.hh file Thu Apr 27 08:24:14 CDT 2017 */
 #ifndef __MNGMNT_HH
 #define __MNGMNT_HH

#	include <iostream>
#	include <fstream>
#	include <sstream>
#	include <string>
#	include <cstring>
#	include <ctime>
#	include "enumerators.h"
#	include "item.hh"
#	include "commSQL.hh"
#	include "commDB.hh"
#	include "utility.hh"

using namespace std;

	extern commSQL * bt;
	extern void cleanDataBase(string );
	bool		openOperations(string);
	void		closeOperations();
	void		cleanDataBase (string);
	void		openLogFiles(vector <string> );
	void		openLogFile (string);
	void		openFootnoteFile (string);
	extern	 ofstream logFile, footnoteFile; 
	extern time_t now;

#endif   /* End __MNGMNT_HH  */
