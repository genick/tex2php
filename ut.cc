// ut.cc moving the utilities to the cc side.
# include <string>
# include <sstream>
# include <iostream>
# include <cstring>
# include "ut.hh"

using namespace std;


char * trim(char *s) {
	size_t size;
	char *end;
	size = strlen(s);
	if (!size)
		return s;
	
	end = s + size - 1;
	while (end >= s && isspace(*end))
		end--;
	*(end + 1) = '\0';
	
	while (*s && isspace(*s))
		s++;
	//string l(s);
	return s;
}

string trim(string l){
	size_t pos = l.find_last_not_of(" \t");
	if( string::npos != pos ){
		l = l.substr(0, pos+1 );
	}
	pos = l.find_first_not_of(" \t");
	if( string::npos != pos ){
		l = l.substr(pos);
	}
	return l;
}

string lastPart(string token, char s){
	string l = token;
	size_t found; 
	do {
		found = l.find(s);
		l.erase (0,found+1);
	} while (found!=string::npos) ; 
	return l;
}

// string peelFirstPart( string &l, int times, char a ){
string peelFirstPart( string &l, size_t times, char a ){
	string s;
	size_t found = l.find(a);
	// for (int i = 0 ; i < times ; i++){
	for (size_t i = 0 ; i < times ; i++){
		if (found!=string::npos) {
			s = l.substr(0, found);
			l.erase(0,found+1);
		}   
		else {
			return "NULL";
		}   
		found = l.find(a);
	}
	return s;
}

void peelFirstB( string &l, size_t times, char a ){
	size_t found = l.find(a);
	for (size_t i = 0 ; i < times ; i++){
		if (found!=string::npos) {
			l.erase(0,found+1);
		}
		else {
			return;
		}
		found = l.find(a);
	}
	//return;
}

string rmTabNewLine(string  l){
	string space(" ");
	size_t found, sL = l.length();
	while (true) {
		found = l.find('\n');	
		if (found < sL-1){
			l.replace(found, 1, space);
		}	
		else {
			break;
		}
	}
	while (true) {
		found = l.find('\t');	
		if (found < sL){
			l.replace(found, 1, space);
		}	
		else {
			break;
		}
	}
	while (true){
		found = l.find("  ");	
		if (found < sL){
			l.replace(found, 2, space);
		}	
		else {
			break;
		}
	}
	return  l;
}

// function to find the needle nth location.
size_t strpos(string hay, char *needle, int nth) {
	size_t found, sum = 0; 
	for (int i=0 ; i <=nth; i++){
		found =	hay.find(needle);  
		sum = sum + found;
		if (found!=string::npos) {
			hay.erase (0,found+1);
		}
		else{
			return string::npos;
		}
	}
	return sum;
}

void texStrToPhpStr(string l, string s){
	size_t found ;
	peelFirstB(l,1);
	found = l.find('}');
	if (found!=string::npos){
		l.erase(l.begin()+found, l.end()-0);
	}
	cout << "<span class=\"" << s.c_str() << "\">" << l << "</span>" ;
}

string mkNomeNu (dataType a){
	ostringstream ss;
	string l, sog; 
	if ( a == NMN){
		l.assign("nmn");
		sog.assign("nmn");
	}	
	ss << workingFileName << ":" <<  sog << ":" << l << nmnNumber; 
	cerr << ss.str().c_str() << endl;
	return ss.str();
}

