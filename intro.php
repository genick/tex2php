<p>



<p>
<h2 class="chap" id="csec1">Chapter 1 Introduction</h3>



<p>
<h3 class="section" id="sec1.1"> &sect; 1.1 What is Compressible Flow?</h3>


		{intro:sec:whatIsGD}
<p>
This book deals with an introduction\footnote{This book is
gradually sliding to include more material that isn't so introductory. 
But an attempt is made to present the material in introductory
level.} to the flow of compressible substances (gases).
The main difference between compressible flow and almost
incompressible flow is not the fact that compressibility has to
be considered.
Rather, the difference is in two phenomena that
do not exist in incompressible flow\footnote{It can be argued
that in open channel flow there is a hydraulic jump 
(discontinuity)
and in some ranges no effect of downstream conditions on the flow. 
However, the uniqueness of the phenomena in the gas dynamics provides
spectacular situations of a limited length (see Fanno model) and
thermal choking, etc. Further, there is no equivalent to oblique
shock wave.
Thus, this richness is unique to gas dynamics.}.
<div class="invisible"> <!-- Index hock -->
	<a name="intro:sub:sub0">Hydraulic Jump|see{Discontinuity</a>"
</div>  <!-- End of Index -->
}<div class="invisible"> <!-- Index hock -->
	<a name="intro:sub:sub1">Discontinuity</a>"
</div>  <!-- End of Index -->

The first phenomenon is the very sharp discontinuity (jump)
in the flow in properties.
The second phenomenon is the choking of the flow.
Choking is when downstream variations don't effect the
flow\footnote{The thermal choking is somewhat different but
a similarity exists.}.
Though choking occurs in certain pipe flows in astronomy, there also
are situations of choking in general
(external) flow\footnote{This book is
intended for engineers and therefore a discussion about astronomical
conditions isn't presented.}.
Choking is referred to as the situation where downstream conditions,
which are beyond a critical value(s), doesn't affect the flow.
<p>
The shock wave and choking are not intuitive for most people.
However, one has to realize that <span class='underline'>intuition</span> '>intuition</span>on where one uses his past experiences to predict other
situations.
Here one has to learn to use his intuition as a tool for future use. 
Thus, not only aeronautic engineers, but other engineers, and
even manufacturing engineers will be able use this &ldquo;intuition&rdquo;
in design and even research.
<p>
<h3 class="section" id="sec1.2"> &sect; 1.2 Why Compressible Flow is Important?</h3>


<p>
Compressible flow appears in many natural and many technological processes.
Compressible flow deals with more than air, including steam,
natural gas, nitrogen and helium, etc.
For instance, the flow of natural gas in a pipe system, 
a common method of heating in the U.S., should be considered 
a compressible flow. 
These processes include the flow of gas in the exhaust system
of an internal combustion engine, and also gas turbine,
a problem that led to the Fanno flow model. 
The above flows that were mentioned are called internal flows. 
Compressible flow also includes flow around bodies such as the wings
of an airplane, and is considered an external flow.
<p>
These processes include situations 
not expected to have a compressible flow, such as manufacturing
process such as the die casting, injection molding. 
The die casting process is a process in which liquid metal,
mostly aluminum, is injected into a mold to obtain a near final shape. 
The air is displaced by the liquid metal in a very rapid manner,
in a matter of milliseconds, therefore the compressibility has
to be taken into account.
<p>
Clearly, Aero Engineers are not the only ones  who have to deal with
some aspect of compressible flow. 
For manufacturing engineers there are many situations where
the compressibility or compressible flow understating is essential
for adequate design.  
For instance, the control engineers who are using pneumatic systems
use compressed substances. 
The cooling of some manufacturing systems and design of refrigeration
systems also utilizes compressed air flow knowledge.
Some aspects of these systems require consideration of the unique
phenomena of compressible flow.
<p>
Traditionally, most gas dynamics (compressible flow) classes deal
mostly with shock waves and external flow and briefly teach
Fanno flows and Rayleigh flows (two kind of choking flows).
There are very few courses that deal with isothermal flow.
In fact, many books on compressible flow ignore the isothermal 
flow\footnote{Any search on the web on classes of compressible flow
will show this fact and the undersigned can testify that this was true
in his first class as a student of compressible
flow.}.
<div class="invisible"> <!-- Index hock -->
	<a name="intro:sub:sub2">Isothermal Flow</a>"
</div>  <!-- End of Index -->

<p>
In this book, a greater emphasis is on the internal flow.
This doesn't in any way meant that the important topics
such as shock wave and oblique shock wave should be neglected.
This book contains several chapters which deal with external flow
as well.
<p>
<h3 class="section" id="sec1.3"> &sect; 1.3 Historical Background</h3>


<p>
<div class="invisible"> <!-- Index hock -->
	<a name="intro:aut:aut0">Rouse, Hunter</a>"
</div>  <!-- End of Index -->

In writing this book it became clear that there is more unknown and
unwritten about the history of compressible fluid than known.
While there are excellent books about the history of fluid mechanics
(hydraulic) see for example book by Rouse\footnote{Hunter Rouse and
Simon Inc, History of Hydraulics (Iowa City: Institute of Hydraulic
Research, 1957)}.
There are numerous sources dealing with the
history of flight and airplanes (aeronautic)\footnote{Anderson, J. D., Jr. 1997. A History of Aerodynamics: And Its Impact
on Flying Machines, Cambridge University Press, Cambridge, England.}.
Aeronautics is an overlapping
part of compressible flow, however these two fields are different.
For example, the Fanno flow and isothermal flow, which are the
core of gas dynamics, are not part of aerodynamics.
Possible reasons for the lack of written documentation
are one, a large part of this knowledge is relatively new,
and two, for many early contributors this topic was a side issue.  
In fact, only one contributor of the three main models of internal 
compressible flow (Isothermal, Fanno, Rayleigh)
was described by any text book.
This was Lord Rayleigh, for whom the Rayleigh flow was named.
The other two models were, to the undersigned, unknown.
Furthermore, this author did not find any reference to isothermal
flow model
<div class="invisible"> <!-- Index hock -->
	<a name="intro:sub:sub3">Isothermal Flow</a>"
</div>  <!-- End of Index -->

earlier to Shapiro's book.<div class="invisible"> <!-- Index hock -->
	<a name="intro:aut:aut1">Shapiro, Ascher</a>"
</div>  <!-- End of Index -->

There is no book\footnote{The only remark found about
Fanno flow that it was taken from the Fanno Master thesis by his
adviser.
Here is a challenge: find any book describing the history of the
Fanno model.}
that describes the history of these models.
For instance, the question, who was Fanno, and when did he live,
could not be answered by any of the undersigned's colleagues in
University of Minnesota or elsewhere.
<p>
At this stage there are more questions about the history
of compressible flow needing to be answered. 
Sometimes, these questions will appear in a section with a title but
without text or with only a little text. 
Sometimes, they will appear in a footnote like this\footnote{Who
developed the isothermal model? The research so far leads to Shapiro.
Perhaps this flow  should be named after the Shapiro.
Is there any earlier reference to this model?}.
<div class="invisible"> <!-- Index hock -->
	<a name="intro:sub:sub4">Shapiro Flow</a>"
</div>  <!-- End of Index -->

For example, it is obvious that Shapiro published the erroneous
conclusion that all the chocking occurred at $M=1$ in his article
which contradicts his isothermal model.  
Additional example, who was the first to &ldquo;conclude&rdquo; the
&ldquo;all&rdquo; the chocking occurs at $M=1$? Is it Shapiro?
<p>
Originally, there was no idea that there are special effects
and phenomena of compressible flow.
Some researchers even have suggested that compressibility can be
&ldquo;swallowed&rdquo; into the ideal flow (Euler's equation's flow is
sometimes referred to as ideal flow).
Even before Prandtl's idea of boundary layer appeared,
the significant and importance of compressibility emerged.
 
In the first half of nineteen century there was little realization that the
compressibility is important because there were very little applications (if
any) that required the understanding of this phenomenon. As there were no
motivations to investigate the shock wave or choked flow both were treated
as the same, taking compressible flow as if it were incompressible flow.
<p>
It must be noted that researchers were interested in the speed of
sound even long before applications and knowledge could demand any utilization.
The research and interest in the speed of sound was a purely academic interest.
The early application in which compressibility has a major effect was with fire arms.
The technological improvements in fire arms led to a gun capable of
shooting bullets at speeds approaching to the speed of sound.
Thus, researchers were aware that the speed of sound is some kind of limit.
<p>
<div class="invisible"> <!-- Index hock -->
	<a name="intro:aut:aut2">Mach, Ernest</a>"
</div>  <!-- End of Index -->

<div class="invisible"> <!-- Index hock -->
	<a name="intro:aut:aut3">Fliegner, Schweizer Bauztg</a>"
</div>  <!-- End of Index -->

In the second half of the nineteen century, Mach and Fliegner &ldquo;stumbled&rdquo;
over the shock wave and choking, respectively.
Mach observed shock and Fliegner measured the choking but
theoretical science did not provide explanation for it (or
was award that there is an explanation for it.). 
<p>
In the twentieth century the flight industry became the pushing force.
Understandably, aerospace engineering played a significant role in
the development of this knowledge.
Giants like Prandtl <div class="invisible"> <!-- Index hock -->
	<a name="intro:aut:aut4">Prandtl, Ludwig</a>"
</div>  <!-- End of Index -->

and his students like Von Karman<div class="invisible"> <!-- Index hock -->
	<a name="intro:aut:aut5">Von Karman, Theodore</a>"
</div>  <!-- End of Index -->
, as well as
others like Shapiro <div class="invisible"> <!-- Index hock -->
	<a name="intro:aut:aut6">Shapiro, Ascher</a>"
</div>  <!-- End of Index -->
, dominated the field.
During that time, the modern basic classes became &ldquo;solidified.&rdquo;
Contributions by researchers and educators from other fields were
not as dominant and significant, so almost all text books in this
field are written from an aerodynamic prospective.
<p>

<p>
<h3 class="subsec" id="sec1.3.1"> &sect; 1.3.1 Early Developments</h3>

 
<p>
The compressible flow is a subset of fluid mechanics/hydraulics and therefore the knowledge
development followed the understanding of incompressible flow.
Early contributors were motivated from a purely intellectual
curiosity, while most later contributions were driven by necessity.
As a result, for a long time the question of the speed of sound was
bounced around.
<p>
<h3 class="subsubsec" id="sec1.3.1.1"> &sect; 1.3.1.1 Speed of Sound</h3>


<p>
The idea that there is a speed of sound <div class="invisible"> <!-- Index hock -->
	<a name="intro:sub:sub5">Speed of sound</a>"
</div>  <!-- End of Index -->

and that it can be measured is a major achievement.
A possible explanation to this discovery lies in the fact that 
mother nature exhibits in every thunder storm the difference between
the speed of light and the speed of sound.
There is no clear evidence as to who came up with this concept,
but some attribute it to Galileo Galilei: 166x.
<div class="invisible"> <!-- Index hock -->
	<a name="intro:aut:aut7">Galilei, Galileo</a>"
</div>  <!-- End of Index -->

Galileo, an Italian scientist, was one of the earliest contributors
to our understanding of sound.
Dealing with the difference between the two speeds (light, sound)
was a major part of Galileo's work.
However, once there was a realization that sound can be measured,
people found that sound travels in different speeds through
different mediums.
The early approach to the speed of sound was by the measuring
of the speed of sound. 
<p>
Other milestones in the speed of sound understanding development were
by Leonardo Da Vinci,
<div class="invisible"> <!-- Index hock -->
	<a name="intro:aut:aut8">Da Vinci, Leonardo</a>"
</div>  <!-- End of Index -->

who discovered that sound travels in waves (1500). 
Marin Mersenne  was the first to measure the speed of sound in air (1640).
<div class="invisible"> <!-- Index hock -->
	<a name="intro:aut:aut9">Mersenne, Marin</a>"
</div>  <!-- End of Index -->

Robert Boyle discovered that sound waves must travel in a medium (1660) and this lead
to the concept that sound is a pressure change.<div class="invisible"> <!-- Index hock -->
	<a name="intro:aut:aut10">Boyle, Robert</a>"
</div>  <!-- End of Index -->
 
Newton<div class="invisible"> <!-- Index hock -->
	<a name="intro:aut:aut11">Newton, Isaac</a>"
</div>  <!-- End of Index -->
 was the first to formulate a relationship
between the speed of sound in gases by relating the density and
compressibility in a medium (by assuming isothermal process).
Newton's equation is missing the heat ratio, $k$ (late 1660's).
Maxwell was the first to derive the speed of sound for gas as $c=\sqrt{k\,R\,T}$
from particles (statistical) mechanics.<div class="invisible"> <!-- Index hock -->
	<a name="intro:aut:aut12">Maxwell, James Clerk</a>"
</div>  <!-- End of Index -->

Therefore some referred to coefficient $\sqrt{k}$ as Maxwell's coefficient.
<div class="invisible"> <!-- Index hock -->
	<a name="intro:sub:sub6">Maxwell's coefficient</a>"
</div>  <!-- End of Index -->
