// nmn.cc starting point Mon Mar  6 09:45:47 CST 2017
# include <iostream>
# include <fstream>
# include <sstream>
# include <string>
# include <stdio.h>
# include <string.h>
# include "mkHtml.hh"
# include "commonDef.h"
# include "utili.hh"
# include "nmn.hh"
using namespace std;

//extern "C" void nomenc(char* x){
extern "C" void nomencb(char* x){
	x = newLineToSpace (x);
	string l (x);
	peelFirstB(l, 1);
	size_t found = l.find('$');
	l.erase (0,found+1);
	found = l.find('$');
	if (found!=string::npos) {
		char y[256];
		memset(y, '\0', sizeof(char)*sizeof(y));
		l.copy(y,found,0);
		l.erase(0,found);
		sprintf(nmns[nmnMaxNumber].equation, "$%s$", y); 
	}
	peelFirstB(l, 1);
	found = l.find('}');
	if (found!=string::npos) {
		l.copy(nmns[nmnMaxNumber].description,found,0);
	}
	string n = mkNomeNu();
	n.copy(nmns[nmnMaxNumber].ref,n.size(),0);
	strcpy(nmns[nmnMaxNumber].fileName,workingFileName);
	cout << "<div class=\"nmn\" href=\"" << n 
		<< "\">" << nmns[nmnMaxNumber].equation << " - " <<
	 nmns[nmnMaxNumber].description	<< "</div>" << endl ;
	nmnMaxNumber++;
}

extern "C" void nomenc(char* x){
	string l (x);
	peelFirstB(l, 1);
	size_t found = l.find('}');
	if (found!=string::npos) {
		l.copy(nmns[nmnMaxNumber].equation,found,0);
		l.erase (0,found);
	}
	peelFirstB(l, 1);
	found = l.find('}');
	if (found!=string::npos) {
		l.copy(nmns[nmnMaxNumber].description,found,0);
	}
	string n = mkNomeNu();
	n.copy(nmns[nmnMaxNumber].ref,n.size(),0);
	strcpy(nmns[nmnMaxNumber].fileName,workingFileName);
	cout << "<div class=\"nmn\" id=\"" << n 
		<< "\">" << nmns[nmnMaxNumber].equation << " - " <<
	 nmns[nmnMaxNumber].description	<< "</div>" << endl ;
	nmnMaxNumber++;
}

extern "C" void mkNMN (){
	ofstream nmnFile ("nmn.php");
  ofstream *iFile;
  iFile =  &nmnFile;
  if (iFile->is_open()) {
    mkPHPheader(nmn, *iFile);
		for (unsigned i = 0 ; i < nmnMaxNumber ; i++){
    	*iFile << "\t\t\t<li>" << nmns[i].equation << " - "<<
			 		nmns[i].description << "</li>" << endl;
		}
    //mkLetter (nmn, nmnFile);
    mkPHPfooter(nmn, *iFile);
    iFile->close();
  }
  else {
    cout << "Unable to open file" << endl;
  }
}
