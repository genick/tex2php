#ifndef __DEF_H
#define __DEF_H
	#include "latex.h"
	#include "label.h"
	#include "sec.h"
	struct RECORD labels[2500]; // labels data structure
	struct INDEX auts[900];
	struct INDEX subs[900];
	struct LOS tocs[500];
	struct NOMENCLATURE nmns[200];
	struct RECORD  exms [300];
	extern struct LABEL figures[500];
	extern bool foundMainIndex;
	extern bool foundIndexFile;
	FILE* labelFile;
	FILE* footNoteFile;
	FILE* indexAutPHP;
	FILE* indexAutData;
	FILE* indexSubPHP;
	FILE* indexSubData;
	FILE* indexFigPHP;
	FILE* indexFigData;
	FILE* tocPHP;
	FILE* tocData;
	FILE* nmnPHP;
	FILE* nmnData;
	FILE* exmPHP;
	FILE* exmData;
	char workingFileName [100] ;
	extern const char EOL;
	extern int yy_flex_debug;
	size_t ln ;
	extern const int no ; 
	extern const int yes ; 
	int isIndexAutExist;
	unsigned indexAutMaxNumber, indexSubMaxNumber, tocMaxNumber, nmnMaxNumber;
	unsigned int figMaxNumber, exmMaxNumber, secMaxNumber;
#endif /* __DEF_H */
