// mkHtml.h initial build Mon Dec 12 09:30:18 CST 2016
#ifndef __MKHTML_H
#define __MKHTML_H
	#  include <cmath>
	#	 include <cstdio>
	#	 include <iostream>
	#	 include <fstream>
	#	 include <iomanip>
	#	 include <limits>
  #	 include <string>
	#	 include <unistd.h>
  #	 include <ctype.h>
  #	 include <stdlib.h>
  #	 include <stdbool.h>
  #	 include "commonDef.h"

	using namespace std;
	extern "C" char * extractFirstPart(char *, char);
	extern "C" char * extractSecondPart(char *, char);
	extern "C" struct RTN extractFileParts(char*, char);
	extern "C" void cleanNumeric (char*, char**) ;
	void mkLetter( enum dataType, std::ofstream& );
	void mkIndexItem (struct INDEX, enum dataType, std::ofstream&);
	void mkMarkerB (char* , enum dataType, std::ofstream&);
	void mkMarkerE (char* , enum dataType, std::ofstream&);
	void mkIndexHeaderFile(enum dataType, std::ofstream& );
	void mkIndexFooterFile(enum dataType, std::ofstream& );
	void mkPHPheader(enum dataType, std::ofstream& );
	void mkPHPfooter(enum dataType, std::ofstream& );

// struct tmpHold {
// 	char fileName[10][100];
// };
	
	/* set the default */
	extern uint indexAutMaxNumber, indexSubMaxNumber, figMaxNumber, nmnMaxNumber;
	extern enum listStatus lS ;
	extern int numberOfLabels;
	extern int  isSubstring(char *, char * ) ;
	// void openFiles(void);
	extern int closeTheFile (void);
	extern "C" char* trim(char* ) ; 
	//void mkIndexAut(char*, indexStatus, char* ) ;
	extern void mkIndexSub(char* ) ;
	extern void section (char*, char*);
	extern void  mkIA (char * tokenText, indexStatus stats , char* FN);
	extern char* removeBrackets(char*, char, char ) ; 
	extern void  wImgTMP (char * ) ;

	//extern struct INDEX indexs[900];
# include "extenVer.hh"

// 	extern struct INDEX subs[900];
// 	extern int isIndexAutExist;
// 	extern uint indexAutMaxNumber;
#endif /* __MKHTML_H */
