/* label.c file to deal with label */
	#include "label.h"
	#include "ioFiles.h"

char * lookupLabel (char * label, enum labelMember x ) {
	int lengthOne, lengthTwo, len;
	lengthOne = strlen(label);
	for( int i = 0; i < numberOfLabels; i++) {
		lengthTwo = strlen(labels[i].label);
		len = (lengthOne < lengthTwo) ? lengthOne : lengthTwo;
		if(strncmp(trim(label), trim(labels[i].label), len) == 0) {
			if ( x == nu){ 
				if (strncmp (trim(labels[i].level),"subsubs",7) != 0) {
					return labels[i].number ; 
				}
				else {
					return "*";
				}
			}
			else if (x == tl ){
				return labels[i].title; 
			}
			else if (x == ds ){
				return labels[i].description ; 
			}
			else if (x == kw ){
				return labels[i].keywords ; 
			}
			else if (x == lv ){
				return labels[i].level ; 
			}
 		}
	}
	return ("NO");
}

int readLabels (void) {
	char* line = NULL;
	int iLabel = 0;
	size_t len = 0;
	ssize_t read;
	labelFile = openDataFile("labelFile.data", oRdata );
  if (labelFile == NULL) {
		fprintf(stderr,"Cannot open Label file!\n");
    return 0;
  }
	char *token;
	char *search = ",";
	while ((read = getline(&line, &len, labelFile)) != -1) {
		if (strncmp (line,"EndOfData",9) == 0 ) { 
			numberOfLabels = iLabel ;
			break;
		}
		token = strtok(line, search);
		if (token != NULL ){
			strcpy(labels[iLabel].number ,trim(token));
		}
		else{
			continue;
		}
		token = strtok(NULL, search);
		if (token != NULL ){
			strcpy(labels[iLabel].level ,trim(token));
		}
		else{
			continue;
		}
		token = strtok(NULL, search);
		if (token != NULL ){
			strcpy(labels[iLabel].label ,trim(token));
		}
		else{
			continue;
		}
		token = strtok(NULL, search);
		if (token != NULL ){
			strcpy(labels[iLabel].title ,token);
		}
		else{
			continue;
		}
		token = strtok(NULL, search);
		if (token != NULL ){
			strcpy(labels[iLabel].page ,trim(token));
		}
		else{
			continue;
		}
		token = strtok(NULL, search);
		if (token != NULL ){
			strcpy(labels[iLabel].keywords ,trim(token));
		}
		else{
			continue;
		}
		++iLabel ;
	}
	fclose(labelFile);
	if (line)
 		free(line);
	return 1 ;
}

bool ref (char *token) {
	char x[240];
	char* totalS = strchr(token,'{');	
	totalS++;
	char* closing = strchr(totalS,'}');
	int totalSLength = strlen(totalS);
	int closingLength = strlen(closing);
	char* cleanLabel = (char *) malloc ((int) (totalSLength+40) * sizeof (char));
	cleanLabel=strndup(totalS, totalSLength-closingLength);	
	memset(x, '\0', sizeof(char)*sizeof(x));
	strcpy (x, lookupLabel(cleanLabel, nu));
	if (strcmp("NO", x)!=0){
		fprintf (stdout, "%s", x);
	}
	else {  
		fprintf (stdout, "%s", token);
	}
	free( cleanLabel);
	return true;
}

bool preRef(char *token) {
	char* totalS = strchr(token,'{');	
	totalS++;
	char* closing = strchr(totalS,'}');
	int totalSLength = strlen(totalS);
	int closingLength = strlen(closing);
	char* cleanLabel = (char *) malloc ((int) (totalSLength) * sizeof (char));
	cleanLabel=strndup(totalS, totalSLength-closingLength);	
	printf ( "%s", lookupLabel(cleanLabel, nu));
	free( cleanLabel);
	return true;
}
