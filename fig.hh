/* fi.hh file Thu Apr 27 08:24:14 CDT 2017 */
# ifndef __FIG_H
# define __FIG_H

#	include <iostream>
#	include <fstream>
#	include <sstream>
#	include <string>
#	include <cstring>
#	include "enumerators.h"
#	include "item.hh"
#	include "commSQL.hh"
#	include "utility.hh"
#	include "commDB.hh"
//#	include "mangerSQL.hh"
//# include "t2p.l"

	using namespace std;
	// extern commSQL * bt;
	// extern FIGURES *ptrF;

// #ifdef __FIG_C
// #define EXTERN
// #else
// #define EXTERN extern
// #endif
//
// 	EXTERN vector <string> subFigureLabels;

	void imgPDF(string);
	void pFigureIni(string, int, string );
	void pFigureEnd(string, int, string );
	void figureIni(string, int, string );
	void figureEnd(string, int, string );
	void figIni(string, int, string);
	void figEnd(string, int, string);
	void subFigure(string, int, string );
	void caption (string, int, string);
	void wImg(string, int, string);
	void picTextPDF( string, int, string);
	void setRef (string, int, string);
//	void subfigure( char* );
//	void caption( char* );
	extern string itemName;

#endif   /* End __FIG_H  */
