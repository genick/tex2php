# # Author: Genick Bar-Meir  Thu Mar  7 14:08:43 CST 2019 add footnote
# # last change Date: Sun Dec 11 17:52:17 CST 2016 add posTt2p
# # Sun Dec 11 17:52:17 CST 2016 initial construction
# # Date: Fri Sep  2 17:49:26 CDT 2016
# # Description: makefile to convert tex to php 
# #
FLEX = flex 
FLEXFLAGS = -lfl
MYSQL = `mysql_config --cflags --libs`
MYSQL = -I/usr/include/mysql -L/usr/lib64/mysql
MYSQL += -lmysqlclient -lpthread -lz -lm -lssl -lcrypto -ldl

CC = gcc
# CXX = clang 
CXX = g++
CFLAGS = -g -c -Wall -Wno-parentheses
CXXFLAGS = -std=c++17
#LD = gcc  for the c version of the compilation
LD = g++
# set the flags for debugging and warning
LDFLAGS = -g -Wall 

# the declarations of the pret2p program 
# the headers that many compelation depent on.
headers  	=  preDef.hh commDB.hh commSQL.hh fig.hh item.hh mngmnt.hh \
					nomen.hh sec.hh utility.hh io.hh label.hh footnote.hh

# the objects related to MySQL 

t2pObjs = t2p.o utility.o math.o fig.o mngmnt.o commDB.o \
				 	nomen.o sec.o index.o io.o exm.o footnote.o 

preObj = pret2p.o utility.o mngmnt.o commDB.o nomen.o fig.o io.o \
					 exm.o math.o	
						
sqlObjs = item.o commSQL.o

postObj = posTt2p.o inLineEQ.o utility.o mngmnt.o commDB.o nomen.o  \
				 	fig.o io.o  

t2pHeaders = math.hh utility.hh enumerators.h commDB.hh exm.hh 

p2pHeaders = item.hh commSQL.hh enumerators.h utility.hh mngmnt.hh

postHeaders = io.h mngmnt.h 

# rules on how to make the objects
all:t2p

t2p:  t2p.l t2p.cc $(t2pObjs) $(t2pHeaders) $(sqlObjs)
	${LD} ${LDFLAGS} $(t2pObjs) $(sqlObjs) -o $@ ${FLEXFLAGS} \
		`mysql_config --cflags --libs`
	chmod a+x t2p
	echo "done, your executable is ./t2p."

pret2p: 	pret2p.l pret2p.cc $(preObj) 
	${LD} ${LDFLAGS}  $(preObj) $(sqlObjs) -o $@ ${FLEXFLAGS} \
		`mysql_config --cflags --libs`  
	chmod a+x pret2p
	echo "done, your executable is ./pret2p."

posTt2p: posTt2p.l posTt2p.cc  $(postObj) 
	${LD} ${LDFLAGS}  $(postObj) $(sqlObjs) -o $@ ${FLEXFLAGS} \
		`mysql_config --cflags --libs`  
	chmod a+x $@
	echo "done, your executable is ./postt2p."

t2p.cc: t2p.l
	${FLEX} --outfile=t2p.cc t2p.l 

posTt2p.cc: posTt2p.l
	${FLEX} --outfile=$@ posTt2p.l

t2p.o:	t2p.l t2p.cc $(t2pHeaders) $(p2pHeaders) 
	${CXX} -o t2p.o ${CFLAGS} ${CXXFLAGS} t2p.cc ${FLEXFLAGS} \
		`mysql_config --cflags --libs` 

utility.o: utility.cc $(headers) 
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $<

utilities.o: utilities.cc $(headers) 
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $<

inLineEQ.o: inLineEQ.cc $(posTt2p) 
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $< \
		`mysql_config --cflags --libs` 

io.o: io.cc $(headers) 
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $< `mysql_config --cflags --libs`

sec.o: sec.cc $(headers) 
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $< `mysql_config --cflags --libs`

fig.o: fig.cc $(headers) 
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $< `mysql_config --cflags --libs`

footnote.o: footnote.cc $(headers) 
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $< `mysql_config --cflags --libs`

index.o: index.cc $(headers) 
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $< `mysql_config --cflags --libs`

nomen.o: nomen.cc $(headers) 
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $< `mysql_config --cflags --libs`

label.o: label.cc  
	${CXX} -o $@  ${CFLAGS} ${CXXFLAGS} $< `mysql_config --cflags --libs`

commDB.o: commDB.cc $(headers) $(sqlObjs)
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $< `mysql_config --cflags --libs`

mngmnt.o: mngmnt.cc mngmnt.hh 
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $< `mysql_config --cflags --libs`

math.o: math.cc math.hh ut.*
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $<

item.o: item.cc item.hh enumerators.h
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $< 

commSQL.o: commSQL.cc commSQL.hh item.hh enumerators.h
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $< `mysql_config --cflags --libs`

main.o: callSQL.cc commSQL.hh item.hh enumerators.h
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $< `mysql_config --cflags --libs`

utlty.o: utlty.cc  utlty.hh 
	${CXX} -o  $@  ${CFLAGS} ${CXXFLAGS} $< 

misc.o: misc.cc misc.h misc.hh $(headers) 
	${CXX} -o misc.o ${CFLAGS} ${CXXFLAGS} misc.cc 

nmn.o: nmn.cc nmn.h nmn.hh $(headers) 
	${CXX} -o nmn.o ${CFLAGS} ${CXXFLAGS} nmn.cc 

utili.o: utili.cc utili.h utili.hh $(headers) 
	${CXX} -o utili.o ${CFLAGS} ${CXXFLAGS} utili.cc 

mkHtml.o: mkHtml.cc mkHtml.h mkHtml.hh  
	${CXX} -o mkHtml.o ${CFLAGS} ${CXXFLAGS} mkHtml.cc 

postt2p.c: 	postt2p.l $(pHeaders) 
	${FLEX} --outfile=postt2p.c postt2p.l 

postt2p.o: postt2p.c  $(pHeaders)
	${CC} -o postt2p.o ${CFLAGS} postt2p.c 

pret2p.cc: 	pret2p.l $(pHeaders) 
	${FLEX} --outfile=pret2p.cc pret2p.l 

pret2p.o: pret2p.cc  $(pHeaders) utility.cc
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $<  `mysql_config --cflags --libs`

posTt2p.o: posTt2p.cc  utility.cc
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $<  

exm.o: exm.cc  $(pHeaders) utility.cc
	${CXX} -o $@ ${CFLAGS} ${CXXFLAGS} $<  `mysql_config --cflags --libs`

management.o: management.c management.h $(headers)
	${CC} -o management.o ${CFLAGS} management.c 

#  the c version of the footnote
# footnote.o: footnote.c  $(headers)
# 	${CC} -o footnote.o ${CFLAGS} footnote.c 

list.o: list.c  $(headers)
	${CC} -o list.o ${CFLAGS} list.c 

ioFiles.o: ioFiles.c  $(headers)
	${CC} -o ioFiles.o ${CFLAGS} ioFiles.c 

test: tex2php.l  index.h 
	echo "target:" $@
	echo "prereqs:" $<
	echo "newer than target" $?

ctags: 
	ctags -x *.c > ctags

clean:
	rm -rfv *.o core* a.out 

cleanAll:
	rm -rfv *.o tex2php a.out core* texTOphp.c pret2p postt2p pret2p.c \
 	t2p.c texTOphp.c pret2p 

# the old stuff before total removing
objects =  tex2php.o ioFiles.o utilities.o \
					 section.o footnote.o list.o index.o figure.o 

cppObjs = mkHtml.o fig.o utili.o sec.o nmn.o math.o ioData.o misc.o

addObjs 	= management.o

tex2php:  $(objects) $(cppObjs) $(addObjs) $(sqlObjs) 
	${LD} ${LDFLAGS}  $(objects) $(cppObjs) $(addObjs) $(sqlObjs) -o tex2php ${FLEXFLAGS} `mysql_config --cflags --libs` 
	chmod a+x tex2php
	echo "done, your executable is ./tex2php."
#echo "prereqs:" $<

objPostt2p =  postt2p.o ioFiles.o utility.o

OBJECTS  = t2p.o ioFiles.o utilities.o section.o footnote.o \
					 list.o index.o figure.o mkHtml.o fig.o  utili.o sec.o \
					 nmn.o math.o ioData.o misc.o management.o 

# not clear what thse hedear are doing to be remove in the futre
pHeaders = def.h latex.h ioFiles.h index.h label.h utility.hh misc.h ioData.h

main: item.cc item.hh commSQL.cc commSQL.hh enumerators.h $(sqlObjs) main.o callSQL.cc
	${LD} ${LDFLAGS} $(sqlObjs) main.o -o main `mysql_config --cflags --libs`

ioData.o: ioData.cc ioData.h ioData.hh $(headers) 
	${CXX} -o ioData.o ${CFLAGS} ${CXXFLAGS} ioData.cc  ${MYSQL} 
#	${CXX} -o ioData.o ${CFLAGS} ${CXXFLAGS} ioData.cc  `mysql_config --cflags --libs` 

