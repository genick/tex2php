#ifndef __UTLTY_HH
#define __UTLTY_HH

#include <iostream>
#include <string>
using namespace std;

static const char* wss = " \t\n\r\f\v";
//inline  
string& rtrim(string& s, const char* t = wss);
//inline 
string& ltrim(string& s, const char* t = wss);
//inline 
string& trim(string& s, const char* t = wss);

#endif   // __UTLTY_HH
