// Wed Jul  3 15:42:35 CDT 2019 change from FIGURES to LABELS
//nomen.cc starting point Fri May  5 09:05:49 CDT 2017
#	include "nomen.hh"

void nomencb(string  l, int lineNumber, indexStatus y){
	// nmnNumber++;
	logFile << "nomencb " << "line " << lineNumber << ": "<< l << endl ;
	l = rmTabNewLine (l);
	// ostringstream ss;
	string buffer, n;
	LABELS fig(1);
	LABELS *F;
	F = &fig;
	enum actionType z = doNothing; 
	if ( y == subSup || y == subSupIni) {
		z = subSupCase;
	}
	extractParameter(F, l, 1, AUX,'{', '}', z); // keep dollar signs
	extractParameter(F, l, 1, CA,'{', '}'); // keep dollar signs
	F->setMemberValue ("nmn",LV);
	n = mkRefNu(NMN);
	F->setMemberValue (n,RF);
	F->setMemberValue (n,LB);
	F->setMemberValue (workingFileName, FN);
	// regularIni, see, subSup, subSupIni
	if ( (y == regularIni) || (y == subSupIni) ){
		cout << "<div class=\"nomen\">" << endl;
	}
	cout << "\t<div class=\"NMNrow\">" << endl;
	cout << "\t\t<span class=\"symbol\">" <<
		"<a name=\"" << n << "\">" <<	F->getMemberValue(AUX, 0, ' ') 
		<< "</a> :"  << endl << "\t\t</span>" << endl 
		<< "\t\t<span class=\"depiction\">\n\t\t\t\t" 
		<< F->getMemberValue(CA, 0, ' ') 
		<< "\n\t\t</span>" << endl  ; 
		cout << "\t</div> <!-- nomenclature row -->" << endl;
		// << "</div>" << endl ;
	// nmnNumber++;
	vector <itemMember> data = {CA, AUX, RF, FN};                                   
	vector <intMember> dataI = {};
	insertDataDB(F, data, dataI, NMN, "nomen");
}

void endNomen(string l, int lineNumber){
	// toBeChanged(l, lineNumber,"end nomen");
	logFile << "end nomenclature on line " << lineNumber  << " :"<< l ;
	cout << "</div> <!-- nomenclature end --> " << endl;
	logFile << "converted into: </div>" << endl;
}

void endNomenclature(string l, int lineNumber){
	logFile << "end nomenclature on line " << lineNumber  << " :"<< l ;
	cout << "endNomenclature" << endl;
	logFile << "converted into: " << "endNomenclature" << endl;
}
// 	
// 	extern "C" void nomenc(char* x){
// 		string l (x);
// 		peelFirstB(l, 1);
// 		size_t found = l.find('}');
// 		if (found!=string::npos) {
// 			l.copy(nmns[nmnMaxNumber].equation,found,0);
// 			l.erase (0,found);
// 		}
// 		peelFirstB(l, 1);
// 		found = l.find('}');
// 		if (found!=string::npos) {
// 			l.copy(nmns[nmnMaxNumber].description,found,0);
// 		}
// 		string n = mkNomeNu(NMN);
// 		n.copy(nmns[nmnMaxNumber].ref,n.size(),0);
// 		strcpy(nmns[nmnMaxNumber].fileName,workingFileName);
// 		cout << "<div class=\"nmn\" id=\"" << n 
// 			<< "\">" << nmns[nmnMaxNumber].equation << " - " <<
// 		 nmns[nmnMaxNumber].description	<< "</div>" << endl ;
// 		nmnMaxNumber++;
// 	}
// 	
// 	extern "C" void mkNMN (){
// 		ofstream nmnFile ("nmn.php");
// 		ofstream *iFile;
// 		iFile =  &nmnFile;
// 		if (iFile->is_open()) {
// 			mkPHPheader(nmn, *iFile);
// 			for (unsigned i = 0 ; i < nmnMaxNumber ; i++){
// 				*iFile << "\t\t\t<li>" << nmns[i].equation << " - "<<
// 						nmns[i].description << "</li>" << endl;
// 			}
// 			//mkLetter (nmn, nmnFile);
// 			mkPHPfooter(nmn, *iFile);
// 			iFile->close();
// 		}
// 		else {
// 			cout << "Unable to open file" << endl;
// 		}
// 	}
