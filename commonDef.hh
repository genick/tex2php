#ifndef __COMMONDEF_H
#define __COMMONDEF_H
# include <stddef.h>
#include <stdbool.h>
#include "enumerators.h"

	//enum labelType	{FIG, SUBFIG};
	//enum labelMember {lb=11, nu=12, pg=13, tl=14, ds=15, fn=16, lv=17, kw=18, nn=24};

	struct LABEL {// figures especially for subfigure 
		//char title[256]; // title of the associated with the label 
		//char description[256]; // title of the associated with the label 
		char level[24]; // level like section or subsection
		char label[5][56]; // actual label name for a
		char number[5][12]; // number correspond to the label
		char caption[5][256]; // caption 0 main and rest for sub 
		char captionT[5][256]; // caption table of figure 0 main and rest for sub
		char captionO[5][256]; // caption overtext 0 main and rest for sub
		char fileName[5][100]; // file names A=0 B=1 C=2 etc 
		char keywords[256]; // key words for search
		char page[12]; // page in the book
		int nn;
		//int associatedNumber;
		enum labelType type;
	};

	struct INDEX { //
		public:
			INDEX(int = 0, string = "", string = "", double = 0.0);
			char indexName[100];
			char fin[256];
			char textOver[524];
			char captionA[1024];
			char captionB[1024];
			char captionT[1024];
			char fileName[8][100];
			char linkedFile[2][100];
			char seeAlso[256];
			int  numberFiles;
			int  in;
	};

	struct LOS { //
		char title[1024];
		char label[32];
		char fileName[100];
		char number[32];
		int  n[7];
		char apdxN;
		int  nN;
		bool isAleph;
	};

	struct NOMENCLATURE { //
		char ref [24];
		char equation [124];
		char description [214];
		char fileName [64];
		char page [6];
	};

	struct RECORD { // to change name of the definition
		char fileName[64];
		char label[32];
		char keywords[256];
		char description[256];
		char title[256];
		char page[6];
		char level[36];
		char number[9];
	};

	struct RTN {  // returning the data in function call
		char fP [264];
		char sP [64];
	};


	// enum listStatus {unOpenList=1, firstOpenList, middleFirstList, finishedFirstList,
	//		secondOpenList, middleSecondList, finishedSecondList};
	// typedef enum { regular=100, see=101 } indexStatus;
	// enum indexStatus { regular=100, see=101 };
	// enum dataType { sub = 314, aut=315, fig=316, table=317, 
	//		toc=318, sec=319, nmn=320, exm=321, reference=322};
	// 	enum FILEnAME { label=0,  overTxt  };
	// 	enum oMode { oWdata=10, oRdata, oWphp };

#endif /* __COMMONDEF_H  */
