//  mkHtml.cc initial Mon Dec 12 09:36:08 CST 2016
	# include "mkHtml.hh"
	# include "commonDef.h"
	# include	<cstring>
	# include <cctype> 
	# include <string>
	# include <iostream>     // std::cout
	#	include <sstream>      // std::stringstream, std::stringbuf
	#include <time.h>
	//# include <ctype.h>

extern "C" void printCH(void){
	cout <<  "thiSis CC test" ;
}

void mkIndexItem (struct INDEX indexPtr, enum dataType x, std::ofstream& indexFile) {
	//static char firstPart[64], secondPart[64] ;
	//extractFileName ("test");
	for ( int j=0;  j < indexPtr.numberFiles; j++){
		struct RTN items;
		items = extractFileParts(indexPtr.fileName[j], '#');
		char * numbericFree;
		cleanNumeric( items.sP, &numbericFree );
		indexFile << "\t\t\t\t\t<li><a href=\""<< indexPtr.fileName[j] 
			<< "\"" << endl << "\t\t\t\t\t title=\"" << numbericFree <<  " in file "
			<< items.fP << "\">"
			<< indexPtr.fin << "</a></li>" << endl ;
		if (numbericFree)
			free(numbericFree);
	}
}

void mkMarkerB (char * token, enum dataType x, std::ofstream& iFile){
	iFile << "\t\t\t<li>" << endl  
		<< "\t\t\t\t<a class= \"toggle\" href=\"javascript:void(0);\" >"
		<< ((char) toupper(token[0])) << "</a>" << endl 
		<< "\t\t\t\t<ul class=\"inner\">" << endl; 
}

void mkMarkerE (char* token, enum dataType x, std::ofstream& iFile){
	iFile << "\t\t\t\t</ul>" << endl 
		<<  "\t\t\t</li>" <<  endl; 
}

void  mkLetter( enum dataType x, std::ofstream& iFile){
	struct INDEX *indexPtr, *placeHolder, *lastMark, *lastIndex ;
  uint *maxPtr;
  if ( x == aut ) { 
    indexPtr = auts ;
    maxPtr = &indexAutMaxNumber ;
  }
  else if ( x == sub ) { 
    indexPtr = subs ;
    maxPtr = &indexSubMaxNumber ;
  }   
  else if ( x == fig ) { 
    //indexPtr = figures ;
    maxPtr = &figMaxNumber ;
  }
  else if ( x == nmn ) { 
    maxPtr = &nmnMaxNumber ;
  }
  else {
    indexPtr = auts ;
    maxPtr = &indexAutMaxNumber ;
  }
	placeHolder = indexPtr;
	lastIndex = indexPtr + (*maxPtr) - 1; // lastIndex->fin 
	while ((lastIndex - indexPtr)>= 0){
		mkMarkerB (indexPtr->fin, x, iFile);
		placeHolder = indexPtr;	
		while (indexPtr->fin[0] == placeHolder->fin[0]){
			mkIndexItem ((*placeHolder), x, iFile);
			placeHolder++;
			lastMark = placeHolder; //lastMark->fin
		}
		mkMarkerE (indexPtr->fin, x, iFile);
			indexPtr = lastMark ;
	}
	return;
}
	//string junk = symbol.str();
	//strncpy(lll, junk.c_str(), sizeof(lll)); 
	//lll[sizeof lll - 1] = '\0';
	//letter =  junk.c_str();
	//letter = const_cast<char*> ( junk.str())
	//letter = symbol ;
	//return lll;

extern "C" void mkTOC (){ 
	ofstream tocFile ("toc.php");
	ofstream *iFile;
	iFile =  &tocFile;
	if (iFile->is_open()) {
		mkPHPheader(toc, *iFile);
		*iFile <<   tocs[0].title << " " << tocs[0].number <<  " " << " " 
			<< workingFileName << endl ;
		mkLetter (sub, tocFile);
		mkPHPfooter(sub, *iFile);
		iFile->close();
	}
	else {
		cerr << "Unable to open file" << endl;
	}
}

extern "C" void mkIndexHTML() {
	ofstream autFile ("indexAut.php");
	ofstream subFile ("indexSub.php");
	ofstream *iFile ;	
	for ( int iName = sub; iName <= aut; iName++) {
		enum dataType x;
		if (iName  == aut) {
			x = aut;
			iFile = & autFile;
		}
		else if (iName  == sub) {
			x = sub;
			iFile = & subFile;
		}
		else {
			x = aut;
			iFile = & autFile;
		}
		if (iFile->is_open()) {
			mkIndexHeaderFile(x, *iFile);
			mkLetter (x, *iFile);
			mkIndexFooterFile(x, *iFile);
			iFile->close();
		}
		else {
			cout << "Unable to open file";
		}
	}
  return ;
}

void  mkPHPheader(enum dataType x, std::ofstream& iFile){
	char title[64];
	memset(title, '\0', sizeof(title));
	if  (x == toc){
		strncpy(title, "Table of Contents", strlen("Table of Contents"));  	
	}
	else if (x == nmn){
		strncpy(title, "Nomenclature", strlen("Nomenclature"));  	
	}
	else {
		strncpy(title, "Table of Contents", strlen("Table of Contents"));  	
	}
	char *tP = title;
	iFile << "<h2 class=\"chapter\"> " << tP << " </h2>" << endl  ;
	iFile << "<div class=\"indexWrap\">" << endl  ;
	iFile << "\t<div class=\"accordion\">" << endl  ;
	iFile << "\t\t<ul class=\"letter\">" << endl  ;
}

void mkIndexHeaderFile(enum dataType x, std::ofstream& indexFile){
	char title[64];
	memset(title, '\0', sizeof(title));
	if  (x == sub){
		strncpy(title, "Authors", strlen("Authors"));  	
	}
	else if (x == aut){
		strncpy(title, "Subjects", strlen("Subjects"));  	
	}
	char *tP = title;
	indexFile << "<h2 class=\"chapter\"> " << tP << " Index</h2>" << endl  ;
	indexFile << "<div class=\"indexWrap\">" << endl  ;
	indexFile << "\t<div class=\"accordion\">" << endl  ;
	indexFile << "\t\t<ul class=\"letter\">" << endl  ;
}

void  mkPHPfooter(enum dataType x, std::ofstream& indexFile){
	char title[64];
	memset(title, '\0', sizeof(title));
	if  (x == toc){
		strncpy(title, "Authors", strlen("Authors"));  	
	}
	else if (x == aut){
		strncpy(title, "Subjects", strlen("Subjects"));  	
	}
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);
	char s[64];
	strftime(s, sizeof(s), "%c", tm);
	//printf("%s\n", s);

	char *tP = title;
	indexFile << "\t\t</ul>" << endl  ;
	indexFile << "\t</div>" << endl  ;
	indexFile << "</div>" << endl  ;
	indexFile << "<footer> Constructed: " << tP << " Index  " <<  s << "</footer>" << endl  ;
}

void  mkIndexFooterFile(enum dataType x, std::ofstream& indexFile){
	char title[64];
	memset(title, '\0', sizeof(title));
	if  (x == sub){
		strncpy(title, "Authors", strlen("Authors"));  	
	}
	else if (x == aut){
		strncpy(title, "Subjects", strlen("Subjects"));  	
	}
	time_t t = time(NULL);
	struct tm *tm = localtime(&t);
	char s[64];
	strftime(s, sizeof(s), "%c", tm);
	//printf("%s\n", s);

	char *tP = title;
	indexFile << "\t\t</ul>" << endl  ;
	indexFile << "\t</div>" << endl  ;
	indexFile << "</div>" << endl  ;
	indexFile << "<footer> Constructed: " << tP << " Index  " <<  s << "</footer>" << endl  ;
}
