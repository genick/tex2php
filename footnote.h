#ifndef __FOOTNOTE_H_LIB
#define __FOOTNOTE_H_LIB

	#include <stdio.h>
  #include <unistd.h>
  #include <string.h>
  #include <ctype.h>
  #include <stdlib.h>
  #include <stdbool.h>
	#include "def.h"

	void doFootNoteMark(void) ;
	void doFootNoteText (char* );
	void doFootNote(char* ) ;
	int closeTheFile(void) ;
	void tColorBox (char* ) ;

	static uint footNoteNumber ; 

#endif /* end footnote.h */
