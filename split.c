/* strtok example */
#include <stdio.h>
#include <string.h>

int main ()
{
  char str[512] ;
	sprintf ( str, "- This, \n a \n sample \n string.") ;
  char * pch;
  printf ("Splitting string \"%s\" into tokens:\n",str);
  pch = strtok (str,"- ,\n");
	/*
  pch = strtok (str," ,.-");
	*/
  while (pch != NULL)
  {
    printf ("%s\n",pch);
    pch = strtok (NULL, "\n ,.-");
  }
  return 0;
}
