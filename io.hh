/* io.hh file Tue Oct 24 06:54:06 CDT 2017 */
# ifndef __IO_HH
# define __IO_HH

#	include <iostream>
#	include <fstream>
#	include <sstream>
#	include <string>
#	include <cstring>
#	include "enumerators.h"
#	include "item.hh"
// #	include "commSQL.hh"
#	include "utility.hh"
// #	include "commDB.hh"

	using namespace std;
	extern ofstream logFile;
	extern ofstream footnoteFile;
	// extern commSQL * bt;
	// extern FIGURES *ptrF;

// #ifdef __FIG_C
// #define EXTERN
// #else
// #define EXTERN extern
// #endif
//
// 	EXTERN vector <string> subFigureLabels;

	void footnoteMsg (string);	
	void errMsg(string );

#endif   /* End __IO_HH  */
