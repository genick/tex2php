#ifndef __UTILITIES_H__
#define __UTILITIES_H__
	#include <stdio.h>
  #include <unistd.h>
  #include <string.h>
  #include <ctype.h>
  #include <stdlib.h>
  #include <stdbool.h>
	#include "includeFiles.h"
	#include "def.h"
	#include "latex.h"

	//
	int  isSubstring(char*, char* ) ;
	void cleanNumeric( char *, char ** );
	struct RTN extractFileParts (char*, char);
	extern char * extractFirstPart (char*, char);
	extern char * extractSecondPart (char*, char);
	const char* getEnumName(enum dataType);
	char * removeBrackets(char*, char, char ) ;
	extern char * trim(char* ) ;
	char * extractFileName (char *);
	char * replaceChar (char*, char, char*);
	char * cleanText(char* , char* );
	char * cleanTabNewLine (char* );
	char * newLineToSpace (char* ) ;
	void comment (char *);
	void emptyLine ( char *) ;
	void emptySpace ( char *, char *);
	void realPercent ( char *);
	void sortIndexData (enum dataType);
	void printIndex(size_t, enum dataType, char* ) ;
	void printLos(size_t, char * );
	void mkSee(char *, char *, char*, char*) ;
	int compareIndexs(const void *, const void *) ;
	int compareLos(const void *, const void *) ;
	char * rmChar(char *, char );
	//char * shortnStg (char *, char*, int );
	void shortnStg(char *, char **, int );
	void tocToNumber(char*);

#endif /* __UTILITIES_H__ */
