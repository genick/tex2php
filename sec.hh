#ifndef __SEC_HH
#define __SEC_HH
// fig.hh initial build Fri Feb 17 15:16:15 CST 2017
	# include <cmath>
	#	include <cstdio>
	#	include <cstring>
	#	include <iostream>
	#	include <iomanip>
	#	include <limits>
  #	include <string>
  #	include <stdlib.h>
  #	include <stdbool.h>
  // #	include <ctime>
	#	include "commSQL.hh"
	#	include "commDB.hh"
	// #	include "preDef.hh"
	#	include "enumerators.h"
	#	include "utility.hh"
	#	include "item.hh"
	extern commSQL * bt;
	using namespace std;
	void section (string, string, int, string );
	void issue (string, string, int, string );
	extern string workingFileName;
	extern int issueNumber;
	//extern unsigned figMaxNumber, nmnMaxNumber, secMaxNumber;
	//extern struct LABEL figures[500];
	//extern struct nomenclature nmns[200];
	//extern "C" char * lookupLabel (char *, enum labelMember);
	//

#endif /* __SEC_HH */
