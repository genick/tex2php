#ifndef __ioDATA_HH
#define __ioDATA_HH
// ioData.hh initial build

	# include <cmath>
	#	include <cstdio>
	#	include <iostream>
	#	include <fstream>
	#	include <iomanip>
	#	include <limits>
  #	include <string>
	#	include <unistd.h>
  #	include <ctype.h>
  #	include <stdlib.h>
  #	include <stdbool.h>
	#	include "extenVer.hh"
	using namespace std;
	extern unsigned figMaxNumber, nmnMaxNumber, exmMaxNumber;
	extern struct NOMENCLATURE nmns[200];
	extern struct RECORD exms[300];
	extern "C" char * trim(char*); 
	extern "C" char * lookupLabel (char *, enum labelMember );
	extern "C" char * newLineToSpace(char*); 

	struct  sqlDetails{
		char server[24];
		char user[16];
		char password[28];
		char database[36];
	};

#endif /* __ioDATA_HH */
