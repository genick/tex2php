/* section translation table of content toc */
/* Section manipulation */
#include "section.h"
#include "utilities.h"
char * mkSectionName (char * secName, char ** rV ){
	char sectionName [50];
	uint length;
	length = strlen (secName);
	*rV = (char *) malloc ( 10*length * (sizeof(char)) + 1 );
	memset(sectionName, '\0', sizeof(sectionName)); 
	if ( (strcmp(secName, "chapter") == 0) || (strcmp(secName, "Chapter") == 0)){ 
		strcpy (sectionName, "Chapter") ;
		strcpy (*rV, "Chapter") ;
	}
	else if ( (strcmp(secName, "sub") == 0) || (strcmp(secName, "Sub") == 0)){ 
		strcpy (sectionName, "Section") ;
		strcpy (*rV, "Section") ;
	}
	else if ( (strcmp(secName, "subsub") == 0) || (strcmp(secName, "SubSub") == 0)){ 
		strcpy (sectionName, "Section") ;
		strcpy (*rV, "Section") ;
	}
	else { 
		strcpy (sectionName, "Section") ;
		strcpy (*rV, "Section") ;
	}
	return secName;
}

void cleanSection(char* tokenText, char* sectionLevel){
	char sectionName [50], *sV;
	memset(sectionName, '\0', sizeof(sectionName)); 
	mkSectionName(sectionLevel, &sV );
	strcpy (sectionName, sV);
	if (sV)  
		free (sV);
	char* totalSectionString = strchr(tokenText,'{');
	++totalSectionString ;
	while (totalSectionString[0] == '~'){ /* strip out the tilde */
		totalSectionString++;
	}
	int totalStringLength = strlen(totalSectionString);
	int labelPosition = isSubstring (totalSectionString, "label");
	int labelLength = totalStringLength - labelPosition ;
	char* labelLocation = strchr(totalSectionString,'{');
	++labelLocation ;
	int sectionTextLength = totalStringLength - labelLength ; 
	char* labelClosingLocation = strchr(labelLocation,'}');
	char* labelString = (char *) malloc ((int) (labelLength+40) * sizeof (char));
	int labelClosingLocationLength =  strlen(labelClosingLocation)+1;
	labelString=strndup(labelLocation , strlen(labelLocation)- labelClosingLocationLength +1);
	char* sectionString = (char *) malloc ((int) (totalStringLength) * sizeof (char));
	sectionString=strndup(totalSectionString, sectionTextLength-2);
	char toKey [30];
	memset (toKey, '\0', sizeof(toKey)); 
	strcpy (toKey,lookupLabel(labelString, nu)); 
	strtok(toKey, "\n"); // remove the new line character
	char* toPHP = (char *) malloc ((int) (10*totalStringLength+40) * sizeof (char));
	collectTOC(sectionString, sectionLevel, labelString, toKey);
	sprintf ( toPHP, "<h2 class=\"%s\"><a id=\"%s\"></a>%s %s %s</h2>",
	 sectionLevel, trim(labelString), sectionName, toKey, sectionString); 
	printf ("\n%s", toPHP);
	if (labelString)
		free (labelString);
	if (sectionString)
		free (sectionString);
	if (toPHP)
		free (toPHP);
	return;
}

void cleanSectionS(char* token, char* sectionLevel){
	char opening ='{';
  char closing ='}';
  // char * cleanTxt =
	removeBrackets( token, opening, closing);
}

void collectTOC(char * token, char * level, char * label, char * number ){
	char potentialFileName [64];
	memset(potentialFileName, '\0', sizeof(potentialFileName));
	sprintf(potentialFileName, "%s#s%s",workingFileName, number);
	strncpy(tocs[tocMaxNumber].title,token, strlen(token));
	strncpy(tocs[tocMaxNumber].label, label , strlen(label));
	strncpy(tocs[tocMaxNumber].number,number, strlen(number));
	strncpy(tocs[tocMaxNumber].fileName,potentialFileName, strlen(potentialFileName));
	tocToNumber(number); // put the numbers in digit forms.
	tocMaxNumber++;
	//printLos (tocMaxNumber, "collectTOC");
}
