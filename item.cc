/*  item.cc */
#	include "item.hh"

using namespace std;

string setDlim(string l, char d){
	if ((d == '\0')|| d== ' ') {
		return l;
	}
	else{ 
		stringstream ss;
		ss << d << l << d;
		return ss.str();
	}
}

ITEM::ITEM(string& l, string& n, string& p)
	: page(move(p)) {
	//label.clear();
	label.push_back(move(l));
	number.push_back(move(n));
}

ITEM::ITEM(int n) : nn(move (n)) {
}

ITEM::~ITEM(){
}

void ITEM::setMemberValue (string l, enum itemMember x, bool n ){
	if (x == LB) {
		label.push_back(l);
	}
	else {
		refNAME = l;
	}
}

void ITEM::setMemberValueI (int num, enum intMember x, bool y ){
	 // Lb;
	if ( x == Nn ) {
		nn = num;
	}
	else if ( x==Pg ) {
		p= num;
	}	
	else {
		errMsg ("no variable to set \n")  ; 
	}
}

int ITEM::getMemberValueI(intMember i, int n) {
	if (i == nn){
			return Nn; 
	}
	else if (i == Pg){
			return p; 
	}
	// else if (i == CN){
	// 		return CL[n];
	// }
	else {
			return Nn; 
	}
}

string ITEM::getMemberValue(enum itemMember x, int n, char d){
	if (x == LB){
		return setDlim(label[n], d);
	}
	else if (x == RF){
		return setDlim(refNAME, d);
	}
	else {
		return setDlim(refNAME, d);
	}
}

FIGURES::FIGURES(int n) :ITEM (n),
	MEMBERS({"keywords", "level", "page"}),
	MMEMBERS ({"label","number","caption","captionT","captionO","fileName"}){
}

FIGURES::FIGURES(string l, string n, string p, string kw) 
	: ITEM(l, n, p), keywords(move(kw)),
 	MEMBERS ({"keywords", "level", "page"}),	
	MMEMBERS ({"label","number","caption","captionT","captionO","fileName"})
	{
}

FIGURES::FIGURES(string & l, string & n, string & p, string & kw, string & ca,
	 	string & caT, string & caO, string & fn, string & lv )
	:ITEM(l, n, p), keywords(move(kw)), level(move(lv)),
	MEMBERS({"keywords", "level", "page"}),
	MMEMBERS ({"label","number","caption","captionT","captionO","fileName"}){
	//label.clear();
	caption.push_back(ca);
	captionT.push_back(caT);
	captionO.push_back(caO);
	fileName.push_back(fn);
}

string FIGURES::returnVariable (enum itemMember x, int n){
	stringstream ss;
	if (x == LB){
		ss << "label" << n;
	}
	else if (x == NU) {
		ss << "number"<< n;
	}
	else if (x == PG) {
		ss << "page";
	}
	else if (x == CA) {
		ss << "caption" << n;
	}
	else if (x == CAt) {
		ss << "captionT" << n;
	}
	else if (x == CAo) {
		ss << "captionO" << n;
	}
	else if (x == FN) {
		ss << "fileName"  << n;
	}
	else if (x == KW) {
		ss << "keywords";
	}
	else if (x == LV) {
		ss << "level";
	}
	else if (x == RF) {
		ss << "refNAME";
	}
	else if (x == NN) {
		ss << "numberV";
	}
	else if (x == TY) {
		ss << "type";
	}
	else if (x == MN) {
		ss << "mainName";
	}
	else if (x == SN) {
		ss << "subExsist";
	}
	else if (x == SN1) {
		ss << "subName1";
	}
	else if (x == SN2) {
		ss << "subName2";
	}
	else if (x == SN3) {
		ss << "subName3";
	}
	else if (x == SN4) {
		ss << "subName4";
	}
	else if (x == SN5) {
		ss << "subName5";
	}
	else {
		ss << "label" << n;
	}
	// cout << ss.str() << endl;
	return ss.str();
}

string FIGURES::returnVariableI (enum intMember x, int n){
	stringstream ss;
	if (x == Pg){
		ss << "pageNu";
	}
	else if (x == Nn) {
		ss << "NumberNu";
	}
	else if (x == Lb) {
		ss << "LabelNu";
	}
	else {
		ss << "Page" << n;
	}
	cerr << ss.str() << endl;
	return ss.str();
}

int FIGURES::returnVariableL (enum itemMember x, int n){
	int small=24, Small=56, medium=128, large=512, Large=1024; 
	if (x == LB){
		return medium;
	}
	else if (x == NU) {
		return small;
	}
	else if (x == PG) {
		return small;
	}
	else if (x == CA) {
		return Large;
	}
	else if (x == CAt) {
		return Large;
	}
	else if (x == CAo) {
		return Large;
	}
	else if (x == FN) {
		return large;
	}
	else if (x == KW) {
		return Large;
	}
	else if (x == LV) {
		return Small;
	}
	else if (x == RF) {
		return medium;
	}
	else if (x == MN) {
		return Small;
	}
	else if (x == SN) {
		return Small;
	}
	else if (x == AUX) {
		return Small;
	}
	else if (x >= SN1 && x <= SN5 )  {
		return Small;
	}
	else {
		return small;
	}
}

int FIGURES::returnVariableLI (enum intMember x, int n){
	int small=6, Small=12 ;// , large=100, Large=256; // medium=56, 
	if (x == Pg){
		return small;
	}
	else if (x == Nn) {
		return Small;
	}
	else if (x == Lb ) {
		return small;
	}
	else {
		return small;
	}
}


string FIGURES::getMemberValue (enum itemMember x, int n, char d){
	if (x == LB){
		// return setDlim(label[n], d);
		return ITEM::getMemberValue(x,n,d);
	}
	else if (x == NU) {
		return setDlim(number[n], d);
	}
	else if (x == PG) {
		return setDlim(page, d);
	}
	else if (x == CA) {
		return setDlim(caption[n], d);
	}
	else if (x == CAt) {
		return setDlim(captionT[n], d);
	}
	else if (x == CAo) {
		return setDlim(captionO[n], d);
	}
	else if (x == FN) {
		return setDlim(fileName[n], d);
	}
	else if (x == KW) {
		return setDlim(keywords, d);
	}
	else if (x == LV) {
		return setDlim(level, d);
	}
	else if (x == RF) {
		return ITEM::getMemberValue(x, n, d);
	}
	else if (x == AUX) {
		return setDlim(auxilary, d);
	}
	else if (x == MN) {
		return setDlim(mainName, d);
	}
	else if (x == SN1) {
		return setDlim(sub1, d);
	}
	else if (x == SN2) {
		return setDlim(sub2, d);
	}
	else if (x == SN3) {
		return setDlim(sub3, d);
	}
	else if (x == SN4) {
		return setDlim(sub4, d);
	}
	else if (x == SN5) {
		return setDlim(sub5, d);
	}
	else if (x == INDtype) {
		return setDlim(indexType, d);
	}
	else {
		return setDlim(label[n], d);
	}
}

void FIGURES::clearVectors (bool y){
	if (y ==true){ 
		label.clear();
		number.clear();
		caption.clear() ;
		captionT.clear() ;
		captionO.clear() ;
		fileName.clear() ;
	}
	return;
}

void FIGURES::setMemberValue (string l, enum itemMember x, bool y){
	l = trim (l);
	clearVectors(y);
	if (x == LB){
		// label.push_back(l);
		ITEM::setMemberValue (l, x, y); 
	}
	else if (x == NU) {
		number.push_back(l);
	}
	else if (x == PG) {
		page = l;
	}
	else if (x == CA) {
		caption.push_back(l) ;
	}
	else if (x == CAt) {
		captionT.push_back(l);
	}
	else if (x == CAo) {
		captionO.push_back(l);
	}
	else if (x == FN) {
		fileName.push_back(l);
	}
	else if (x == KW) {
		keywords = l;
	}
	else if (x == LV) {
		level = l;
	}
	else if (x == RF) {
		ITEM::setMemberValue (l, x, y); 
	}
	else if (x == AUX) {
		auxilary = l;
	}
	else if (x == SN1) {
		sub1 = l;
	}
	else if (x == SN2) {
		sub2 = l;
	}
	else if (x == SN3) {
		sub3 = l;
	}
	else if (x == SN4) {
		sub4 = l;
	}
	else if (x == SN5) {
		sub5 = l;
	}
	else if (x == INDtype) {
		indexType = l;
	}
	else {
		ITEM::setMemberValue (l, x, y); 
		// label.push_back(l);
	}
}

void FIGURES::setMemberValueI (int l, enum intMember x, bool y){
	ITEM::setMemberValueI (l, x, y); 
}

vector <string>  FIGURES::getMembers(void){
	return MEMBERS;
}

vector <string>  FIGURES::getMMembers(void){
	return MMEMBERS;
}

int FIGURES::getNumber(void){
	return (nn);
}

void FIGURES::setNumber(int a){
	 nn = a;
}

void FIGURES::setMembersValue(vector <string> a) {
	//int numStrings = sizeof(a)/sizeof(a[0]);
	keywords = a[0];
	label.push_back(a[1]);
	caption.push_back (a[2]);
}

string FIGURES::getMember(itemMember x, int y){
	stringstream ss;
	if (x == LB){
		ss << "label" << y;
	}
	else if (x == NU){
		ss << "number" << y;
	}
	else if (x == PG){
		ss << "page";
	}
	else if (x == CA){
		ss << "caption" << y;
	}
	else if (x == CAt){
		ss << "captionT" << y;
	}
	else if (x == CAo){
		ss << "captionO" << y;
	}
	else if (x == FN){
		ss << "fileName" << y;
	}
	else if (x == KW){
		ss << "keywords";
	}
	else if (x == LV){
		ss << "label";
	}
	else if (x == RF){
		ss << "refNAME";
	}
	else {
		ss << "type";
	}
	return ss.str();
}

vector <string> FIGURES::getMembersValue(void) {
	vector <string> a ;
	a.push_back(label[0]);
	a.push_back(number[0]);
	a.push_back(caption[0]) ;
	a.push_back(keywords); 
	a.push_back(page); 
	return a;
	// (label, number, caption, keywords, page, type)
}

LABELS::LABELS(int n) :FIGURES (n) { 
	vector <string> b;
	setMembersValue(b);
}

LABELS::LABELS(string& l, string& n, string& p, string& kw, string& t)
	:FIGURES(l, n, p, kw), type(t) {//,
	//setMembersValue();
	//MEMBERS({"keywords", "page"}),
	//MMEMBERS ({"label","number","caption"}){
	//label.clear();
}

void LABELS::setMemberValue (string l, enum itemMember x, bool n ){
	l = trim (l);
	if (x == TY){
		type = l;
	}
	else {
		FIGURES::setMemberValue (l, x, n); 
	}
}

void LABELS::setMemberValueI (int l, enum intMember x, bool y ){
	if (x != CN){
		FIGURES::setMemberValueI (l, x, y); 
	}
	else {
		CL[0]=l;
	}
	// else if ( x==CN ) {
	// 	CL[0] = num;
	// }
}

vector <string> LABELS::getMembersValue(void) {
	vector <string> a; 
	a = FIGURES::getMembersValue();
	a.push_back(type); 
	return a;
}

void LABELS::setMembersValue(vector <string> a) {
	vector <string> b = 
				{"label0", "number0", "caption0", "keywords", "page", "type"};
	MMEMBERS.clear();	
	MEMBERS.clear();
	MEMBERS=move(b);
	//cout << MEMBERS[0] << endl ;
}

vector <string>  LABELS::getMembers(void){
	return MEMBERS;
}

string LABELS::getMemberValue(enum itemMember x, int n, char d){
	if (x == TY){
		// return setDlim(label[n], d);
		return setDlim(type, d);
	}
	else{ 
		return FIGURES::getMemberValue(x,n,d);
	}
}

int LABELS::returnVariableL (enum itemMember x, int n){
	int Small=30; 
	if (x == TY){
		return Small;
	}
	else {
		return FIGURES::returnVariableL(x,n);
	}
}

//string * FIGURES::getMembers(void){
//	string * r;
//	r = &members[0];
//	return r;
//}

