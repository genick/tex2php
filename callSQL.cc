#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <mysql.h>
 
#include "item.hh"
#include "commSQL.hh"
 
using namespace std;

int main(int argc, char** argv) {
	string h("localhost"), n("genick"), p("bolabola"), db("gasDynamics");
	commSQL  fig (h, n, p, db);
	commSQL * bt;
	bt = &fig;
	string tn("Figures");
	bt->setTableName(tn);
	if ( bt->isTableExists(tn) < 1){
		cerr <<" trying to crate table  figures."<<  endl;
		bt->createTable();
	}
	else {
		cerr <<" there is a table with figures "<<  endl;
	}
	string l("this label"), nn("this number"), pp("this page"), kw("keeward");
	string ca("this caption"), caT("this captionT"), caO("this captionO"),
				 fn("this fileName"), lv("this lavel");

	FIGURES G(1);
	FIGURES F(l, nn, pp, kw, ca, caT, caO, fn, lv);
	F.setMemberValue("green", LB, 1);
	F.getMemberValue(LB, 1);
	//FIGURES F(l, nn, pp, "keeward",
	//"this caption", "this captionT", "this captionO", "this fileName", "this lavel");
	FIGURES *ptrF; 
	ptrF = &F;
	bt->insertRow (ptrF, 2) ;
}
