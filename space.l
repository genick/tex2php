%{
#include <iostream>
using namespace std;
#define YY_DECL extern "C" int yylex()
%}

WORD [:alnum:]+
%x SPACE
%x AND

%%

WORD ECHO;
^[ ]*and[ ] BEGIN(AND);
[ ]* { cout << " "; BEGIN(SPACE); }

<SPACE>{
and[ ] ECHO; BEGIN(AND);
.|\n ECHO; BEGIN(INITIAL);
}

<AND>{
and$
([ ]*and[ ])*
.|\n ECHO; BEGIN(INITIAL);
}

%%

main()
{
    // lex through the input:
    yylex();
}
