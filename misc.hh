#ifndef __MISC_HH
#define __MISC_HH
// mics.hh initial build

	# include <cmath>
	#	include <cstdio>
	#	include <iostream>
	#	include <fstream>
	#	include <iomanip>
	#	include <limits>
  #	include <string>
	#	include <unistd.h>
  #	include <ctype.h>
  #	include <stdlib.h>
  #	include <stdbool.h>
	#	include "extenVer.hh"
	using namespace std;
	extern unsigned figMaxNumber, nmnMaxNumber, exmMaxNumber;
	extern struct NOMENCLATURE nmns[200];
	extern struct RECORD exms[300];
	extern "C" char * trim(char*); 
	extern "C" char * lookupLabel (char *, enum labelMember );
	//extern "C" void nomenc(char* );
	extern "C" char * newLineToSpace(char*); 

#endif /* __MISC_HH */
