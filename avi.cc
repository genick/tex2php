
#include <iostream>

using namespace std;

union Num { 
   int   ValueI;
   float    ValueF;
   double   ValueD;
   char    ValueC;
};



int main() {
  Num TestVal = {100};
  cout<<"\nInteger = "<<TestVal.ValueI<<endl;
  TestVal.ValueF = 2.123L;
  cout<<"Float = "<<TestVal.ValueF<<endl;
  cout<<"Uninitialized double = "<<TestVal.ValueD<<endl;
  cout<<"Some rubbish???"<<endl;
  TestVal.ValueC = 'U';
	cout<<"Character = "<<TestVal.ValueC<<endl;
	return 0;
}
