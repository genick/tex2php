#ifndef _MANAGEMENT_H__
#define _MANAGEMENT_H__
	#	 include "includeFiles.h"
	#	 include "def.h"
	#	 include "latex.h"
	#	 include "mkHtml.h"

// #define strlens(s) (s==NULL?0:strlen(s))
  extern struct INDEX indexs[900];
  extern void mkIndexHTML(void);
  extern void mkTOC();
  extern void mkEXM();

	bool openOperations(void);
	int closeOperations(void);

#endif /* End _MANAGEMENT_H__ */
