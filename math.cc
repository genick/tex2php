// Wed Jul  3 15:38:54 CDT 2019 change FIGURES to LABELS
// math.cc starting point Thu Mar  9 09:25:24 CST 2017
# include "math.hh"

void empheq (string l, int lineNu) {
	// cout << l.c_str() << endl ;
	LABELS fig(1);
	LABELS *F;
	F = &fig;
	extractParameter(F, l, 1, TY,'{', '}',emEq); // kind of equation
	extractParameter(F, l, 1, CAo,'[', ']'); // title
	extractParameter(F, l, 1, LV,'{', '}'); // equation type 
	extractParameter(F, l, 1, LB,'{', '}'); // equation type 
	emphEq = F->getMemberValue(TY,0,' '); // put equation type the endEmpheq
	cout << "<div class=\"" << emphEq << "\">" << endl;
	cout << "\t<h3 class=\"eqHead\" >" << F->getMemberValue(CAo,0,' ') << "</h3>\n";
	cout << "\t\\begin{" << F->getMemberValue(LV,0,' ') << "}" << endl;
	cout << "\t\t\\label{" << F->getMemberValue(LB,0,' ') << "}" << endl;
	emphEq = F->getMemberValue(LV,0,' '); // put equation type into endEmpheq
	}

void endEmpheq(string l, int lineNu ){
	cout << "\t\\end{" << emphEq.c_str() << "}\n";
	cout << "</div> <!-- end empheq --> \n" ;
}

void bLongEq(string l, int lineNu ){
	cout << "<div class=\"longEq\"> <!-- begin long equation -->" << endl
		<< l;
}

void eLongEq(string l, int lineNu ){
	cout << l << endl << "</div> <!-- end long equation -->";
}

void calSymbol(string l, int lineNo) {
	cout << "{\\pmb{\\mathcal " <<  l[2] << "}}" ; 
}

void eqLine(string l, int lineNo, string y) {
	cout << "\t" << l ;  
}
