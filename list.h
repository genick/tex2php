#ifndef __LIST_H_LIB
#define __LIST_H_LIB
	#include <stdio.h>
  #include <unistd.h>
  #include <string.h>
  #include <ctype.h>
  #include <stdlib.h>
  #include <stdbool.h>
	#include "def.h"

	void makeList (char *, char * );
	void closeList (char *, char * );
	void makeListItem (char *, char * );
	void setListSwitch (char* );

	static uint listSwitchStatus;  /* 0 close, 1 openIni, 2 regOpen,    */

#endif /* end list.h */

