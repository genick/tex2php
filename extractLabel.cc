// ex or extract labels from aux file
#	 include <iostream>
#	 include <fstream>
#	 include <sstream>
#	 include <string>
#	 include <string.h>
#	 include "extractLabel.hh"


using namespace std;

string intToRoman(int a) {
	string ans;
	string M[] = {"","M","MM","MMM"};
	string C[] = {"","C","CC","CCC","CD","D","DC","DCC","DCCC","CM"};
	string X[] = {"","X","XX","XXX","XL","L","LX","LXX","LXXX","XC"};
	string I[] = {"","I","II","III","IV","V","VI","VII","VIII","IX"};
	ans = M[a/1000]+C[(a%1000)/100]+X[(a%100)/10]+I[(a%10)];
	return ans;
}

string mkNumber(void){
//	static int first, second, third;
//	static bool firstC, seconedC, thirdC;
	string::size_type sz;
	ostringstream convert;
	string Result;
	if ( firstC == true) {
		first++;
		firstC = false;
	}
	else if (seconedC == true){
		second++;
		seconedC=false;
	}
	else {
		third++;
		firstC=true;
		seconedC=true;
	}
	convert << intToRoman(first);
	Result = convert.str();
	//int value = atoi(Result.c_str());
	cout << Result << endl ; // " and int is " << value <<  endl;
	return Result;
}

string getFB(string l){
	string rtn;
	ostringstream convert;
	char label[124], number[16], page[8], title[256], secType[30] ;
	memset(label,'\0',sizeof(char)*sizeof(label));
	memset(number,'\0',sizeof(char)*sizeof(number));
	memset(page,'\0',sizeof(char)*sizeof(page));
	memset(title,'\0',sizeof(char)*sizeof(title));
	memset(secType,'\0',sizeof(char)*sizeof(secType));
//	for (unsigned i=0; i<l.length(); ++i){
		size_t found = l.find('{');
		if (found!=string::npos) {
			l.erase (0,found+1);
		}
		found = l.find('}');
		size_t length = l.copy(label,found,0);
		if (found!=string::npos) {
			l.erase (0,found+2);
		}
		found = l.find('}');
		if (found <  2) { // no number
			cout << found << endl ;
			cout << mkNumber() << endl ;
			strcpy(number, mkNumber().c_str());
		}
		else {
			length = l.copy(number,found,1);
		}
		l.erase(0,found+2);
		found = l.find('}');
		length = l.copy(page,found,0);
		l.erase (0,found+2);
		found = l.find('}');
		length = l.copy(title,found,0);
		if  (strcmp(title, "[")== 0){
			convert << "eMpty" ;
			rtn = convert.str();
			return rtn; // nothing to do 
		}
		l.erase (0,found+2);
		found = l.find('}');
		length = l.copy(secType,found,0);
		convert << number << "," << label << "," << title <<  "," 
			<< secType ;
		rtn = convert.str();
	return rtn;
}
			//cout << l.at(i) << " and it is at ";
			//cout << l.at(i+2) << endl;
			//cout << "first 'needle' found at: " << found << '\n';
//	}
	//cout << l << '\n';

int main () {
  string line, restults;
  ifstream iFile ("aux.data");
  if (iFile.is_open()) {
    while ( getline (iFile,line) ) {
			restults = getFB(line);
			string strTest("eMpty");
			if ( strTest != restults ) { 
				cout << restults << endl;
			}
    }
    iFile.close();
  }

	//ofstream oFile ("l.data");
  else cout << "Unable to open file"; 
  return 0;
}
