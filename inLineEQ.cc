// math.cc starting point Thu Mar  9 09:25:24 CST 2017
# include "inLineEQ.hh"


void inLineEq(string l ){
	// cout << "we are in EQS " << l << endl; 
  ostringstream ss;
	l.erase(0,1);
	size_t found = l.find('$');
	l.erase(found,found+1);
	ss << "\\" << "(" << l.c_str() << "\\" << ")" ; 
  errMsg(ss.str());
	l = ss.str();
  cout << l ;
};

