# ifndef _ITEM_HH
# define _ITEM_HH
# include <iostream>
# include <string>
# include <sstream>
# include <vector>
# include <cstring>
# include "utility.hh"
#	include "enumerators.h"
using namespace std;

class ITEM {
	public:
		ITEM (int = 0);
		ITEM (string&, string&, string&);
		~ITEM ();
		virtual void setMemberValue (string, enum itemMember=LB, bool=false );
		virtual void setMemberValueI (int, enum intMember=Pg,bool=false);
		virtual string getMemberValue(enum itemMember=RF, int=0, char='\'');
		virtual int getMemberValueI(intMember, int=0);

	protected:
		vector <string> label; // actual label name for a
		vector <string> number; // number correspond to the label
		string page; // page in the book
		string refNAME; // name of created ref
		int p; //page in integer fromat
		int nn; // number of element in the ITEM
};

class FIGURES : public ITEM {
	public:
		FIGURES(int);
		FIGURES(string, string, string, string);
		FIGURES(string&, string&, string&, string&, string&,
			 string&,	string&, string&, string&);
		~FIGURES() {};
		//string * getMembers(void); 
		virtual vector <string>  	getMembers(void); 
		virtual vector <string>  	getMMembers(void); 
		virtual string    getMember(itemMember, int);  
		virtual void	setMembersValue (vector <string>);
		virtual void 	setMemberValue (string, enum itemMember=LB, bool=false);
		virtual void	setMemberValueI (int, enum intMember=Pg, bool=false );
		virtual void clearVectors( bool=false );
		virtual string 						getMemberValue(enum itemMember=LB, 
						int=0, char='\'');
		virtual string						returnVariable (enum itemMember=LB, int=0);
		virtual string						returnVariableI (enum intMember=Pg, int=0);
		virtual int								returnVariableL (enum itemMember=LB, int=0);
		virtual int								returnVariableLI (enum intMember=Pg, int=0);
		virtual vector <string>  	getMembersValue (void);
		virtual int								getNumber(void);
		virtual void							setNumber(int);

	protected:
		vector <string> caption; // caption 0 main and rest for sub 
		vector <string> captionT; // captionT figure 0 main and rest for sub
		vector <string> captionO; // caption overtext 0 main and rest for sub
		vector <string> fileName; // file names A=0 B=1 C=2 etc 
		string keywords; // key words for search
		string auxilary, indexType; // equation etc 
		string mainName, sub1, sub2, sub3, sub4, sub5;
		string level; // level like section or subsection
		vector <string> MEMBERS; // names of the variables
		vector <string> MMEMBERS; // initializing of the member issues
};

class LABELS : public FIGURES {
	public:
		LABELS(int);
		LABELS(string&, string&, string&, string&, string&);
		void setMembersValue(vector <string> );
		virtual void	setMemberValue (string, enum itemMember=LB, bool=false );
		virtual void	setMemberValueI (int, enum intMember=Pg, bool=false );
		virtual vector <string>  	getMembers(void); 
		virtual vector <string>  	getMembersValue (void);
		virtual string getMemberValue(enum itemMember=TY, int=0, char='\'');
		virtual int	returnVariableL (enum itemMember=LB, int=0);
		~LABELS(){};
	protected:
		string type; // level like figure nmn toc foc etc 
		int CL[5] = {}; // counter levels
};

#endif   // _ITEM_HH

