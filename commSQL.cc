// file commSQL.cc modified on april 1, 2017 
//#	 include "item.hh"
#	 include "commSQL.hh"
using namespace std;

static void displayWarnings( MYSQL *conn ) {
  MYSQL_RES *sqlResults;
  MYSQL_ROW row;
  if (( conn == NULL ) || mysql_query( conn, "SHOW WARNINGS" )){
    printf( "Can not display list of errors/warnings!\n" );
    return;
  }

  sqlResults = mysql_store_result( conn );
  if (( sqlResults == NULL ) || ( sqlResults->row_count == 0 )) {
    printf( "Can not display list of errors/warnings!\n" );
    if ( sqlResults )
      mysql_free_result( sqlResults );
    return;
  }
  row = mysql_fetch_row( sqlResults );
  if ( row == NULL ) {
    printf( "Can not display list of errors/warnings!\n" );
    mysql_free_result( sqlResults );
    return;
  }
  do {
    // Format: "message [Type: number]"
    printf( "%s [%s: %s]\n", row[2], row[0], row[1] );
    row = mysql_fetch_row( sqlResults );
  } while ( row );
  mysql_free_result( sqlResults );
}

commSQL::commSQL(string HOST, string USER, string PASSWORD,
	 	string DATABASE){
	host =  HOST;
	user = USER;
	password = PASSWORD;
	dbase = DATABASE;
	dbCon = mysql_init(NULL);
	if(!dbCon){
		message("MySQL initialization failed! mysql_init() failded.");
	}
	dbCon = mysql_real_connect(dbCon, host.c_str(), user.c_str(),
			password.c_str(), dbase.c_str(), 0, NULL, 0); 
	if(!dbCon) {
		message("Connection Error! ");
		exit (1);
	}
	// int a = mysql_query(dbCon,"status;");
	// a = mysql_ping(dbCon);
	// mysql_free_result(rset);
	// int a = mysql_query(dbCon,"status;");
	// cerr << " Error:" << mysql_error(dbCon) << endl ;
}

commSQL::~commSQL(){
	mysql_close(dbCon);
}

int commSQL::intExecute (string l ){
	stringstream ss;
	MYSQL_RES* rset;
	MYSQL_ROW row;
	// ss << "select count(*) from information_schema.tables where "
 	// 	<< "table_schema = '" << l << "';" ;
	// cerr << ss.str().c_str() << endl;
	cerr << l.c_str() << endl;
	// if(mysql_query(dbCon, ss.str().c_str()) != 0) { // error
	if(mysql_query(dbCon, l.c_str()) != 0) { // error
		cerr << "mysql error" << endl 
		<< mysql_error(dbCon) <<  endl ;
		return -1;
	}
	else {
		rset = mysql_use_result(dbCon);
		row = mysql_fetch_row(rset);
		int aa;
		if ( row != NULL) { 
			aa = atoi(row[0]) ;
			cerr << "Found " << "'" << aa-1 <<"' tables" << endl; 
			cerr << "If more table(s) is/are needed they will be created!"<<endl; 
			mysql_free_result(rset);
 			return  aa ;
		}
		else 
			return -1;
	}
}

void  commSQL::updateLine(string key, string newValue, string fieldName,
	 	string tn){
	//UPDATE figure SET subName1='ILIO' WHERE label0='thermo:fig:TU' ;
	//update figure set mainName='main' where label0='thermo:fig:TU' ;
	//UPDATE tn SET fieldName='newValue' WHERE label0='key' ;
	stringstream ss;
	ss << "update " << tn << " set " << fieldName <<
		"='" << newValue << "' where label0='" << key << "';"  ;
	logFile <<  "Send updating command: " << ss.str().c_str() << endl ;
	if(!mysql_query(dbCon, ss.str().c_str())) {
		logFile << "the update was successful: " << 
		 ss.str().c_str() <<	endl;
	}
	else {
		cerr << "it is not working " << endl;
		cerr << "Cannot update the database!" << endl
					<< "with commend: "<< ss.str().c_str() <<  endl;		
		logFile << "Cannot update the database!" << endl
					<< "with commend: "<< ss.str().c_str() <<  endl;		
		cerr << mysql_error(dbCon) <<  endl ;    
		logFile << mysql_error(dbCon) <<  endl ;    
		exit(-1);
	}
}

vector <string> commSQL::excuteCMD (string l){
	stringstream ss;
	vector <string> a ;
	MYSQL_RES* rset;
	MYSQL_ROW row;
	// int numFields;
	// ss  << "select table_name from information_schema.tables "
	// 	<< "where table_schema = '" << trim(l) << "' and table_name "
	// 	<< "not like '%raw%';";
	cerr << ss.str() << endl;
	// if (mysql_query(dbCon, ss.str().c_str() ) == 0 ){
	if (mysql_query(dbCon, l.c_str() ) == 0 ){
		rset = mysql_use_result(dbCon);
		// numFields = mysql_num_fields(rset);
		int k=0;
		while ((row = mysql_fetch_row (rset))) { 
			a.push_back(row[k]);
		}
		// row = mysql_fetch_row(rset);
		// for ( int i=0; i < numFields ; i++ ) { 
			// a.push_back(row[i]);
		// }
		mysql_free_result(rset);
		return a;
	}
	else {
		cerr << "Cannot read the data base!" << endl;
		cerr << mysql_error(dbCon) <<  endl ;
		exit(-1);
		// mysql_free_result(rset);
	}
	// return a;
}

vector <string> commSQL::makeQuary (string l, int & freshTbl){
	stringstream ss;
	vector <string> a ;
	MYSQL_RES* rset;
	MYSQL_ROW row;
	// int numFields;
	ss  << "select table_name from information_schema.tables "
		<< "where table_schema = '" << trim(l) << "' and table_name "
		<< "not like '%raw%';";
	cerr << ss.str() << endl;
	// string str(ss.str());
	// cerr << str << endl;
	// const char* cstr = str.c_str();
	// const string& str2 = ss.str();
	// string dummy("empty");
	// cerr << "resluts of query " << mysql_error(dbCon) << endl ; 
	// if(!mysql_query(dbCon, ss.str().c_str())) { // error
	if (mysql_query(dbCon, ss.str().c_str() ) == 0 ){
		rset = mysql_use_result(dbCon);
		freshTbl = mysql_num_fields(rset);
		row = mysql_fetch_row(rset);
		for ( int i=0; i < freshTbl ; i++ ) { 
			a.push_back(row[i]);
		}
		mysql_free_result(rset);
		return a;
		// row = mysql_fetch_row(rset);
		// do{
		// 	cerr << row[0] << endl;
		// 	a.push_back(row[0]);
		// } while ((row = mysql_fetch_row(rset)) !=NULL);
	}
	else {
		cerr << mysql_error(dbCon) <<  endl ;
		rset = mysql_use_result(dbCon);
		freshTbl = mysql_num_fields(rset);
		freshTbl++;
		row = mysql_fetch_row(rset);
		int numberTable = atoi(row[0]) ;
		cerr	<< numberTable << endl ;
	}
	mysql_free_result(rset);
	return a;
}

string  commSQL::getTableName(dataType x){
	if (x == SEC ) {
		return "section";
	}
	else if (x == ISSUE ){
		return "issue";
	}
	else if (x == FIGU ){
		return "figure";
	}	 
	else if (x == TABLE ){
		return "table";
	}
	else if (x == NMN ){
		return "nomen";
	}
	else if (x == AUT ){
		return "aut";
	}
	else if (x == SUB ){
		return "sub";
	}
	else if (x == LABEL ){
		return "rawLabel";
	}
	else{
		return "section";
	}
// SUB, AUT, FIGU, TABLE, TOC, SEC, NMN, EXM,LABEL, ISSUE, REFERENCE}; 	

}

int commSQL::isTableExists(dataType x){
	string y = getTableName(x);
	return (isTableExists(y));
	// if (x == SEC ) {
	// 	return isTableExists("section");
	// }
	// else if (x == FIGU ){
	// 	return isTableExists("figure");
	// }	 
	// else if (x == TABLE ){
	// 	return isTableExists("table");
	// }
	// else{
	// 	return isTableExists("section");
	// }
}

int commSQL::isTableExists(string tableName){
	stringstream ss;
	ss << "SELECT COUNT(1) FROM information_schema.tables WHERE table_name='"
		<< tableName  << "'";
	int numberTables=0;
	MYSQL_RES* rset;
	MYSQL_ROW row;
	if(!mysql_query(dbCon, ss.str().c_str())) {
		rset = mysql_use_result(dbCon);
		row = mysql_fetch_row(rset);
		numberTables = atoi(row[0]);
		mysql_free_result(rset);
	}
	return numberTables;
}

 int commSQL::isDataIxist(string l, enum itemMember x){
	stringstream ss;
	ss << "SELECT COUNT(1) FROM " << tableName 
		 << " WHERE text LIKE '%something%' ";
	MYSQL_RES* rset;
	MYSQL_ROW row;
	int numberData=0;
	if(!mysql_query(dbCon, ss.str().c_str())) {
		rset = mysql_use_result(dbCon);
		row = mysql_fetch_row(rset);
		numberData = atoi(row[0]);
		mysql_free_result(rset);
	}
  return numberData;
 }

void commSQL::insertRow(LABELS * f, enum dataType x){
	stringstream ss;
	vector <string>  b, c;
	b = f->getMembers();
	// c = f->getMembersValue();
	ss << "INSERT INTO " << tableName  << " (";  //  ss.str()
	if (x == LABEL ){
		// vector <string>::iterator i;
		// for (i = b.begin(); i != --b.end(); ++i){
		for (auto i : b){
			ss << i <<  ", ";
		}
		cerr << ss.str().c_str() << endl;
		ss << ") VALUES (";  
		cerr << ss.str().c_str() << endl;
		// for (i = c.begin(); i != --c.end(); ++i){
		for (auto i : b){
			ss << "'" << i <<  "', ";
		}
		// ss << "'" << *i <<  "');";
		cerr << ss.str().c_str() << endl;
	}
	else if (x != LABEL ){
		b = f->getMMembers();
		for(auto i : b) {
			ss << i << "0, ";// this will print all the contents of *features*
		}
	}
	if (mysql_query(dbCon, ss.str().c_str())){
		cerr << "cannot insert line to table: " << tableName <<
			endl << mysql_error(dbCon) << endl;
		exit(1);
	}
	else {
		//cerr << ss.str() << endl;
		//cerr << "Label data was inserted to Table " << tableName << endl;
	}
}

void commSQL::setTableName(string l){
	tableName = l;
}

void commSQL::updateData (LABELS * pF, vector <itemMember> x, string tn) {

}

	//while (x != nullptr) {
void commSQL::insertData (LABELS * pF, vector <itemMember> x, string tn) {
	stringstream ss;
	ss << "INSERT INTO " << tn  << " (";
	auto i = begin(x);
	ss << pF->returnVariable(*i,0) ;
	for (i++ ; i != end (x); i++) {
		ss << ", " << pF->returnVariable(*i,0) ;
	}
	ss << ") VALUES (" ;
	i = begin(x);
	ss << pF->getMemberValue(*i,0) ;
	//cerr << ss.str() << endl;
	for (i++ ; i != end (x); i++) {
		ss << ", " << pF->getMemberValue(*i,0) ;
		cerr << pF->getMemberValue(*i,0) << endl ;
	}
	ss << ");";
	string buffer("inserting SQL DATA with: ");
	// buffer.append(ss.str().begin(),ss.str().end());
	buffer.append(ss.str());
	errMsg(buffer);
	// logFile << "inserting SQL DATA with: " <<  ss.str() << endl; 
	if (mysql_query(dbCon, ss.str().c_str())){
		buffer.clear();
		buffer.append("MySQL fail to insert data to table '");
	 	buffer.append(tn);
		buffer.append("'.\n");
		buffer.append(mysql_error(dbCon));
		errMsg(buffer);
		exit(1);
	}
	else {
		buffer.clear();
		buffer.append("Inserting data into Table '");
		buffer.append(tn);
		buffer.append("'.");
		errMsg(buffer);
	}
}

void commSQL::cleanData(string dbTable, dataType dT) {
	string buffer;
	tableName = dbTable;
	stringstream ss;
	vector <itemMember> x;
	vector <intMember> y;
	if ( isTableExists (dbTable) > 0){
		ss << "DELETE FROM " << dbTable << " WHERE " ;
		if ( dT != ISSUE) {  
			// ss << "label0 "	<< "LIKE '%" << workingFile << "%';" ;
			ss << "label0 "	<< "LIKE '%" << mainName << "%';" ;
		}
		// cerr << ss.str().c_str() << endl;
		buffer.append("Cleaning table '");
		buffer.append(dbTable);
		buffer.append("' from chapter '");
		buffer.append(mainName);
		buffer.append("' data.");
		errMsg(buffer);
		// cerr << "Cleaning table " << dbTable << " from chapter '"
		// << mainName << "' data." << endl;
		// logFile << "Cleaning table " << dbTable << " from chapter '"
		// << mainName << "' data." << endl;
		// << workingFile << "' data." << endl;
	}
	else {
		createTable(dT, tableName);
		cerr << ss.str() << endl;
		logFile << "creating table " << tableName  << endl;
	}
	cerr << ss.str() << endl;
	if (mysql_query(dbCon, ss.str().c_str()) != 0){
		buffer.clear();
		buffer.append("MySQL cannot clean table: ");
		buffer.append(dbTable);
		buffer.append(" ");
		cerr << "MySQL cannot clean table: " << dbTable <<
			endl << mysql_error(dbCon) << endl;
		exit(1);
	}
	else {
		buffer.clear();
		// buffer("");
		cerr << "Table '" << dbTable << "' was cleaned." << endl;
	}
}

void commSQL::cleanCurrentFileData(string l){
	cout << l << endl ;
	//DELETE FROM bar WHERE col1 LIKE '%foo%' OR col2 LIKE '%foo%'....etc
}

//dt data type and dN number within the data 
void commSQL::createTable(dataType z, string tn){
	char DENU [] = "NOT NULL";
	LABELS fig(1);
	LABELS *ptrF;
	ptrF = &fig;
	stringstream ss;
	ss << "CREATE TABLE `" << tn <<	"` (`labelID` int(10) "
	 	<<  DENU << " AUTO_INCREMENT" ; 
	vector <intMember> iM ;
	vector <itemMember> iMM ;
	if (z == ISSUE) {
		for (auto iit : {Pg, Lb}){
			iM.push_back(iit);
		}
		for (auto iit: {LB, PG, LV, CA, KW, RF}){
			iMM.push_back(iit);
		}
	}
	else if (z == LABEL) {
		for (auto iit : {Pg}){
			iM.push_back(iit);
		}
		for (auto iit: {NU, LB, CA, KW, PG, TY}){
			iMM.push_back(iit);
		}
	}
	else if (z == SEC) {
		for (auto iit : {Pg, Nn, Lb}){
			iM.push_back(iit);
		}
		for (auto iit: {NU, LB, PG, CA, CAt, CAo, KW, LV, RF, TY}){
			iMM.push_back(iit);
		}
	}
	else if (z == FIGU) {
		for (auto iit : {Pg, Nn}){
			iM.push_back(iit);
		}
		for (auto iit: {LB, NU, PG, CA, CAt, CAo, FN, KW, LV, NN, RF,
			 	TY, MN, SN, SN1, SN2, SN3, SN4, SN5}){
			iMM.push_back(iit);
		}
	}
	else if (z == NMN) {
		// for (auto iit : {Pg, Nn}){
		// 	iM.push_back(iit);
		// }
		for (auto iit: {CA, RF, FN, AUX}){
			iMM.push_back(iit);
		}
	}
	else if (z == AUT || z == SUB) {
		// for (auto iit : {Pg, Nn}){
		// 	iM.push_back(iit);
		// }
		for (auto iit: {CA, RF, FN, AUX}){
			iMM.push_back(iit);
		}
	}
	else if(z == FNT){
		for (auto iit: {LV, NU, CA, CAo, RF, FN, KW}){
			iMM.push_back(iit);
		}
	}
	ss << ", " ; 
	cerr << ss.str() << endl;
	for(auto it : iMM) { 
		ss << " `" << ptrF->returnVariable(it,0) << "` varchar(" <<
			ptrF->returnVariableL(it,0) << ") DEFAULT NULL, ";
			// cerr << ss.str() << endl;
	}
	for (auto it : iM) {
	  ss << ptrF->returnVariableI(it, 0) 
			 << " int(" << ptrF-> returnVariableLI(it, 0)
			 << ") NOT NULL DEFAULT 0, "  ;
			// cerr << ss.str() << endl;
	}
	// cerr << ss.str() << endl;
	ss	<<	"`cTime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, "
			<<	"`upTime` DATETIME DEFAULT CURRENT_TIMESTAMP "
			<<	"ON UPDATE CURRENT_TIMESTAMP, "
			<<	"PRIMARY KEY (`labelID`) ), ADD INDEX (`labelID`) "
			<<	"ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";
	string buffer("The sql command: ");
	// buffer.append(ss.str().begin(),ss.str().end());
	// buffer.append(ss.str());
	buffer = ss.str();
	// cerr << ss.str() << endl;
	if (mysql_query(dbCon, ss.str().c_str())){
		buffer.append("\nIs not working\n");
		buffer.append ("MySQL cannot create table: ");
		buffer.append(tn);
		buffer.append(".\n");
		buffer.append( mysql_error(dbCon));
		exit(1);
	}
	else {
		cerr << "Table '" << tn << "' was created." << endl;
	}
}

void commSQL::createTable(){
	stringstream ss;
	ss << "CREATE TABLE `" << tableName 
		<<  "` (`labelID` int(10) NOT NULL AUTO_INCREMENT, "
		<<  "`refNAME` varchar(128) DEFAULT NULL, "
		<<  "`label0` varchar(128) DEFAULT NULL, "
		<<  "`label1` varchar(128) DEFAULT NULL, "
		<<  "`label2` varchar(128) DEFAULT NULL, "
		<<  "`label3` varchar(128) DEFAULT NULL, "
		<<  "`label4` varchar(128) DEFAULT NULL, "
		<< 	"`lavel` varchar(24) DEFAULT NULL, "
		<<	"`number0` varchar(20) DEFAULT NULL, "
		<<	"`number1` varchar(20) DEFAULT NULL, "
		<<	"`number2` varchar(20) DEFAULT NULL, "
		<<	"`number3` varchar(20) DEFAULT NULL, "
		<<	"`number4` varchar(20) DEFAULT NULL, "
		<<  "`caption0` varchar(256) DEFAULT NULL, "
		<<  "`caption1` varchar(256) DEFAULT NULL, "
		<<  "`caption2` varchar(256) DEFAULT NULL, "
		<<  "`caption3` varchar(256) DEFAULT NULL, "
		<<  "`caption4` varchar(256) DEFAULT NULL, "
		<<  "`captionT0` varchar(256) DEFAULT NULL, "
		<<  "`captionT1` varchar(256) DEFAULT NULL, "
		<<  "`captionT2` varchar(256) DEFAULT NULL, "
		<<  "`captionT3` varchar(256) DEFAULT NULL, "
		<<  "`captionT4` varchar(256) DEFAULT NULL, "
		<<  "`captionO0` varchar(256) DEFAULT NULL, "
		<<  "`captionO1` varchar(256) DEFAULT NULL, "
		<<  "`captionO2` varchar(256) DEFAULT NULL, "
		<<  "`captionO3` varchar(256) DEFAULT NULL, "
		<<  "`captionO4` varchar(256) DEFAULT NULL, "
		<<  "`fileName0` varchar(100) DEFAULT NULL, "
		<<  "`fileName1` varchar(100) DEFAULT NULL, "
		<<  "`fileName2` varchar(100) DEFAULT NULL, "
		<<  "`fileName3` varchar(100) DEFAULT NULL, "
		<<  "`fileName4` varchar(100) DEFAULT NULL, "
		<<  "`keywords` varchar(256) DEFAULT NULL, "
		<<	"`page` INT NOT NULL DEFAULT 0, "
		<<  "`nn` INT NOT NULL DEFAULT 0, "
		<<  "`cTime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, "
		<<  "`upTime` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, "
		<<  "PRIMARY KEY (`labelID`) ) "
		<<  "ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
	if (mysql_query(dbCon, ss.str().c_str())){
	  cerr << "MySQL cannot create table: " << tableName <<
			endl << mysql_error(dbCon) << endl;
		exit(1);
	}
}

void commSQL::removeTableData(string l){
	stringstream ss;
	ss << "TRUNCATE " << l << ";" ;  	
	if (mysql_query(dbCon, ss.str().c_str())){
	  ss << "MySQL cannot delete table: " << tableName <<
			endl << mysql_error(dbCon) << endl;
		errMsg(ss.str());
		exit(1);
	}
	else {
		ss << "Tabel " <<  tableName << " data was deleted and index was reset"; 
		errMsg(ss.str());
	}
}

string commSQL::getValue (string x, enum itemMember in, enum itemMember out){
	// int freshTbl;
	stringstream ss; 
	MYSQL_RES* rset;
	MYSQL_ROW row;
	LABELS f(0);
	string  l;
	ss << "SELECT " ; 
	ss << f.returnVariable(out, 0) ; 
	ss <<	" FROM rawLabel WHERE ";
	//cerr << ss.str() << endl;
	if (in == LB){
	 	ss << "label0 LIKE '" << x << "' ;";
	}
	// cerr << ss.str() << endl;
	// cerr << a << " Error:" << mysql_error(dbCon) << endl ;	
	if (mysql_query(dbCon, ss.str().c_str() )){
		displayWarnings(dbCon);
		cerr << "MySQL cannot quary table: " << "rawLabel!" <<
			endl << mysql_error(dbCon) << endl;
	}
	else{
		rset = mysql_use_result(dbCon);
		row = mysql_fetch_row(rset);
		// freshTbl = mysql_num_fields(rset);
		mysql_num_fields(rset);
		if (row != NULL) {
			l = row[0] ; 
		}
		else {
			cerr << "Label '" << x << "' is missing the label file"
					<< " need to assign value manaully." << endl; 	
			l.assign("???");
		}
		// }
		if (rset != NULL){
			mysql_free_result(rset);
		}
		return l;
	}
	return ss.str();
}

void commSQL::message(string msg) {
	cerr << msg << endl << 
	 " mysql error message " <<	mysql_error(dbCon) << endl;
}

bool commSQL::isDataIxist(string key, string tableName,
				enum itemMember x, unsigned int n ){
		return true;
}

// the code from the create table
// 	if (tn == "issue" ){
// 		ss << " `" << ptrF->returnVariable(*i,0) 
// 			 << "` varchar(" << ptrF-> returnVariableL(*i,0)
// 			 << ") DEFAULT NULL";
// 		cerr << ss.str() << endl;
// 		for (i++ ; i != end (x); i++) {
// 		  ss << ", `" << ptrF->returnVariable(*i,0) 
// 				 << "` varchar(" << ptrF-> returnVariableL(*i,0)
// 				 << ") DEFAULT NULL"  ;
// 		}
// 		ss << ", `" << ptrF->returnVariableI(*j,0) 
// 			 << "` int(" << ptrF-> returnVariableLI(*j,0)
// 			 << ")  NOT NULL DEFAULT 0";
// 		//cerr << ss.str() << endl;
// 		for (j++ ; j != end (y); j++) {
// 		  ss << ", `" << ptrF->returnVariableI(*j,0) 
// 				 << "` int(" << ptrF-> returnVariableLI(*j,0)
// 				 << ") NOT NULL DEFAULT 0"  ;
// 		}
// 		cerr << ss.str() << endl;
// 	}
// 	if (tn == "section" ){
// 		ss << " `" << ptrF->returnVariable(*i,0) 
// 			 << "` varchar(" << ptrF-> returnVariableL(*i,0)
// 			 << ") DEFAULT NULL";
// 		//cerr << ss.str() << endl;
// 		for (i++ ; i != end (x); i++) {
// 		  ss << ", `" << ptrF->returnVariable(*i,0) 
// 				 << "` varchar(" << ptrF-> returnVariableL(*i,0)
// 				 << ") DEFAULT NULL"  ;
// 		}
// 		ss << ", `" << ptrF->returnVariableI(*j,0) 
// 			 << "` int(" << ptrF-> returnVariableLI(*j,0)
// 			 << ")  NOT NULL DEFAULT 0";
// 		//cerr << ss.str() << endl;
// 		for (j++ ; j != end (y); j++) {
// 		  ss << ", `" << ptrF->returnVariableI(*j,0) 
// 				 << "` int(" << ptrF-> returnVariableLI(*j,0)
// 				 << ") NOT NULL DEFAULT 0"  ;
// 		}
// 		cerr << ss.str() << endl;
// 	}
// 	else if (tn == "figure" ){
// 		ss << " `" << ptrF->returnVariable(*i,0) 
// 			 << "` varchar(" << ptrF-> returnVariableL(*i,0)
// 			 << ") DEFAULT NULL";
// 		//cerr << ss.str() << endl;
// 		for (i++ ; i != end (x); i++) {
// 		  ss << ", `" << ptrF->returnVariable(*i,0) 
// 				 << "` varchar(" << ptrF-> returnVariableL(*i,0)
// 				 << ") DEFAULT NULL"  ;
// 		}
// 		ss << ", `" << ptrF->returnVariableI(*j,0) 
// 			 << "` int(" << ptrF-> returnVariableLI(*j,0)
// 			 << ")  NOT NULL DEFAULT 0";
// 		//cerr << ss.str() << endl;
// 		for (j++ ; j != end (y); j++) {
// 		  ss << ", `" << ptrF->returnVariableI(*j,0) 
// 				 << "` int(" << ptrF-> returnVariableLI(*j,0)
// 				 << ") NOT NULL DEFAULT 0"  ;
// 		}
// 		cerr << ss.str() << endl;
// 		// vector <int> nn  = {256, 24, 6};
// 		// vector <int> nnm = {56, 12, 256, 256, 256, 100};
// 		// for( size_t i = 0; i < mm.size(); ++i ) {
// 		// 	ss << ", `" << mm[i] << "` varchar (" <<  nn[i] << ") " << DENU ;
// 		// }
// 		// for( size_t i = 0; i < mmm.size(); ++i ) {
// 		// 	for (int j=0; j < 6; j++){
// 		// 		ss << ", `" << mmm[i] << j << "` varchar (" <<  nnm[i] << ") " << DENU ;
// 		// 	}
// 		// }
// 	}
