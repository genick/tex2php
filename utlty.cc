#include <iostream>
#include <string>
#include "utlty.hh" 
using namespace std;

//const char* wss = " \t\n\r\f\v";

// trim from end of string (right)
//inline
string& rtrim(string& s, const char* t ) {
	s.erase(s.find_last_not_of(t) + 1);
	return s;
}

// trim from beginning of string (left)
//inline 
string& ltrim(string& s, const char* t ) {
	s.erase(0, s.find_first_not_of(t));
	return s;
}

// trim from both ends of string (left & right)
//inline 
string& trim(string& s, const char* t ) {
	return ltrim(rtrim(s, t), t);
}

//	
//	std::string trim(const std::string &s)
//	{
//		std::string::const_iterator it = s.begin();
//		while (it != s.end() && isspace(*it))
//			it++;
//	 
//		std::string::const_reverse_iterator rit = s.rbegin();
//		while (rit.base() != it && isspace(*rit))
//			rit++;
//	 
//		return std::string(it, rit.base());
//	}
