/* nomen.hh Fri May  5 08:44:52 CDT 2017 */
#ifndef __NOMEN_HH
#define __NOMEN_HH

#	include <iostream>
#	include <fstream>
#	include <sstream>
#	include <string>
#	include <cstring>
#	include "enumerators.h"
#	include "item.hh"
#	include "commSQL.hh"
#	include "utility.hh"
#	include "commDB.hh"

	using namespace std;
	extern commSQL * bt;
	extern int nmnNumber;
	void nomencb(string, int, indexStatus);
	void endNomenclature(string, int );
	void endNomen(string, int ) ;

#endif   /* End __NOMEN_HH  */
