/* inLineEq.hh file */
 #ifndef __inLineEQ_H
 #define __inLineEQ_H

# include <iostream>
# include <fstream>
# include <sstream>
# include <string>
# include <cstdio>
# include <cstring>
# include "utility.hh"
# include "io.hh"

using namespace std;

	void eqLine(string, int, string);
	void inLineEq(string);
	extern string workingFileName;
	extern string emphEq;

#endif   /* End __inLineEQ_H  */
