/* definition of c progrm to be part of the main variables.  */

struct INDEX indexs [900]={
 		{ .numberFiles = 0,
 			.seeAlso = "eMpty"
 		} 
};
struct INDEX auts [900]={
	  { .numberFiles = 0,
			.seeAlso = "eMpty"
	 	} 
};
struct INDEX subs [900]={
	  { .numberFiles = 0,
			.seeAlso = "eMpty"
	 	} 
};

struct LABEL figures [500]={
	  { .caption[0] = "",
	  	.caption[1] = "",
	  	.caption[2] = "",
	  	.caption[3] = "",
	  	.caption[4] = "",
	  	.captionT[0] = "",
	  	.captionT[1] = "",
	  	.captionT[2] = "",
	  	.captionT[3] = "",
	  	.captionT[4] = "",
	  	.label[0] = "",
	  	.label[1] = "",
	  	.label[2] = "",
	  	.label[3] = "",
	  	.label[4] = "",
	  	.fileName[0] = "",
	  	.fileName[1] = "",
	  	.fileName[2] = "",
	  	.fileName[3] = "",
	  	.fileName[4] = "",
			.keywords = "",
			.nn = 1
	 	} // nn=0 is reserved for the total figure.
};

struct NOMENCLATURE nmns [200]={
	  { .equation = "",
	  	.page = "",
	  	.fileName = "",
			.description = ""
	 	} 
};

struct RECORD exms [300]={
	  { .fileName = "",
	  	.label = "",
	  	.keywords = "",
			.description = "",
			.page= ""
	 	} 
};

struct LOS tocs[500]={
	  { .isAleph = false, 
			.n[0] = -1, 
			.n[1] = -1,
			.n[2] = -1,
			.n[3] = -1,
			.n[4] = -1,
			.n[5] = -1,
			.n[6] = -1 
	 	}
};

bool foundMainIndex = false ;
bool foundIndexFile = false ;
const int no = 0;
const int yes = 1;
enum listStatus lS = unOpenList;
const char EOL = '\n';
uint figMaxNumber=0;
uint indexAutMaxNumber=0;
uint tocMaxNumber=0;

/* static int listSwitchStatus=0 ; #<{(| 0 close, 1 openIni, 2 regOpen,    |)}># */
/* static int footNoteNumber=0 ; #<{(| start from one by using 0+1 |)}># */
/* static int indexAutNumber=0 ; #<{(| start from one by using 0+1 |)}># */
/* static int indexAutMaxNumber=0; */


