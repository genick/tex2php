// utili.hh initial build Mon Dec 12 09:30:18 CST 2016
#ifndef __UTILI_HH
#define __UTILI_HH
	#  include <cmath>
	#	 include <cstdio>
	#	 include <iostream>
	#	 include <fstream>
	#	 include <iomanip>
	#	 include <limits>
  #	 include <string>
	#	 include <unistd.h>
  #	 include <ctype.h>
  #	 include <stdlib.h>
  #	 include <stdbool.h>
  // #	 include "commonDef.h"
  #	 include "preDef.hh"
	using namespace std;

	string lastPart(string, char );
	void peelFirstB( string & , size_t, char a='{');
	size_t strpos(string, char *, int );
	string mkNomeNu (void);
	string rmTabNewLine(string );
	extern unsigned int nmnMaxNumber;
	extern char workingFileName [100];
// 		extern "C" char * extractFirstPart(char *, char);
// 		extern "C" char * extractSecondPart(char *, char);
// 		extern "C" struct RTN extractFileParts(char*, char);
// 		extern "C" void cleanNumeric (char*, char**) ;
// 		//void mkIndexFooterFile(enum dataType, std::ofstream& );
// 	
// 		/* set the default */
// 		extern uint indexAutMaxNumber, indexSubMaxNumber, figMaxNumber;
// 		extern enum listStatus lS ;
// 		extern int numberOfLabels;
// 		extern int  isSubstring(char *, char * ) ;
// 		// void openFiles(void);
// 		extern int closeTheFile (void);
// 		extern char* trim(char* ) ; 
// 		//void mkIndexAut(char*, indexStatus, char* ) ;
// 		extern void mkIndexSub(char* ) ;
// 		extern void section (char*, char*);
// 		extern void  mkIA (char * tokenText, indexStatus stats , char* FN);
// 		extern char* removeBrackets(char*, char, char ) ; 
// 		extern void  wImgTMP (char * ) ;
// 	
// 		//extern struct INDEX indexs[900];
// 	// 	extern struct LABEL figures[500];
#endif /* __UTILI_HH */
