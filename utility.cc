//last change Tue Jul 2 22:26:00 CDT 2019 extract with two back slash  
//utility.cc moving the utilities to the cc side. Mon Aug 28 16:22:07 CDT 2017
// fstream logFile;
# include "utility.hh"
using namespace std;

string cleanFileName (string l){
	size_t found = l.find('.');
	if(found!=string::npos){
		return l.substr(0,found);
	}
	else {
		return l;
	}
}

// void extractParameter(FIGURES* F, string& l, int inLine=1,
// 	 	enum itemMember x=LB, char opening = '{', char closing='}',
// 	 	actionType y=doNothing, char separator = '/',
// 	 	enum itemMember z = CAt ){
void extractParameter(LABELS* F, string& l, int inLine,
	 	enum itemMember x, char opening, char closing,
	 	actionType y, char separator, enum itemMember z){
	// int w; // for sub sup case action
	size_t found;
	ostringstream ss;
	logFile << "extracting parameter from line: " << l << endl; 
	string buffer, label;
	peelFirstB(l, inLine, opening);
	// is line should be eliminated with use line of $ sign.
	if (y == subSupCase) {
		found = l.find("$}")+1;
	}
	else {
		found = l.find(closing);
	}
	if (y == emEq){
		// errMsg ("found the emph equation");
		l = l.substr(1); // remove the first 2 chars
		found = l.find('[');
		if (found!=string::npos){
			buffer = l.substr(0,found);
			l = l.substr(found);
			F->setMemberValue (buffer,TY);
		}
		else{
			cout << "\n******** Could not find the equation type ****" << endl;
			ss << "\n******** Could not find the equation type" << endl
				<< l  << endl ;
			errMsg (ss.str());
		}
		return;
	}
	if (found!=string::npos){
		buffer = l.substr(0,found);
		if ( y == lastPrt){ 
			buffer = (lastPart(buffer, separator));
			buffer = cleanFileName(buffer);
			logFile << "found last part parameter: " << buffer << endl; 
		}
		else if ( y == doLabel){ 
			buffer = l.substr(0,found);
			F->setMemberValue (buffer,LB);
			label = lookUpNumber(buffer, LB, NU);
			F->setMemberValue (label,NU);
			label = lookUpNumber(buffer, LB, PG);
			F->setMemberValue (label,PG);
			logFile << "found parameters: label=" << label << endl; 
			logFile << "\t\t\t\t page:	=" << label << endl; 
		}
		else if ( y == doNothing){
		}
		else if ( y == adParameter) {
			F->setMemberValue (buffer,z);
		}
		else if ( y == subSupCase) {
			l.erase(0,found+1);
		}
		else {
		}
		F->setMemberValue (buffer,x);
	}
}

void empty(string l, int y, string From){
	ostringstream ss;
	ss << "L " << y << " rmvd: " << trim(l)
	 	<< " Reason: " << trim(From) << endl; 
	errMsg(ss.str());
}

void realPercent(string l, int y, string From){
	ostringstream ss;
	ss << "Line " << y << " real percent: " << trim(l)
	 	<< " From: " << trim(From) << endl; 
	errMsg(ss.str());
	cout << "%";
}

void unToutched (string l, int y, string From){
	ostringstream ss;
	ss << "unToutched line " << y << " is kept: " << trim(l)
	 	<< " Source: " << trim(From) << endl; 
	errMsg(ss.str());
	// cout << l ;
}

void toBeChanged (string l, int y, string From){
	ostringstream ss;
	ss << "To be changed " << y << " keep: " << trim(l)
	 << " " << y << " found sub figure: "  << l 	
	 	<< " Reason: " << trim(From) << endl; 
	errMsg(ss.str());
	// cout << l ;
}

char* trim(char *s) {
	size_t size;
	char *end;
	size = strlen(s);
	if (!size)
		return s;
	
	end = s + size - 1;
	while (end >= s && isspace(*end))
		end--;
	*(end + 1) = '\0';
	
	while (*s && isspace(*s))
		s++;
	//string l(s);
	return s;
}

string trim(string l){
	size_t pos = l.find_last_not_of(" \t");
	if( string::npos != pos ){
		l = l.substr(0, pos+1 );
	}
	pos = l.find_first_not_of(" \t");
	if( string::npos != pos ){
		l = l.substr(pos);
	}
	return l;
}

string firstPart(string l, char s){
	size_t found = l.find(s);
	if (found!=string::npos) {
		l.erase (found);
		return l;
	}
	else{
		return l;
	}
}

string lastPart(string token, char s){
	string l = token;
	size_t found;
	do {
		found = l.find(s);
		l.erase (0,found+1);
	} while (found!=string::npos);
	return l;
}

// string peelFirstPart( string &l, int times, char a ){
string peelFirstPart( string &l, size_t times, char a ){
	string s;
	size_t found = l.find(a);
	// for (int i = 0 ; i < times ; i++){
	for (size_t i = 0 ; i < times ; i++){
		if (found!=string::npos) {
			s = l.substr(0, found);
			l.erase(0,found+1);
		}
		else{
			return "NULL";
		}
		found = l.find(a);
	}
	return s;
}

string peelLastPart( string &l, size_t times, char a ){
	string s;
	size_t found = l.find(a);
	if ( found > l.size()) {
		return "NULL";
	}
	else {
		l.erase(found);
		s = l;
		return s;
	}
}

void peelFirstB( string &l, size_t times, char a ){
	size_t found = l.find(a);
	for (size_t i = 0 ; i < times ; i++){
		if (found!=string::npos) {
			l.erase(0, found+1);
		}
		else {
			return;
		}
		found = l.find(a);
	}
	//return;
}

string rmTabNewLine(string  l){
	string space(" ");
	size_t found, sL = l.length();
	while (true) {
		found = l.find('\n');	
		if (found < sL){
			l.replace(found, 1, space);
		}	
		else {
			break;
		}
	}
	while (true) {
		found = l.find('\t');	
		if (found < sL){
			l.replace(found, 1, space);
		}	
		else {
			break;
		}
	}
	while (true){
		found = l.find("  ");	
		if (found < sL){
			l.replace(found, 2, space);
		}	
		else {
			break;
		}
	}
	return  l;
}

// function to find the needle nth location.
size_t strpos(string hay, char *needle, int nth) {
	size_t found, sum = 0;
	for (int i=0 ; i <=nth; i++){
		found =	hay.find(needle);
		sum = sum + found;
		if (found!=string::npos){
			hay.erase (0,found+1);
		}
		else{
			return string::npos;
		}
	}
	return sum;
}

void texStrToPhpStr(string l, string s){
	size_t found ;
	peelFirstB(l,1);
	found = l.find('}');
	if (found!=string::npos){
		l.erase(l.begin()+found, l.end()-0);
	}
	cout << "<span class=\"" << s.c_str() << "\">" << l << "</span>";
}

// string mkNomeNu (dataType a){
string mkRefNu (dataType a){
	ostringstream ss;
	string l, sog; 
	int *attachedNumber;
	if ( a == NMN){
		l.assign("nmn");
		sog.assign("nmn");
		attachedNumber = &nmnNumber;
		// nmnNumber++;
	}
	else if ( a == SUB){
		l.assign("sub");
		sog.assign("sub");
		attachedNumber = &subNumber;
	}
	else if ( a == AUT){
		l.assign("aut");
		sog.assign("aut");
		attachedNumber = &autNumber;
	}
	else if ( a == ISSUE){
		l.assign("iss");
		sog.assign("issue");
		attachedNumber = &issueNumber;
	}

	else {
		errMsg("Something is wrong with make number at mkRefNu!");
		exit(45); // something wrong with 
	}
	ss << firstPart(workingFileName,'.') << ":" <<  sog << ":" 
		<< l << *attachedNumber; 
	logFile << "Setting new reference: " << ss.str().c_str() << endl;
	(*attachedNumber)++;
	return ss.str();
}

string toLOWER(string s) {
	for (int i = 0; i < (int) s.length(); i++){
		if (s[i] >= 'A' and s[i] <= 'Z')
			s[i] += 32;
	}
	return s;
}

string toUPPER(string s) {
	for (int i = 0; i < (int) s.length(); i++){
		if (s[i] >= 'a' and s[i] <= 'z')
			s[i] -= 32;
	}
	return s;
}

string toUpper(string s, int i) {
	if (s[i] >= 'a' and s[i] <= 'z') {
		s[i] = putchar((int) s[i] + (int) 32);
	}
	return s;
}

string toLower(string s, int i) {
	if (s[i] >= 'A' and s[i] <= 'Z') {
		s[i] -= 32;
	}
	return s;
}
