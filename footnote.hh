/* fi.hh file Thu Mar  7 09:56:13 CST 2019 */
# ifndef __FOOTNOTE_HH
# define __FOOTNOTE_HH

#	include <iostream>
#	include <fstream>
#	include <sstream>
#	include <string>
#	include <cstring>
#	include "enumerators.h"
#	include "item.hh"
#	include "commSQL.hh"
#	include "utility.hh"
#	include "commDB.hh"
#	include "mngmnt.hh"

	using namespace std;

	void doFootNoteMark(void) ;
	void doFootNoteText (string );
	void doFootNote(string) ;
	int closeTheFile(void) ;
	void tColorBox (string ) ;

	int footnote( string, int, string );
	void footnoteMark (int, string ) ;	
	static uint footNoteNumber ; 

#endif   /* End __FOOTNOTE_HH  */
