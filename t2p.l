/* t2p.l working Tue Feb 26 19:13:12 CST 2019 */
%{
#	 include <cstdio>
#	 include <iostream>
#	 include <string>
#	 include <sstream>
#  include "preDef.hh"
#  include "enumerators.h"
#  include "commSQL.hh"
#  include "utility.hh"
#  include "math.hh"
#  include "fig.hh"
#  include "sec.hh"
#  include "exm.hh"
#  include "index.hh"
#  include "nomen.hh"
#  include "mngmnt.hh"
#  include "footnote.hh"

#	 define YY_DECL extern "C" int yylex()
%}

WORD [:alnum:]+
SPACE [:space:]*
LINE (.*)\n
EQS	\$[^$]+?\$
SOT stepOutOfThis
EQ	\$([^$])+?[^\\]\$
BF	bIGfIG
EF	eNDbIGfIG
FGRF Figure[ \t]+
TBRF Table[ \t]+
REF \{[^}]+\}
CPN [[:space:]]+\\caption\[([^\]])+\]\{([^\}])+\}[[:space:]]+\\label[^\}]+\}
LBN [[:space:]]+\\label\[([^\]])+\]\{([^\}])+\}[[:space:]]+
BLT	\\begin\{longtable\}
ELT	\\end\{longtable\}
BCR	\\begin\{tcolorbox\}
ECR	\\end\{tcolorbox\}
sbFIG			\\[[:alpha:]]+(\[[[:alnum:][:space:]-]+\]){2}[[:space:]]+\{(\\[[:alpha:]]+[\[\{][[:alnum:][:punct:]]+\%?([^\}]+)?){2}\}\}
STEx	\\TEx\{[^}]+\}[:space:]?
STExC \{[:space:]+
SSTEx	\\splitTEx\{
FTT		\\footnote\{
BLE   \\begin\{align\}
ELE   \\end\{align\}
%option	nounput noinput yylineno
%x	COLORBOX
%x	EMPHEQ 
%x	SSTEX
%x	MKNOMEN
%x	MKFIGURE
%x	MKTABLERF
%x  KEEPTHESAME
%x  MKFOOTNOTE

%%

	/* empheq converting to the regular equation wrapping into div */
\\begin\{empheq\}				{BEGIN(EMPHEQ);}
<EMPHEQ>{
\[box.*\n						empheq(yytext, yylineno);
\\end\{empheq\}					{ 
											/* toBeChanged(yytext, yylineno,"empheq");  */
											endEmpheq(yytext, yylineno);
											BEGIN(INITIAL);} 
^\t.{10,99}\n				eqLine(yytext, yylineno, "eqLine");
}
{BLE}									bLongEq(yytext, yylineno);
{ELE}									eLongEq(yytext, yylineno);
{ELE}									{cout << "\\end{align}"<< endl << "</div>";}
{TBRF}									{ itemName = yytext ; BEGIN(MKTABLERF);}
{FGRF}									{ itemName = yytext ; BEGIN(MKTABLERF);}
<MKTABLERF>{
\\(eq)?ref
{REF}									{setRef(yytext,yylineno, "ref"); BEGIN(INITIAL);}
}
kEEpTHESame							BEGIN(KEEPTHESAME);
<KEEPTHESAME>{
{SOT}										BEGIN(INITIAL);
}
	/* in text font modification */
\\textbf\{[^}]*\}			texStrToPhpStr(yytext, "bf");
\\texttt\{[^}]*\}			texStrToPhpStr(yytext, "tt");
\\textit\{[^}]*\}			texStrToPhpStr(yytext, "it");
	/* fixing the center of the paragraph */
\\begin\{center\}[[:space:]+?\n]				cout << "<p class=\"center\">\n";
\\end\{center\}[[:space:]+?\n]					cout << "</p >\n";
	/* headers in various levels */
\\section\{[^}]*\}\}							section(yytext, "section",yylineno, "main");
\\subsection(\*?)\{[^}]*\}\}			section(yytext, "subsec", yylineno, "main");
\\subsubsection(\*?)\{[^}]*\}\}		section(yytext, "subsubsec", yylineno,"main");
\\chapter(\*?)\{[^}]*\}\}					section(yytext, "chap", yylineno, "main");
\\issue\{[^}]*\}									issue(yytext, "issue", yylineno, "main");
	/* indexes and subject and authors */
\\index\{sub\}\{[^}]+\}				index(yytext, yylineno, SUB);
\\index\{aut\}\{[^}]+\}				index(yytext, yylineno, AUT);
	/* figures in the various configureation like wrap around and major */
\\imgPDF(\{[^}]*\}[^{]*){5}\{[^}]*\}[ \n]*			imgPDF(yytext);
\\wImg(\{[^\}]+\}[[:space:]]+){9}					wImg(yytext, yylineno, "wImg");
\\picTextPDF					picTextPDF(yytext, yylineno, "picTextPDF");
{BF}							{figIni(yytext, yylineno, "BF");  BEGIN(MKFIGURE);}
\\hspace						empty (yytext, yylineno, "figure");
<MKFIGURE>{
{EF}				   {figEnd (yytext, yylineno, "EF"); BEGIN(INITIAL);}
{sbFIG}				subFigure(yytext, yylineno," found subfig sub");	
{CPN}					caption(yytext, yylineno,"bigCaption");
}
	/* nomenclature  issue and wrapping into div */
\\nomenclature(\{[^}]*?\}){2}(\n|[:space:])?(\n)?				{
						nomencb(yytext, yylineno, regularIni );
						BEGIN(MKNOMEN);
					}
<MKNOMEN>{
\\nomenclature(\{[^}]*?\}){2}(\n|[:space:])?(\n)?			nomencb(yytext, yylineno, regular);
\\nomenclature(\{[^}]*?\})[^}]\}(\{[^}]*?\})[[:space:]]		nomencb(yytext, yylineno, subSup);
endNomenclature		endNomen(yytext, yylineno) ; BEGIN(INITIAL);
.									unToutched (yytext, yylineno, "endNomen");
}
\\nomenclature(\{[^}]*?\})[^}]\}(\{[^}]*?\})[[:space:]] {
						nomencb(yytext, yylineno, subSupIni);
						BEGIN(MKNOMEN);
}

{FTT}						{ openParentheses = 1; 
									footnoteMark ( yylineno, "FOOTNOTE");
									BEGIN(MKFOOTNOTE) ;
								}
<MKFOOTNOTE>{
\{								openParentheses++;
\}								{ openParentheses--;
										if ( openParentheses < 1) { 
											footnote(yytext, yylineno, "footnote");
											BEGIN(INITIAL);
										}
									}
.									yymore();
\n								cout << yytext;
}


	/* examples and solution of example */
{STEx}								{			;					
											openParentheses = 0;
											TEx(yytext, yylineno, "TEx");
											BEGIN(SSTEX);}

   /* This is a space between the actual segments.*/

<SSTEX>{
\{\n						{openParentheses++;} 
\\eqref\{[^}]+\}			cout << yytext ;
\}								{ openParentheses--;
										if ( openParentheses < 1) { 
											exm(yytext, yylineno, "exm");
											BEGIN(INITIAL);
										}
									}
\.												cout << ".";
.												yymore();
\n											yymore();
}

\\index\{sub\}\{[^\}]+\}				index(yytext, yylineno, SUB);
\\index\{aut\}\{[^\}]+\}				index(yytext, yylineno, AUT);
\\\[															{cout << "<div class=\"longEq\"> " ;
															cout << endl << "\\begin{align}" ;}
\\\]															{cout << "\\end{align}" << endl;
																	cout << "</div>" ;}

%%

/* \\index(\{[^\}]+\}){2}				index(yytext, yylineno, "sub"); */
/* \\nomenclature(\{[^}]*\}){2}(\n|[:space:])?(\n)? nomencb(yytext);  */
/* PDF										imgPDF(yytext); */

int main( int argc, char **argv ) {
	ostringstream ss;
	++argv, --argc;
	yyin = fopen( argv[0], "r" );
	if ( argc > 0 ) {
		if ( yyin == NULL ) {
			cerr << "Unable to open " << argv[1] << ": " << strerror(errno) << endl;  
			exit(EXIT_FAILURE);
		}
		else{
			char *token; /*find the name of the file*/
			string wf;
			strtok_r (argv[0],  ".", &token);
			ss <<  argv[0] << ".php";
			workingFileName = ss.str();
			ss.clear();
			cerr << ss.str() << endl;
			ss.str("");
			ss << argv[0] ;
			mainName = ss.str();
			ss.str("");
			ss <<  argv[0] << ".log";
			cerr << ss.str() << endl;
			cerr << "Working on file " << workingFileName << "." << endl;
			openLogFile(ss.str().c_str());
			logFile << "Working on file " << argv[0] << "." << endl;
		}
	}
	else {
		yyin = stdin;
	}
	readLabels();
	yy_flex_debug = 1;
	yylex();
	exit(1);
}

/* \\kSeclog(\{[^\}]*\}\n?){4}		empty(yytext, yylineno, "fig mkSeclog"); */
/* {EQS}					inLineEq(yytext);	 */
/* {REF}                   setRef(yytext, yylineno, "Table Ref"); */
