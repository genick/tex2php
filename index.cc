// Wed Jul  3 15:41:50 CDT 2019 change from FIGURES to LABELS
//index.cc starting point Sun Oct  8 09:29:26 CDT 2017
#	include "index.hh"

void doIndex(LABELS * F, string l, enum dataType x){
	// if (x == SUB) {
	ostringstream ss;
	ss << "working on the index: " << F->getMemberValue(CA, 0, ' ') << endl;
	errMsg (ss.str().c_str());
	// cout << "<div class=\"invisibleAnchor\"> <!-- Index hock -->" << endl; 
	cout << "<div class=\"invisible\"> <!-- Index hock -->" 
		<< endl 
		<< "\t<a name=\"" << F->getMemberValue(RF, 0, ' ')  
		<< "\">" << F->getMemberValue(CA, 0, ' ') << "</a>\"" << endl  
		<< "</div>  <!-- End of Index -->" << endl ;	
	return;
}

string mkStrName(dataType indexType){
	if ( indexType == SUB){
		return "sub";
	}
	else {
		return "aut";
	}
}

void index (string l, int lineNu, dataType indexType){
	ostringstream ss;
	LABELS fig(1);
	LABELS *F;
	F = &fig;
	string buffer, indexName;
	extractParameter(F, l, 1, INDtype,'{', '}');
	extractParameter(F, l, 1, CA,'{', '}');
	F->setMemberValue(workingFileName,FN);
	logFile << "Index preparation on line "<< lineNu << ": " << l << endl ;   
	indexName = mkStrName(indexType);
	// if (F->getMemberValue(INDtype, 0, ' ') == "sub"){
	vector <itemMember> data;
	vector <intMember> dataI;
	// if ((F->getMemberValue(INDtype, 0, ' ')).compare("sub") == 0){
	// if (indexType == SUB){
	buffer = mkRefNu(indexType);
	F->setMemberValue(buffer, RF); 
	F->setMemberValue(buffer, LB); 
	doIndex(F, l, indexType);
	ss << "inserting " << indexName << " index: " << 
		F->getMemberValue(INDtype, 0, ' ') <<  endl ;
	errMsg (ss.str().c_str());
	for (auto i : {CA, FN, LB, RF}){
		data.push_back(i);
	}
	for (auto i : {Pg, Nn}){
		dataI.push_back(i);
	}
	insertDataDB(F, data, dataI, indexType, indexName);
	return;
		buffer = mkRefNu(indexType);
		F->setMemberValue(buffer, RF); 
		F->setMemberValue(buffer, LB); 
		doIndex(F, l, indexType);
		ss << "inserting " << indexName << " index: " << 
			F->getMemberValue(INDtype, 0, ' ') <<  endl ;
		errMsg (ss.str().c_str());
		for (auto i : {CA, FN, LB, RF}){
			data.push_back(i);
		}
		for (auto i : {Pg, Nn}){
			dataI.push_back(i);
		}
	// }
	// else if ((F->getMemberValue(INDtype, 0, ' ')).compare("aut") == 0){
	// 	buffer = mkRefNu(AUT);
	// 	F->setMemberValue(buffer, RF); 
	// 	F->setMemberValue(buffer, LB); 
	// 	doIndex(F, l, AUT); 
	// 	logFile << "inserting aut index: " << 
	// 		F->getMemberValue(INDtype, 0, ' ') <<  endl ;
	// 	logFile << "inserting sub index: " << 
	// 		F->getMemberValue(INDtype, 0, ' ') <<  endl ;
	// 	for (auto i : {CA, FN, LB, RF}){
	// 		data.push_back(i);
	// 	}
	// 	for (auto i : {Pg, Nn}){
	// 		dataI.push_back(i);
	// 	}
	// }
	// else{
		// logFile << 
			// "something is wrong with this line exiting the program!"<< endl;  
		// exit (22);
	// }
	insertDataDB(F, data, dataI, indexType, indexName);
  return;
}
