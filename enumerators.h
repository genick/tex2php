#ifndef ENUMTEXTOPHP_H
#define ENUMTEXTOPHP_H

		// FNT footnote
  enum labelType  {FIG, SUBFIG, MAINFIG, NMNType, RFIG, notGIVEN};
  enum labelMember {lb, nu, pg, tl, ds, fn, lv, kw, nn};
  enum itemMember {LB, NU, PG, CA, CAt, CAo, FN, KW, LV, NN, RF,
				TY, MN, SN, SN1, SN2, SN3, SN4, SN5, INDXa, INDXs,
			 INDtype, AUX };
  enum intMember {Pg, Nn, Lb, CN};
	enum actionType {lastPrt, firstPrt, doLabel, doNothing, 
		subSupCase, adParameter, emEq};
  enum listStatus {unOpenList=1, firstOpenList, middleFirstList,
			finishedFirstList, secondOpenList, middleSecondList,
		 	finishedSecondList};
  enum indexStatus { regular=100, regularIni, see, subSup, subSupIni };
  enum dataType { SUB, AUT, FIGU, TABLE, TOC, SEC, NMN, EXM,
				LABEL, ISSUE, FNT, REFERENCE, LOG};
  enum FILEnAME { label=0,  overTxt };
  enum oMode { oWdata=10, oRdata, oWphp };


#endif /* ENUMTEXTOPHP_H */
