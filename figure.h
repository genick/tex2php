#ifndef __FIGURE_H
#define __FIGURE_H
	#include <stdio.h>
  #include <unistd.h>
  #include <string.h>
  #include <ctype.h>
  #include <stdlib.h>
  #include <stdbool.h>
  #include "utilities.h" 

	void cwImg(char *); 
	void twImg(char *); 
	void figOverText (char*, char * ) ;
	//void twoFigures (char* , char* ,  char*);
	void twoFigures (char *, char *, char*, char * , char *, char *, char* );
	void finishFigure (void) ;

#endif /* utilities.h */
