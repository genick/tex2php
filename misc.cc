// misc.cc starting point Fri Mar 10 18:58:04 CST 2017
# include <iostream>
# include <fstream>
# include <sstream>
# include <string>
# include <stdio.h>
# include <string.h>
# include "mkHtml.hh"
# include "commonDef.h"
# include "utili.hh"
# include "misc.hh"

extern "C" void TEx(char* x){
	cout << "<div class=\"TEx\">" << endl;
}

extern "C" void exLabel(char* x){
	char eLabel[56], tmp[56];
	memset(eLabel, '\0', sizeof(eLabel));
	memset(tmp, '\0', sizeof(tmp));
	string l(x);
	size_t found = l.find('}');
	if (found!=string::npos) {
		l.copy(tmp,found,0);
	}
	strcpy(eLabel, trim(tmp));
	strcpy(exms[exmMaxNumber].fileName, workingFileName);
	strcpy(exms[exmMaxNumber].label, eLabel);
	strcpy(exms[exmMaxNumber].keywords, lookupLabel(eLabel,kw));
	strcpy(exms[exmMaxNumber].description, lookupLabel(eLabel,ds));
	strcpy(exms[exmMaxNumber].page, lookupLabel(eLabel, pg));
	strcpy(exms[exmMaxNumber].number, lookupLabel(eLabel,nu));
	cout << "<h5> Example " << lookupLabel (eLabel, nu) << "</h5>" << endl;	
	cout << "<p>" ;
}

extern "C" void solutionT(){
	cout << "<div class=\"solutionT\">" << endl;
}

extern "C" void dollars(char* x){
	cout << "dollars" << endl;
}

extern "C" void commentedLine(char* x){
	cout << "commentedLine " << x << " End" << endl;
}
