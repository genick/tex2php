#ifndef __EXTENVER_HH
#define __EXTENVER_HH

	extern 	uint indexAutMaxNumber, indexSubMaxNumber, tocMaxNumber;
	extern struct INDEX subs[900];
	extern struct INDEX auts[900];
	extern struct LABEL figures[500];
	extern struct LOS  	tocs[500];	
	extern bool foundMainIndex;
	extern bool foundIndexFile;
	extern FILE* labelFile;
	extern FILE* footNoteFile;
	extern FILE* indexAutPHP;
	extern FILE* indexAutData;
	extern FILE* indexSubPHP;
	extern FILE* indexSubData;
	extern FILE* indexFigPHP;
	extern FILE* indexFigData;
	extern char workingFileName [100] ;
	extern const char EOL;
	extern int yy_flex_debug;
	extern size_t ln ;
	extern const int no ; 
	extern const int yes ; 
	extern int isIndexAutExist;
	extern uint indexAutMaxNumber;

#endif /* __EXTENVER_HH */
