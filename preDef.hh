# ifndef __DEF_HH
# define __DEF_HH

#  include <string>
#  include <fstream>
#  include <ctime>

using namespace std;
	
	string workingFileName, mainName, itemName;
	string emphEq;
	int nmnNumber, secNumber, issueNumber, figNumber, subNumber, autNumber;
	ofstream logFile, footnoteFile;
	// std::unique_ptr<std::ofstream> logFile ;
	time_t now;
	extern string peelFirstPart( string &, size_t, char );
	extern void cleanDataBase (string);
	extern void openLogFile(string);
	int openParentheses=0; // in case of stuff in the search of footnote etc

# endif /* __DEF_HH */
