#ifndef __INDEX_H_LIB
#define __INDEX_H_LIB
	#include <stdio.h>
  #include <unistd.h>
  #include <string.h>
  #include <ctype.h>
  #include <stdlib.h>
  #include <stdbool.h>
	#include "def.h"
	
	bool makeInTextIndex (char*, char*, int, char*,  enum dataType);
	bool makeInFileIndex (char*, char*) ;
	char* getCleanIndexName (char* ) ;
	char* getMainIndex (char* ) ;
	void mkIndexAut(char*, enum indexStatus, enum dataType, char*);
	void mkIndexElement (char *, enum indexStatus, enum dataType, char*);
	void mkIndex(char*, enum dataType, char*);

	// static unsigned short *indexPtr;
	static unsigned short indexAutNumber; 
	// static unsigned short indexSubNumber; 
//	unsigned short currentIndexNumber; 

#endif /* end index.h */

