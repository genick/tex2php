# ifndef COMMSQL_HH
# define COMMSQL_HH
	#	include <sstream>
	#	include <iostream>
	#	include <iomanip>
	#	include <string>
	#	include <vector>
	#	include <map>
	#	include <cstdlib>
// #  include <sstream>
	// # include <mysql.h>
	# include  <mysql/mysql.h>
	// # include <mysql>
	# include <vector>
	# include "enumerators.h"
	# include "item.hh"
	# include "io.hh"

using namespace std;
class LABELS;
extern string workingFileName, mainName;
 
class commSQL {
public:
  commSQL(string, string, string, string);
  ~commSQL();
  //virtual void createAccount(LABELS*);
  //virtual void closeAccount(int);
  //virtual LABELS* getAccount(int);
  virtual void message(string);
  virtual int isTableExists(string="labelFile.data");
  virtual int isTableExists(dataType);
  virtual void createTable();
	virtual string getTableName(dataType);
  virtual void createTable(dataType = FIGU, string="figure");
	virtual void removeTableData(string);
	virtual void updateLine(string, string, string, string);
  virtual void insertRow(LABELS *,  enum dataType);
  virtual void setTableName(string);
	virtual int isDataIxist(string, enum itemMember);
	virtual bool isDataIxist(string,string, enum itemMember,unsigned int=0);
	virtual string getValue (string, enum itemMember, enum itemMember);
	// virtual void  cleanData(string, string, dataType=FIGU );
	virtual void  cleanData(string, dataType=FIGU );
	virtual void cleanCurrentFileData (string);
	virtual void insertData(LABELS*,vector <itemMember>, string);
	virtual void updateData(LABELS*,vector <itemMember>, string);
	virtual vector <string> makeQuary (string, int&);
	virtual vector <string> excuteCMD(string);
	virtual int intExecute (string );

protected:
   MYSQL* dbCon;
   string host, user, password, dbase;
	 string tableName;
};

#endif   // COMMSQL_HH
