/* pret2p.l */
%{
#	 include <cstdio>
#	 include <iostream>
#	 include <string>
#	 include <sstream>
#  include "preDef.hh"
#  include "lable.hh"
#  include "fig.hh"
#  include "enumerators.h"
#  include "commSQL.hh"
#  include "nomen.hh"
#  include "math.hh"

	/* void exLabel (char *); */
#	 define YY_DECL extern "C" int yylex()
%}

WORD		[:alnum:]+
EQS			\$[^$]+\$
EQ		\$([^$])+?[^\\]\$
BLT			\\begin\{longtable\}
ELT		\\end\{longtable\}
BF		\\begin\{figure\}
EF		\\end\{figure\}
MKsECLOG		\\mkSeclog(\{[^\}]*?\}([\n])?([\t])?([\t])?){4}
sbFIG			\\[[:alpha:]]+(\[[[:alnum:][:space:]-]+\]){2}[[:space:]]+\{(\\[[:alpha:]]+[\[\{][[:alnum:][:punct:]]+\%?([^\}]+)?){2}\}\}
LATEXtoRM  \\[:
%option nounput
%option noinput
%option yylineno
%x LONGTABLE
%x MKVISIBLE
%x SOLUTIONT
%x COMMENT
%x IMGPDF
%x MKNOMEN
%x MKFIGURE
%x SSTEX

%%
(\%){1,3}[[:space:]]?endNomen([^\n])*\n	endNomenclature(yytext,yylineno);
\\\%					realPercent (yytext, yylineno, "real %");

{BLT}								{ cout << "<div class = \"space\"> </div>" << endl; 
										BEGIN(LONGTABLE);
										}
<LONGTABLE>{
{ELT}								BEGIN(INITIAL);
^\t(.)*?\n					empty(yytext, yylineno, "BLT \t(.)*?");
^%(\t|[ ])						BEGIN(MKVISIBLE);
.|\n									empty (yytext, yylineno, "BLT any CH|\\n ");
^[^%](.)*?\n					empty (yytext, yylineno, "BLT ^[^%](.)*?\\n ");
}

<MKVISIBLE>{
\\end\{longtable\}		BEGIN(INITIAL);
\\%										empty (yytext, yylineno, "emptyLine");
^%										empty (yytext, yylineno, "emptyLine"); /*spit*/
}

{BF}\[([a-z]){3}\]?\n						{pFigureIni(yytext, yylineno,"BF"); BEGIN(MKFIGURE);}	
<MKFIGURE>{
{EF}						{pFigureEnd(yytext, yylineno,"BF"); BEGIN(INITIAL);}
\\hspace\{([^\}])+\}(\\\\)?					empty(yytext, yylineno,"empty BF");
{MKsECLOG}						empty(yytext, yylineno, "sfig mkSeclog");
{sbFIG}				unToutched(yytext, yylineno,"New subfig sub");	
}

\\s[a-z]+(\[[[^\]]*\]){2}[[^\{]]*\{		empty(yytext, yylineno," new subfig sub");
\\subfigure					empty(yytext, yylineno," sub empty BF");
^\\solutionT[ ]*?\{						{cout << "<div class=\"solutionT\">" << endl; BEGIN(SOLUTIONT);}
<SOLUTIONT>{
(\%)?\%endSolution\n\}						{cout << "</div>"; BEGIN(INITIAL);}
.														unToutched(yytext, yylineno, "solution");
}

\\begin\{comment\} 						{BEGIN(COMMENT);}	
<COMMENT>{
\\end\{comment\}   					{BEGIN(INITIAL);}
. 										empty (yytext, yylineno, "comment");
}

\{\\[AB]\}							calSymbol(yytext, yylineno);
\\mbox									;
\\imgPDF\{ 								{cout << yytext ; BEGIN(IMGPDF);}
<IMGPDF>{
\\mkSeclog(\{([^}]|\n|\t)*?\}){4}				BEGIN(INITIAL);
\}\%+.*?\n\t 													cout << "}";
[ |\t]*?\n\t 													cout << " ";
}

 /* remove lines make them empty */
(^$)+					/*remove two empty lines*/;
\\newcommand(.)*?\n							empty(yytext, yylineno, "newcommand");
\\setlongtables(.)*?\n					empty(yytext, yylineno, "setlongtables");
\\bigskip(.)*?\n								empty(yytext, yylineno, "bigskip");
\\parindent(.)*?\n							empty(yytext, yylineno, "parindent");
\\arrayrulecolor(.)*?\n					empty(yytext, yylineno, "arrayrulecolor");
\\makeChapLog(.)*?\n						empty(yytext, yylineno, "makeChapLog");
{MKsECLOG}											empty(yytext, yylineno, "shortCut mkSeclog");
\\mkSeclog(\{([^}]|\n|\t)*?\}){4}		empty(yytext, yylineno, "mkSeclog");
\\setlength\{\\.*?\n						empty(yytext, yylineno, "setlength");
\}\n\t\{											cout <<"}{";

 /* change to html char */
--											cout << "&mdash;";
\\\'\{[e]*?\}						cout << "&egrave;";  
\\\"\{[u]*?\}						cout << "&uuml;" ;    
\\\"\{[U]*?\}						cout << "&Uuml;" ;    
\\\"\{[o]*?\}						cout << "&ouml;" ;    
\\\"\{[O]*?\}						cout << "&Ouml;" ;    
\'\'										cout << "&rdquo;";
\`\`										cout << "&ldquo;";
\\LaTeX\{\}							cout << "<span class=\"latex\">L<sup>a</sup>T<sub>e</sub>X</span>";

 /* keep the same char */
\\\$			unToutched (yytext, yylineno, "dollar Sign");
\$\$			unToutched (yytext, yylineno, "dollar Sign");
 /* remove this line for a while {WORD}		ECHO; */
{EQ}			unToutched (yytext, yylineno, "EQn");

	/*add to end empheq  so it work in the tex2php */
\\end\{empheq\}								cout << yytext ; /*<< "#"; */
^\%\%\%*\n				empty(yytext, yylineno, "repetitive percent");
\%{1,2}[\t [^\n][:alnum:][:punct:]']*?		empty(yytext, yylineno,"bl %");
\%{1,2}[[:space:]]?[a-df-zA-Z\\\$].?\n		empty(yytext, yylineno,"G %");
^\%[\t [:alnum:]:\\)(-.]*?\n empty(yytext, yylineno,"startPercent");
\%?\%[^e].*?\n								empty(yytext, yylineno,"single percent");
%%

int main( int argc, char **argv ) {
	ostringstream ss;
	++argv, --argc;
	yyin = fopen( argv[0], "r" );
	if ( argc > 0 ) {
		if ( yyin == NULL ) {
			cerr << "Unable to open " << argv[1] << ": " << strerror(errno) << endl;  
			exit(EXIT_FAILURE);
		}
		else{
			char *token; /*find the name of the file*/
			string wf;
			strtok_r (argv[0],  ".", &token);
			ss <<  argv[0] << ".php";
			workingFileName = ss.str();
			ss.clear();
			cerr << ss.str() << endl;
			ss.str("");
			ss <<  argv[0] << ".log";
			cerr << ss.str() << endl;
			cerr << "Working on file " << workingFileName << "." << endl;
			openLogFile(ss.str().c_str());
			logFile << "Working on file " << argv[0] << "." << endl;
		}
	}
	else {
		yyin = stdin;
	}
	readLabels();
	yy_flex_debug = 1;
	yylex();
	exit(1);
}

/* \\kSeclog(\{[^\}]*\}\n?){4}		empty(yytext, yylineno, "fig mkSeclog"); */
/* {EQS}					inLineEq(yytext);	 */
