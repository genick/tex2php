// created on Sun Apr 16 14:12:30 CDT 2017
#ifndef __LABLE_HH
#define __LABLE_HH
// #	include <cmath>
// #	include <cstdio>
// #	include <iostream>
// #	include <fstream>
// #	include <iomanip>
// #	include <limits>
// #	include <string>
// #	include <unistd.h>
// #	include <ctype.h>
// #	include <stdlib.h>
// #	include <stdbool.h>
#	include <mysql/mysql.h>
#	include "enumerators.h"
# include "item.hh"
# include "commSQL.hh"
// # include "mangerSQL.hh"
# include "utility.hh"
	using namespace std;

	extern string peelFirstPart( string &, size_t, char );
	extern commSQL * bt;
	void readLabels(void);
	string lookUpNumber (string, enum itemMember, enum itemMember);
	void exLabel(string );

#endif /* __LABLE_HH */
