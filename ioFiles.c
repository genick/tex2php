/*
 * ioFiles.c
 * ioFiles i/o for the tex2php and pret2p files
*/
# include "ioFiles.h"
# include "utilities.h"
# include "def.h"
# include "commonDef.h"
# include "mkHtml.h"
# include "nmn.h"
# include "ioData.h"

bool openFile(FILE* pFile,  char* fileName, char mode) {
	switch (mode) {
 		case 'w':
			indexAutPHP = fopen(fileName, "r+");
			if (indexAutPHP == NULL) {
				indexAutPHP = fopen(fileName, "w+");
			}
 			break;
 		case 'r':
 		default:
			indexAutPHP = fopen(fileName, "r+");
			if (indexAutPHP == NULL) {
				indexAutPHP = fopen(fileName, "w+");
			}
	}
	return true;
}

int	saveIndexFiles (void){
  indexAutData = fopen("indexAut.data", "r+");
  if (indexAutData == NULL) {
    indexAutData = fopen("indexAut.data", "w+");
  }
   else {
		//printIndex(indexAutMaxNumber, aut, "index of authors");
		//printIndex(indexSubMaxNumber, sub, "index of subjects");
   	saveIData(indexSubData, sub);
   	saveIData(indexAutData, aut);
 	}
	return 1;
}
 
void closeFile( FILE * file ){
	if (file != NULL) {
		fclose (file);
		file = NULL;
	}
	return ;
}

void saveEXM(uint indexMax, FILE * file, struct RECORD *indexPtr ) {
	int limit = (int) indexMax ;
  for (int i=0 ; i < limit; i++ ) {
		fprintf(file,"%s|%s|%s|%s|%s|",indexPtr->fileName, indexPtr->label,
			 	indexPtr->keywords, indexPtr->description, indexPtr->page ); 
		indexPtr++;
		fprintf (file, "\n");
  }
}

void saveNMN(uint indexMax, FILE * file, struct NOMENCLATURE *indexPtr ) {
	int limit = (int) indexMax ;
  for (int i=0 ; i < limit; i++ ) {
		fprintf(file,"%s|%s|%s|%s|",indexPtr->ref, indexPtr->equation,
			 	indexPtr->description, indexPtr->fileName ); 
		indexPtr++;
		fprintf (file, "\n");
  }
}

void saveTOI(uint indexMax, FILE * file, struct LOS *indexPtr ) {
	int limit = (int) indexMax ;
  for (int i=0 ; i < limit; i++ ) {
		fprintf(file,"%s|%s|%s|%s|%d|",indexPtr->fileName,  indexPtr->number,
			indexPtr->title, indexPtr->label, indexPtr->nN); 
		for (int k=0; k < indexPtr->nN; k++){
     	fprintf (file, "%d|", indexPtr->n[k]);
		}   
		indexPtr++;
		fprintf (file, "\n");
  }
}

void saveIndex(uint indexMax, FILE * file, struct INDEX *indexPtr ) {
	int limit = (int) indexMax ;
  for (int i=0 ; i < limit; i++ ) {
		fprintf(file,"%s|%s|%s",
		indexPtr->indexName,  indexPtr->fin, indexPtr->seeAlso); 
		for (int k=0; k <= indexPtr->numberFiles; k++){
			if (indexPtr->fileName[k][0] != '\0' ){
       	fprintf (file, "|%s", indexPtr->fileName[k]);
			}
		}   
		indexPtr++;
		fprintf (file, "|\n");
  }
}

int saveData ( FILE * file, enum dataType x){
	struct INDEX *indexPtr ;
	struct LOS *toiPtr ;
	struct NOMENCLATURE *nmnPtr;
	uint *indexMax;
	//char iName[30];
	if ( x == aut ) {
		indexPtr = auts ;
    indexMax = &indexAutMaxNumber ;
		goto inidi;
	//	strcpy(iName, "Authors");
	}
  else if ( x == sub ) { 
		indexPtr = subs ;
    indexMax = &indexSubMaxNumber ;
		goto inidi;
	//	strcpy(iName, "Subjects");
	}
  else if ( x == toc ) { 
    toiPtr = tocs ;
    indexMax = &tocMaxNumber ;
		goto toi;
	//	strcpy(iName, "Table of Contents");
  }
  else if ( x == fig ) { 
    //indexPtr = figures ;
    indexMax = &figMaxNumber ;
		goto toi;
	//	strcpy(iName, "Figures");
  }
  else if ( x == nmn ) { 
		nmnPtr = nmns;
    indexMax = &nmnMaxNumber ;
		goto inmn;
  }
	inidi:
		saveIndex(*indexMax, file, indexPtr);
		goto finishing;
	inmn:
		saveNMN(*indexMax, file, nmnPtr);
		goto finishing;
	toi:
		saveTOI(*indexMax, file, toiPtr);
	finishing:
		fprintf (stderr, "finished writing the index files \"%s\".\n", getEnumName(x));
		return 1;
}

int saveIData ( FILE * file, enum dataType x){
	struct INDEX *indexPtr ;
	uint *indexMax;
	if ( x == aut ) {
		indexPtr = auts ;
    indexMax = &indexAutMaxNumber ;
	}   
  else if ( x == sub ) { 
		indexPtr = subs ;
    indexMax = &indexSubMaxNumber ;
	}   
  else if ( x == fig ) {
		return 1;
    //indexPtr = figures ;
    //indexMax = &figMaxNumber ;
  }   
  for (int i=0 ; i < *indexMax; i++ ) { 
		fprintf(file,"%s|%s|%s",
		indexPtr->indexName,  indexPtr->fin, indexPtr->seeAlso); 
		for (int k=0; k <= indexPtr->numberFiles; k++){
			if (indexPtr->fileName[k][0] != '\0' ){
       	fprintf (file, "|%s", indexPtr->fileName[k]);
			}
		}   
		indexPtr++;
		fprintf (file, "|\n");
  }
	fprintf (stderr, "finished writing the index files\n");
	return 1;
}

FILE * openDataFile( char * dName, enum oMode y) {
	FILE * file;
	if (y == oRdata){
		file = fopen(dName , "r");
		if (file == NULL) {
			file = fopen(dName, "w+");
		}
	}
	else if (y == oWdata){
		file = fopen(dName , "w");
		if (file == NULL) {
			file = fopen(dName, "r+");
		}
	}
	else {
		file = fopen(dName , "r");
		if (file == NULL) {
			file = fopen(dName, "w+");
		}
	}
	return file;
}

void readDataFiles( FILE * file, enum dataType x ){
	char * dName; 
	if ( x == aut ) {
		char dataFileName [] = "indexAut.data" ;
		dName = dataFileName;
	}
	else if ( x == sub ) {
		char dataFileName [] = "indexSub.data" ;
		dName = dataFileName;
	}
	else if ( x == fig ) {
		//readDADA("fig.data");
		//char dataFileName [] = "indexFig.data" ;
		//dName = dataFileName;
	}
	else if ( x == toc ) {
		char dataFileName [] = "toc.data" ;
		dName = dataFileName;
	}
	else if ( x == sec ) {
		char dataFileName [] = "section.data" ;
		dName = dataFileName;
	}
	else if ( x == nmn ) {
		char dataFileName [] = "nmn.data" ;
		dName = dataFileName;
	}
	else if ( x == exm ) {
		char dataFileName [] = "exm.data" ;
		dName = dataFileName;
	}
	else if ( x == fig ) {
		char dataFileName [] = "exm.data" ;
		dName = dataFileName;
	}
	else { // default sub.
		char dataFileName [] = "indexSub.data" ;
		dName = dataFileName;
	}
	file = openDataFile( dName, oRdata );
	if (file == NULL) {
		return;
	}
	else {
		readData (file, x);
		closeFile(file);
	}
	return;
}

void saveDataFiles( FILE * file, enum dataType x ){
	char * dName; 
	if ( x == aut ) {
		char dataFileName [] = "indexAut.data" ;
		dName = dataFileName ; 
	}
	else if ( x == sub ) {
		char dataFileName [] = "indexSub.data" ;
		dName = dataFileName ; 
	}
	else if ( x == fig ) {
		char dataFileName [] = "indexFig.data" ;
		dName = dataFileName ; 
	}
	else if ( x == toc ) {
		char dataFileName [] = "toc.data" ;
		dName = dataFileName ; 
	}
	else if ( x == nmn ) {
		char dataFileName [] = "nmn.data" ;
		dName = dataFileName ; 
	}
	file = openDataFile( dName, oWdata ) ;
	if (file == NULL) {
		return;
	}
	else {
		saveData(file, x);
		if (file)
			fclose (file);
	}
	return;
}

  /* to remove the old file unlink ("footnote.php"); */
void readData (FILE * file, enum dataType x ) {
	if ( x == aut ) {
		readIData(file, x);
	}
	else if ( x == sub ) {
		readIData(file, x);
	}
	else if ( x == fig ) {
		readFigure (file, x);
	}
	else if ( x == toc) {
		readTOI (file, x);
	}
	else if ( x == nmn) {
		readNMN (file, x);
	}
	else if ( x == exm) {
		readEXM (file, x);
	}
	else { // for both sub and aut
		readIData(file, x);
	}
}

void readEXM ( FILE * file, enum dataType x ) {
	char* line = NULL , *firstPart;
	char lineC[256];
	size_t len = 0;
	ssize_t read; // length of the line
	uint *maxPtr;
	struct RECORD *exmPtr;
	struct RTN items;
	if ( x == exm ) {
		maxPtr = &exmMaxNumber;
		exmPtr = exms;
	}
	while ((read = getline(&line, &len, file)) != -1) {
		memset(lineC,'\0',sizeof(char)*sizeof(lineC));
		strcpy(lineC, line);
		if (strchr(line,'|')!=NULL) {
			firstPart = trim(strtok(line,  "|")); 
			if (strchr(firstPart,'#') == NULL ){
				continue;
			} 
			items = extractFileParts(firstPart, '#'); 
			if (strcmp(items.fP,workingFileName) == 0){
				continue;
			}
			strcpy((exmPtr+*maxPtr)->fileName,firstPart);
			firstPart =  trim(strtok(NULL,  "|")); 
			strcpy((exmPtr+*maxPtr)->label,firstPart); 
			firstPart =  trim(strtok(NULL,  "|")); 
			strcpy((exmPtr+*maxPtr)->keywords,firstPart); 
			firstPart =  trim(strtok(NULL,  "|")); 
			strcpy((exmPtr+*maxPtr)->description,firstPart);
			firstPart =  trim(strtok(NULL,  "|")); 
			strcpy((exmPtr+*maxPtr)->page,firstPart);
			(*maxPtr)++;
		}
	}
	if(line) 
		free(line);
	return ;
}

void readNMN ( FILE * file, enum dataType x ) {
	char* line = NULL , *firstPart;
	char lineC[256];
	size_t len = 0;
	ssize_t read; // length of the line
	uint *maxPtr;
	struct NOMENCLATURE *nmnPtr;
	struct RTN items;
	if ( x == nmn ) {
		maxPtr = &nmnMaxNumber;
		nmnPtr = nmns;
	}
	while ((read = getline(&line, &len, file)) != -1) {
		memset(lineC,'\0',sizeof(char)*sizeof(lineC));
		strcpy(lineC, line);
		if (strchr(line,'|')!=NULL) {
			firstPart = trim(strtok(line,  "|")); 
			if (strchr(firstPart,'#') == NULL ){
				continue;
			} 
			items = extractFileParts(firstPart, '#'); 
			if (strcmp(items.fP,workingFileName) == 0){
				continue;
			}
			strcpy((nmnPtr+*maxPtr)->ref,firstPart);
			firstPart =  trim(strtok(NULL,  "|")); 
			strcpy((nmnPtr+*maxPtr)->equation,firstPart); 
			firstPart =  trim(strtok(NULL,  "|")); 
			strcpy((nmnPtr+*maxPtr)->description,firstPart); 
			firstPart =  trim(strtok(NULL,  "|")); 
			strcpy((nmnPtr+*maxPtr)->page,firstPart);
			(*maxPtr)++;
		}
	}
	if(line) 
		free(line);
	return ;
}

void readTOI ( FILE * file, enum dataType x ) {
	char* line = NULL ,  *firstPart; //, *fullIndex, *fileName ;
	char lineC[256];
	size_t len = 0;
	ssize_t read; // length of the line
	uint *maxPtr;
	struct LOS *tocPtr;
	struct RTN items;
	if ( x == toc ) {
		maxPtr = &tocMaxNumber;
		tocPtr = tocs;
	}
	//char *fp, *sp;
	while ((read = getline(&line, &len, file)) != -1) {
		memset(lineC,'\0',sizeof(char)*sizeof(lineC));
		strcpy(lineC, line);
		if (strchr(line,'|')!=NULL) {
			firstPart = trim(strtok(line,  "|")); 
			if (strchr(firstPart,'#') == NULL ){
				continue;
			} 
			items = extractFileParts(firstPart, '#'); 
			if (strcmp(items.fP,workingFileName) == 0){
				continue;
			}
			strcpy((tocPtr+*maxPtr)->fileName,firstPart);
			firstPart =  trim(strtok(NULL,  "|")); 
			strcpy((tocPtr+*maxPtr)->number,firstPart); 
			firstPart =  trim(strtok(NULL,  "|")); 
			strcpy((tocPtr+*maxPtr)->title,firstPart); 
			firstPart =  trim(strtok(NULL,  "|")); 
			strcpy((tocPtr+*maxPtr)->label,firstPart);
			firstPart =  trim(strtok(NULL,  "|")); 
			(tocPtr+*maxPtr)->nN = atoi(firstPart); 
			int i=0;
			while (strcmp(firstPart = strtok(NULL, "|"),"\n") != 0 ) {
				(tocPtr+*maxPtr)->n[i] = atoi(firstPart); 
				i++;
			}
			if ( i != (tocPtr+*maxPtr)->nN ){
				(tocPtr+*maxPtr)->nN = i;
			}
			(*maxPtr)++;
		}
	}
	if(line) 
		free(line);
	return ;
}

void readFigure ( FILE * file, enum dataType x ) {
	int tempChar;
	uint *counter ;
	char * mainIndex, *fullIndex, *fileName ,	line[BUFFER_MAX_LENGTH];
	unsigned int tempCharIdx = 0U;
	memset(line, '\0', sizeof(line)); // file with zeros eg. 0000000 
	struct INDEX *indexPtr ;
	if ( x == aut ) {
		indexPtr = indexs ;
	}	
	else if ( x == fig ) {
		//indexPtr = figures ;
		counter = &figMaxNumber ;
	}	
		/* get a character from the file pointer */
	while(tempChar = fgetc(file)) {
		/* avoid buffer overflow error */
		if (tempCharIdx == BUFFER_MAX_LENGTH) {
			fprintf(stderr, "error: line is too long. increase BUFFER_MAX_LENGTH.\n");
			break;
		}
		/* test character value */
		else if (tempChar == EOF) {
			line[tempCharIdx] = '\0';
			fprintf(stderr, "%s\n", line);
			break;
		}
		else if (tempChar == '\n') {
			if (strchr(line,'|')!=NULL) {
				line[tempCharIdx] = '\0';
				tempCharIdx = 0U;
				/* mainIndex = trim(strtok_r (line,  ",", &save)); */
				mainIndex = trim(strtok(line,  "|"));
				if ( mainIndex != NULL ) {
					fullIndex  = strtok (NULL, "|");
				}
				else {
					fprintf (stderr, "\nreading index failed\n");
				}
				fileName  = strtok (NULL, "|");
				if ( mainIndex != NULL ) { 
					
					strcpy(indexPtr->fileName[0], fileName );
					strcpy(indexPtr->fin, fullIndex);  
					strcpy(indexPtr->indexName, mainIndex);  
					indexPtr->numberFiles = 1;
				}
				else {
					fprintf (stderr, "\nreading index failed\n");
				}
				{
					int j=1; // j is already done 
					char *tok = NULL;
					while ((tok = strtok(tok, "|")) != NULL) {
						strcpy(indexPtr->fileName[j], tok );
						indexPtr->numberFiles = ++j;
						tok = NULL;
						//j++;
					}
				}
				indexPtr++;
				(*counter)++;
				continue;
			}
			else {
				break;
			}
		}
		else
			line[tempCharIdx++] = (char) tempChar;
	}
	//indexAutMaxNumber--;
	indexPtr--;
	(*counter)++;
	fprintf (stderr, "finishing reading the index file\n");
	return ;
}

void readIData ( FILE * file, enum dataType x ) {
	char* line = NULL, *mainIndex, *fullIndex, *fileName, *firstPart, *seeALSO ;
	char container[64], lineC[256];
	size_t len = 0;
	ssize_t read; // length of the line 
	int j;
	bool canBeRejected=true, rejectIndex;
	uint *indexMaxPtr;
	struct INDEX *indexPtr ;
	if ( x == aut ) {
		indexMaxPtr = &indexAutMaxNumber;
		indexPtr = auts;
	}
	else if ( x == sub ) { 
		indexMaxPtr = &indexSubMaxNumber;
		indexPtr = subs;
	}
	else {
		indexMaxPtr = &indexAutMaxNumber;
		indexPtr = auts;
	}
	while ((read = getline(&line, &len, file)) != -1) {
		memset(lineC,'\0',sizeof(char)*sizeof(lineC));
		strcpy(lineC, line);
		j = 0 ;	
		rejectIndex=true;
		if (strchr(line,'|')!=NULL) {
			memset(container, '\0', sizeof(char)*sizeof(container));
			mainIndex = trim(strtok(line,  "|")); 
			fullIndex = trim(strtok (NULL, "|"));
			seeALSO = trim(strtok (NULL, "|"));
			fileName  = trim(strtok (NULL, "|"));
			firstPart = extractFirstPart(fileName, '#') ;
			strcpy(container, firstPart);
			firstPart = container;
			//strncpy(container, firstPart,  sizeof container - 1);
			//container[sizeof container -1] = '\0'; 
			if ((strcmp (container, "FALSE")) == 0) {
				continue;
			}
			if (strcmp (container, workingFileName) !=0 ) {
				//strcpy(indexs[*indexMaxPtr]->fileName[j], fileName);
				strcpy(indexPtr->fileName[j], fileName);
				rejectIndex = false;
				j++;
				indexPtr->numberFiles = 1;
				indexPtr->in = *indexMaxPtr; 
				//indexs[indexAutMaxNumber].numberFiles = 1;
			}
			strcpy(indexPtr->fin, fullIndex);  
			strcpy(indexPtr->indexName, mainIndex );  
			strcpy(indexPtr->seeAlso, seeALSO );  
			// strcpy(indexs[indexAutMaxNumber].fin, fullIndex);  
			// strcpy(indexs[indexAutMaxNumber].indexName, mainIndex);  
			{ // working on the loop
				char *tok = NULL;
				while ((tok = strtok(tok, "|")) != NULL) {
					firstPart = extractFirstPart(tok, '#');
					if (strcmp(firstPart,"FALSE") != 0 ) {
						if (strcmp (firstPart, workingFileName) !=0 ) {
							rejectIndex =false ;
							strcpy(indexs[indexAutMaxNumber].fileName[j], tok );
							indexs[indexAutMaxNumber].numberFiles = ++j;
							j++; // j=0 is already done 
							if (canBeRejected == true ) {
								canBeRejected=false;
							}
						}
					}
					tok = NULL;
				}
			}
			if (rejectIndex == false ){
				indexPtr++;
				(*indexMaxPtr)++;
				indexPtr->in= *indexMaxPtr;
				//indexAutMaxNumber++;
			}
					//memset(line, '\0', sizeof(line)); // file with zeros eg. 0000000 
			continue;
		}
		else {
			break;
		}
	}
	if (line)
		free(line);
	return;
}

