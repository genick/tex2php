// utili.cc moving the utilities to the cc side.
# include <string>
# include <sstream>
# include <string.h>
# include "utili.hh"

using namespace std;

extern "C" void cTxt(char * x, char* type ){
	string l (x);
	string str ;
	peelFirstB(l, 1);
	size_t found = l.find('}');
	size_t endStr = l.length();
	str.assign(l,0,found);
  if (found!=string::npos) {
		l.erase (found, endStr);
		//strncpy(b,l.c	
		//l.erase (begin()+5, end());
  }
	cout << "<span class=\"" << type << "\">" 
		<< l << "</span>" << endl;
}

string lastPart(string token, char s){
	string l = token;
	size_t found; 
	do {
		found = l.find(s);
		l.erase (0,found+1);
	} while (found!=string::npos) ; 
	return l;
}

void peelFirstB( string &l, size_t times, char a ){
	size_t found = l.find(a);
	for (size_t i = 0 ; i < times ; i++){
		if (found!=string::npos) {
			l.erase(0,found+1);
		}
		else {
			return;
		}
		found = l.find(a);
	}
	//return;
}

string rmTabNewLine(string l){
	string space(" ");
	size_t found, sL = l.length();
	while (true) {
		found = l.find('\n');	
		if (found < sL-1){
			l.replace(found, 1, space);
		}	
		else {
			break;
		}
	}
	while (true) {
		found = l.find('\t');	
		if (found < sL){
			l.replace(found, 1, space);
		}	
		else {
			break;
		}
	}
	while (true){
		found = l.find("  ");	
		if (found < sL){
			l.replace(found, 2, space);
		}	
		else {
			break;
		}
	}
	return  l;
}

// function to find the needle nth location.
size_t strpos(string hay, char *needle, int nth) {
	size_t found, sum = 0; 
	for (int i=0 ; i <=nth; i++){
		found =	hay.find(needle);  
		sum = sum + found;
		if (found!=string::npos) {
			hay.erase (0,found+1);
		}
		else{
			return string::npos;
		}
	}
	return sum;
}

string mkNomeNu (void){
	//string Result;
	ostringstream ss;
	ss << "nmn" << nmnMaxNumber; 
	return ss.str();
	//return Result;
}

void texStrToPhpStr(string l, string s){
	size_t found ;
	peelFirstB(l,1);
	found = l.find('}');
	if (found!=string::npos){
		l.erase(l.begin()+found, l.end()-0);
	}
	cout << "<span class=\"" << s.c_str() << "\">" << l << "</span>" ;
}

extern "C" void texStrToPhpStr(char* x, char* s){
	string l(x);
	size_t found ;
	peelFirstB(l,1);
	found = l.find('}');
	if (found!=string::npos){
		l.erase(l.begin()+found, l.end()-0);
	}
	cout << "<span class=\"" << s << "\">" << l << "</span>" ;
}
