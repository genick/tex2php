// commDB.cc Thu Apr 27 21:39:01 CDT 2017
#	include <mysql.h>
// #	include "commSQL.hh"
#	include "mangerSQL.hh"
#	include "commDB.hh"
using namespace std;
	LABELS leb(0);
	LABELS* ptrF = &leb;

void cleanDataBase(string wF){
	stringstream ss;
	ss << "select count(*) from information_schema.tables where "
		<< "table_schema = 'gasDynamics';";

	int freshTbl = bt->intExecute (ss.str().c_str()) ;
	ss.clear();
	ss.str("");
	if (freshTbl != -1)  {
		ss << (freshTbl-1) << " tables where found." << endl; 
		errMsg(ss.str());
		if ((freshTbl -1) == 0 ) {
			ss.clear();
			ss.str("");
			ss << "Additional Tables will be created if needed" ;
			errMsg(ss.str());
			return;
		}
		else {
			ss.clear();
			ss.str("");
			ss <<"If additional tables needed they will be created." ;
			errMsg(ss.str());
		}
		ss.str("");
		ss << ss.str() << endl;
		errMsg(ss.str());
		vector <string> currentTables;
		ss	<< "select table_name from information_schema.tables "
				<< "where table_schema = 'gasDynamics' anD table_name "
				<< "not like '%raw%';";
		// currentTables = bt->excuteCMD("gasDynamics");
		// cerr << ss.str() << endl;
		currentTables = bt->excuteCMD(ss.str());
		ss.clear();
		ss.str("");
		ss << "Cleaning the current tables." ;
		errMsg(ss.str());
		for (auto i : currentTables){
			ss.clear();
			ss.str("");
			ss << "Cleaning table " << i << endl;
			errMsg(ss.str());
		 	// bt->cleanData(wF, i);
		 	bt->cleanData(i);
		}
		return;
	}
		// bt->createTable(data,dataI,"figure");
	// vector <itemMember> data = {LB, NU, CA, CAt, CAo, FN, PG};
	// vector <intMember> dataI = {Pg, Nn};
}

string lookUpNumber (string l, enum itemMember x, enum itemMember y){
	l = bt->getValue(l, x, y);
	return l;
}

void updateData(string key, string newValue, string field, string tn){
	//UPDATE figure SET subName1='ILIO' WHERE label0='thermo:fig:TU' ;
	//update figure set subName2='sub2' where label0='thermo:fig:TU' ;
	logFile << "Atempting to update table '" << tn <<  
		"' with label '" << field <<"'" << endl;
	bt->updateLine(key, newValue, field, tn);
	// cout << tn << endl ; 
	// cout << l << endl;
}	

void insertDataDB (LABELS * pF, vector <itemMember> x, 
	vector <intMember> y, dataType z ,string tn){
	stringstream ss;
	string tName =  bt->getTableName(z);
	if (bt->isTableExists(z) == 0 ){
		ss <<"Tabel '" << tName <<"' is not exist, trying to crate it:"
			<<  endl;
		errMsg(ss.str());
		// bt->createTable(x, y, tn);
		bt->createTable(z, tName);
	}
	ss << "inserting data to table '"<< tName<< "'." << endl;
	errMsg(ss.str());
	bt->insertData(pF, x, tName);
	return;
}

void readLabels(void) {
	vector <itemMember> data = {NU, TY, LB, CA, PG, KW};
	vector <intMember> dataI = {};
	LABELS leb(0);
 	LABELS *F; 
	F = &leb;
	stringstream ss;
	vector <itemMember> x;
	vector <intMember> y;
	ptrF = &leb;
	string tn ("rawLabel");
	bt->setTableName(tn);
	if (bt->isTableExists(tn) == 1 ){
		ss <<"Removing data from table " << tn << "!" ;
		bt->removeTableData(tn);
		ss.clear();
		ss.str("");
		ss <<"Data from table \'" << tn << "\' is removed!" ;
		errMsg (ss.str());
	}
	else {
		ss <<"Trying to crate table: " << tn << " !" ;
		errMsg (ss.str());
		bt->createTable( LABEL, tn);
		// ss <<"Table: " << tn << " was created!" ;
		// errMsg (ss.str());
	}
	for(string line; getline( input, line ); ){
		//cout << line << endl;
		leb.setMemberValue (peelFirstPart(line, 1, ','),NU,true);
		leb.setMemberValue (peelFirstPart(line, 1, ','),TY);
		leb.setMemberValue (peelFirstPart(line, 1, ','),LB);
		leb.setMemberValue (peelFirstPart(line, 1, ','),CA);
		leb.setMemberValue (peelFirstPart(line, 1, ','),PG);
		leb.setMemberValue (peelFirstPart(line, 1, ','),KW);
		insertDataDB(F, data, dataI, LABEL, "rawLabel"); 
	}
	input.close();
	ss.clear();
	ss.str("");
	ss << "Label data for " << leb.returnVariable(LB, 0) <<
			" inserted to Table " << tn << " successfully." << endl;
	errMsg(ss.str());
	return;
}

bool checkInData (string key, string tableName, enum itemMember x,
	 	unsigned int y){
	return true;
}
