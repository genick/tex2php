# include "io.hh"

void errMsg(string l){
	cerr << l <<  endl;
  if(! logFile.is_open()) {
    cerr << "Impossible to open the log file" << endl;
    exit(-1);
  }
	logFile << l  << endl;
	// logFile.sync(); not working
	// logFile.close();
}

void footnoteMsg(string l ) {
	errMsg(l);
  if(! footnoteFile.is_open()) {
    errMsg("Impossible to open the footnote file\n");
    exit(-1);
  }
	// footnoteFile << l  << endl;
}
