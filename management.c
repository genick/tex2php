/*
 *  management.c
 * ioFiles i/o for the tex2php and pret2p files
*/
# include "ioFiles.h"
# include "utilities.h"
# include "def.h"
# include "commonDef.h"
//# include "mkHtml.h"
# include "nmn.h"
# include "ioData.h"
# include "management.h"

bool openOperations(void){
	readLabels();
//	readDATA("fig.data");
//	readDataFiles ( indexAutData, aut );
//	readDataFiles ( indexSubData, sub );
//	//readDataFiles ( indexFigData, figure);
//	readDataFiles ( tocData, toc);
//	readDataFiles ( nmnData, nmn);
//	readDataFiles ( exmData, exm);
	return true;
}

int closeOperations(void) {
//	sortIndexData (sub);
//	sortIndexData (aut);
//	sortIndexData (toc);
//	saveDataFiles (indexSubData, sub );
//	saveDataFiles (indexAutData, aut );
//	saveDataFiles (tocData, toc );
//	saveDataFiles (tocData, nmn );
	mkIndexHTML();
	mkTOC();
	mkNMN();
	return 1;
}

