#ifndef __MATH_HH
#define __MATH_HH
// math.hh initial build Mon Mar  6 10:45:35 CST 2017
	# include <cmath>
	#	include <cstdio>
	#	include <iostream>
	#	include <fstream>
	#	include <iomanip>
	#	include <limits>
  #	include <string>
	#	include <unistd.h>
  #	include <ctype.h>
  #	include <stdlib.h>
  #	include <stdbool.h>
	#	include "extenVer.hh"
	using namespace std;
	extern unsigned figMaxNumber, nmnMaxNumber;
	extern struct NOMENCLATURE nmns[200];
	extern "C" char * trim(char*); 
	//extern "C" void nomenc(char* );
	extern "C" char * newLineToSpace(char*); 

#endif /* __MATH_HH */
