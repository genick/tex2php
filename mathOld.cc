// math.cc starting point Thu Mar  9 09:25:24 CST 2017
# include <iostream>
# include <fstream>
# include <sstream>
# include <string>
# include <stdio.h>
# include <string.h>
# include "mkHtml.hh"
# include "commonDef.h"
// # include "utili.hh"
# include "ut.hh"
# include "math.hh"
//using namespace std;

// extern "C" void empheq (char * x) {
void empheq (string l) {
	char kindEq[40], title[240], eqType[56];
	memset(kindEq, '\0', sizeof(kindEq));
	memset(title, '\0', sizeof(title));
	memset(eqType, '\0', sizeof(eqType));
	//x = newLineToSpace (x);
	// string l (x);
	peelFirstB(l, 1, '[');
	peelFirstB(l, 1, '\\');
	size_t found = l.find('[');
	if (found!=string::npos) {
		l.copy(kindEq,found,0);
		l.erase(0,found+1);
		strcpy(kindEq, trim(kindEq));
	}
	found = l.find(']');
	if (found!=string::npos) {
		l.copy(title,found,0);
		l.erase(0,found+1);
	}
	peelFirstB(l, 1);
	found = l.find('}');
	if (found!=string::npos) {
		l.copy(eqType,found,0);
		l.erase(0,found+1);
	}
	found = l.find("\\end\{empheq");
	if (found!=string::npos) {
		l.erase (l.begin()+found, l.end()-0);
	}
	cout << "<div class=\"" << kindEq << "\" >" << endl ;
	cout << "<h3 class=\"eqHead\" >" << title << "</h3>\n";
	cout << "\\begin{" << eqType << "}";
	cout << l ;
	cout << "\\end{" << eqType << "}\n";
	cout << "</div>\n" ;
}

// 	extern "C" void nomencb(char* x){
// 		x = newLineToSpace (x);
// 		string l (x);
// 		peelFirstB(l, 1);
// 		size_t found = l.find('$');
// 		l.erase (0,found+1);
// 		found = l.find('$');
// 		if (found!=string::npos) {
// 			char y[256];
// 			l.copy(y,found,0);
// 			l.erase(0,found);
// 			sprintf(nmns[nmnMaxNumber].equation, "$%s$", y); 
// 		}
// 		peelFirstB(l, 1);
// 		found = l.find('}');
// 		if (found!=string::npos) {
// 			l.copy(nmns[nmnMaxNumber].description,found,0);
// 		}
// 		string n = mkNomeNu();
// 		n.copy(nmns[nmnMaxNumber].ref,n.size(),0);
// 		strcpy(nmns[nmnMaxNumber].fileName,workingFileName);
// 		cout << "<div class=\"nmn\" href=\"" << n 
// 			<< "\" >" << nmns[nmnMaxNumber].equation << " - " <<
// 		 nmns[nmnMaxNumber].description	<< "</div>" << endl ;
// 		nmnMaxNumber++;
// 	}
// 
// 	extern "C" void nomenc(char* x){
// 		string l (x);
// 		peelFirstB(l, 1);
// 		size_t found = l.find('}');
// 		if (found!=string::npos) {
// 			l.copy(nmns[nmnMaxNumber].equation,found,0);
// 			l.erase (0,found);
// 		}
// 		peelFirstB(l, 1);
// 		found = l.find('}');
// 		if (found!=string::npos) {
// 			l.copy(nmns[nmnMaxNumber].description,found,0);
// 		}
// 		string n = mkNomeNu();
// 		n.copy(nmns[nmnMaxNumber].ref,n.size(),0);
// 		strcpy(nmns[nmnMaxNumber].fileName,workingFileName);
// 		cout << "<div class=\"nmn\" href=\"" << n 
// 			<< "\" >" << nmns[nmnMaxNumber].equation << " - " <<
// 		 nmns[nmnMaxNumber].description	<< "</div>" << endl ;
// 		nmnMaxNumber++;
// 	}
// 
// 	extern "C" void mkNMN (){
// 		ofstream nmnFile ("nmn.php");
// 		ofstream *iFile;
// 		iFile =  &nmnFile;
// 		if (iFile->is_open()) {
// 			mkPHPheader(nmn, *iFile);
// 			for (unsigned i = 0 ; i < nmnMaxNumber ; i++){
// 				*iFile << "\t\t\t<li>" << nmns[i].equation << " - "<<
// 						nmns[i].description << "</li>" << endl;
// 			}
// 			//mkLetter (nmn, nmnFile);
// 			mkPHPfooter(nmn, *iFile);
// 			iFile->close();
// 		}
// 		else {
// 			cout << "Unable to open file" << endl;
// 		}
// 	}
