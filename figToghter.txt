<!DOCTYPE html>
<html>
<head>
<style>
ul.fans li {
  display: inline-block;
}
figcaption {
  text-align: center
}
</style>
</head>
<body>

<section class="products">
  <h1>Fans</h1>
  <p>
    <ul class="fans">
      <li>
        <a href="#">
          <figure>
            <img class="fans" src="http://placehold.it/300/300" alt="Shrouded"/>
            <figcaption>Shrouded</figcaption>
          </figure>
        </a>
      </li>
      <li>
        <a href="#">
          <figure>
            <img class="fans" src="http://placehold.it/300/300" alt="Shrouded" />
            <figcaption>Shallow Recess</figcaption>
          </figure>
        </a>
      </li>
    </ul>
  </p>
</section>

<p>The Pulpit Rock is a massive cliff 604 metres (1982 feet) above Lysefjorden, opposite the Kjerag plateau, in Forsand,
Ryfylke, Norway. The top of the cliff is approximately 25 by 25 metres (82 by 82 feet) square and almost flat, and is a
famous tourist attraction in Norway.</p>

<figure>
	<div>
    <span>
    <figure>
  <img src="img_pulpit.jpg" alt="The Pulpit Rock" width="304" height="228">
  <figcaption>Fig.1 - A view of the pulpit rock in Norway.</figcaption>
    </figure>
    </span>
    <span>
    <figure>
  <img src="img_pulpit.jpg" alt="The Pulpit Rock" width="304" height="228">
   <figcaption>Fig.1 - A view of the pulpit rock in Norway.</figcaption>
  </figure>
  </span>
  </div>
 
</figure>
<p><strong>Note:</strong> The figure tag is not supported in Internet Explorer 8 and earlier versions.</p>
</body>
</html>

